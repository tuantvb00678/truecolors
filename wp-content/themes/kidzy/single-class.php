<?php
/**
 * Display Single Class
 *
 * @author      Themeum
 * @category    Template
 * @package     Kidzy
 * @version     1.0
 *-------------------------------------------------------------*/

if ( ! defined( 'ABSPATH' ) )
    exit; // Exit if accessed directly

get_header(); ?>

<section id="main">

<?php
get_template_part('lib/sub-header');

global $themeum_options;

if ( have_posts() ){
    while ( have_posts() ) {
        the_post();

        $class_status = get_post_meta(get_the_ID(),'themeum_class_status',true);
        $class_link = get_post_meta(get_the_ID(),'themeum_class_link',true);
        $years_old = get_post_meta(get_the_ID(),'themeum_years_old',true);
        $class_size = get_post_meta(get_the_ID(),'themeum_class_size',true);
        $tution_fee = get_post_meta(get_the_ID(),'themeum_tution_fee',true);
        $performers = rwmb_meta('themeum_class_teacher','type=checkbox_list');
        $course_list = rwmb_meta('themeum_course_list');
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-medium' );
        ?>

        <!-- Classes Section -->
        <section class="classes-section-3">
            <div class="container">

                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="class-detail-img">
                        
                        <?php if($image){ ?>
                            <img class="img-responsive" src="<?php echo esc_url( $image[0] ) ?>" alt="<?php echo get_the_title() ?>">
                        <?php } ?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="class-information">
                            <?php echo '<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>'; ?>
                            <h5><a href="<?php echo $class_link; ?>" class="black"><?php echo $class_status; ?></a></h5>
                            <p>
                                <?php echo the_content(); ?>
                            </p>
                        </div>
                        <div class="class-meta-data">
                            
                            <?php if($years_old){ ?>
                            <div class="class-single-meta pull-left">
                                <p><?php esc_html_e('Lứa tuổi','kidzy') ?></p>
                                <h4><?php echo $years_old; ?></h4>
                            </div>
                            <?php }
                            if($class_size){
                            ?>
                            <div class="class-single-meta pull-left">
                                <p><?php esc_html_e('Sĩ số','kidzy') ?></p>
                                <h4><?php echo $class_size; ?></h4>
                            </div>
                            <?php
                            }
                            if($tution_fee){
                            ?>
                            <div class="class-single-meta pull-left">
                                <p><?php esc_html_e('Học phí','kidzy') ?></p>
                              <a href="/contact" class="block"><?php echo $tution_fee; ?></a>
                            </div>
                            <?php } ?>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.classes-section-2 -->


        <!-- Subject and teacher Section -->
        <section class="subject-and-teacher-details">
            <div class="container">

            <?php if(!empty($course_list)) { ?>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="subject-details">

                            <?php
                            $tcourse = 0;
                            if ( is_array($course_list) && !empty($course_list) ) {
                            foreach ($course_list as $value) { $tcourse ++;}}
                            ?>
                            <div class="sub-heading"><h4><?php esc_html_e('Subject List','kidzy') ?><span>( <?php echo $tcourse ?> Subject )</span></h4><p><?php esc_html_e('Class Schedule','kidzy') ?></p></div>

                            <ul class="subject-list">
                            <?php
                                $tcourse = 1;
                                if ( is_array($course_list) && !empty($course_list) ) {
                                foreach ($course_list as $value) {
                            ?>
                                <li>
                                    <a href=""><?php echo $tcourse.'. '.esc_html($value['themeum_class_subject_name']); ?></a>
                                    <span><?php echo esc_html($value['themeum_class_schedule']); ?></span>
                                </li>
                            <?php
                               $tcourse ++;
                                }
                            }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php }//End of not Empty Course List ?>





                <?php
                if(!empty($performers)) {
                    $posts_id = array();
                    $teacher = 0;
                    foreach ($performers as $value) {
                        $posts = get_posts(array('post_type' => 'teacher', 'name' => $value));
                        $posts_id[] = $posts[0]->ID;
                        $teacher++;
                    }
                    $performers = get_posts( array( 'post_type' => 'teacher', 'post__in' => $posts_id, 'posts_per_page' => -1) );
                ?>

                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="sub-heading"><h4><?php esc_html_e('Course Teacher','kidzy') ?><span>( <?php echo $teacher; esc_html_e('Teachers','kidzy') ?> )</span></h4></div>
                    </div>
                </div>

                <div class="row">
                <?php
                    foreach ($performers as $key=>$post) {
                        setup_postdata( $post );
                        $thumb_src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );

                        $teacher_facebook = rwmb_meta('themeum_facebook');
                        $teacher_twitter = rwmb_meta('themeum_twitter');
                        $teacher_gplus = rwmb_meta('themeum_gplus');
                        $teacher_linkedin = rwmb_meta('themeum_linkedin');
                        $teacher_instagram = rwmb_meta('themeum_instagram');
                        $teacher_flickr = rwmb_meta('themeum_flickr');
                        ?>


                        <div class="col-sm-3 col-xs-12 text-center">
                        <div class="single-team">
                            <div class="team-img">
                                <a href="<?php echo get_the_permalink() ?>">
                                  <img alt="<?php echo get_the_title(); ?>" src="<?php echo $thumb_src; ?>">
                                </a>
                                <ul class="team-social">
                                    <?php if($teacher_facebook){
                                        echo '<li><a href="'.$teacher_facebook.'" class="facebook"><i class="fa fa-facebook"></i></a></li>';
                                    }
                                    if($teacher_twitter){
                                        echo '<li><a href="'.$teacher_twitter.'" class="twitter"><i class="fa fa-twitter"></i></a></li>';
                                    }
                                    if($teacher_gplus){
                                        echo '<li><a href="'.$teacher_gplus.'" class="gplus"><i class="fa fa-google-plus"></i></a></li>';
                                    }
                                    if($teacher_linkedin){
                                        echo '<li><a href="'.$teacher_linkedin.'" class="linkedin"><i class="fa fa-linkedin"></i></a></li>';
                                    }
                                    if($teacher_instagram){
                                        echo '<li><a href="'.$teacher_instagram.'" class="gplus"><i class="fa fa-instagram"></i></a></li>';
                                    }
                                    if($teacher_flickr){
                                        echo '<li><a href="'.$teacher_flickr.'" class="flickr"><i class="fa fa-flickr"></i></a></li>';
                                    } ?>
                                </ul>
                            </div>
                            <h3><a href="<?php echo get_the_permalink() ?>"><?php echo get_the_title() ?></a></h3>
                            <p><?php echo rwmb_meta( 'themeum_designation' ); ?></p>
                        </div>
                    </div>

                    <?php   }
                    wp_reset_postdata();  ?>


                </div>
                <?php } ?>
            </div>
        </section>
        <!-- /.subject-and-teacher-details -->

<?php
    }//End of While
}//End of if have post
?>

</section>

<?php get_footer();
