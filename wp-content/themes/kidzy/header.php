<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php  if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
      if( kidzy_options('favicon') ){  ?>
        <link rel="shortcut icon" href="<?php echo esc_url( kidzy_options_url('favicon','url') ); ?>" type="image/x-icon"/>
      <?php }else{ ?>
        <link rel="shortcut icon" href="<?php echo esc_url(get_template_directory_uri().'/images/plus.png'); ?>" type="image/x-icon"/>
      <?php }
    }
    ?>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php wp_head(); ?>
  <style type="text/css">
    <?php print kidzy_options('custom_css'); ?>
  </style>
  <script>
    jQuery(document).ready(function() {
      jQuery('#menu-main-menu a').each(function() {
        var href = jQuery(this).attr('href');
        if (href.includes('our-classes')) {
          jQuery(this).click(function(e) {
            e.preventDefault();

            var offset = jQuery('.our-classes').offset().top - 120;
            jQuery('html, body').animate({
              scrollTop: offset
            }, 800);
          });
        }
      });
      
      // mobile menu
      jQuery('#mobile-menu a').each(function() {
        var href = jQuery(this).attr('href');
        if (href.includes('our-classes')) {
          jQuery(this).click(function(e) {
            e.preventDefault();

            var offset = jQuery('.our-classes').offset().top - 80;
            jQuery('html, body').animate({
              scrollTop: offset
            }, 800);
          });
        }
      });
    });
  </script>
</head>

 <?php
     $menu_style = 'none';
     if ( kidzy_options('boxfull-en') ) {
      $layout = esc_attr( kidzy_options('boxfull-en') );
     }else{
        $layout = 'fullwidth';
     }
 ?>

<body <?php body_class(); ?>>
  <?php if ( kidzy_options('preloader-en') ) { ?>
    <div class="loader-wrap">
        <div class="loader"><?php esc_html_e( 'Loading...', 'kidzy' ); ?></div>
    </div>
  <?php }?>

  <div id="page" class="hfeed site <?php echo esc_attr($layout); ?>">




    <!-- sign in form -->
    <div id="sign-form">
         <div id="sign-in" class="modal fade">
            <div class="modal-dialog modal-md">
                 <div class="modal-content">
                     <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                         <i class="fa fa-close" data-dismiss="modal"></i>
                      </button>
                     </div>
                     <div class="modal-body">
                         <h2><?php esc_html_e('Already Registered','kidzy'); ?></h2>
                         <form id="login" action="login" method="post">
                            <div class="login-error alert alert-info" role="alert"></div>
                            <input type="text" id="username" name="username" class="form-control" placeholder="<?php esc_html_e('User Name','kidzy'); ?>">
                            <input type="password" id="password" name="password" class="form-control" placeholder="<?php esc_html_e('Password','kidzy'); ?>">

                            <div class="clearfix"></div>
                            <div class="button-wrap">
                              <input type="submit" class="btn btn-default btn-block submit_button"  value="<?php esc_html_e('Login','kidzy'); ?>" name="submit">
                            </div>

                            
                            <?php esc_html_e('Forgot','kidzy'); ?> <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Username','kidzy'); ?></a> <?php esc_html_e('or','kidzy'); ?>  <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Password','kidzy'); ?></a>

                            <p>
                              <span>
                              <?php _e('Not a member?','kidzy'); ?> <a href="<?php echo esc_url(get_permalink(get_option('register_page_id'))); ?>"><?php esc_html_e('Join today','kidzy'); ?></a>
                              </span>
                            </p>

                            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                         </form>
                     </div>
                 </div>
             </div>
         </div>
    </div>
    
    <div id="logout-url" class="hidden"><?php echo wp_logout_url( esc_url( home_url('/') )); ?></div>



<!-- Header Area -->
<header class="header-area">
  <!-- Header Top Area -->
  <?php if ( kidzy_options('topbar-en')) { ?>
  <section class="header-top">
      <div class="container">
          <div class="row">
              <div class="col-sm-12 col-xs-12">
                  <div class="header-elements pull-right">
                      
                      <?php if ( kidzy_options('topbar-social') ) { ?>
                          <?php get_template_part( 'lib/social-icons'); ?>
                      <?php } ?>

                      <?php if ( kidzy_options('topbar-login') ) {
                        if(is_user_logged_in()){
                          echo ' <div class="login-or-signup pull-left">
                                  <a href="'.wp_logout_url( home_url() ).'"><i class="fa fa-sign-out"></i><span>Sign out</span></a>
                                </div>';
                        }else{ ?>
                          <div class="login-or-signup pull-left">
                            <a href="#sign-in" data-toggle="modal" data-target="#sign-in"><i class="icon-user"></i><span><?php esc_html_e( 'Login/sign up','kidzy'); ?></span></a>
                        </div>
                      <?php }
                       } ?>


                       

                  </div>
              </div>
          </div>
      </div>
  </section>
  <?php } ?>





  <!-- /.header-top -->
  <nav id="masthead" class="navigation">
    <div id="header-container">
      <div id="navigation" class="container">
          <div class="row">
              
              <div class="col-sm-3">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>

                    <div class="logo logo-wrapper">
                        <a class="navbar-brand" href="<?php echo site_url(); ?>">
                            <?php
                              if (kidzy_options('logo') )
                               {
                                
                                if(kidzy_options('logo-text-en') ) { ?>
                                  <h1> <?php echo esc_html(kidzy_options('logo-text') ); ?> </h1>
                                <?php }
                                else
                                {
                                  if(kidzy_options('logo')) {
                                  ?>
                                    <img class="enter-logo img-responsive" src="<?php echo esc_url(kidzy_options_url('logo','url')); ?>" alt="" title="">
                                  <?php
                                  }else{
                                    echo esc_html(get_bloginfo('name'));
                                  }
                                }
                               }
                              else
                               {
                                  echo esc_html(get_bloginfo('name'));
                               }
                            ?>
                      </a>
                  </div>
                </div>
              </div>

            <div class="col-sm-9 woo-menu-item-add">
                <?php
                    global $woocommerce;
                    if($woocommerce) {
                    if( kidzy_options('cart_open') ) { ?>
                        <span id="themeum-woo-cart" class="woo-cart" style="display:none; width:100%;">
                            <?php
                                $has_products = '';
                                $has_products = 'cart-has-products';
                            ?>
                            <span class="woo-cart-items">
                                <span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
                            </span>
                            <i class="fa fa-shopping-cart"></i>
                            <?php the_widget( 'WC_Widget_Cart', 'title= ' ); ?>
                        </span>
                <?php }} ?>

                <div id="main-menu" class="hidden-xs">
                    <?php
                    if ( has_nav_menu( 'primary' ) ) {
                        wp_nav_menu(  array(
                            'theme_location' => 'primary',
                            'container'      => '',
                            'menu_class'     => 'main-menu pull-right',
                            'fallback_cb'    => 'wp_page_menu',
                            'depth'          => 3,
                            'walker'         => new Megamenu_Walker()
                            )
                        );
                    }
                    ?>

                    
                </div><!--/#main-menu-->
            </div>


            

            <div id="mobile-menu" class="visible-xs">
                <div class="collapse navbar-collapse">
                    <?php
                    if ( has_nav_menu( 'primary' ) ) {
                        wp_nav_menu( array(
                            'theme_location'      => 'primary',
                            'container'           => false,
                            'menu_class'          => 'nav navbar-nav',
                            'fallback_cb'         => 'wp_page_menu',
                            'depth'               => 3,
                            'walker'              => new wp_bootstrap_mobile_navwalker()
                            )
                        );
                    }
                    ?>
                </div>
            </div><!--/.#mobile-menu-->

        </div><!--/.row-->
      </div><!--/.container-->
    </div>
  </nav>
</header>
<!-- /.header-area -->
