<div class="col-sm-4 col-md-3 col-xs-12" role="complementary">
  <aside class="widget-area widget">
    <?php
      if ( is_active_sidebar( 'sidebar' ) ) {
        dynamic_sidebar('sidebar');
      }
    ?>
  </aside>
</div> <!-- #sidebar -->
