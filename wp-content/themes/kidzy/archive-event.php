<?php

get_header();


$x = 1;

?>

<section id="main">
    
   <?php get_template_part('lib/sub-header')?>

    
        <div id="content" class="site-content" role="main">
            <div class="event-section <?php echo esc_attr($class); ?>00">
                <div class="container">
                <?php while ( have_posts() ): the_post(); ?>

                    <?php
                        $start_date = get_post_meta(get_the_ID(),'themeum_event_start_datetime',true);
                        $end_date   = get_post_meta(get_the_ID(),'themeum_event_end_datetime',true);                
                        $event_address = get_post_meta(get_the_ID(),'themeum_event_location',true);
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-medium' );
                    ?>
                    <?php if( $x == 1 ): ?>
                        <div class="row">
                    <?php endif; ?>










                    <div class="col-xs-12 col-sm-6 col-md-6">

                        <div class="single-event">

                        <?php if($image): ?>
                            <div class="event-img">
                                <?php if (has_post_thumbnail( get_the_ID() ) ): ?>
                                  <img class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo get_the_title(); ?>">
                                <?php endif; ?>

                                <div class="class-hover">
                                    <a class="popup" href="<?php echo get_permalink(); ?>"><i class="icon-link"></i></a>
                                </div>
                                <a href="<?php echo get_permalink(); ?>"><?php echo date_i18n("j", strtotime($start_date)); ?><span><?php echo date_i18n("M", strtotime($start_date)); ?></span></a>
                            </div>
                        <?php endif; ?>

                            <h3><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                            <span><?php echo date_i18n("F j, Y, h:i", strtotime($start_date)); ?> - <?php echo date_i18n("F j, Y, h:i", strtotime($end_date)); ?></span>
                            <span><?php echo esc_html($event_address); ?></span>
                            <p><?php echo kidzy_excerpt_max_char(200); ?></p>

                        </div>
                    </div>















                    <?php if( $x == (12/6) ): $x = 1; ?>
                        </div>
                    <?php else: $x++; endif; ?>
                <?php endwhile; ?>
                <?php themeum_pagination(); ?>
                </div>
            </div> <!--/#content-->.
        </div> <!--/#content-->
    

</section> <!--/#main-->
<?php get_footer();
