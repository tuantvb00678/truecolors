/*global $:false */
jQuery(document).ready(function($){'use strict';
    
    // Loader
    $(window).load(function () {
        if ($(".loader-wrap").length > 0)
        {
            $(".loader-wrap").delay(500).fadeOut("slow");
        }
    });
	
	
	/* -------------------------------------- */
	// 		RTL Support Visual Composer
	/* -------------------------------------- */	
	var delay = 1;
	function themeum_rtl() {
		if( $("html").attr("dir") == 'rtl' ){
			if( $( ".entry-content > div" ).attr( "data-vc-full-width" ) =='true' )	{
				$('.entry-content > div').css({'left':'auto','right':$('.entry-content > div').css('left')});	
			}
		}
	}
	setTimeout( themeum_rtl , delay);

	$( window ).resize(function() {
		setTimeout( themeum_rtl , delay);
	});


//Woocommerce
    jQuery( ".woocart" ).hover(function() {
        jQuery(this).find('.widget_shopping_cart, .product_list_widget').stop( true, true ).fadeIn();
    }, function() {
        jQuery(this).find('.widget_shopping_cart, .product_list_widget').stop( true, true ).fadeOut();
    }); 

    jQuery('.woocart a').html( jQuery('.woo-cart').html() );

    jQuery('.add_to_cart_button').on('click',function(){'use strict';

            jQuery('.woocart a').html( jQuery('.woo-cart').html() );            

            var total = 0;
            if( jQuery('.woo-cart-items span.cart-has-products').html() != 0 ){
                if( jQuery('#navigation ul.cart_list').length  > 0 ){
                    for ( var i = 1; i <= jQuery('#navigation ul.cart_list').length; i++ ) {
                        var total_string = jQuery('#navigation ul.cart_list li:nth-child('+i+') .quantity').text();
                        total_string = total_string.substring(-3, total_string.length);
                        total_string = total_string.replace('×', '');
                        total_string = parseInt( total_string.trim() );
                        //alert( total_string );
                        if( !isNaN(total_string) ){ total = total_string + total; }
                    }
                }
            }
            jQuery('.woo-cart-items span.cart-has-products').html( total+1 );

    }); 

    


    //-------------------------------------
    //Home Slider
    //-------------------------------------
    if ($('#owl-init').length > 0) {
        $('#owl-init').owlCarousel({
            margin: 0,
            loop: true,
            autoplay: true,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoplayHoverPause: true,
            autoplaySpeed: 2500,
            dots: false,
            nav: true,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
    }
    //----------------------------------------
    // Class Sort
    //----------------------------------------
    if ($('#mixer').length > 0) {
        $('.sort-btn li.filter a').on('click', function (event) {
            event.preventDefault();
        });
        $('#mixer').joomShaper();
    }
    //-------------------------------------
    // Start: Count Down
    //-------------------------------------
    if ($("#count-down").length > 0){
        $("#count-down").countdown(
            $('#count-down').data( 'datedata' ), 
            function(event) {
                $("#count-down .days").text( event.strftime('%D') );
                $("#count-down .hours").text( event.strftime('%H') );
                $("#count-down .minutes").text( event.strftime('%M') );
                $("#count-down .seconds").text( event.strftime('%S') );
            });
    }

    //-------------------------------------
    //On Load Animation
    //-------------------------------------
    $('.breadcrumb-section,.slider-wrap').on('mousemove', function (e) {
        var amountMovedX = (e.pageX * -1 / 60);
        var amountMovedY = (e.pageY * +1 / 20);
        $('.breadcrumb-section,.slider-wrap').css('background-position', amountMovedX + 'px ' + amountMovedY + 'px');

    });
    //----------------------------------------
    // Fixed Header
    //----------------------------------------
    if ($(".navigation").length > 0) {
        $(window).on('scroll', function () {
            if ($(window).scrollTop() > 300)
            {
                $(".navigation").addClass('fixed-menu animated fadeInDown');
            } else
            {
                $(".navigation").removeClass('fixed-menu animated fadeInDown');
            }
        });
    }
    
    //----------------------------------------
    // Search
    //----------------------------------------
    $("#search-pop").on('click', function (e) {
        e.preventDefault();
        $(".search-area").fadeIn(500);
    });
    $(".search-close").on('click', function (e) {
        e.preventDefault();
        $(".search-area").fadeOut(500);
    });
    if ($(window).width() > 767) {
        $(document).mouseup(function (e) {
            var container = $(".search-area");
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.fadeOut(500);
            }
        });
    }
    //----------------------------------------
    // Mobile Menu
    //----------------------------------------
    if ($('.menu-has-child').length > 0 && $(window).width() < 768) {
        var activeClass = true;
        $('.main-menu > li').on('click', function () {
            if (activeClass) {
                $(this).addClass('active');
                activeClass = false;
            } else
            {
                $(this).removeClass('active');
                activeClass = true;
            }
            $(this).find('ul').slideToggle();
        });
    }
    if ($('.mobile-menu').length > 0) {
        $('.mobile-menu').on('click', function (e) {
            e.preventDefault();
            $('.main-menu,.search-area').slideToggle();
        });
    }

    
    //-------------------------------------
    // End: Count Down
    //-------------------------------------    

    // Sticky Nav
    $(window).on('scroll', function(){
        if ( $(window).scrollTop() > 300 ) {
            $('#masthead').addClass('sticky');
        } else {
            $('#masthead').removeClass('sticky');
        }
    });

	// Social Share ADD
	$('.prettySocial').prettySocial();

     $("a[data-rel^='prettyPhoto']").prettyPhoto();


    //title first word
    $('#sidebar .widget_title').not('#sidebar .widget_rss .widget_title').each(function() {'use strict';
      var txt = $(this).html();
      var index = txt.indexOf(' ');
      if (index == -1) {
         index = txt.length;
      }
    $(this).html('<span>' + txt.substring(0, index) + '</span>' + txt.substring(index, txt.length));
    });


    // Search onclick
    $('.hd-search-btn').on('click', function(event) { 'use strict';
        event.preventDefault();
        var $searchBox = $('.home-search');
        if ($searchBox.hasClass('show')) {
            $searchBox.removeClass('show');
            $searchBox.fadeOut('fast');
        }else{
            $searchBox.addClass('show');
            $searchBox.fadeIn('slow');
        }
    });

    $('.hd-search-btn-close').on('click', function(event) { 'use strict';
        event.preventDefault();

        var $searchBox = $('.home-search');
        $searchBox.removeClass('show');
        $searchBox.fadeOut('fast');
    }); 

});