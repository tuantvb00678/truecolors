    <!-- Footer Section -->
    
<?php global $kidzy_options; 
?>

    <?php if(is_active_sidebar('bottom')){ ?>
    <?php if( kidzy_options('footer-block')) {?>
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <?php 
                    dynamic_sidebar('bottom');                                
                ?>
            </div>
        </div>
    </footer>
    <?php } ?>

    <?php } //End of if is active bottom widget

        if ( class_exists( 'ReduxFramework' ) && kidzy_options('copyright-text') && kidzy_options('copyright-en')) {
    ?>
        <footer class="blank-footer-section">
            <div class="container">
                <div class="row">
                    <div><?php echo kidzy_options('copyright-text'); ?></div>
                </div>
            </div>
        </footer>
        
        <?php 
        }//End of if: Redux Framework

    ?>

</div> <!-- #page -->
<?php wp_footer(); ?>
<script type="text/javascript">
    <?php print kidzy_options('custom_js'); ?>
</script>
</body>
</html>