<?php
global $themeum_options;
?>

<ul class="social-icon pull-left">
	<?php if ( (isset($themeum_options['wp-facebook']) && ($themeum_options['wp-facebook'] !='')) ) { ?>	
	<li><a class="facebook" href="<?php echo esc_url($themeum_options['wp-facebook']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-twitter'])) && ($themeum_options['wp-twitter'] !='')  ) { ?>
	<li><a class="twitter" href="<?php echo esc_url($themeum_options['wp-twitter']); ?>" target="_blank" ><i class="fa fa-twitter"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-google-plus'])) && ($themeum_options['wp-google-plus'] !='') ) { ?>
	<li><a class="g-plus" href="<?php echo esc_url($themeum_options['wp-google-plus']); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
	<?php } ?>		

	<?php if ( (isset($themeum_options['wp-youtube'])) && ($themeum_options['wp-youtube'] !='') ) { ?>
	<li><a class="youtube" href="<?php echo esc_url($themeum_options['wp-youtube']); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-soundcloud'])) && ($themeum_options['wp-soundcloud'] !='') ) { ?>
	<li><a class="soundcloud" href="<?php echo esc_url($themeum_options['wp-soundcloud']); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-linkedin'])) && ($themeum_options['wp-linkedin'] !='') ) { ?>
	<li><a class="linkedin" href="<?php echo esc_url($themeum_options['wp-linkedin']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-pinterest'])) && ($themeum_options['wp-pinterest'] !='') ) { ?>
	<li><a class="pinterest" href="<?php echo esc_url($themeum_options['wp-pinterest']); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-delicious'])) && ($themeum_options['wp-delicious'] !='') ) { ?>	
	<li><a class="delicious" href="<?php echo esc_url($themeum_options['wp-delicious']); ?>" target="_blank"><i class="fa fa-delicious"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-tumblr'])) && ($themeum_options['wp-tumblr'] !='') ) { ?>	
	<li><a class="tumblr" href="<?php echo esc_url($themeum_options['wp-tumblr']); ?>" target="_blank"><i class="fa fa-tumblr"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-stumbleupon'])) && ($themeum_options['wp-stumbleupon'] !='') ) { ?>	
	<li><a class="stumbleupon" href="<?php echo esc_url($themeum_options['wp-stumbleupon']); ?>" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-instagram'])) && ($themeum_options['wp-instagram'] !='') ) { ?>	
	<li><a class="instagram" href="<?php echo esc_url($themeum_options['wp-instagram']); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-dribbble'])) && ($themeum_options['wp-dribbble'] !='') ) { ?>	
	<li><a class="dribbble" href="<?php echo esc_url($themeum_options['wp-dribbble']); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
	<?php } ?>


	<?php if ( (isset($themeum_options['wp-behance'])) && ($themeum_options['wp-behance'] !='') ) { ?>	
	<li><a class="behance" href="<?php echo esc_url($themeum_options['wp-behance']); ?>" target="_blank"><i class="fa fa-behance"></i></a></li>
	<?php } ?>
	
	<?php if ( (isset($themeum_options['wp-flickr'])) && ($themeum_options['wp-flickr'] !='') ) { ?>	
	<li><a class="flickr" href="<?php echo esc_url($themeum_options['wp-flickr']); ?>" target="_blank"><i class="fa fa-flickr"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-vk'])) && ($themeum_options['wp-vk'] !='') ) { ?>	
	<li><a class="flickr" href="<?php echo esc_url($themeum_options['wp-vk']); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
	<?php } ?>	

	<?php if ( (isset($themeum_options['wp-skype'])) && ($themeum_options['wp-skype'] !='') ) { ?>	
	<li><a class="flickr" href="<?php echo esc_url($themeum_options['wp-skype']); ?>" target="_blank"><i class="fa fa-skype"></i></a></li>
	<?php } ?>

	<?php if ( (isset($themeum_options['wp-envelope'])) && ($themeum_options['wp-envelope'] !='') ) { ?>	
	<li><a class="flickr" href="<?php echo esc_url($themeum_options['wp-envelope']); ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
	<?php } ?>				
</ul>