<?php
/*-------------------------------------------*
 *      Themeum Widget Registration
 *------------------------------------------*/

if(!function_exists('kidzy_widdget_init')):

    function kidzy_widdget_init()
    {
        global $kidzy_options;

        if(!isset($kidzy_options['bottom-column'])){
            $kidzy_options['bottom-column'] = 3;
        }
        $bottomcolumn = kidzy_options('bottom-column');

        register_sidebar(array(
            'name'          => esc_html__( 'Sidebar', 'kidzy' ),
            'id'            => 'sidebar',
            'description'   => esc_html__( 'Widgets in this area will be shown on Sidebar.', 'kidzy' ),
            'before_title'  => '<h3 class="widget_title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div id="%1$s" class="widget %2$s" >',
            'after_widget'  => '</div>'
        ));

        register_sidebar(array(
            'name'          => esc_html__( 'Bottom', 'kidzy' ),
            'id'            => 'bottom',
            'description'   => esc_html__( 'Widgets in this area will be shown before Footer.' , 'kidzy'),
            'before_title'  => '<h3 class="widget_title">',
            'after_title'   => '</h3>',
            'before_widget' => '<div class="col-sm-'.esc_attr($bottomcolumn).' col-xs-12"><div class="footer-widget">',
            'after_widget'  => '</div></div>'
            )
        );

    }
    
    add_action('widgets_init','kidzy_widdget_init');

endif;


/*-------------------------------------------*
 *      Themeum Style
 *------------------------------------------*/

if(!function_exists('kidzy_style')):

    function kidzy_style(){

        wp_enqueue_media();
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css',false,'all');
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css',false,'all');
        wp_enqueue_style( 'kidzy-fonts', get_template_directory_uri() . '/css/fonts.css',false,'all');
        wp_enqueue_style( 'kidzy-main', get_template_directory_uri() . '/css/main.css',false,'all');
        
        // Custom css
        wp_enqueue_style( 'kidzy-custom-style', get_template_directory_uri() . '/assets/css/main.min.css', false, 'all' );
  
  
      wp_enqueue_style( 'kidzy-prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css',false,'all');
        wp_enqueue_style( 'kidzy-responsive', get_template_directory_uri() . '/css/responsive.css',false,'all');

        wp_enqueue_style('thm-style',get_stylesheet_uri());
        wp_enqueue_script('bootstrap',KIDZY_JS.'bootstrap.min.js',array(),false,true);
        wp_enqueue_script('jquery.prettySocial',KIDZY_JS.'jquery.prettySocial.min.js',array(),false,true);
        wp_enqueue_script('jquery.prettyPhoto',KIDZY_JS.'jquery.prettyPhoto.js',array(),false,true);
        wp_enqueue_script('jquery.ajax.login',KIDZY_JS.'ajax-login-script.js',array(),false,true);
        // Required for nested reply function that moves reply inline with JS
        if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
        
        global $themeum_options;
        if( isset($themeum_options['custom-preset-en']) && $themeum_options['custom-preset-en']==0 ) {
            wp_enqueue_style( 'themeum-preset', get_template_directory_uri(). '/css/presets/preset' . $themeum_options['preset'] . '.css', array(),false,'all' );
        }

        wp_enqueue_script('jquery.countdown',KIDZY_JS.'jquery.countdown.min.js',array(),false,true);
        wp_enqueue_script('main',KIDZY_JS.'main.js',array(),false,true);

    }

    add_action('wp_enqueue_scripts','kidzy_style');

endif;


add_action('enqueue_block_editor_assets', 'kidzy_action_enqueue_block_editor_assets' );
function kidzy_action_enqueue_block_editor_assets() {
    wp_enqueue_style( 'kidzy-gutenberg-editor-font-awesome-styles', get_template_directory_uri() . '/css/font-awesome.css', null, 'all' );
    wp_enqueue_style( 'thm-style', get_stylesheet_uri() );
    wp_enqueue_style( 'kidzy-gutenberg-editor-customizer-styles', get_template_directory_uri() . '/css/gutenberg-editor-custom.css', null, 'all' );
    wp_enqueue_style( 'kidzy-gutenberg-editor-styles', get_template_directory_uri() . '/css/gutenberg-custom.css', null, 'all' );
}


if(!function_exists('kidzy_admin_style')):

    function kidzy_admin_style(){
        wp_enqueue_media();
        wp_register_script('thmpostmeta', get_template_directory_uri() .'/js/admin/post-meta.js');
        wp_enqueue_script('themeum-widget-js', get_template_directory_uri().'/js/admin/widget-js.js', array('jquery'));
        wp_enqueue_script('thmpostmeta');
    }
    add_action('admin_enqueue_scripts','kidzy_admin_style');

endif;


/*-------------------------------------------------------
*           Include the TGM Plugin Activation class
*-------------------------------------------------------*/

require_once( get_template_directory()  . '/lib/class-tgm-plugin-activation.php');

add_action( 'tgmpa_register', 'kidzy_plugins_include');

if(!function_exists('kidzy_plugins_include')):

    function kidzy_plugins_include()
    {
        $plugins = array(

                array(
                    'name'                  => 'Themeum Core',
                    'slug'                  => 'themeum-core',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/kidzy/themeum-core.zip',
                    'required'              => true,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),
                array(
                    'name'                  => esc_html__( 'Qubely Blocks – Full-fledged Gutenberg Toolkit', 'corlate' ),
                    'slug'                  => 'qubely',
                    'required'              => true,
                    'version'               => '',
                    'force_activation'      => true,
                    'force_deactivation'    => false
                ),
                array(
                    'name'                  => 'Woocoomerce',
                    'slug'                  => 'woocommerce',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/woocommerce.2.6.4.zip',
                ),

                array(
                    'name'                  => 'Slider Revolution',
                    'slug'                  => 'revslider',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/revslider.zip',
                    'required'              => true,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),
                
                array(
                    'name'                  => 'WPBakery Visual Composer',
                    'slug'                  => 'js_composer',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/js_composer.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),
                array(
                    'name'                  => 'MailChimp for WordPress',
                    'slug'                  => 'mailchimp-for-wp',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/mailchimp-for-wp.4.0.4.zip',
                ),
                array(
                    'name'                  => 'Widget Importer Exporter',
                    'slug'                  => 'widget-importer-exporter',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/widget-importer-exporter.1.4.5.zip',
                ),
                array(
                    'name'                  => 'Contact Form 7',
                    'slug'                  => 'contact-form-7',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/contact-form-7.4.5.zip',
                ),
            );
    $config = array(
            'domain'            => 'kidzy',
            'default_path'      => '',
            'parent_menu_slug'  => 'themes.php',
            'parent_url_slug'   => 'themes.php',
            'menu'              => 'install-required-plugins',
            'has_notices'       => true,
            'is_automatic'      => false,
            'message'           => '',
            'strings'           => array(
                'page_title'         => esc_html__( 'Install Required Plugins', 'kidzy' ),
                'menu_title'         => esc_html__( 'Install Plugins', 'kidzy' ),
                'installing'         => esc_html__( 'Installing Plugin: %s', 'kidzy' ),
                'oops'               => esc_html__( 'Something went wrong with the plugin API.', 'kidzy'),
                'return'             => esc_html__( 'Return to Required Plugins Installer', 'kidzy'),
                'plugin_activated'   => esc_html__( 'Plugin activated successfully.','kidzy'),
                'complete'           => esc_html__( 'All plugins installed and activated successfully. %s', 'kidzy')
            )
    );

    tgmpa( $plugins, $config );

    }

endif;
