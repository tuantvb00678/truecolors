<?php
  
  if(!function_exists('kidzy_setup')):
    
    function kidzy_setup()
    {
      //Textdomain
      load_theme_textdomain( 'kidzy', get_template_directory() . '/languages' );
      
      add_theme_support( 'post-thumbnails' );
      add_image_size( 'kidzy-blog-thumbnails', 458, 295, true );
      add_image_size( 'kidzy-sidebar-thumbnails', 49, 49, true );
      add_image_size( 'kidzy-blog-single', 845, 545, true );
      add_image_size( 'kidzy-medium', 554, 554, true );
      add_theme_support( 'post-formats', array( 'audio','gallery','image','link','quote','video'));
      add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form'));
      add_theme_support( 'automatic-feed-links' );
      
      add_editor_style('');
      
      add_theme_support( 'align-wide' );
      add_theme_support( 'editor-styles' );
      add_theme_support( 'wp-block-styles' );
      
      if ( ! isset( $content_width ) )
        $content_width = 660;
    }
    
    add_action('after_setup_theme','kidzy_setup');
  
  endif;
  
  function kidzy_body_classes( $classes ) {
    if ( is_active_sidebar( 'sidebar' ) ) {
      $classes[] = 'sidebar-active';
    }else{
      $classes[] = 'sidebar-inactive';
    }
    return $classes;
  }
  add_filter( 'body_class','kidzy_body_classes' );
  
  
  if(!function_exists('kidzy_pagination')):
    
    function kidzy_pagination( $page_numb , $max_page )
    {
      $big = 999999999; // need an unlikely integer
      echo '<div class="themeum-pagination">';
      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'             => '?paged=%#%',
        'current'            => $page_numb,
        'prev_text'          => '<i class="fa fa-angle-left"></i>',
        'next_text'          => '<i class="fa fa-angle-right"></i>',
        'total'              => $max_page,
        'type'               => 'list',
      ) );
      echo '</div>';
    }
  endif;
  
  
  /*-------------------------------------------------------
   *              Themeum Comment
   *-------------------------------------------------------*/
  
  if(!function_exists('kidzy_comment')):
    
    function kidzy_comment($comment, $args, $depth)
    {
      $GLOBALS['comment'] = $comment;
      switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
          // Display trackbacks differently than normal comments.
          ?>
          <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
          
          <?php
          break;
        default :
          global $post;
          ?>
          <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
          <div id="comment-<?php comment_ID(); ?>" class="comment-body media">

            <div class="comment-avartar pull-left">
              <?php
                echo get_avatar( $comment, $args['avatar_size'] );
              ?>
            </div>
            <div class="comment-context media-body">
              <div class="comment-head">
                <?php
                  printf( '<span class="comment-author">%1$s</span>',
                    get_comment_author_link());
                ?>
                <span class="comment-date"><?php echo get_comment_date('d / m / Y') ?></span>
                
                <?php edit_comment_link( esc_html__( 'Edit', 'kidzy' ), '<span class="edit-link">', '</span>' ); ?>
                <span class="comment-reply">
                                <?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'kidzy' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                            </span>
              </div>
              
              <?php if ( '0' == $comment->comment_approved ) : ?>
                <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'kidzy' ); ?></p>
              <?php endif; ?>

              <div class="comment-content">
                <?php comment_text(); ?>
              </div>
            </div>

          </div>
          <?php
          break;
      endswitch;
    }
  
  endif;
  
  
  /*-------------------------------------------------------
  *           Themeum Breadcrumb
  *-------------------------------------------------------*/
  if(!function_exists('kidzy_breadcrumbs')):
    function kidzy_breadcrumbs(){ global $wp_query; ?>
      <ul>
        <li><a href="<?php echo esc_url(site_url()); ?>" class="breadcrumb_home"><?php esc_html_e('Home', 'kidzy') ?></a><span>/</span></li>
        <?php
          if(function_exists('is_product')){
            $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
            if(is_product()){
              echo "<li><a href='".$shop_page_url."'>".esc_html__( 'shop /','kidzy' )."</a></li>";
            }
          }
        ?>
        <li class="active">
          <?php if(function_exists('is_shop')){ if(is_shop()){ esc_html_e('shop','kidzy'); } } ?>
          
          <?php if( is_tag() ) { ?>
            <?php esc_html_e('Posts Tagged ', 'kidzy') ?><span class="raquo">/</span><?php single_tag_title(); echo('/'); ?>
          
          <?php } elseif ($wp_query->queried_object->post_type == 'post') { ?>
            <span class="raquo">/</span><?php echo $wp_query->queried_object->post_title; ?>
          
          <?php } elseif (is_day()) { ?>
            <?php esc_html_e('Posts made in', 'kidzy') ?> <?php the_time('F jS, Y'); ?>
          <?php } elseif (is_month()) { ?>
            <?php esc_html_e('Posts made in', 'kidzy') ?> <?php the_time('F, Y'); ?>
          <?php } elseif (is_year()) { ?>
            <?php esc_html_e('Posts made in', 'kidzy') ?> <?php the_time('Y'); ?>
          <?php } elseif (is_search()) { ?>
            <?php esc_html_e('Search results for', 'kidzy') ?> <?php the_search_query() ?>
          <?php } elseif (is_single()) { ?>
            <?php $category = get_the_category();
            if ( $category ) {
              $catlink = get_category_link( $category[0]->cat_ID );
              echo ('<a href="'.esc_url($catlink).'">'.esc_html($category[0]->cat_name).'</a> '.'<span class="raquo"> /</span> ');
            }
            echo get_the_title(); ?>
          <?php } elseif (is_category()) { ?>
            <?php single_cat_title(); ?>
          <?php } elseif (is_tax()) { ?>
            <?php
            $themeum_taxonomy_links = array();
            $themeum_term = get_queried_object();
            $themeum_term_parent_id = $themeum_term->parent;
            $themeum_term_taxonomy = $themeum_term->taxonomy;
            
            while ( $themeum_term_parent_id ) {
              $themeum_current_term = get_term( $themeum_term_parent_id, $themeum_term_taxonomy );
              $themeum_taxonomy_links[] = '<a href="' . esc_url( get_term_link( $themeum_current_term, $themeum_term_taxonomy ) ) . '" title="' . esc_attr( $themeum_current_term->name ) . '">' . esc_html( $themeum_current_term->name ) . '</a>';
              $themeum_term_parent_id = $themeum_current_term->parent;
            }
            
            if ( !empty( $themeum_taxonomy_links ) ) echo implode( ' <span class="raquo">/</span> ', array_reverse( $themeum_taxonomy_links ) ) . ' <span class="raquo">/</span> ';
            echo esc_html( $themeum_term->name );
          } elseif (is_author()) {
            global $wp_query;
            $curauth = $wp_query->get_queried_object();
            
            esc_html_e('Posts by ', 'kidzy');
            echo ' ',$curauth->nickname;
          } elseif (is_page()) {
            echo get_the_title();
          } elseif (is_home()) {
            esc_html_e('Blog', 'kidzy');
          }
          ?>
        </li>
      </ul>
      
      <?php
    }
  endif;
  
  
  /*-----------------------------------------------------
   *              Coming Soon Page Settings
   *----------------------------------------------------*/
  if ( kidzy_options('comingsoon-en') ) {
    if(!function_exists('kidzy_my_page_template_redirect')):
      function kidzy_my_page_template_redirect()
      {
        if( is_page( ) || is_home() || is_category() || is_single() )
        {
          if( !is_super_admin( get_current_user_id() ) ){
            get_template_part( 'coming','soon');
            exit();
          }
        }
      }
      add_action( 'template_redirect', 'kidzy_my_page_template_redirect' );
    endif;
    
    if(!function_exists('kidzy_cooming_soon_wp_title')):
      function kidzy_cooming_soon_wp_title(){
        return 'Coming Soon';
      }
      add_filter( 'wp_title', 'kidzy_cooming_soon_wp_title' );
    endif;
  }
  
  
  
  /*-----------------------------------------------------
   *              Kidzy CSS Generator
   *----------------------------------------------------*/
  
  if(!function_exists('kidzy_css_generator')){
    function kidzy_css_generator(){
      
      $output = '';
      
      /*********************************
       **********      Preset   *********
       **********************************/
      
      if( kidzy_options('preset') ){
        $link_color = esc_attr(kidzy_options('link-color'));
        
        if( kidzy_options('custom-preset-en') ) {
          
          if(isset($link_color)){
            $output .= '
                    .rs-hover-ready{background-color: '.esc_attr($link_color).' !important;}

                    .tagcloud a,#comments .form-submit #submit, .sub-cls,.single-post-quote .entry-quote-post-format, .single-post-link{background-color: '.esc_attr($link_color).';}

                    .welcome-kidzy h2, .our-classes h2, .school-fecilities h2, .meet-staffs h2, .latest-news h2, .enrollment h2,.who-are h2, .atmosphere-details h2, .atmosphere-content ul li i,.leave-comment h2{color: '.esc_attr($link_color).' !important;}

                    .breadcrumb-section,.footer-section{background-color: '.esc_attr($link_color).' !important;}

                    .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab.vc_active > a{background: '.esc_attr($link_color).' !important;}

                    .author-tag li i,.author-tag li a i,.class-details h3 a,.class-meta p,.class-single-meta h4,.sub-heading h4,.sort-btn li.active a, .class-search button,.vc_tta-container h2,.title-2,.single-team > h3,.bio-details:before, a, a:focus{
                        color: '.esc_attr($link_color).';}

                    .bio-content:after, .widget h3.widget_title, .widget-area .widget_categories h3.widget_title{background: '.esc_attr($link_color).' none repeat scroll 0 0;}

                    .post-thumb-link,.post-thumb-quote,.schedule-table thead tr th:first-child,.schedule-table thead tr th{background: '.esc_attr($link_color).';}

                    .primary-btn{background: '.esc_attr($link_color).' !important; box-shadow: 0 4px 0px 0 '.esc_attr($link_color).', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1) !important; -webkit-box-shadow:0 4px 0px 0 '.esc_attr($link_color).', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1) !important;}

                    .inputs-group input:focus, textarea:focus{border-color:'.esc_attr($link_color).';}

                    .pagination li.active a{background: '.esc_attr($link_color).';border-color: '.esc_attr($link_color).';box-shadow: 0 4px 0 0 '.esc_attr($link_color).', 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1);}

                    .primary-btn span{background: '.esc_attr($link_color).' none repeat scroll 0 0;}

                    .page-numbers li .current{box-shadow:0 4px 0 0 '.esc_attr($link_color).', 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1); background: '.esc_attr($link_color).';}

                    .class-hover{background:'.esc_attr($link_color).';}

                    .event-img > a{ background: '.esc_attr($link_color).' none repeat scroll 0 0; box-shadow:0 4px '.esc_attr($link_color).';}

                    .team-img:after{background: '.esc_attr($link_color).' none repeat scroll 0 0;}

                    .subscribe-form input{box-shadow: 0 4px '.esc_attr($link_color).';}
                ';
          }
        }
        
        if( kidzy_options('custom-preset-en') ) {
          
          if( kidzy_options('hover-color') ){
            $output .= '.pagination li.active a:hover{
                    background: '.esc_attr( kidzy_options('hover-color') ).';border-color:'.esc_attr( kidzy_options('hover-color')).';box-shadow: 0 4px 0 0 '.esc_attr(kidzy_options('hover-color')).', 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1);}';
            
            $output .= '.widget ul li a:before{ background: '.esc_attr( kidzy_options('hover-color')).' none repeat scroll 0 0;; }';
            
            $output .= '.tagcloud a:hover, #comments .form-submit #submit:hover{ background-color: '.esc_attr( kidzy_options('hover-color') ).'; }';
            
            $output .= '
                .subject-list li a:hover,.wid-post-title h6 a:hover, .date li a:hover, .author-tag li a:hover, .post-details h3 a:hover,.widget ul li a:hover,.single-event h3 a:hover, .single-event > a:hover,.sort-btn li a:hover,.single-teacher .tsocial-icon li a:hover,.single-news h5 a:hover, .single-news p a:hover,a:hover{color:'.esc_attr( kidzy_options('hover-color') ) .'; }
                ';
          }
        }
      }//End of if: Kidzy Options Preset
      
      else {
        $output ='
        .rs-hover-ready{background-color: #92278f !important;}

        .welcome-kidzy h2, .our-classes h2, .school-fecilities h2, .meet-staffs h2, .latest-news h2, .enrollment h2,.who-are h2, .atmosphere-details h2, .atmosphere-content ul li i,.leave-comment h2{color: #92278f !important;}

        .breadcrumb-section,.footer-section{background-color: #92278f !important;}

        .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab.vc_active > a{background: #92278f !important;}

        .author-tag li i,.author-tag li a i,.class-details h3 a,.class-meta p,.class-single-meta h4,.sub-heading h4,.subject-list li a:hover,.wid-post-title h6 a:hover, .date li a:hover, .author-tag li a:hover, .post-details h3 a:hover,.widget ul li a:hover,.sort-btn li.active a, .sort-btn li a:hover,.class-search button,.vc_tta-container h2,.single-event h3 a:hover, .single-event > a:hover,.title-2,.single-team > h3,.bio-details:before,.single-teacher .tsocial-icon li a:hover,.single-news h5 a:hover, .single-news p a:hover{
            color: #92278f;}

        .bio-content:after{background: #92278f none repeat scroll 0 0;}

        .post-thumb-link,.post-thumb-quote,.schedule-table thead tr th:first-child,.schedule-table thead tr th{background: #92278f;}

        .primary-btn{background: #92278f !important; box-shadow: 0 4px 0px 0 #92278f, 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1) !important; -webkit-box-shadow:0 4px 0px 0 #92278f, 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1) !important;}

        .inputs-group input:focus{border-color:#92278f;}

        .pagination li.active a,.pagination li.active a:hover{background: #92278f;border-color: #92278f;box-shadow: 0 4px 0 0 #92278f, 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1);}

        .primary-btn span{background: #92278f none repeat scroll 0 0;}

        .widget ul li a:before{ background: #92278f none repeat scroll 0 0;}

        .page-numbers li .current{box-shadow:0 4px 0 0 #92278f, 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1); background: #92278f;}

        .class-hover{background:#92278f;}

        .event-img > a{ background: #92278f none repeat scroll 0 0; box-shadow:0 4px #92278f;}

        .team-img:after{background: #92278f none repeat scroll 0 0;}

        .subscribe-form input{box-shadow: 0 4px #92278f;}
    ';
      }//End of else preset
      
      
      /*********************************
       **********  Quick Style **********
       **********************************/
      
      $headerbg = kidzy_options('header-bg');
      if(isset($headerbg)){
        $output .= '.header{ background: '.esc_attr(kidzy_options('header-bg')) .'; }';
      }
      
      
      if (kidzy_options('header-fixed')){
        
        if(isset($headerbg)){
          $output .= '#masthead.sticky{ background-color: rgba(255,255,255,.97); }';
        }
        $output .= 'nav.sticky{ position:fixed;top:0; z-index:99;margin:0 auto 30px; width:100%;box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.22);}';
        $output .= 'nav.sticky #header-container{ padding:0;transition: padding 200ms linear; -webkit-transition:padding 200ms linear;}';
        $output .= 'nav.sticky .navbar.navbar-default{ background: rgba(255,255,255,.95); border-bottom:1px solid #f5f5f5}';
      }
      
      
      if( kidzy_options('header-padding-top') ){
        $output .= '.navigation{ padding-top: '. (int) esc_attr( kidzy_options('header-padding-top')) .'px; }';
      }
      
      if(kidzy_options('header-padding-bottom')){
        $output .= '.navigation{ padding-bottom: '. (int) esc_attr( kidzy_options('header-padding-bottom') ) .'px; }';
      }
      
      if(kidzy_options('footer-background')){
        $output .= '.footer-area, .footer-two-area{ background: '.esc_attr(kidzy_options('footer-background')) .'; }';
      }
      
      if (kidzy_options('comingsoon-en')) {
        $output .= "body {
            background: #fff;
            display: table;
            width: 100%;
            height: 100%;
            min-height: 100%;
        }";
      }
      
      
      if ( kidzy_options('errorbg') ) {
        $output .= "body.error404,BODY.page-template-404{
            width: 100%;
            height: 100%;
            min-height: 100%;
            background-size: cover;
        }";
      }
      
      if ( kidzy_options('logo-width') ) {
        $output .= ".navbar-brand>img{width: ".kidzy_options('logo-width')."px;}";
      }
      
      if ( kidzy_options('logo-height') ) {
        $output .= ".navbar-brand>img{height: ".kidzy_options('logo-height')."px;}";
      }
      
      if ( kidzy_options('subtitle_font_size') ) {
        $output .= ".breadcrumb-content h2{font-size: ".kidzy_options('subtitle_font_size')."px;}";
      }
      
      $menu_color = kidzy_options('menu_color');
      
      if ( kidzy_options('menu_custom_color') && !empty($menu_color) ) {
        $output .= ".main-menu > li > a, .main-menu .menu-item-has-children:before{color: ".$menu_color['regular']." !important;}";
        $output .= ".main-menu > li:hover > a, .main-menu .menu-item-has-children:hover:before, .main-menu > li a:after{color: ".$menu_color['hover']." !important;}";
      }
      
      $sub_menu_color = kidzy_options('sub_menu_color');
      
      if ( kidzy_options('menu_custom_color') && !empty($sub_menu_color) ) {
        $output .= ".main-menu > li > ul a, .main-menu > li > ul .menu-item-has-children:before{color: ".$sub_menu_color['regular']." !important;}";
        $output .= ".main-menu > li > ul a:hover, .main-menu > li > ul .menu-item-has-children:hover:before{color: ".$sub_menu_color['hover']." !important;}";
      }
      
      $sub_menu_bg_color = kidzy_options('sub_menu_bg_color');
      
      if ( kidzy_options('menu_custom_color') && !empty($sub_menu_bg_color) ) {
        $output .= ".main-menu > li > ul a{background-color: ".$sub_menu_bg_color['regular']." !important;}";
        $output .= ".main-menu > li > ul a:hover{background-color: ".$sub_menu_bg_color['hover']." !important;}";
      }
      
      $output = '<style>'.$output.'</style>';
      echo $output;
      
      
    }//End of function
  }//End of function exists
  
  add_action('wp_head', 'kidzy_css_generator');





// woocommerce Cart Button

// add_action( 'woocommerce_after_shop_loop_item', 'kidzy_add_more_info_buttons', 1 );
// function kidzy_add_more_info_buttons() {
//     add_action( 'woocommerce_after_shop_loop_item', 'kidzy_more_info_button' );
// }
// function kidzy_more_info_button() {
//     global $product;
//     echo '<a href="' . get_permalink( $product->id ) . '" class="button add_to_cart_button product_type_external"><i class="fa fa-shopping-basket cart-icons-shop" aria-hidden="true"></i> Add to Cart</a>';
// }
  
  add_action( 'woocommerce_after_shop_loop_item', 'kidzy_more_info_button', 1 );
  function kidzy_more_info_button() {
    add_action( 'woocommerce_simple_add_to_cart', 'politist_more_info_button' );
  }
  function politist_more_info_button() {
    global $product;
    echo '<a href="' . get_permalink( $product->id ) . '" class="button add_to_cart_button product_type_external">Add to Cart</a>';
  }


// if(!function_exists('politist_css_generator')){
//     function politist_css_generator(){

//         $output = '';

//         /* *******************************
//         **********      Preset   **********
//         ********************************** */
//         if( politist_options('preset') ){
//             $link_color = esc_attr(politist_options('link-color'));

//             if( politist_options('custom-preset-en') ) {

//                 if(isset($link_color)){
//                     $output .= 'a,a:focus,.top-user-login i,.themeum-title .title-link:hover,.sub-title .breadcrumb>.active,.modal-content .lost-pass:hover,.home-two-crousel .carousel-control:hover,
//             #mobile-menu ul li:hover > a,#mobile-menu ul li.active > a,.top-align .sp-moview-icon-user,#sidebar .widget ul li a:hover,.continue:hover:after,.common-menu-wrap .nav>li>ul li.active > a,.common-menu-wrap .nav>li>ul li:hover > a,
//             .themeum-pagination ul li .current,.themeum-pagination ul li a:hover,.woocommerce div.product p.price, .woocommerce div.product span.price,
//             .product-thumbnail-outer h3 a:hover,.entry-title.blog-entry-title a:hover,.woocommerce table.shop_table td a:hover,.common-menu-wrap .nav>li>a:hover,.small-news h4 a:hover,.news-wrap h3 a:hover,.footer-menu li a:hover,.tribe-events-list-separator-month span,.tribe-event-schedule-details:before,.tribe-events-venue-details:before,.tribe-events-month .tribe-events-page-title,.btn-blog,
//             .singnature a:hover,.home-one-crousel .carousel-control:hover,#sidebar h3.widget_title span,.news-feed-wrap a:hover,.meta-date,.product-details h4 a:hover,#product-crousel.owl-theme .owl-controls .owl-nav div:hover,#videoPlay:hover,
//             .timeline-description h6,.news-post h3 a:hover,.news-post > a,.widget-title,.widget ul li a:hover,.widget-post h5,.widget-post a:hover,
//             .woocommerce .comment-text .star-rating span,.woocommerce form .form-row.woocommerce-invalid label { color: '. esc_attr($link_color) .'; }';
//                 }

//                 if(isset($link_color)){
//                     $output .= '.error-page-inner a.btn.btn-primary.btn-lg,.btn.btn-primary,.news-feed .news-feed-item .news-feed-info .meta-category a:hover,
//             .spotlight-post .list-inline>li>a:after,input[type=submit],.form-submit input[type=submit],
//             .widget .tagcloud a:hover,.carousel-left:hover, .carousel-right:hover,input[type=button],
//             .woocommerce div.product form.cart .button,.woocommerce #respond input#submit,.woocommerce a.button, .woocommerce button.button,
//             .woocommerce input.button,.widget.widget_search #searchform .btn-search,#tribe-events .tribe-events-button,
//             #tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type=submit], .tribe-events-button,
//             .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover,
//             .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],
//             .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a,.woocommerce a.button.alt,.woocommerce input.button.alt,
//             .woocommerce .woocommerce-message,.filled-button:hover,.filled-button:focus,.news-img:hover .news-date:before,
//             .featured-wrap:hover .news-date:before,.single-social-info.user,.single-social-info:hover,.single-social-info:focus,
//             .common-menu-wrap .nav>li>ul li:hover > a:before,.common-menu-wrap .nav>li>ul li.active > a:before,.filled-button.user,
//             .filled-button:hover,.filled-button:focus,.coming-soon-form button,.woocommerce .product-thumbnail-outer-inner .addtocart-btn a.button:hover,#resume-carousel .carousel-indicators li.active:before,#resume-carousel .carousel-indicators li:hover:before,.timeline-description:hover:before,.timeline-date:hover:after,.error-page-inner a.btn.btn-primary.btn-lg:hover, .btn.btn-primary:hover,.widget ul li a:before,a.tags.active,a.tags:hover,.woocommerce .site-content .single-product-details .cart .button:hover,.woocommerce #review_form #respond .comment-form .form-submit input:hover,.widget table thead tr th,ul.tribe-events-sub-nav li a{background-color: '. esc_attr($link_color) .'; }';
//                 }
//                 if(isset($link_color)){
//                     $output .= 'input:focus, textarea:focus, keygen:focus, select:focus,.woocommerce form .form-row.woocommerce-invalid .select2-container, .woocommerce form .form-row.woocommerce-invalid input.input-text, .woocommerce form .form-row.woocommerce-invalid select { border-color: '. esc_attr($link_color) .'; }';
//                 }

//             }

//             if( politist_options('custom-preset-en') ) {

//                 if( politist_options('hover-color') ){
//                     $output .= 'a:hover, .widget.widget_rss ul li a{ color: '.esc_attr( politist_options('hover-color') ) .'; }';
//                     $output .= '.error-page-inner a.btn.btn-primary.btn-lg:hover,.btn.btn-primary:hover,input[type=button]:hover,
//                     .woocommerce div.product form.cart .button:hover,.woocommerce #respond input#submit:hover,
//                     .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
//                     .widget.widget_search #searchform .btn-search:hover,.woocommerce a.button.alt:hover,
//                     .woocommerce input.button.alt:hover,.btn-block.submit_button:hover{ background-color: '.esc_attr( politist_options('hover-color') ) .'; }';
//                     $output .= '.btn.btn-primary{ border-color: '.esc_attr( politist_options('hover-color') ) .'; }';
//                 }
//             }

//         } else {
//         $output ='a,a:focus,.small-news h4 a:hover,.top-user-login i,.themeum-title .title-link:hover,.sub-title .breadcrumb>.active,.modal-content .lost-pass:hover,.common-menu-wrap .nav>li>ul li.active > a,.common-menu-wrap .nav>li>ul li:hover > a,
//             #mobile-menu ul li:hover > a,#mobile-menu ul li.active > a,.top-align .sp-moview-icon-user,#sidebar .widget ul li a:hover,.continue:hover:after,.btn-blog,.singnature a:hover,.home-one-crousel .carousel-control:hover,
//             .themeum-pagination ul li .current,.themeum-pagination ul li a:hover,.woocommerce div.product p.price, .woocommerce div.product span.price,
//             .product-thumbnail-outer h3 a:hover,.entry-title.blog-entry-title a:hover,.woocommerce table.shop_table td a:hover,.common-menu-wrap .nav>li>a:hover,.news-wrap h3 a:hover,.footer-menu li a:hover,.tribe-events-list-separator-month span,.tribe-event-schedule-details:before,.tribe-events-venue-details:before,.tribe-events-month .tribe-events-page-title,.home-two-crousel .carousel-control:hover,
//             #sidebar h3.widget_title span,.news-feed-wrap a:hover,.meta-date,.product-details h4 a:hover,#product-crousel.owl-theme .owl-controls .owl-nav div:hover,#videoPlay:hover,.timeline-description h6,.news-post h3 a:hover,.news-post > a,.widget-title,.widget ul li a:hover,.widget-post h5,.widget-post a:hover,.woocommerce .comment-text .star-rating span,.woocommerce form .form-row.woocommerce-invalid label { color: #ed1c24; }.error-page-inner a.btn.btn-primary.btn-lg,.btn.btn-primary,.news-feed .news-feed-item .news-feed-info .meta-category a:hover,
//             .spotlight-post .list-inline>li>a:after,input[type=submit],.form-submit input[type=submit],
//             .widget .tagcloud a:hover,.carousel-left:hover, .carousel-right:hover,input[type=button],
//             .woocommerce div.product form.cart .button,.woocommerce #respond input#submit,.woocommerce a.button, .woocommerce button.button,
//             .woocommerce input.button,.widget.widget_search #searchform .btn-search,#tribe-events .tribe-events-button,
//             #tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type=submit], .tribe-events-button,
//             .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover,
//             .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],
//             .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a,.woocommerce a.button.alt,.woocommerce input.button.alt,
//             .woocommerce .woocommerce-message,.news-img:hover .news-date:before,.featured-wrap:hover .news-date:before,.single-social-info.user,.single-social-info:hover,.single-social-info:focus,.common-menu-wrap .nav>li>ul li:hover > a:before,
//             .common-menu-wrap .nav>li>ul li.active > a:before,.filled-button.user,.filled-button:hover,.filled-button:focus,
//             .coming-soon-form button,.woocommerce .product-thumbnail-outer-inner .addtocart-btn a.button:hover,#resume-carousel .carousel-indicators li.active:before,#resume-carousel .carousel-indicators li:hover:before,.timeline-description:hover:before,.timeline-date:hover:after,.error-page-inner a.btn.btn-primary.btn-lg:hover, .btn.btn-primary:hover,.widget ul li a:before,a.tags.active,a.tags:hover,.woocommerce .site-content .single-product-details .cart .button:hover,.woocommerce #review_form #respond .comment-form .form-submit input:hover,.widget table thead tr th,ul.tribe-events-sub-nav li a{ background-color: #ed1c24; }
//             input:focus, textarea:focus, keygen:focus, select:focus  { border-color: #ed1c24; }a:hover, .widget.widget_rss ul li a{ color: #ed0007; }.error-page-inner a.btn.btn-primary.btn-lg:hover,.btn.btn-primary:hover,input[type=button]:hover,
//             .woocommerce div.product form.cart .button:hover,.woocommerce #respond input#submit:hover,
//             .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
//             .widget.widget_search #searchform .btn-search:hover,.woocommerce a.button.alt:hover,
//             .woocommerce input.button.alt:hover,.btn-block.submit_button:hover,.filled-button:hover,.filled-button:focus{ background-color: #ed0007; }
//             .btn.btn-primary,.woocommerce form .form-row.woocommerce-invalid .select2-container,
// .woocommerce form .form-row.woocommerce-invalid input.input-text,
// .woocommerce form .form-row.woocommerce-invalid select{ border-color: #ed0007; }';
//         }

//                 /* *******************************
//         **********  Quick Style **********
//         ********************************** */

//         $headerbg = politist_options('header-bg');
//         if(isset($headerbg)){
//             $output .= '.header{ background: '.esc_attr(politist_options('header-bg')) .'; }';
//         }

//         if (politist_options('header-fixed')){

//             if(isset($headerbg)){
//                 $output .= '#masthead.sticky{ background-color: rgba(255,255,255,.97); }';
//             }
//             $output .= 'header.sticky{ position:fixed;top:0; z-index:99;margin:0 auto 30px; width:100%;box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.22);}';
//             $output .= 'header.sticky #header-container{ padding:0;transition: padding 200ms linear; -webkit-transition:padding 200ms linear;}';
//             $output .= 'header.sticky .navbar.navbar-default{ background: rgba(255,255,255,.95); border-bottom:1px solid #f5f5f5}';
//         }


//         if( politist_options('header-padding-top') ){
//             $output .= '.site-header{ padding-top: '. (int) esc_attr( politist_options('header-padding-top')) .'px; }';
//         }

//         if(politist_options('header-height')){
//             $output .= '.site-header{ min-height: '. (int) esc_attr(politist_options('header-height')) .'px; }';
//         }

//         if(politist_options('header-padding-bottom')){
//             $output .= '.site-header{ padding-bottom: '. (int) esc_attr( politist_options('header-padding-bottom') ) .'px; }';
//         }

//         if(politist_options('footer-background')){
//             $output .= '.footer-area, .footer-two-area{ background: '.esc_attr(politist_options('footer-background')) .'; }';
//         }

//         if (politist_options('comingsoon-en')) {
//             $output .= "body {
//                 background: #fff;
//                 display: table;
//                 width: 100%;
//                 height: 100%;
//                 min-height: 100%;
//             }";
//         }


//         if ( politist_options('errorbg') ) {
//             $output .= "body.error404,BODY.page-template-404{
//                 width: 100%;
//                 height: 100%;
//                 min-height: 100%;
//                 background-size: cover;
//             }";
//         }

//         $output = '<style>'.$output.'</style>';

//         echo $output;
//     }
// }
// add_action('wp_head', 'politist_css_generator');
