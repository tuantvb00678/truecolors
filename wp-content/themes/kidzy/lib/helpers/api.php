<?php
  
  new KID_API();
  
  class KID_API {
    /**
     * Constructor: Filters and Actions.
     */
    public function __construct() {
      $this->domain = 'kid';
      function_exists( 'register_rest_route' ) && add_action('rest_api_init', function(){
        register_rest_route( "$this->domain/v1", '/handle-like', array(
          'methods' => array('GET'),
          'callback' => 'KID_API::ajax_handle_like'
        ));
      });
    }
    
    /**
     * @param $request
     */
    public static function ajax_handle_like($request) {
      $html = 'tesss';
  
      echo "<pre>"; print_r($html);
      echo "<pre>"; print_r($request); die;
      $params = $request->get_params();
      $like_numbers = intval(get_post_meta( 1584, 'themeum__post_like_number',true ));
      
      if ($like_numbers >= 0) {
        $counter = $like_numbers + 1;
        update_post_meta( 1584, 'themeum__post_like_number',$counter );
      }
      
      self::http_response( 200, array(
          'html' => get_post_meta( 1584, 'themeum__post_like_number',true ),
        )
      );
    }
    
    
    public static function http_response( $code = 400, $payload = "" ){
      switch ( $code ){
        case 400:
          $res = array(
            'is_valid' => false,
            'message' => '400 error: The request could not be understood by the server.'
          );
          break;
        case 405:
          $res = array(
            'is_valid' => false,
            'message' => '405 error: The request method is not allowed.'
          );
          break;
        case 200:
          $res = array(
            'is_valid' => true,
            'message' => 'Success!',
          );
          break;
      }
      
      $_die_string = empty( $payload ) ? json_encode($res) : json_encode($payload);
      
      die( $_die_string );
    }
  }
  
  add_action('wp_ajax_nopriv_handle_like', 'ajax_handle_like');
  add_action('wp_ajax_handle_like', 'ajax_handle_like');
  function ajax_handle_like() {
    $json = array();
    
    $post_id = (int) $_GET['post_id'];
    if (!empty($post_id)) {
      $like_numbers = intval(get_post_meta($post_id, 'themeum__post_like_number', true));
  
      if ($like_numbers >= 0) {
        $counter = $like_numbers + 1;
        update_post_meta($post_id, 'themeum__post_like_number', $counter);
        $json['like'] = $counter;
      }
      $json['success'] = "300";
    } else {
      $json['error'] = "201";
    }
    echo json_encode($json); exit();
  }
  
  function ajax_enqueue() {
    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/assets/js/ajax-script.js', array('jquery') );
    wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  }
  add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );
