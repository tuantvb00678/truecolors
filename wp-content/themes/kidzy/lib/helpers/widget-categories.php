<?php
  
  /**
   * Creating the widget
   * Class widget_categories
   */
  class widget_categories extends WP_Widget {
    
    function __construct() {
      parent::__construct(
        'widget_categories', // Base ID of your widget
        __('True Colors Chuyên Mục', 'widget_categories_domain'), // Widget name will appear in UI
        array( 'description' => __( 'True Colors Chuyên Mục', 'widget_categories_domain' ), ) // Widget description
      );
    }
  
    /**
     * Creating widget front-end
     * @param array $args
     * @param array $instance
     * @return false|string|void
     */
    public function widget( $args, $instance ) {
      $title = apply_filters( 'widget_title', $instance['title'] );
      $number = $instance['number'];
  
      // before and after widget arguments are defined by themes
      echo $args['before_widget'];
      if ( ! empty( $title ) )
        echo $args['before_title'] . $title . $args['after_title'];
  
      $terms = get_terms([
        'taxonomy' => 'category',
        'hide_empty' => false,
      ]);
  
      // This is where you run the code and display the output
      ob_start();
      ?>
      <?php if (!empty($terms) || !empty($title) || !empty($number)) : ?>
        <div class="widget-categories">
          <?php foreach ($terms as $index => $term) : ?>
            <?php if ($term->slug != 'uncategorized' && $index <= $number): ?>
              <?php
                $tax_link = get_category_link($term->term_id);
                $title = $term->name;
                $count = $term->count;
                $image_id = get_term_meta( $term->term_id, 'category-image-id', true );
                $image = wp_get_attachment_image_src($image_id);
              ?>
              <div class="widget-categories__item">
                <div class="widget-categories__item-inner">
                  <a href="<?php echo $tax_link; ?>" class="widget-categories__item-link relative block white">
                    <?php if (!empty($image)) : ?>
                      <div class="widget-categories__item-image">
                        <figure class="widget-categories__item-image-wrapper" style="background-image: url(<?php echo $image[0]; ?>);">
                          <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $title ); ?>"></span>
                        </figure>
                      </div>
                    <?php endif; ?>
                    <div class="widget-categories__item-content">
                      <h4 class="widget-categories__item-title uppercase white"><?php echo $title; ?></h4>
                      <div class="widget-categories__item-count uppercase white"><?php echo $count; ?> POST<?php echo ($count > 1) ? '(s)' : ''; ?></div>
                    </div>
                  </a>
                </div>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
      <?php
      echo ob_get_clean();
  
      echo $args['after_widget'];
    }
    
    /**
     *  Widget Backend
     * @param array $instance
     * @return string|void
     */
    public function form( $instance ) {
      if ( !empty( $instance['title'] ) ) {
        $title = $instance['title'];
      }
      else {
        $title = __( 'Tiêu Đề', 'widget_categories_domain' );
      }
      if ( !empty( $instance['number'] ) ) {
        $number = $instance['number'];
      }
      else {
        $number = 5;
      }
      ?>
      <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Tiêu Đề:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>

      <p>
        <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Số bài viết hiển thị:' ); ?></label>
        <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo esc_attr( $number ); ?>" size="3">
      </p>
      <?php
    }
    
    /**
     * Updating widget replacing old instances with new
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags($new_instance['number']) : '';
      return $instance;
    }
  } // Class widget_categories ends here
  
  
  /**
   * Register and load the widget
   */
  function load_widget_categories() {
    register_widget( 'widget_categories' );
  }
  add_action( 'widgets_init', 'load_widget_categories' );
