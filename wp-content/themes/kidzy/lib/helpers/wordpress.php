<?php

/**
 * Theme PATH
 */
if (!defined('THEME_DIR')) {
	/**
	 * @internal TEMPLATEPATH is the WordPress-defined constant for the
	 * theme's directory. should we be using this?
	 */
	define( 'THEME_DIR', get_template_directory() );
}
/**
 * Theme URI
 */
if (!defined('THEME_URI')) {
	define( 'THEME_URI', get_template_directory_uri() );
}

/**
 * Components PATH
 */
if (!defined('COMPONENTS_DIR')) {
	define('COMPONENTS_DIR',THEME_DIR .'/components');
}

/**
 * Uploads PATH and URI
 */
$upload_dir = wp_upload_dir();
if ( !defined('UPLOADS_DIR')) {
	define( 'UPLOADS_DIR', $upload_dir['basedir'] );
}

if ( !defined('UPLOADS_URI')){
	define( 'UPLOADS_URI', $upload_dir['baseurl'] );
}

if ( ! function_exists( '_wp_render_title_tag' ) ) :
	/**
	 * Wordpress 4.4 deprecates wp_title(), but this may change.
	 * @link https://make.wordpress.org/core/2015/10/20/document-title-in-4-4/
	 */
	function theme_slug_render_title() {
		echo '<title>';
		wp_title( '|', true, 'right' );
		echo '</title>';
	}
	add_action( 'wp_head', 'theme_slug_render_title' );
endif;

/**
 * Get page by page template
 */
function get_page_by_template($path) {
  if(empty($path)) {
    return false;
  }
  $args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => $path
  ];
  $pages = get_posts($args);
  return $pages[0];
}

add_filter( 'login_url', 'custom_login_url', 10, 2 );
function custom_login_url( $login_url, $redirect ) {
  $login_url = add_query_arg(array(
    'login' => true,
    'redirect_to' => urlencode( $redirect )
  ), home_url());
  return $login_url;
}

/**
 * Searches the database for transients stored there that match a specific prefix.
 *
 * @param string $prefix prefix to search for.
 * @return array          nested array response for wpdb->get_results.
 */
function search_database_for_transients_by_prefix($prefix) {

  global $wpdb;

  // Add our prefix after concating our prefix with the _transient prefix
  $prefix = $wpdb->esc_like('_transient_' . $prefix . '_');

  // Build up our SQL query
  $sql = "SELECT `option_name` FROM $wpdb->options WHERE `option_name` LIKE '%s'";

  // Execute our query
  $transients = $wpdb->get_results($wpdb->prepare($sql, $prefix . '%'), ARRAY_A);

  // If if looks good, pass it back
  if ($transients && !is_wp_error($transients)) {
    return $transients;
  }

  // Otherwise return false
  return false;
}

/**
 * Expects a passed in multidimensional array of transient keys
 *  array(
 *    array( 'option_name' => '_transient_blah_blah' ),
 *    array( 'option_name' => 'transient_another_one' ),
 * Can also pass in an array of transient names
 *
 * @param array|string $transients Nested array of transients, keyed by option_name,
 *                                   or array of names of transients.j
 * @return array                     Count of total vs deleted.
 */
function delete_transients_from_keys($transients) {

  if (!isset($transients)) {
    return false;
  }

  // If we get a string key passed in, might as well use it correctly
  if (is_string($transients)) {
    $transients = array(array('option_name' => $transients));
  }

  // If its not an array, we can't do anything
  if (!is_array($transients)) {
    return false;
  }

  $results = array();

  // Loop through our transients
  foreach ($transients as $transient) {

    if (is_array($transient)) {

      // If we have an array, grab the first element
      $transient = current($transient);
    }

    // Remove that sucker
    $results[$transient] = delete_transient(str_replace('_transient_', '', $transient));
  }

  // Return an array of total number, and number deleted
  return array(
    'total' => count($results),
    'deleted' => array_sum($results),
  );
}

function kidzy_excerpt_max_charlength_post_id($char_length, $post_id) {
  $excerpt = get_the_excerpt($post_id);
  $char_length++;
  
  if ( mb_strlen( $excerpt ) > $char_length ) {
    $subex = mb_substr( $excerpt, 0, $char_length - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      return mb_substr( $subex, 0, $excut );
    } else {
      return $subex;
    }
    
  } else {
    return $excerpt;
  }
}
