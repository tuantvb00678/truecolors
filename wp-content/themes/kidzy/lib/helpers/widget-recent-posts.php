<?php
  
  /**
   * Creating the widget
   * Class widget_recent_posts
   */
  class widget_recent_posts extends WP_Widget {
    
    function __construct() {
      parent::__construct(
        'widget_recent_posts', // Base ID of your widget
        __('True Colors Bài Viết Mới', 'widget_recent_posts_domain'), // Widget name will appear in UI
        array( 'description' => __( 'True Colors Bài Viết Mới', 'widget_recent_posts_domain' ), ) // Widget description
      );
    }
  
    /**
     * Creating widget front-end
     * @param array $args
     * @param array $instance
     * @return false|string|void
     */
    public function widget( $args, $instance ) {
      $title = apply_filters( 'widget_title', $instance['title'] );
      $number = apply_filters( 'widget_number', $instance['number'] );

      // before and after widget arguments are defined by themes
      echo $args['before_widget'];
      if ( ! empty( $title ) )
        echo $args['before_title'] . $title . $args['after_title'];
  
      $args = array( 'numberposts' => $number );
      $recent_posts = wp_get_recent_posts( $args );
      
      // This is where you run the code and display the output
      ob_start();
      ?>
      <?php if (!empty($title) || !empty($number)) : ?>
        <div class="widget-recent-posts">
          <?php if (!empty($recent_posts)) : ?>
            <?php foreach ($recent_posts as $recent_post) : ?>
              <?php
                $image = get_the_post_thumbnail_url($recent_post['ID']);
                $post_title = $recent_post['post_title'];
              ?>
              <?php if (!empty($image)): ?>
                <div class="widget-recent-posts__item">
                  <div class="widget-recent-posts__item-inner">
                    <a href="<?php echo get_permalink($recent_post['ID']); ?>" class="widget-recent-posts__item-link relative block white">
                      <?php if (!empty($image)) : ?>
                        <div class="widget-recent-posts__item-image">
                          <figure class="widget-recent-posts__item-image-wrapper" style="background-image: url(<?php echo $image; ?>);">
                            <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $post_title ); ?>"></span>
                          </figure>
                        </div>
                      <?php endif; ?>
                    </a>
  
                    <div class="widget-recent-posts__item-content">
                      <h4 class="widget-recent-posts__item-title black">
                        <a href="<?php echo get_permalink($recent_post['ID']); ?>" class="black">
                          <?php echo $post_title; ?></h4>
                        </a>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php
      echo ob_get_clean();
    }
    
    /**
     *  Widget Backend
     * @param array $instance
     * @return string|void
     */
    public function form( $instance ) {
      if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
      }
      else {
        $title = __( 'Tiêu Đề', 'widget_recent_posts_domain' );
      }
      if ( isset( $instance[ 'number' ] ) ) {
        $number = $instance[ 'number' ];
      }
      else {
        $number = 5;
      }
      ?>
      <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Tiêu Đề:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>
  
      <p>
        <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Số bài viết hiển thị:' ); ?></label>
        <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3">
      </p>
      <?php
    }
    
    /**
     * Updating widget replacing old instances with new
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
      $instance = array();
      $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
      $instance['number'] = ( ! empty( $new_instance['number'] ) ) ? $new_instance['number'] : 5;
      return $instance;
    }
  } // Class widget_recent_posts ends here
  
  
  /**
   * Register and load the widget
   */
  function load_widget_recent_posts() {
    register_widget( 'widget_recent_posts' );
  }
  add_action( 'widgets_init', 'load_widget_recent_posts' );
