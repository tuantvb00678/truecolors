<?php

add_action('widgets_init','register_themeum_popular_post_widget');

function register_themeum_popular_post_widget()
{
	register_widget('themeum_popular_post_widget');
}

class themeum_popular_post_widget extends WP_Widget{

	public function __construct() {
		$widget_ops = array( 
			'classname' => 'themeum_popular_post_widget',
			'description' => 'Themeum post widget to display Popular Post',
		);
		parent::__construct( 'themeum_popular_post_widget', 'Themeum Popular Post', $widget_ops );
	}	


	function widget($args, $instance)
	{
		extract($args);

		$title 	= apply_filters('widget_title', $instance['title'] );
		$count 	= esc_attr($instance['count']);
		global $themeum_options; 

		$popular_post = new WP_Query( 
			array(
				'post_type' 	=> 'post', 
				'posts_per_page'=> $count, 
				'meta_key' 		=> '_post_views_count' ,
				'orderby' 		=> 'meta_value_num', 
				'order' 		=> 'DESC'  
			) );


		echo $before_widget;
       	if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		// The Loop
		if ( $popular_post->have_posts() ) {
			echo '<div>';
			while ( $popular_post->have_posts() ) {
				$popular_post->the_post();

				//$image = wp_get_attachment_image_src( get_post_thumbnail_id( $popular_post->ID ), 'kidzy-sidebar-thumbnails' );

				echo '<div class="widget-post media">';
                    echo '<div class="widget-img pull-left">';
                        echo '<a href="'.get_permalink().'">'.get_the_post_thumbnail( get_the_ID(), 'kidzy-sidebar-thumbnails', array('class' => 'img-responsive')).'</a>';;
                    echo '</div>';
                    echo '<div class=" media-body wid-post-title">';
                        echo '<h6><a href="'.get_the_permalink().'">'.get_the_title().'</a></h6>';
                    echo '</div>';
                    echo '<div class="clearfix"></div>';
				echo '</div>';
			}
			echo '</div>';
		} else {
			echo 'No post found';
		}

		echo $after_widget;

		/* Restore original Post Data */
		wp_reset_postdata();
		
	}


	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		$instance['title'] 	= strip_tags( $new_instance['title'] );
		$instance['count'] 	= strip_tags( $new_instance['count'] );

		return $instance;
	}


	function form($instance)
	{
		$defaults = array( 
			'title' 	=> 'Popular Post',
			'count' 	=> 5
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Widget Title', 'kidzy'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'count' )); ?>"><?php _e('Count', 'kidzy'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" value="<?php echo esc_attr($instance['count']); ?>" style="width:100%;" />
		</p>

	<?php
	}
}