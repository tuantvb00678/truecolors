<?php

add_action('widgets_init','kidzy_register_themeum_class_list_widget');

function kidzy_register_themeum_class_list_widget()
{
	register_widget('themeum_class_list_widget');
}

class themeum_class_list_widget extends WP_Widget{

	public function __construct() {
		$widget_ops = array( 
			'classname' => 'themeum_class_list_widget',
			'description' => 'Kidzy class list to display list of classes',
		);
		parent::__construct( 'themeum_class_list_widget', 'Kidzy Class List', $widget_ops );
	}	


	function widget($args, $instance)
	{
		extract($args);

		$title 	= apply_filters('widget_title', $instance['title'] );
		$count 	= esc_attr($instance['count']);

		$class_list = new WP_Query( 
			array(
				'post_type' 	=> 'class', 
				'posts_per_page'=> $count, 
				'orderby' 		=> 'meta_value_num', 
				'order' 		=> 'DESC'  
			) );


		echo $before_widget;
       	if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		// The Loop
		if ( $class_list->have_posts() ) {
			echo '<ul>';
			while ( $class_list->have_posts() ) {
				$class_list->the_post();

				echo '<li>';
    				echo '<a href="'.get_the_permalink().'">'. get_the_title() .'</a>';
				echo '</li>';

			}
			echo '</ul>';
		} else {
			echo 'No Class Found';
		}

		echo $after_widget;

		/* Restore original Post Data */
		wp_reset_postdata();
		
	}


	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		$instance['title'] 	= strip_tags( $new_instance['title'] );
		$instance['count'] 	= strip_tags( $new_instance['count'] );

		return $instance;
	}


	function form($instance)
	{
		$defaults = array( 
			'title' 	=> 'Popular Post',
			'count' 	=> 5
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Widget Title', 'kidzy'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'count' )); ?>"><?php _e('Count', 'kidzy'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" value="<?php echo esc_attr($instance['count']); ?>" style="width:100%;" />
		</p>

	<?php
	}
}
