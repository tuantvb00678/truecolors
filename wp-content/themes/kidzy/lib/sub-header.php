<?php
  $output = '';
  $sub_img = array();
  global $post;
  if(!function_exists('kidzy_call_sub_header')){
    function kidzy_call_sub_header(){
      if( kidzy_options_url('blog-banner','url')){
        $output = 'style="background:'.esc_attr(kidzy_options('blog-subtitle-bg-color')).' url('.esc_url( kidzy_options_url('blog-banner','url')).');background-size: cover;background-position: 50% 50%;"';
        return $output;
      }else{
        if( kidzy_options('blog-subtitle-bg-color') ){
          $output = 'style="background-color:'.esc_attr(kidzy_options('blog-subtitle-bg-color')).'; color: red;"';
          return $output;
        }else{
          $output = 'style="background-color:#151515;"';
          return $output;
        }
      }
    }
  }
  
  if( isset($post->post_name) ){
    if(!empty($post->ID)){
      if(function_exists('rwmb_meta')){
        
        $image_attached = esc_attr(get_post_meta( $post->ID , 'themeum_subtitle_images', true ));
        
        if(!empty($image_attached)){
          $sub_img = wp_get_attachment_image_src( $image_attached , 'blog-full');
          $output = 'style="background:'.esc_attr(rwmb_meta("themeum_subtitle_color")).' url('.esc_url($sub_img[0]).');background-size: cover;background-position: 50% 50%;"';
          if(empty($sub_img[0])){
            $output = 'style="background-color:'.esc_attr(rwmb_meta("themeum_subtitle_color")).';"';
            if(rwmb_meta("themeum_subtitle_color") == ''){
              $output = kidzy_call_sub_header();
            }
          }
        }else{
          if(rwmb_meta("themeum_subtitle_color") != "" ){
            $output = 'style="background-color:'.esc_attr(rwmb_meta("themeum_subtitle_color")).';"';
          }else{
            $output = kidzy_call_sub_header();
          }
        }
        
      }else{
        $output = kidzy_call_sub_header();
      }
    }else{
      $output = kidzy_call_sub_header();
    }
  }else{
    $output = kidzy_call_sub_header();
  }

?>
<?php if (!is_front_page()) { ?>

  <section class="breadcrumb-section" <?php echo $output;?>>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-xs-12 text-center">
          <div class="breadcrumb-content">
            
            <?php
              global $wp_query;
              
              if(isset($wp_query->queried_object->name)){
                if($wp_query->queried_object->name != ''){
                  if($wp_query->queried_object->name == 'product' ){
                    echo '<h2>'.esc_html__('Shop','kidzy').'</h2>';
                  } elseif($wp_query->queried_object->name == 'tribe_events' )  {
                    echo '<h2>'.esc_html__('Events','kidzy').'</h2>';
                  }
                  else{
                    echo '<h2 class="test">'.$wp_query->queried_object->name.'</h2>';
                  }
                }else{
                  echo '<h2>'.get_the_title().'</h2>';
                }
              }else{
                if( is_search() ){
                  echo '<h2>'.esc_html__('Search','kidzy').'</h2>';
                }
                else if( is_home() ){
                  echo '<h2>'.esc_html__('Blog','kidzy').'</h2>';
                }
                else if(is_archive()){
                  echo '<h2>'.esc_html__('Archive','kidzy').'</h2>';
                }
                else if(is_singular('event')){
                  echo '<h2>'.esc_html__('Event Details','kidzy').'</h2>';
                }
                else if(is_singular('class')){
                  echo '<h2>'.esc_html__('Class Details','kidzy').'</h2>';
                }
                else if(is_singular('teacher')){
                  echo '<h2>'.esc_html__('Teacher Details','kidzy').'</h2>';
                }
                
                else if( is_single('blog')){
                  echo '<h2>'.esc_html__('Blog Details','kidzy').'</h2>';
                }
                
                else if( is_single('shop') ){
                  echo '<h2>'.esc_html__('Shop Details','kidzy').'</h2>';
                }
                else if ($wp_query->queried_object->post_type == 'post') {
                  echo '<h2>'.$wp_query->queried_object->post_title.'</h2>';
                }
                else{
                  // echo "<pre>"; print_r($post); die;
                  echo '<h2>'.get_the_title().'</h2>';
                }
              }
            ?>
            <?php kidzy_breadcrumbs(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php } ?>



