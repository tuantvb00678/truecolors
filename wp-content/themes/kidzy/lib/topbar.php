<div class="topbar">
    <div class="container">
      <div class="row">

        <?php if ( kidzy_options('topbar-social') ) { ?>
          <div class="col-xs-6"> 
            <?php get_template_part('lib/social-icon'); ?>
          </div>
        <?php } ?>

        <div class="col-xs-6"> 
            <div class="menu-social text-right">
              <?php if ( kidzy_options('topbar-login') ) { ?>
                  <div class="top-align home-login">
                      <a href="#sign-in" data-toggle="modal" data-target="#sign-in"><i class="fa fa-user"></i></a>
                  </div> 
              <?php } ?>   
              <?php if ( kidzy_options('topbar-cart') ) {             
                  global $woocommerce;
                  if($woocommerce) { ?>               
                    <div class="top-align home-cart woocart">
                      <a href="#" class="btn-cart"></a>
                              <span id="themeum-woo-cart" class="woo-cart" style="display:none;">
                               <i class="fa fa-shopping-basket"></i> 
                                      <?php
                                          $has_products = '';
                                          $has_products = 'cart-has-products';
                                      ?>
                                     (<span class="woo-cart-items"><span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span></span>)
                                  <?php the_widget( 'WC_Widget_Cart', 'title= ' ); ?>
                              </span>
                          
                    </div>
                  <?php } 
               } ?> 
              <?php if ( kidzy_options('topbar-search') ) { ?>   
                  <span class="top-align home-search-btn">
                    <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                  </span>
              <?php } ?> 
            </div>
        </div>
      </div><!--/.row--> 
    </div><!--/.container--> 
</div>