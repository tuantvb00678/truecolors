=== kidzy ===
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Kidzy is beautiful and feature rich theme for kindergarten or elementary school sites using WordPress. Kidzy features an adorable menu design with dynamic colors and looks great on every device. It can adapt to any screen size. There is Visual Composer drag-and-drop page builder in the theme package with three homepage variations which can present your institution exactly as you desire. The theme comes with Slider Revolution plugin and 3 built-in slider variations as well.