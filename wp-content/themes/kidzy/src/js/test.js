function detectIE() {
  var t = window.navigator.userAgent,
    e = t.indexOf("MSIE ");
  if (e > 0) return parseInt(t.substring(e + 5, t.indexOf(".", e)), 10);
  var i = t.indexOf("Trident/");
  if (i > 0) {
    var o = t.indexOf("rv:");
    return parseInt(t.substring(o + 3, t.indexOf(".", o)), 10);
  }
  var n = t.indexOf("Edge/");
  return n > 0 && parseInt(t.substring(n + 5, t.indexOf(".", n)), 10);
}
function scrollHandler() {
  var t = jQuery(".parallax .secondary"),
    e = 0.08;
  if ((jQuery(window).width() < 640 && (e = 0.13), t.length > 0))
    for (var i = 0; i < t.length; i++) {
      var o = t[i],
        n = jQuery(o).offset().top,
        s = jQuery(window).scrollTop(),
        r = window.innerHeight,
        a = n - s,
        l = r - a;
      jQuery(o).css("transform", "translateY(" + l * e + "%)");
    }
}
function howWeRollScrollHandler() {
  var t = jQuery("#how-we-roll-5 .grid-left .image"),
    e = -0.03;
  if (t.length > 0)
    for (var i = 0; i < t.length; i++) {
      var o = t[i],
        n = jQuery(o).offset().top,
        s = jQuery(window).scrollTop(),
        r = window.innerHeight,
        a = n - s,
        l = r - a;
      jQuery(o).css("transform", "translateY(" + l * e + "%)");
    }
}
function viewportRatio() {
  var t = jQuery(window).width() / jQuery(window).height();
  t > 2.2
    ? (jQuery("body").removeClass("viewport-tall"), jQuery("body").addClass("viewport-wide"))
    : t < 0.6
    ? (jQuery("body").removeClass("viewport-wide"), jQuery("body").addClass("viewport-tall"))
    : jQuery("body").removeClass("viewport-wide viewport-tall");
}
jQuery(document).ready(function (t) {
  "use strict";
  var e,
    i = 0,
    o = 5;
  t("#sidebar-1").length > 0 &&
  t(window).scroll(function () {
    var n = t(this).scrollTop();
    if (!(Math.abs(i - n) <= o || t(window).width() <= 1024)) {
      if (n > i)
        t("#sidebar-1").hasClass("fixed-top")
          ? (t("#sidebar-1, #sidebar-2, #sidebar-3").removeClass("fixed-top").removeAttr("style"),
            (e = n - t("#sidebar-1").offset().top),
            t("#sidebar-1").css("transform", "translateY(" + e + "px)"),
            t("#sidebar-2").css("transform", "translateY(" + e + "px)"),
            t("#sidebar-3").css("transform", "translateY(" + e + "px)"))
          : t("#sidebar-3").offset().top + t("#sidebar-3").height() < n + window.innerHeight &&
          (t("#sidebar-1, #sidebar-2, #sidebar-3").addClass("fixed-bottom"),
            t("#sidebar-1").css("bottom", t("#sidebar-2").outerHeight() + t("#sidebar-3").outerHeight()),
            t("#sidebar-2").css("bottom", t("#sidebar-3").outerHeight()),
            t("#sidebar-3").css("bottom", 0),
            t("#sidebar-1").css("transform", "translateY(0)"),
            t("#sidebar-2").css("transform", "translateY(0)"),
            t("#sidebar-3").css("transform", "translateY(0)"));
      else if (t("#sidebar-3").hasClass("fixed-bottom")) {
        t("#sidebar-1, #sidebar-2, #sidebar-3").removeClass("fixed-bottom").removeAttr("style");
        var s = t("#sidebar-1").outerHeight() + t("#sidebar-2").outerHeight() + t("#sidebar-3").outerHeight();
        (e = n + window.innerHeight - s - t("#latest-episode").outerHeight()),
          t("#sidebar-1").css("transform", "translateY(" + e + "px)"),
          t("#sidebar-2").css("transform", "translateY(" + e + "px)"),
          t("#sidebar-3").css("transform", "translateY(" + e + "px)");
      } else
        t("#primary").offset().top > n
          ? t("#sidebar-1, #sidebar-2, #sidebar-3").removeClass("fixed-top").removeAttr("style")
          : t("#sidebar-1").offset().top > n &&
          (t("#sidebar-1, #sidebar-2, #sidebar-3").addClass("fixed-top"),
            t("#sidebar-1").css("top", 0),
            t("#sidebar-2").css("top", t("#sidebar-1").outerHeight()),
            t("#sidebar-3").css("top", t("#sidebar-1").outerHeight() + t("#sidebar-2").outerHeight()),
            t("#sidebar-1").css("transform", "translateY(0)"),
            t("#sidebar-2").css("transform", "translateY(0)"),
            t("#sidebar-3").css("transform", "translateY(0)"));
      i = n;
    }
  });
  var n = t("article.post").eq(3).attr("id");
  if (void 0 !== n) {
    new Waypoint({
      element: document.getElementById(n),
      handler: function (e) {
        "down" == e ? t(".back-to-top").addClass("active") : t(".back-to-top").removeClass("active");
      },
      offset: "98%",
    });
  }
}),
  jQuery(document).ready(function (t) {
    t(".bctt-ctt-btn").html("Tweet This"),
      t(".bctt-click-to-tweet").prepend("<img src='/wp-content/themes/marieforleo/assets/images/social/twitter-black.svg'/><span class='topline'></span>"),
      t(".bctt-click-to-tweet a.bctt-ctt-btn").before("<span class='via'>via @marieforleo</span>"),
      t(".js-accordion-trigger").bind("click", function (t) {
        jQuery(this).parent().find(".accordion-inner").slideToggle("fast"), jQuery(this).parent().toggleClass("is-expanded"), t.preventDefault();
      }),
      t(".post").fitVids();
  }),
  (function (t) {
    "use strict";
    t.fn.fitVids = function (e) {
      var i = {
        customSelector: null,
        ignore: null,
      };
      if (!document.getElementById("fit-vids-style")) {
        var o = document.head || document.getElementsByTagName("head")[0],
          n =
            ".fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}",
          s = document.createElement("div");
        (s.innerHTML = '<p>x</p><style id="fit-vids-style">' + n + "</style>"), o.appendChild(s.childNodes[1]);
      }
      return (
        e && t.extend(i, e),
          this.each(function () {
            var e = ['iframe[src*="player.vimeo.com"]', 'iframe[src*="youtube.com"]', 'iframe[src*="youtube-nocookie.com"]', 'iframe[src*="kickstarter.com"][src*="video.html"]', "object", "embed"];
            i.customSelector && e.push(i.customSelector);
            var o = ".fitvidsignore";
            i.ignore && (o = o + ", " + i.ignore);
            var n = t(this).find(e.join(","));
            (n = n.not("object object")),
              (n = n.not(o)),
              n.each(function (e) {
                var i = t(this);
                if (!(i.parents(o).length > 0 || ("embed" === this.tagName.toLowerCase() && i.parent("object").length) || i.parent(".fluid-width-video-wrapper").length)) {
                  i.css("height") || i.css("width") || (!isNaN(i.attr("height")) && !isNaN(i.attr("width"))) || (i.attr("height", 9), i.attr("width", 16));
                  var n = "object" === this.tagName.toLowerCase() || (i.attr("height") && !isNaN(parseInt(i.attr("height"), 10))) ? parseInt(i.attr("height"), 10) : i.height(),
                    s = isNaN(parseInt(i.attr("width"), 10)) ? i.width() : parseInt(i.attr("width"), 10),
                    r = n / s;
                  if (!i.attr("id")) {
                    var a = "fitvid" + e;
                    i.attr("id", a);
                  }
                  i
                  .wrap('<div class="fluid-width-video-wrapper"></div>')
                  .parent(".fluid-width-video-wrapper")
                  .css("padding-top", 100 * r + "%"),
                    i.removeAttr("height").removeAttr("width");
                }
              });
          })
      );
    };
  })(window.jQuery),
  jQuery(document).ready(function () {
    function t(t) {
      var e = Date.parse(t) - Date.parse(new Date()),
        i = Math.floor((e / 1e3) % 60),
        o = Math.floor((e / 1e3 / 60) % 60),
        n = Math.floor((e / 36e5) % 24),
        s = Math.floor(e / 864e5);
      return {
        total: e,
        days: s,
        hours: n,
        minutes: o,
        seconds: i,
      };
    }
    function e(e, i) {
      function o() {
        var o = t(i),
          d = o.days < 10 ? "0" + o.days : o.days;
        e.data("redirect_url") && o.total <= 0 && (window.location.href = e.data("redirect_url")),
          n.html(d),
          s.html(("0" + o.hours).slice(-2)),
          r.html(("0" + o.minutes).slice(-2)),
          a.html(("0" + o.seconds).slice(-2)),
        o.total <= 0 && clearInterval(l);
      }
      if (Date.parse(i) - Date.parse(new Date()) <= 0) return !1;
      var n = e.find(".days"),
        s = e.find(".hours"),
        r = e.find(".minutes"),
        a = e.find(".seconds");
      o();
      var l = setInterval(o, 1e3);
    }
    var i = jQuery(".js-countdown");
    i.length && e(i, i.data("deadline"));
  });
var IEversion = detectIE();
IEversion !== !1 && (document.getElementsByTagName("body")[0].className += " ie ie-" + IEversion),
  jQuery(document).ready(function (t) {
    function e(e) {
      var i;
      (i = e ? e : t("body")),
      s.utm_source && i.find('input[name="utm_source"]').val(s.utm_source),
      s.utm_medium && i.find('input[name="utm_medium"]').val(s.utm_medium),
      s.utm_campaign && i.find('input[name="utm_campaign"]').val(s.utm_campaign),
      s.utm_content && i.find('input[name="utm_content"]').val(s.utm_content);
    }
    function i() {
      a && (a.find(".state-one").fadeOut(800).removeClass("is-active is-visible"), a.find(".state-three").fadeIn(800).css("display", "block"));
    }
    function o() {
      a && (a.find(".state-one").fadeOut(800).removeClass("is-active is-visible"), a.find(".state-two").fadeIn(800).css("display", "block"));
    }
    if (!t("body").hasClass("page-template-page-quiz-page")) {
      var n = window.location.search.replace("?", "").split(/\&/),
        s = {},
        r = null,
        a = t(".eif-1");
      n &&
      n.length &&
      n.forEach(function (t) {
        var e = t.split("=");
        s[e[0]] = e[1];
      }),
      r && t(".terms").remove(),
        a.find('input[name="firstname"]').on("keyup", function () {
          a.find(".js-firstname").text(this.value);
        }),
        t.ajax({
          type: "GET",
          url: "/wp-json/mfi/v1/european",
          success: function (e) {
            e.success &&
            (t('input[name="eea"]').val(e.european).trigger("change"),
              t('input[name="newsletter"]')
              .val(!e.european || null)
              .trigger("change"),
              (r = e.european));
          },
        }),
        e(),
        t(document).on("blur", 'input[type="email"]', function () {
          var e = t(this).parents("form");
          t(this).mailcheck({
            suggested: function (i, o) {
              var n = t('<div class="mailcheck-suggestion">Did you mean <a href="#">' + o.full + "</a>?</div>");
              i.siblings(".mailcheck-suggestion").remove(),
                e.addClass("mailcheck-active"),
                n.find("a").on("click", function (t) {
                  t.preventDefault(), i.val(o.full), n.remove(), e.removeClass("mailcheck-active");
                });
            },
            empty: function (t) {
              t.siblings(".mailcheck-suggestion").remove(), e.removeClass("mailcheck-active");
            },
          });
        }),
        a.find(".js-trigger-newsletter").on("click", function () {
          var e = t(this),
            i = a.find("form");
          e.attr("disabled", !0),
            i.find('input[name="newsletter"]').val(!0).trigger("change"),
            t.ajax({
              type: "POST",
              url: i.attr("action"),
              data: i.serialize(),
              success: function (t) {
                t.success &&
                (a.find(".state-two").fadeOut(800).removeClass("is-active is-visible"),
                  a.find(".state-three").fadeIn(800).css("display", "block"),
                  setTimeout(function () {
                    a.find(".signature").addClass("js-load-active");
                  }, 50)),
                  e.attr("disabled", !1);
              },
              error: function () {
                e.attr("disabled", !1);
              },
            });
        }),
        a.find(".js-trigger-no-newsletter").on("click", function (t) {
          t.preventDefault(),
            a.find(".state-two").fadeOut(500).removeClass("is-active is-visible"),
            a.find(".state-three").fadeIn(500).css("display", "block"),
            setTimeout(function () {
              a.find(".signature").addClass("js-load-active");
            }, 1e3);
        }),
        t(document).on("submit", ".eif-optin-form", function (e) {
          var n = t(this),
            s = n.find('input[type="submit"]');
          e.preventDefault(),
            s.attr("disabled", !0),
            n.siblings(".form-notification").remove(),
            t.ajax({
              type: "POST",
              url: n.attr("action"),
              data: n.serialize(),
              success: function (t) {
                t.success ? (r ? o() : i()) : n.after('<div class="form-notification">' + t.data + "</div>"), s.attr("disabled", !1);
              },
              error: function () {
                s.attr("disabled", !1);
              },
            });
        });
    }
  }),
  jQuery(document).ready(function (t) {
    var e = t(".fade-in-element");
    t(e).addClass("js-fade-element-hide"),
      t(window).scroll(function () {
        if (e.length > 0)
          for (var i = 0; i < e.length; i++) {
            var o = e[i],
              n = t(o).offset().top,
              s = t(window).scrollTop(),
              r = window.innerHeight,
              a = n - s,
              l = r - a,
              d = 100;
            l > d ? t(o).addClass("js-fade-element-show") : l < 0 && (t(o).removeClass("js-fade-element-show"), t(o).addClass("js-fade-element-hide"));
          }
      });
    var i = t(".scroll-active-element");
    t(window).scroll(function () {
      if (i.length > 0)
        for (var e = 0; e < i.length; e++) {
          var o = i[e],
            n = t(o).offset().top,
            s = t(window).scrollTop(),
            r = window.innerHeight,
            a = n - s,
            l = r - a,
            d = 300;
          l > d ? t(o).addClass("js-scroll-active") : l < 0 && t(o).removeClass("js-scroll-active");
        }
    });
    var o = t(".fade-in-elements");
    if (o.length > 0)
      for (var n = 0; n < o.length; n++)
        for (var s = t(o[n]).children(), r = 0; r < s.length; r++) {
          var a = s[r];
          t(a)
          .delay(500 * (r + 1))
          .queue(function () {
            t(this).addClass("js-fade-element-show").clearQueue();
          });
        }
    t(window).scroll(function () {
      t("article#page-giving-back").length > 0 ? window.fadeInChildren(t(".fade-in-element-children"), 200) : window.fadeInChildren(t(".fade-in-element-children"));
    }),
      (window.fadeInChildren = function (e, i) {
        var i = i || 500;
        if (e.length > 0)
          for (var o = 0; o < e.length; o++) {
            var n = e[o],
              s = t(n).offset().top,
              r = t(window).scrollTop(),
              a = window.innerHeight,
              l = s - r,
              d = a - l,
              c = 300,
              u = t(n).children();
            if (d > c)
              for (var p = 0; p < u.length; p++) {
                var h = u[p];
                t(h)
                .delay(i * (p + 1))
                .queue(function () {
                  t(this).addClass("js-fade-element-show").clearQueue();
                });
              }
            else if (d < 0)
              for (var p = 0; p < u.length; p++) {
                var h = u[p];
                t(h).removeClass("js-fade-element-show"), t(h).addClass("js-fade-element-hide");
              }
          }
      });
    var l = t(".load-active-element");
    l.addClass("js-load-active");
  }),
  jQuery(document).ready(function (t) {
    (window.MFFirstTimeHere = {
      _bindEvents: function () {
        t("#first-time-dark-photo-slider").slick({
          arrows: !1,
          autoplay: !0,
          autoplaySpeed: 3e3,
          cssEase: "linear",
          prevArrow: "",
          nextArrow: "",
        }),
          t(".square-dog em").each(function (e) {
            t(".square-dog em.slide-" + e).click(function () {
              t("#first-time-dark-photo-slider").slick("slickGoTo", e);
            });
          });
      },
    }),
      window.MFFirstTimeHere._bindEvents();
  }),
  jQuery(document).ready(function (t) {
    function e(e) {
      var i;
      (i = e ? e : t("body")),
      n.utm_source && i.find('input[name="utm_source"]').val(n.utm_source),
      n.utm_medium && i.find('input[name="utm_medium"]').val(n.utm_medium),
      n.utm_campaign && i.find('input[name="utm_campaign"]').val(n.utm_campaign),
      n.utm_content && i.find('input[name="utm_content"]').val(n.utm_content);
    }
    function i() {
      var e = t("#om-" + s),
        i = e.find("form").data("steps");
      ("f8pwq0rqvcwf3fx4m8mt" !== s && "e5gmuy0lfm6yq68nmgrp" !== s) ||
      (e.find(".state2").removeClass("is-active is-visible"),
        e.find(".state3").addClass("is-active"),
        setTimeout(function () {
          e.find(".state3").addClass("is-visible");
        }, 50)),
      "e5gmuy0lfm6yq68nmgrp" === s && e.find(".mfl-optin2__title").addClass("is-hidden"),
      !i ||
      ("fempzeiwilacqzsl15lk" !== s && "feezcl0hqazmijkgjc1t" !== s && "bubqzftaxss3zmwkpbqn" !== s) ||
      (e.find(".modal-state--1").removeClass("is-active is-visible"),
        e.find(".modal-state--2").addClass("is-active"),
        setTimeout(function () {
          e.find(".modal-state--2").addClass("is-visible");
        }, 50));
    }
    if (!t("body").hasClass("page-template-page-quiz-page")) {
      var o = window.location.search.replace("?", "").split(/\&/),
        n = {},
        s = null,
        r = null;
      o &&
      o.length &&
      o.forEach(function (t) {
        var e = t.split("=");
        n[e[0]] = e[1];
      }),
        t.ajax({
          type: "GET",
          url: "/wp-json/mfi/v1/european",
          success: function (e) {
            e.success &&
            (t('input[name="eea"]').val(e.european).trigger("change"),
              t('input[name="newsletter"]')
              .val(!e.european || null)
              .trigger("change"),
              (r = e.european));
          },
        }),
        e(),
        document.addEventListener("om.Campaign.load", function (i) {
          var o = i.detail.Campaign,
            n = t("#om-" + o.data.id),
            a = n.find('input[name="eea"]'),
            l = n.find('input[name="newsletter"]');
          (s = o.data.id),
            n.find('input[name="type"]').val(o.data.id),
          a.val() || a.val(r),
          l.val() || l.val(!r || null),
            e(n),
          r || ("fempzeiwilacqzsl15lk" !== s && "feezcl0hqazmijkgjc1t" !== s && "bubqzftaxss3zmwkpbqn" !== s) || (n.find(".modal-state--2").remove(), n.find("form").attr("data-steps", !1)),
          ("pr9shnou17u8dxhozvlp" !== s && "fempzeiwilacqzsl15lk" !== s && "f8pwq0rqvcwf3fx4m8mt" !== s) || n.find(".mfl-c-canvas").removeClass("mfl-c-canvas").addClass("mfl");
        }),
        document.addEventListener("om.Campaign.afterShow", function (e) {
          var i = e.detail.Campaign,
            o = t("#om-" + i.data.id);
          "asmdubgfu1vb4og39nwu" === i.id &&
          (o.addClass("is-showing"),
            setTimeout(function () {
              o.addClass("is-visible");
            }, 500)),
          ("f8pwq0rqvcwf3fx4m8mt" !== i.id && "e5gmuy0lfm6yq68nmgrp" !== i.id) ||
          (o.find(".js-trigger-form").on("click", function () {
            t(this).attr("disabled", !0),
              o.find(".state1").removeClass("is-active is-visible"),
              o.find(".state2").addClass("is-active"),
              setTimeout(function () {
                o.find(".state2").addClass("is-visible"), o.addClass("remove-mobile");
              }, 50);
          }),
            o.find('input[name="firstname"]').on("keyup", function () {
              o.find(".js-firstname").text(this.value);
            })),
          "e5gmuy0lfm6yq68nmgrp" === i.id &&
          (o.find(".js-trigger-newsletter").on("click", function () {
            var e = t(this),
              i = o.find("form");
            e.attr("disabled", !0),
              i.find('input[name="newsletter"]').val(!0).trigger("change"),
              t.ajax({
                type: "POST",
                url: i.attr("action"),
                data: i.serialize(),
                success: function (t) {
                  t.success &&
                  (o.find(".state3").removeClass("is-active is-visible"),
                    o.find(".state4--insider").addClass("is-active"),
                    setTimeout(function () {
                      o.find(".state4--insider").addClass("is-visible"), o.find(".mfl-optin2__signature").addClass("js-load-active");
                    }, 50)),
                    e.attr("disabled", !1);
                },
                error: function () {
                  e.attr("disabled", !1);
                },
              });
          }),
            o.find(".js-trigger-no-newsletter").on("click", function (t) {
              t.preventDefault(),
                o.find(".state3").removeClass("is-active is-visible"),
                o.find(".state4--default").addClass("is-active"),
                setTimeout(function () {
                  o.find(".state4--default").addClass("is-visible"), o.find(".mfl-optin2__signature").addClass("js-load-active");
                }, 50);
            })),
          ("fempzeiwilacqzsl15lk" !== i.id && "feezcl0hqazmijkgjc1t" !== i.id && "bubqzftaxss3zmwkpbqn" !== i.id) ||
          o.find(".js-trigger-newsletter").on("click", function () {
            var e = t(this),
              i = o.find("form"),
              n = i.data("redirect");
            e.attr("disabled", !0),
              i.find('input[name="newsletter"]').val(!0).trigger("change"),
              t.ajax({
                type: "POST",
                url: i.attr("action"),
                data: i.serialize(),
                success: function (t) {
                  t.success && n && (window.location.href = n), e.attr("disabled", !1);
                },
                error: function () {
                  e.attr("disabled", !1);
                },
              });
          });
        }),
        t(document).on("blur", 'input[type="email"]', function () {
          var e = t(this).parents("form"),
            i = e.find('input[name="type"]').val();
          t(this).mailcheck({
            suggested: function (o, n) {
              var s = t('<div class="mailcheck-suggestion">Did you mean <a href="#">' + n.full + "</a>?</div>");
              o.siblings(".mailcheck-suggestion").remove(),
                e.addClass("mailcheck-active"),
                s.find("a").on("click", function (t) {
                  t.preventDefault(), o.val(n.full), s.remove(), e.removeClass("mailcheck-active");
                }),
                "fempzeiwilacqzsl15lk" === i || "bubqzftaxss3zmwkpbqn" === i || "feezcl0hqazmijkgjc1t" === i || "pr9shnou17u8dxhozvlp" === i || "asmdubgfu1vb4og39nwu" === i ? o.parent().after(s) : o.after(s);
            },
            empty: function (t) {
              t.siblings(".mailcheck-suggestion").remove(), e.removeClass("mailcheck-active");
            },
          });
        }),
        t(document).on("submit", ".js-optin-form", function (e) {
          var o = t(this),
            n = o.find('input[type="submit"]'),
            s = o.data("redirect"),
            r = o.data("steps");
          e.preventDefault(),
            n.attr("disabled", !0),
            o.siblings(".form-notification").remove(),
            t.ajax({
              type: "POST",
              url: o.attr("action"),
              data: o.serialize(),
              success: function (t) {
                t.success ? (s && !r && (window.location.href = s), i()) : o.after('<div class="form-notification">' + t.data + "</div>"), n.attr("disabled", !1);
              },
              error: function () {
                n.attr("disabled", !1);
              },
            });
        });
    }
  }),
  (function (t, e, i, o) {
    t('.inputfile input[type="file"]').each(function () {
      var e = t(this),
        i = e.parent().parent().children(".gfield_label"),
        o = i.html();
      e.on("change", function (t) {
        var e = "";
        this.files && this.files.length > 1 ? (e = (this.getAttribute("data-multiple-caption") || "").replace("{count}", this.files.length)) : t.target.value && (e = t.target.value.split("\\").pop()), e ? i.html(e) : i.html(o);
      }),
        e
        .on("focus", function () {
          e.addClass("has-focus");
        })
        .on("blur", function () {
          e.removeClass("has-focus");
        });
    }),
    jQuery("#gform_confirmation_wrapper_6, .gform_validation_error").length && (e.location.hash = "#jobsform");
  })(jQuery, window, document),
  jQuery(document).ready(function (t) {
    function e(e, o) {
      var n = new Date().getTime();
      if (n - i < 1500) return void e.preventDefault();
      var s = t(".slider-get-started").slick("slickCurrentSlide"),
        r = 1;
      o < 0
        ? 0 == s && o < -r
        ? t(".slider-get-started").slick("slickNext")
        : 1 == s && t(".slider-get-started").removeClass("inView")
        : 1 == s && o > r
        ? t(".slider-get-started").slick("slickPrev")
        : 0 == s && t(".slider-get-started").removeClass("inView"),
        (i = n);
    }
    if (
      ((window.MFFrontPage = {
        _bindEvents: function () {
          var e = this;
          t(window).on("load", function () {
            e.deferLoadImages();
          }),
          t("#front-page-get-started").length > 0 && e.getStartedStickyNav(),
            t(document).on("get-started-sticky-section", function (t) {
              e.setActiveNavDot(t.sectionID);
            });
        },
        setActiveNavDot: function (e) {
          t(".nav-dot, .nav-dot-text").removeClass("active"), t('.nav-dot[data-sticky-section-id="' + e + '"]').addClass("active"), t('.nav-dot-text[data-sticky-section-id="' + e + '"]').addClass("active");
        },
        deferLoadImages: function () {
          t("[data-src]").each(function () {
            var e = t(this).data("src");
            "IMG" === this.tagName ? t(this).prop("src", e) : t(this).css("background-image", 'url("' + e + '")');
          });
        },
        makeNavDotsSticky: function () {
          t(".nav-dots").addClass("stuck"), t(".nav-dots").removeClass("stuck-bottom"), t(".nav-dots").css("top", 0);
        },
        navDotsBottomPosition: function (e, i) {
          var o = e - i;
          t(".nav-dots").removeClass("stuck"), t(".nav-dots").addClass("stuck-bottom"), t(".nav-dots").css("top", o);
        },
        getStartedStickyNav: function () {
          var e = this,
            i = !1;
          t(window).on("scroll resize", function () {
            var o = t("#front-page-get-started").offset().top,
              n = t("#front-page-get-started .section-marietv").offset().top,
              s = t(window).scrollTop();
            o < s && n > s
              ? (e.makeNavDotsSticky(),
                t(document).trigger({
                  type: "get-started-sticky-section",
                  sectionID: 1,
                }),
              n - 300 < s &&
              t(document).trigger({
                type: "get-started-sticky-section",
                sectionID: 2,
              }),
                (i = !1))
              : (n < s && !i && (e.navDotsBottomPosition(s, o), (i = !0)), t(".nav-dots").removeClass("stuck"));
          });
        },
      }),
        window.MFFrontPage._bindEvents(),
        t(window).scroll(function (e) {
          if (t("#front-1").length) {
            var i = t("#front-1").offset().top - t(window).scrollTop();
            i < 0 ? t("#page-front-page .entry-banner").fadeTo(0, 0) : t("#page-front-page .entry-banner").fadeTo(0, 1);
          }
        }),
        (function () {
          var e,
            i,
            o,
            n,
            s,
            r = t("#page-front-page").find(".parallax blockquote");
          r.removeClass(),
            r.clone().appendTo(t("#page-front-page").find(".parallax")),
          t("body.page-home").length &&
          t(window).on("scroll resize", function (a) {
            (i = t(window).height()),
              (o = t(document).scrollTop()),
              (r = t("#page-front-page").find(".parallax > blockquote")),
              (parallaxSection = t("#page-front-page").find(".parallax")),
              (parallaxSectionHeight = parallaxSection.height()),
              (e = parallaxSection.find("> section")),
              parallaxSection.offset().top - 0.33 * i < o
                ? (e.each(function () {
                  (n = t(this)), n.offset().top - 0.4 * i < o && (s = n[0].id);
                }),
                  r.removeClass("active"),
                  r.filter('[data-id="' + s + '"]').addClass("active"))
                : r.removeClass("active"),
            o > parallaxSection.offset().top + parallaxSectionHeight - 0.7 * i && r.removeClass("active");
          });
        })(),
        t("#page-front-page").length)
    ) {
      new Waypoint({
        element: document.getElementById("word-stream-footer"),
        handler: function () {
          t(".word-stream-footer span:first-child").addClass("js-fade-element-show"),
            setTimeout(function () {
              t(".word-stream-footer span:last-child").addClass("js-fade-element-show");
            }, 1e3);
        },
        offset: "bottom-in-view",
      });
    }
    t(window).resize(function () {
      t(".slider-get-started").removeClass("inView");
    }),
      t(document).bind("mousewheel DOMMouseScroll MozMousePixelScroll", function (i) {
        if (t(".slider-get-started").hasClass("inView")) {
          i.preventDefault();
          var o = i.originalEvent.wheelDelta || -i.originalEvent.detail;
          e(i, o);
        }
      });
    var i;
  }),
  !(function () {
    "use strict";
    function t(o) {
      if (!o) throw new Error("No options passed to Waypoint constructor");
      if (!o.element) throw new Error("No element option passed to Waypoint constructor");
      if (!o.handler) throw new Error("No handler option passed to Waypoint constructor");
      (this.key = "waypoint-" + e),
        (this.options = t.Adapter.extend({}, t.defaults, o)),
        (this.element = this.options.element),
        (this.adapter = new t.Adapter(this.element)),
        (this.callback = o.handler),
        (this.axis = this.options.horizontal ? "horizontal" : "vertical"),
        (this.enabled = this.options.enabled),
        (this.triggerPoint = null),
        (this.group = t.Group.findOrCreate({
          name: this.options.group,
          axis: this.axis,
        })),
        (this.context = t.Context.findOrCreateByElement(this.options.context)),
      t.offsetAliases[this.options.offset] && (this.options.offset = t.offsetAliases[this.options.offset]),
        this.group.add(this),
        this.context.add(this),
        (i[this.key] = this),
        (e += 1);
    }
    var e = 0,
      i = {};
    (t.prototype.queueTrigger = function (t) {
      this.group.queueTrigger(this, t);
    }),
      (t.prototype.trigger = function (t) {
        this.enabled && this.callback && this.callback.apply(this, t);
      }),
      (t.prototype.destroy = function () {
        this.context.remove(this), this.group.remove(this), delete i[this.key];
      }),
      (t.prototype.disable = function () {
        return (this.enabled = !1), this;
      }),
      (t.prototype.enable = function () {
        return this.context.refresh(), (this.enabled = !0), this;
      }),
      (t.prototype.next = function () {
        return this.group.next(this);
      }),
      (t.prototype.previous = function () {
        return this.group.previous(this);
      }),
      (t.invokeAll = function (t) {
        var e = [];
        for (var o in i) e.push(i[o]);
        for (var n = 0, s = e.length; s > n; n++) e[n][t]();
      }),
      (t.destroyAll = function () {
        t.invokeAll("destroy");
      }),
      (t.disableAll = function () {
        t.invokeAll("disable");
      }),
      (t.enableAll = function () {
        t.invokeAll("enable");
      }),
      (t.refreshAll = function () {
        t.Context.refreshAll();
      }),
      (t.viewportHeight = function () {
        return window.innerHeight || document.documentElement.clientHeight;
      }),
      (t.viewportWidth = function () {
        return document.documentElement.clientWidth;
      }),
      (t.adapters = []),
      (t.defaults = {
        context: window,
        continuous: !0,
        enabled: !0,
        group: "default",
        horizontal: !1,
        offset: 0,
      }),
      (t.offsetAliases = {
        "bottom-in-view": function () {
          return this.context.innerHeight() - this.adapter.outerHeight();
        },
        "right-in-view": function () {
          return this.context.innerWidth() - this.adapter.outerWidth();
        },
      }),
      (window.Waypoint = t);
  })(),
  (function () {
    "use strict";
    function t(t) {
      window.setTimeout(t, 1e3 / 60);
    }
    function e(t) {
      (this.element = t),
        (this.Adapter = n.Adapter),
        (this.adapter = new this.Adapter(t)),
        (this.key = "waypoint-context-" + i),
        (this.didScroll = !1),
        (this.didResize = !1),
        (this.oldScroll = {
          x: this.adapter.scrollLeft(),
          y: this.adapter.scrollTop(),
        }),
        (this.waypoints = {
          vertical: {},
          horizontal: {},
        }),
        (t.waypointContextKey = this.key),
        (o[t.waypointContextKey] = this),
        (i += 1),
        this.createThrottledScrollHandler(),
        this.createThrottledResizeHandler();
    }
    var i = 0,
      o = {},
      n = window.Waypoint,
      s = window.onload;
    (e.prototype.add = function (t) {
      var e = t.options.horizontal ? "horizontal" : "vertical";
      (this.waypoints[e][t.key] = t), this.refresh();
    }),
      (e.prototype.checkEmpty = function () {
        var t = this.Adapter.isEmptyObject(this.waypoints.horizontal),
          e = this.Adapter.isEmptyObject(this.waypoints.vertical);
        t && e && (this.adapter.off(".waypoints"), delete o[this.key]);
      }),
      (e.prototype.createThrottledResizeHandler = function () {
        function t() {
          e.handleResize(), (e.didResize = !1);
        }
        var e = this;
        this.adapter.on("resize.waypoints", function () {
          e.didResize || ((e.didResize = !0), n.requestAnimationFrame(t));
        });
      }),
      (e.prototype.createThrottledScrollHandler = function () {
        function t() {
          e.handleScroll(), (e.didScroll = !1);
        }
        var e = this;
        this.adapter.on("scroll.waypoints", function () {
          (!e.didScroll || n.isTouch) && ((e.didScroll = !0), n.requestAnimationFrame(t));
        });
      }),
      (e.prototype.handleResize = function () {
        n.Context.refreshAll();
      }),
      (e.prototype.handleScroll = function () {
        var t = {},
          e = {
            horizontal: {
              newScroll: this.adapter.scrollLeft(),
              oldScroll: this.oldScroll.x,
              forward: "right",
              backward: "left",
            },
            vertical: {
              newScroll: this.adapter.scrollTop(),
              oldScroll: this.oldScroll.y,
              forward: "down",
              backward: "up",
            },
          };
        for (var i in e) {
          var o = e[i],
            n = o.newScroll > o.oldScroll,
            s = n ? o.forward : o.backward;
          for (var r in this.waypoints[i]) {
            var a = this.waypoints[i][r],
              l = o.oldScroll < a.triggerPoint,
              d = o.newScroll >= a.triggerPoint,
              c = l && d,
              u = !l && !d;
            (c || u) && (a.queueTrigger(s), (t[a.group.id] = a.group));
          }
        }
        for (var p in t) t[p].flushTriggers();
        this.oldScroll = {
          x: e.horizontal.newScroll,
          y: e.vertical.newScroll,
        };
      }),
      (e.prototype.innerHeight = function () {
        return this.element == this.element.window ? n.viewportHeight() : this.adapter.innerHeight();
      }),
      (e.prototype.remove = function (t) {
        delete this.waypoints[t.axis][t.key], this.checkEmpty();
      }),
      (e.prototype.innerWidth = function () {
        return this.element == this.element.window ? n.viewportWidth() : this.adapter.innerWidth();
      }),
      (e.prototype.destroy = function () {
        var t = [];
        for (var e in this.waypoints) for (var i in this.waypoints[e]) t.push(this.waypoints[e][i]);
        for (var o = 0, n = t.length; n > o; o++) t[o].destroy();
      }),
      (e.prototype.refresh = function () {
        var t,
          e = this.element == this.element.window,
          i = e ? void 0 : this.adapter.offset(),
          o = {};
        this.handleScroll(),
          (t = {
            horizontal: {
              contextOffset: e ? 0 : i.left,
              contextScroll: e ? 0 : this.oldScroll.x,
              contextDimension: this.innerWidth(),
              oldScroll: this.oldScroll.x,
              forward: "right",
              backward: "left",
              offsetProp: "left",
            },
            vertical: {
              contextOffset: e ? 0 : i.top,
              contextScroll: e ? 0 : this.oldScroll.y,
              contextDimension: this.innerHeight(),
              oldScroll: this.oldScroll.y,
              forward: "down",
              backward: "up",
              offsetProp: "top",
            },
          });
        for (var s in t) {
          var r = t[s];
          for (var a in this.waypoints[s]) {
            var l,
              d,
              c,
              u,
              p,
              h = this.waypoints[s][a],
              f = h.options.offset,
              m = h.triggerPoint,
              v = 0,
              g = null == m;
            h.element !== h.element.window && (v = h.adapter.offset()[r.offsetProp]),
              "function" == typeof f ? (f = f.apply(h)) : "string" == typeof f && ((f = parseFloat(f)), h.options.offset.indexOf("%") > -1 && (f = Math.ceil((r.contextDimension * f) / 100))),
              (l = r.contextScroll - r.contextOffset),
              (h.triggerPoint = v + l - f),
              (d = m < r.oldScroll),
              (c = h.triggerPoint >= r.oldScroll),
              (u = d && c),
              (p = !d && !c),
              !g && u
                ? (h.queueTrigger(r.backward), (o[h.group.id] = h.group))
                : !g && p
                ? (h.queueTrigger(r.forward), (o[h.group.id] = h.group))
                : g && r.oldScroll >= h.triggerPoint && (h.queueTrigger(r.forward), (o[h.group.id] = h.group));
          }
        }
        return (
          n.requestAnimationFrame(function () {
            for (var t in o) o[t].flushTriggers();
          }),
            this
        );
      }),
      (e.findOrCreateByElement = function (t) {
        return e.findByElement(t) || new e(t);
      }),
      (e.refreshAll = function () {
        for (var t in o) o[t].refresh();
      }),
      (e.findByElement = function (t) {
        return o[t.waypointContextKey];
      }),
      (window.onload = function () {
        s && s(), e.refreshAll();
      }),
      (n.requestAnimationFrame = function (e) {
        var i = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || t;
        i.call(window, e);
      }),
      (n.Context = e);
  })(),
  (function () {
    "use strict";
    function t(t, e) {
      return t.triggerPoint - e.triggerPoint;
    }
    function e(t, e) {
      return e.triggerPoint - t.triggerPoint;
    }
    function i(t) {
      (this.name = t.name), (this.axis = t.axis), (this.id = this.name + "-" + this.axis), (this.waypoints = []), this.clearTriggerQueues(), (o[this.axis][this.name] = this);
    }
    var o = {
        vertical: {},
        horizontal: {},
      },
      n = window.Waypoint;
    (i.prototype.add = function (t) {
      this.waypoints.push(t);
    }),
      (i.prototype.clearTriggerQueues = function () {
        this.triggerQueues = {
          up: [],
          down: [],
          left: [],
          right: [],
        };
      }),
      (i.prototype.flushTriggers = function () {
        for (var i in this.triggerQueues) {
          var o = this.triggerQueues[i],
            n = "up" === i || "left" === i;
          o.sort(n ? e : t);
          for (var s = 0, r = o.length; r > s; s += 1) {
            var a = o[s];
            (a.options.continuous || s === o.length - 1) && a.trigger([i]);
          }
        }
        this.clearTriggerQueues();
      }),
      (i.prototype.next = function (e) {
        this.waypoints.sort(t);
        var i = n.Adapter.inArray(e, this.waypoints),
          o = i === this.waypoints.length - 1;
        return o ? null : this.waypoints[i + 1];
      }),
      (i.prototype.previous = function (e) {
        this.waypoints.sort(t);
        var i = n.Adapter.inArray(e, this.waypoints);
        return i ? this.waypoints[i - 1] : null;
      }),
      (i.prototype.queueTrigger = function (t, e) {
        this.triggerQueues[e].push(t);
      }),
      (i.prototype.remove = function (t) {
        var e = n.Adapter.inArray(t, this.waypoints);
        e > -1 && this.waypoints.splice(e, 1);
      }),
      (i.prototype.first = function () {
        return this.waypoints[0];
      }),
      (i.prototype.last = function () {
        return this.waypoints[this.waypoints.length - 1];
      }),
      (i.findOrCreate = function (t) {
        return o[t.axis][t.name] || new i(t);
      }),
      (n.Group = i);
  })(),
  (function () {
    "use strict";
    function t(t) {
      this.$element = e(t);
    }
    var e = window.jQuery,
      i = window.Waypoint;
    e.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function (e, i) {
      t.prototype[i] = function () {
        var t = Array.prototype.slice.call(arguments);
        return this.$element[i].apply(this.$element, t);
      };
    }),
      e.each(["extend", "inArray", "isEmptyObject"], function (i, o) {
        t[o] = e[o];
      }),
      i.adapters.push({
        name: "jquery",
        Adapter: t,
      }),
      (i.Adapter = t);
  })(),
  (function () {
    "use strict";
    function t(t) {
      return function () {
        var i = [],
          o = arguments[0];
        return (
          t.isFunction(arguments[0]) && ((o = t.extend({}, arguments[1])), (o.handler = arguments[0])),
            this.each(function () {
              var n = t.extend({}, o, {
                element: this,
              });
              "string" == typeof n.context && (n.context = t(this).closest(n.context)[0]), i.push(new e(n));
            }),
            i
        );
      };
    }
    var e = window.Waypoint;
    window.jQuery && (window.jQuery.fn.waypoint = t(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = t(window.Zepto));
  })(),
  !(function (t) {
    function e() {}
    function i(t) {
      function i(e) {
        e.prototype.option ||
        (e.prototype.option = function (e) {
          t.isPlainObject(e) && (this.options = t.extend(!0, this.options, e));
        });
      }
      function n(e, i) {
        t.fn[e] = function (n) {
          if ("string" == typeof n) {
            for (var r = o.call(arguments, 1), a = 0, l = this.length; l > a; a++) {
              var d = this[a],
                c = t.data(d, e);
              if (c)
                if (t.isFunction(c[n]) && "_" !== n.charAt(0)) {
                  var u = c[n].apply(c, r);
                  if (void 0 !== u) return u;
                } else s("no such method '" + n + "' for " + e + " instance");
              else s("cannot call methods on " + e + " prior to initialization; attempted to call '" + n + "'");
            }
            return this;
          }
          return this.each(function () {
            var o = t.data(this, e);
            o ? (o.option(n), o._init()) : ((o = new i(this, n)), t.data(this, e, o));
          });
        };
      }
      if (t) {
        var s =
          "undefined" == typeof console
            ? e
            : function (t) {
              console.error(t);
            };
        return (
          (t.bridget = function (t, e) {
            i(e), n(t, e);
          }),
            t.bridget
        );
      }
    }
    var o = Array.prototype.slice;
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], i) : i("object" == typeof exports ? require("jquery") : t.jQuery);
  })(window),
  (function (t) {
    function e(e) {
      var i = t.event;
      return (i.target = i.target || i.srcElement || e), i;
    }
    var i = document.documentElement,
      o = function () {};
    i.addEventListener
      ? (o = function (t, e, i) {
        t.addEventListener(e, i, !1);
      })
      : i.attachEvent &&
      (o = function (t, i, o) {
        (t[i + o] = o.handleEvent
          ? function () {
            var i = e(t);
            o.handleEvent.call(o, i);
          }
          : function () {
            var i = e(t);
            o.call(t, i);
          }),
          t.attachEvent("on" + i, t[i + o]);
      });
    var n = function () {};
    i.removeEventListener
      ? (n = function (t, e, i) {
        t.removeEventListener(e, i, !1);
      })
      : i.detachEvent &&
      (n = function (t, e, i) {
        t.detachEvent("on" + e, t[e + i]);
        try {
          delete t[e + i];
        } catch (o) {
          t[e + i] = void 0;
        }
      });
    var s = {
      bind: o,
      unbind: n,
    };
    "function" == typeof define && define.amd ? define("eventie/eventie", s) : "object" == typeof exports ? (module.exports = s) : (t.eventie = s);
  })(window),
  function () {
    "use strict";
    function t() {}
    function e(t, e) {
      for (var i = t.length; i--; ) if (t[i].listener === e) return i;
      return -1;
    }
    function i(t) {
      return function () {
        return this[t].apply(this, arguments);
      };
    }
    var o = t.prototype,
      n = this,
      s = n.EventEmitter;
    (o.getListeners = function (t) {
      var e,
        i,
        o = this._getEvents();
      if (t instanceof RegExp) {
        e = {};
        for (i in o) o.hasOwnProperty(i) && t.test(i) && (e[i] = o[i]);
      } else e = o[t] || (o[t] = []);
      return e;
    }),
      (o.flattenListeners = function (t) {
        var e,
          i = [];
        for (e = 0; e < t.length; e += 1) i.push(t[e].listener);
        return i;
      }),
      (o.getListenersAsObject = function (t) {
        var e,
          i = this.getListeners(t);
        return i instanceof Array && ((e = {}), (e[t] = i)), e || i;
      }),
      (o.addListener = function (t, i) {
        var o,
          n = this.getListenersAsObject(t),
          s = "object" == typeof i;
        for (o in n)
          n.hasOwnProperty(o) &&
          -1 === e(n[o], i) &&
          n[o].push(
            s
              ? i
              : {
                listener: i,
                once: !1,
              }
          );
        return this;
      }),
      (o.on = i("addListener")),
      (o.addOnceListener = function (t, e) {
        return this.addListener(t, {
          listener: e,
          once: !0,
        });
      }),
      (o.once = i("addOnceListener")),
      (o.defineEvent = function (t) {
        return this.getListeners(t), this;
      }),
      (o.defineEvents = function (t) {
        for (var e = 0; e < t.length; e += 1) this.defineEvent(t[e]);
        return this;
      }),
      (o.removeListener = function (t, i) {
        var o,
          n,
          s = this.getListenersAsObject(t);
        for (n in s) s.hasOwnProperty(n) && ((o = e(s[n], i)), -1 !== o && s[n].splice(o, 1));
        return this;
      }),
      (o.off = i("removeListener")),
      (o.addListeners = function (t, e) {
        return this.manipulateListeners(!1, t, e);
      }),
      (o.removeListeners = function (t, e) {
        return this.manipulateListeners(!0, t, e);
      }),
      (o.manipulateListeners = function (t, e, i) {
        var o,
          n,
          s = t ? this.removeListener : this.addListener,
          r = t ? this.removeListeners : this.addListeners;
        if ("object" != typeof e || e instanceof RegExp) for (o = i.length; o--; ) s.call(this, e, i[o]);
        else for (o in e) e.hasOwnProperty(o) && (n = e[o]) && ("function" == typeof n ? s.call(this, o, n) : r.call(this, o, n));
        return this;
      }),
      (o.removeEvent = function (t) {
        var e,
          i = typeof t,
          o = this._getEvents();
        if ("string" === i) delete o[t];
        else if (t instanceof RegExp) for (e in o) o.hasOwnProperty(e) && t.test(e) && delete o[e];
        else delete this._events;
        return this;
      }),
      (o.removeAllListeners = i("removeEvent")),
      (o.emitEvent = function (t, e) {
        var i,
          o,
          n,
          s,
          r = this.getListenersAsObject(t);
        for (n in r)
          if (r.hasOwnProperty(n))
            for (o = r[n].length; o--; ) (i = r[n][o]), i.once === !0 && this.removeListener(t, i.listener), (s = i.listener.apply(this, e || [])), s === this._getOnceReturnValue() && this.removeListener(t, i.listener);
        return this;
      }),
      (o.trigger = i("emitEvent")),
      (o.emit = function (t) {
        var e = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(t, e);
      }),
      (o.setOnceReturnValue = function (t) {
        return (this._onceReturnValue = t), this;
      }),
      (o._getOnceReturnValue = function () {
        return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue;
      }),
      (o._getEvents = function () {
        return this._events || (this._events = {});
      }),
      (t.noConflict = function () {
        return (n.EventEmitter = s), t;
      }),
      "function" == typeof define && define.amd
        ? define("eventEmitter/EventEmitter", [], function () {
          return t;
        })
        : "object" == typeof module && module.exports
        ? (module.exports = t)
        : (n.EventEmitter = t);
  }.call(this),
  (function (t) {
    function e(t) {
      if (t) {
        if ("string" == typeof o[t]) return t;
        t = t.charAt(0).toUpperCase() + t.slice(1);
        for (var e, n = 0, s = i.length; s > n; n++) if (((e = i[n] + t), "string" == typeof o[e])) return e;
      }
    }
    var i = "Webkit Moz ms Ms O".split(" "),
      o = document.documentElement.style;
    "function" == typeof define && define.amd
      ? define("get-style-property/get-style-property", [], function () {
        return e;
      })
      : "object" == typeof exports
      ? (module.exports = e)
      : (t.getStyleProperty = e);
  })(window),
  (function (t, e) {
    function i(t) {
      var e = parseFloat(t),
        i = -1 === t.indexOf("%") && !isNaN(e);
      return i && e;
    }
    function o() {}
    function n() {
      for (
        var t = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0,
          },
          e = 0,
          i = a.length;
        i > e;
        e++
      ) {
        var o = a[e];
        t[o] = 0;
      }
      return t;
    }
    function s(e) {
      function o() {
        if (!p) {
          p = !0;
          var o = t.getComputedStyle;
          if (
            ((d = (function () {
              var t = o
                ? function (t) {
                  return o(t, null);
                }
                : function (t) {
                  return t.currentStyle;
                };
              return function (e) {
                var i = t(e);
                return i || r("Style returned " + i + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), i;
              };
            })()),
              (c = e("boxSizing")))
          ) {
            var n = document.createElement("div");
            (n.style.width = "200px"), (n.style.padding = "1px 2px 3px 4px"), (n.style.borderStyle = "solid"), (n.style.borderWidth = "1px 2px 3px 4px"), (n.style[c] = "border-box");
            var s = document.body || document.documentElement;
            s.appendChild(n);
            var a = d(n);
            (u = 200 === i(a.width)), s.removeChild(n);
          }
        }
      }
      function s(t) {
        if ((o(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType)) {
          var e = d(t);
          if ("none" === e.display) return n();
          var s = {};
          (s.width = t.offsetWidth), (s.height = t.offsetHeight);
          for (var r = (s.isBorderBox = !(!c || !e[c] || "border-box" !== e[c])), p = 0, h = a.length; h > p; p++) {
            var f = a[p],
              m = e[f];
            m = l(t, m);
            var v = parseFloat(m);
            s[f] = isNaN(v) ? 0 : v;
          }
          var g = s.paddingLeft + s.paddingRight,
            y = s.paddingTop + s.paddingBottom,
            w = s.marginLeft + s.marginRight,
            b = s.marginTop + s.marginBottom,
            k = s.borderLeftWidth + s.borderRightWidth,
            S = s.borderTopWidth + s.borderBottomWidth,
            T = r && u,
            x = i(e.width);
          x !== !1 && (s.width = x + (T ? 0 : g + k));
          var C = i(e.height);
          return C !== !1 && (s.height = C + (T ? 0 : y + S)), (s.innerWidth = s.width - (g + k)), (s.innerHeight = s.height - (y + S)), (s.outerWidth = s.width + w), (s.outerHeight = s.height + b), s;
        }
      }
      function l(e, i) {
        if (t.getComputedStyle || -1 === i.indexOf("%")) return i;
        var o = e.style,
          n = o.left,
          s = e.runtimeStyle,
          r = s && s.left;
        return r && (s.left = e.currentStyle.left), (o.left = i), (i = o.pixelLeft), (o.left = n), r && (s.left = r), i;
      }
      var d,
        c,
        u,
        p = !1;
      return s;
    }
    var r =
        "undefined" == typeof console
          ? o
          : function (t) {
            console.error(t);
          },
      a = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
    "function" == typeof define && define.amd
      ? define("get-size/get-size", ["get-style-property/get-style-property"], s)
      : "object" == typeof exports
      ? (module.exports = s(require("desandro-get-style-property")))
      : (t.getSize = s(t.getStyleProperty));
  })(window),
  (function (t) {
    function e(t) {
      "function" == typeof t && (e.isReady ? t() : r.push(t));
    }
    function i(t) {
      var i = "readystatechange" === t.type && "complete" !== s.readyState;
      e.isReady || i || o();
    }
    function o() {
      e.isReady = !0;
      for (var t = 0, i = r.length; i > t; t++) {
        var o = r[t];
        o();
      }
    }
    function n(n) {
      return "complete" === s.readyState ? o() : (n.bind(s, "DOMContentLoaded", i), n.bind(s, "readystatechange", i), n.bind(t, "load", i)), e;
    }
    var s = t.document,
      r = [];
    (e.isReady = !1), "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], n) : "object" == typeof exports ? (module.exports = n(require("eventie"))) : (t.docReady = n(t.eventie));
  })(window),
  (function (t) {
    "use strict";
    function e(t, e) {
      return t[r](e);
    }
    function i(t) {
      if (!t.parentNode) {
        var e = document.createDocumentFragment();
        e.appendChild(t);
      }
    }
    function o(t, e) {
      i(t);
      for (var o = t.parentNode.querySelectorAll(e), n = 0, s = o.length; s > n; n++) if (o[n] === t) return !0;
      return !1;
    }
    function n(t, o) {
      return i(t), e(t, o);
    }
    var s,
      r = (function () {
        if (t.matches) return "matches";
        if (t.matchesSelector) return "matchesSelector";
        for (var e = ["webkit", "moz", "ms", "o"], i = 0, o = e.length; o > i; i++) {
          var n = e[i],
            s = n + "MatchesSelector";
          if (t[s]) return s;
        }
      })();
    if (r) {
      var a = document.createElement("div"),
        l = e(a, "div");
      s = l ? e : n;
    } else s = o;
    "function" == typeof define && define.amd
      ? define("matches-selector/matches-selector", [], function () {
        return s;
      })
      : "object" == typeof exports
      ? (module.exports = s)
      : (window.matchesSelector = s);
  })(Element.prototype),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("fizzy-ui-utils/utils", ["doc-ready/doc-ready", "matches-selector/matches-selector"], function (i, o) {
        return e(t, i, o);
      })
      : "object" == typeof exports
      ? (module.exports = e(t, require("doc-ready"), require("desandro-matches-selector")))
      : (t.fizzyUIUtils = e(t, t.docReady, t.matchesSelector));
  })(window, function (t, e, i) {
    var o = {};
    (o.extend = function (t, e) {
      for (var i in e) t[i] = e[i];
      return t;
    }),
      (o.modulo = function (t, e) {
        return ((t % e) + e) % e;
      });
    var n = Object.prototype.toString;
    (o.isArray = function (t) {
      return "[object Array]" == n.call(t);
    }),
      (o.makeArray = function (t) {
        var e = [];
        if (o.isArray(t)) e = t;
        else if (t && "number" == typeof t.length) for (var i = 0, n = t.length; n > i; i++) e.push(t[i]);
        else e.push(t);
        return e;
      }),
      (o.indexOf = Array.prototype.indexOf
        ? function (t, e) {
          return t.indexOf(e);
        }
        : function (t, e) {
          for (var i = 0, o = t.length; o > i; i++) if (t[i] === e) return i;
          return -1;
        }),
      (o.removeFrom = function (t, e) {
        var i = o.indexOf(t, e);
        -1 != i && t.splice(i, 1);
      }),
      (o.isElement =
        "function" == typeof HTMLElement || "object" == typeof HTMLElement
          ? function (t) {
            return t instanceof HTMLElement;
          }
          : function (t) {
            return t && "object" == typeof t && 1 == t.nodeType && "string" == typeof t.nodeName;
          }),
      (o.setText = (function () {
        function t(t, i) {
          (e = e || (void 0 !== document.documentElement.textContent ? "textContent" : "innerText")), (t[e] = i);
        }
        var e;
        return t;
      })()),
      (o.getParent = function (t, e) {
        for (; t != document.body; ) if (((t = t.parentNode), i(t, e))) return t;
      }),
      (o.getQueryElement = function (t) {
        return "string" == typeof t ? document.querySelector(t) : t;
      }),
      (o.handleEvent = function (t) {
        var e = "on" + t.type;
        this[e] && this[e](t);
      }),
      (o.filterFindElements = function (t, e) {
        t = o.makeArray(t);
        for (var n = [], s = 0, r = t.length; r > s; s++) {
          var a = t[s];
          if (o.isElement(a))
            if (e) {
              i(a, e) && n.push(a);
              for (var l = a.querySelectorAll(e), d = 0, c = l.length; c > d; d++) n.push(l[d]);
            } else n.push(a);
        }
        return n;
      }),
      (o.debounceMethod = function (t, e, i) {
        var o = t.prototype[e],
          n = e + "Timeout";
        t.prototype[e] = function () {
          var t = this[n];
          t && clearTimeout(t);
          var e = arguments,
            s = this;
          this[n] = setTimeout(function () {
            o.apply(s, e), delete s[n];
          }, i || 100);
        };
      }),
      (o.toDashed = function (t) {
        return t
        .replace(/(.)([A-Z])/g, function (t, e, i) {
          return e + "-" + i;
        })
        .toLowerCase();
      });
    var s = t.console;
    return (
      (o.htmlInit = function (i, n) {
        e(function () {
          for (var e = o.toDashed(n), r = document.querySelectorAll(".js-" + e), a = "data-" + e + "-options", l = 0, d = r.length; d > l; l++) {
            var c,
              u = r[l],
              p = u.getAttribute(a);
            try {
              c = p && JSON.parse(p);
            } catch (h) {
              s && s.error("Error parsing " + a + " on " + u.nodeName.toLowerCase() + (u.id ? "#" + u.id : "") + ": " + h);
              continue;
            }
            var f = new i(u, c),
              m = t.jQuery;
            m && m.data(u, n, f);
          }
        });
      }),
        o
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property", "fizzy-ui-utils/utils"], function (i, o, n, s) {
        return e(t, i, o, n, s);
      })
      : "object" == typeof exports
      ? (module.exports = e(t, require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property"), require("fizzy-ui-utils")))
      : ((t.Outlayer = {}), (t.Outlayer.Item = e(t, t.EventEmitter, t.getSize, t.getStyleProperty, t.fizzyUIUtils)));
  })(window, function (t, e, i, o, n) {
    "use strict";
    function s(t) {
      for (var e in t) return !1;
      return (e = null), !0;
    }
    function r(t, e) {
      t &&
      ((this.element = t),
        (this.layout = e),
        (this.position = {
          x: 0,
          y: 0,
        }),
        this._create());
    }
    function a(t) {
      return t.replace(/([A-Z])/g, function (t) {
        return "-" + t.toLowerCase();
      });
    }
    var l = t.getComputedStyle,
      d = l
        ? function (t) {
          return l(t, null);
        }
        : function (t) {
          return t.currentStyle;
        },
      c = o("transition"),
      u = o("transform"),
      p = c && u,
      h = !!o("perspective"),
      f = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "otransitionend",
        transition: "transitionend",
      }[c],
      m = ["transform", "transition", "transitionDuration", "transitionProperty"],
      v = (function () {
        for (var t = {}, e = 0, i = m.length; i > e; e++) {
          var n = m[e],
            s = o(n);
          s && s !== n && (t[n] = s);
        }
        return t;
      })();
    n.extend(r.prototype, e.prototype),
      (r.prototype._create = function () {
        (this._transn = {
          ingProperties: {},
          clean: {},
          onEnd: {},
        }),
          this.css({
            position: "absolute",
          });
      }),
      (r.prototype.handleEvent = function (t) {
        var e = "on" + t.type;
        this[e] && this[e](t);
      }),
      (r.prototype.getSize = function () {
        this.size = i(this.element);
      }),
      (r.prototype.css = function (t) {
        var e = this.element.style;
        for (var i in t) {
          var o = v[i] || i;
          e[o] = t[i];
        }
      }),
      (r.prototype.getPosition = function () {
        var t = d(this.element),
          e = this.layout.options,
          i = e.isOriginLeft,
          o = e.isOriginTop,
          n = t[i ? "left" : "right"],
          s = t[o ? "top" : "bottom"],
          r = this.layout.size,
          a = -1 != n.indexOf("%") ? (parseFloat(n) / 100) * r.width : parseInt(n, 10),
          l = -1 != s.indexOf("%") ? (parseFloat(s) / 100) * r.height : parseInt(s, 10);
        (a = isNaN(a) ? 0 : a), (l = isNaN(l) ? 0 : l), (a -= i ? r.paddingLeft : r.paddingRight), (l -= o ? r.paddingTop : r.paddingBottom), (this.position.x = a), (this.position.y = l);
      }),
      (r.prototype.layoutPosition = function () {
        var t = this.layout.size,
          e = this.layout.options,
          i = {},
          o = e.isOriginLeft ? "paddingLeft" : "paddingRight",
          n = e.isOriginLeft ? "left" : "right",
          s = e.isOriginLeft ? "right" : "left",
          r = this.position.x + t[o];
        (i[n] = this.getXValue(r)), (i[s] = "");
        var a = e.isOriginTop ? "paddingTop" : "paddingBottom",
          l = e.isOriginTop ? "top" : "bottom",
          d = e.isOriginTop ? "bottom" : "top",
          c = this.position.y + t[a];
        (i[l] = this.getYValue(c)), (i[d] = ""), this.css(i), this.emitEvent("layout", [this]);
      }),
      (r.prototype.getXValue = function (t) {
        var e = this.layout.options;
        return e.percentPosition && !e.isHorizontal ? (t / this.layout.size.width) * 100 + "%" : t + "px";
      }),
      (r.prototype.getYValue = function (t) {
        var e = this.layout.options;
        return e.percentPosition && e.isHorizontal ? (t / this.layout.size.height) * 100 + "%" : t + "px";
      }),
      (r.prototype._transitionTo = function (t, e) {
        this.getPosition();
        var i = this.position.x,
          o = this.position.y,
          n = parseInt(t, 10),
          s = parseInt(e, 10),
          r = n === this.position.x && s === this.position.y;
        if ((this.setPosition(t, e), r && !this.isTransitioning)) return void this.layoutPosition();
        var a = t - i,
          l = e - o,
          d = {};
        (d.transform = this.getTranslate(a, l)),
          this.transition({
            to: d,
            onTransitionEnd: {
              transform: this.layoutPosition,
            },
            isCleaning: !0,
          });
      }),
      (r.prototype.getTranslate = function (t, e) {
        var i = this.layout.options;
        return (t = i.isOriginLeft ? t : -t), (e = i.isOriginTop ? e : -e), h ? "translate3d(" + t + "px, " + e + "px, 0)" : "translate(" + t + "px, " + e + "px)";
      }),
      (r.prototype.goTo = function (t, e) {
        this.setPosition(t, e), this.layoutPosition();
      }),
      (r.prototype.moveTo = p ? r.prototype._transitionTo : r.prototype.goTo),
      (r.prototype.setPosition = function (t, e) {
        (this.position.x = parseInt(t, 10)), (this.position.y = parseInt(e, 10));
      }),
      (r.prototype._nonTransition = function (t) {
        this.css(t.to), t.isCleaning && this._removeStyles(t.to);
        for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this);
      }),
      (r.prototype._transition = function (t) {
        if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(t);
        var e = this._transn;
        for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
        for (i in t.to) (e.ingProperties[i] = !0), t.isCleaning && (e.clean[i] = !0);
        if (t.from) {
          this.css(t.from);
          var o = this.element.offsetHeight;
          o = null;
        }
        this.enableTransition(t.to), this.css(t.to), (this.isTransitioning = !0);
      });
    var g = "opacity," + a(v.transform || "transform");
    (r.prototype.enableTransition = function () {
      this.isTransitioning ||
      (this.css({
        transitionProperty: g,
        transitionDuration: this.layout.options.transitionDuration,
      }),
        this.element.addEventListener(f, this, !1));
    }),
      (r.prototype.transition = r.prototype[c ? "_transition" : "_nonTransition"]),
      (r.prototype.onwebkitTransitionEnd = function (t) {
        this.ontransitionend(t);
      }),
      (r.prototype.onotransitionend = function (t) {
        this.ontransitionend(t);
      });
    var y = {
      "-webkit-transform": "transform",
      "-moz-transform": "transform",
      "-o-transform": "transform",
    };
    (r.prototype.ontransitionend = function (t) {
      if (t.target === this.element) {
        var e = this._transn,
          i = y[t.propertyName] || t.propertyName;
        if ((delete e.ingProperties[i], s(e.ingProperties) && this.disableTransition(), i in e.clean && ((this.element.style[t.propertyName] = ""), delete e.clean[i]), i in e.onEnd)) {
          var o = e.onEnd[i];
          o.call(this), delete e.onEnd[i];
        }
        this.emitEvent("transitionEnd", [this]);
      }
    }),
      (r.prototype.disableTransition = function () {
        this.removeTransitionStyles(), this.element.removeEventListener(f, this, !1), (this.isTransitioning = !1);
      }),
      (r.prototype._removeStyles = function (t) {
        var e = {};
        for (var i in t) e[i] = "";
        this.css(e);
      });
    var w = {
      transitionProperty: "",
      transitionDuration: "",
    };
    return (
      (r.prototype.removeTransitionStyles = function () {
        this.css(w);
      }),
        (r.prototype.removeElem = function () {
          this.element.parentNode.removeChild(this.element),
            this.css({
              display: "",
            }),
            this.emitEvent("remove", [this]);
        }),
        (r.prototype.remove = function () {
          if (!c || !parseFloat(this.layout.options.transitionDuration)) return void this.removeElem();
          var t = this;
          this.once("transitionEnd", function () {
            t.removeElem();
          }),
            this.hide();
        }),
        (r.prototype.reveal = function () {
          delete this.isHidden,
            this.css({
              display: "",
            });
          var t = this.layout.options,
            e = {},
            i = this.getHideRevealTransitionEndProperty("visibleStyle");
          (e[i] = this.onRevealTransitionEnd),
            this.transition({
              from: t.hiddenStyle,
              to: t.visibleStyle,
              isCleaning: !0,
              onTransitionEnd: e,
            });
        }),
        (r.prototype.onRevealTransitionEnd = function () {
          this.isHidden || this.emitEvent("reveal");
        }),
        (r.prototype.getHideRevealTransitionEndProperty = function (t) {
          var e = this.layout.options[t];
          if (e.opacity) return "opacity";
          for (var i in e) return i;
        }),
        (r.prototype.hide = function () {
          (this.isHidden = !0),
            this.css({
              display: "",
            });
          var t = this.layout.options,
            e = {},
            i = this.getHideRevealTransitionEndProperty("hiddenStyle");
          (e[i] = this.onHideTransitionEnd),
            this.transition({
              from: t.visibleStyle,
              to: t.hiddenStyle,
              isCleaning: !0,
              onTransitionEnd: e,
            });
        }),
        (r.prototype.onHideTransitionEnd = function () {
          this.isHidden &&
          (this.css({
            display: "none",
          }),
            this.emitEvent("hide"));
        }),
        (r.prototype.destroy = function () {
          this.css({
            position: "",
            left: "",
            right: "",
            top: "",
            bottom: "",
            transition: "",
            transform: "",
          });
        }),
        r
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("outlayer/outlayer", ["eventie/eventie", "eventEmitter/EventEmitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (i, o, n, s, r) {
        return e(t, i, o, n, s, r);
      })
      : "object" == typeof exports
      ? (module.exports = e(t, require("eventie"), require("wolfy87-eventemitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")))
      : (t.Outlayer = e(t, t.eventie, t.EventEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item));
  })(window, function (t, e, i, o, n, s) {
    "use strict";
    function r(t, e) {
      var i = n.getQueryElement(t);
      if (!i) return void (a && a.error("Bad element for " + this.constructor.namespace + ": " + (i || t)));
      (this.element = i), l && (this.$element = l(this.element)), (this.options = n.extend({}, this.constructor.defaults)), this.option(e);
      var o = ++c;
      (this.element.outlayerGUID = o), (u[o] = this), this._create(), this.options.isInitLayout && this.layout();
    }
    var a = t.console,
      l = t.jQuery,
      d = function () {},
      c = 0,
      u = {};
    return (
      (r.namespace = "outlayer"),
        (r.Item = s),
        (r.defaults = {
          containerStyle: {
            position: "relative",
          },
          isInitLayout: !0,
          isOriginLeft: !0,
          isOriginTop: !0,
          isResizeBound: !0,
          isResizingContainer: !0,
          transitionDuration: "0.4s",
          hiddenStyle: {
            opacity: 0,
            transform: "scale(0.001)",
          },
          visibleStyle: {
            opacity: 1,
            transform: "scale(1)",
          },
        }),
        n.extend(r.prototype, i.prototype),
        (r.prototype.option = function (t) {
          n.extend(this.options, t);
        }),
        (r.prototype._create = function () {
          this.reloadItems(), (this.stamps = []), this.stamp(this.options.stamp), n.extend(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize();
        }),
        (r.prototype.reloadItems = function () {
          this.items = this._itemize(this.element.children);
        }),
        (r.prototype._itemize = function (t) {
          for (var e = this._filterFindItemElements(t), i = this.constructor.Item, o = [], n = 0, s = e.length; s > n; n++) {
            var r = e[n],
              a = new i(r, this);
            o.push(a);
          }
          return o;
        }),
        (r.prototype._filterFindItemElements = function (t) {
          return n.filterFindElements(t, this.options.itemSelector);
        }),
        (r.prototype.getItemElements = function () {
          for (var t = [], e = 0, i = this.items.length; i > e; e++) t.push(this.items[e].element);
          return t;
        }),
        (r.prototype.layout = function () {
          this._resetLayout(), this._manageStamps();
          var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
          this.layoutItems(this.items, t), (this._isLayoutInited = !0);
        }),
        (r.prototype._init = r.prototype.layout),
        (r.prototype._resetLayout = function () {
          this.getSize();
        }),
        (r.prototype.getSize = function () {
          this.size = o(this.element);
        }),
        (r.prototype._getMeasurement = function (t, e) {
          var i,
            s = this.options[t];
          s ? ("string" == typeof s ? (i = this.element.querySelector(s)) : n.isElement(s) && (i = s), (this[t] = i ? o(i)[e] : s)) : (this[t] = 0);
        }),
        (r.prototype.layoutItems = function (t, e) {
          (t = this._getItemsForLayout(t)), this._layoutItems(t, e), this._postLayout();
        }),
        (r.prototype._getItemsForLayout = function (t) {
          for (var e = [], i = 0, o = t.length; o > i; i++) {
            var n = t[i];
            n.isIgnored || e.push(n);
          }
          return e;
        }),
        (r.prototype._layoutItems = function (t, e) {
          if ((this._emitCompleteOnItems("layout", t), t && t.length)) {
            for (var i = [], o = 0, n = t.length; n > o; o++) {
              var s = t[o],
                r = this._getItemLayoutPosition(s);
              (r.item = s), (r.isInstant = e || s.isLayoutInstant), i.push(r);
            }
            this._processLayoutQueue(i);
          }
        }),
        (r.prototype._getItemLayoutPosition = function () {
          return {
            x: 0,
            y: 0,
          };
        }),
        (r.prototype._processLayoutQueue = function (t) {
          for (var e = 0, i = t.length; i > e; e++) {
            var o = t[e];
            this._positionItem(o.item, o.x, o.y, o.isInstant);
          }
        }),
        (r.prototype._positionItem = function (t, e, i, o) {
          o ? t.goTo(e, i) : t.moveTo(e, i);
        }),
        (r.prototype._postLayout = function () {
          this.resizeContainer();
        }),
        (r.prototype.resizeContainer = function () {
          if (this.options.isResizingContainer) {
            var t = this._getContainerSize();
            t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1));
          }
        }),
        (r.prototype._getContainerSize = d),
        (r.prototype._setContainerMeasure = function (t, e) {
          if (void 0 !== t) {
            var i = this.size;
            i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth),
              (t = Math.max(t, 0)),
              (this.element.style[e ? "width" : "height"] = t + "px");
          }
        }),
        (r.prototype._emitCompleteOnItems = function (t, e) {
          function i() {
            n.dispatchEvent(t + "Complete", null, [e]);
          }
          function o() {
            r++, r === s && i();
          }
          var n = this,
            s = e.length;
          if (!e || !s) return void i();
          for (var r = 0, a = 0, l = e.length; l > a; a++) {
            var d = e[a];
            d.once(t, o);
          }
        }),
        (r.prototype.dispatchEvent = function (t, e, i) {
          var o = e ? [e].concat(i) : i;
          if ((this.emitEvent(t, o), l))
            if (((this.$element = this.$element || l(this.element)), e)) {
              var n = l.Event(e);
              (n.type = t), this.$element.trigger(n, i);
            } else this.$element.trigger(t, i);
        }),
        (r.prototype.ignore = function (t) {
          var e = this.getItem(t);
          e && (e.isIgnored = !0);
        }),
        (r.prototype.unignore = function (t) {
          var e = this.getItem(t);
          e && delete e.isIgnored;
        }),
        (r.prototype.stamp = function (t) {
          if ((t = this._find(t))) {
            this.stamps = this.stamps.concat(t);
            for (var e = 0, i = t.length; i > e; e++) {
              var o = t[e];
              this.ignore(o);
            }
          }
        }),
        (r.prototype.unstamp = function (t) {
          if ((t = this._find(t)))
            for (var e = 0, i = t.length; i > e; e++) {
              var o = t[e];
              n.removeFrom(this.stamps, o), this.unignore(o);
            }
        }),
        (r.prototype._find = function (t) {
          return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), (t = n.makeArray(t))) : void 0;
        }),
        (r.prototype._manageStamps = function () {
          if (this.stamps && this.stamps.length) {
            this._getBoundingRect();
            for (var t = 0, e = this.stamps.length; e > t; t++) {
              var i = this.stamps[t];
              this._manageStamp(i);
            }
          }
        }),
        (r.prototype._getBoundingRect = function () {
          var t = this.element.getBoundingClientRect(),
            e = this.size;
          this._boundingRect = {
            left: t.left + e.paddingLeft + e.borderLeftWidth,
            top: t.top + e.paddingTop + e.borderTopWidth,
            right: t.right - (e.paddingRight + e.borderRightWidth),
            bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth),
          };
        }),
        (r.prototype._manageStamp = d),
        (r.prototype._getElementOffset = function (t) {
          var e = t.getBoundingClientRect(),
            i = this._boundingRect,
            n = o(t),
            s = {
              left: e.left - i.left - n.marginLeft,
              top: e.top - i.top - n.marginTop,
              right: i.right - e.right - n.marginRight,
              bottom: i.bottom - e.bottom - n.marginBottom,
            };
          return s;
        }),
        (r.prototype.handleEvent = function (t) {
          var e = "on" + t.type;
          this[e] && this[e](t);
        }),
        (r.prototype.bindResize = function () {
          this.isResizeBound || (e.bind(t, "resize", this), (this.isResizeBound = !0));
        }),
        (r.prototype.unbindResize = function () {
          this.isResizeBound && e.unbind(t, "resize", this), (this.isResizeBound = !1);
        }),
        (r.prototype.onresize = function () {
          function t() {
            e.resize(), delete e.resizeTimeout;
          }
          this.resizeTimeout && clearTimeout(this.resizeTimeout);
          var e = this;
          this.resizeTimeout = setTimeout(t, 100);
        }),
        (r.prototype.resize = function () {
          this.isResizeBound && this.needsResizeLayout() && this.layout();
        }),
        (r.prototype.needsResizeLayout = function () {
          var t = o(this.element),
            e = this.size && t;
          return e && t.innerWidth !== this.size.innerWidth;
        }),
        (r.prototype.addItems = function (t) {
          var e = this._itemize(t);
          return e.length && (this.items = this.items.concat(e)), e;
        }),
        (r.prototype.appended = function (t) {
          var e = this.addItems(t);
          e.length && (this.layoutItems(e, !0), this.reveal(e));
        }),
        (r.prototype.prepended = function (t) {
          var e = this._itemize(t);
          if (e.length) {
            var i = this.items.slice(0);
            (this.items = e.concat(i)), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i);
          }
        }),
        (r.prototype.reveal = function (t) {
          this._emitCompleteOnItems("reveal", t);
          for (var e = t && t.length, i = 0; e && e > i; i++) {
            var o = t[i];
            o.reveal();
          }
        }),
        (r.prototype.hide = function (t) {
          this._emitCompleteOnItems("hide", t);
          for (var e = t && t.length, i = 0; e && e > i; i++) {
            var o = t[i];
            o.hide();
          }
        }),
        (r.prototype.revealItemElements = function (t) {
          var e = this.getItems(t);
          this.reveal(e);
        }),
        (r.prototype.hideItemElements = function (t) {
          var e = this.getItems(t);
          this.hide(e);
        }),
        (r.prototype.getItem = function (t) {
          for (var e = 0, i = this.items.length; i > e; e++) {
            var o = this.items[e];
            if (o.element === t) return o;
          }
        }),
        (r.prototype.getItems = function (t) {
          t = n.makeArray(t);
          for (var e = [], i = 0, o = t.length; o > i; i++) {
            var s = t[i],
              r = this.getItem(s);
            r && e.push(r);
          }
          return e;
        }),
        (r.prototype.remove = function (t) {
          var e = this.getItems(t);
          if ((this._emitCompleteOnItems("remove", e), e && e.length))
            for (var i = 0, o = e.length; o > i; i++) {
              var s = e[i];
              s.remove(), n.removeFrom(this.items, s);
            }
        }),
        (r.prototype.destroy = function () {
          var t = this.element.style;
          (t.height = ""), (t.position = ""), (t.width = "");
          for (var e = 0, i = this.items.length; i > e; e++) {
            var o = this.items[e];
            o.destroy();
          }
          this.unbindResize();
          var n = this.element.outlayerGUID;
          delete u[n], delete this.element.outlayerGUID, l && l.removeData(this.element, this.constructor.namespace);
        }),
        (r.data = function (t) {
          t = n.getQueryElement(t);
          var e = t && t.outlayerGUID;
          return e && u[e];
        }),
        (r.create = function (t, e) {
          function i() {
            r.apply(this, arguments);
          }
          return (
            Object.create ? (i.prototype = Object.create(r.prototype)) : n.extend(i.prototype, r.prototype),
              (i.prototype.constructor = i),
              (i.defaults = n.extend({}, r.defaults)),
              n.extend(i.defaults, e),
              (i.prototype.settings = {}),
              (i.namespace = t),
              (i.data = r.data),
              (i.Item = function () {
                s.apply(this, arguments);
              }),
              (i.Item.prototype = new s()),
              n.htmlInit(i, t),
            l && l.bridget && l.bridget(t, i),
              i
          );
        }),
        (r.Item = s),
        r
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("isotope/js/item", ["outlayer/outlayer"], e)
      : "object" == typeof exports
      ? (module.exports = e(require("outlayer")))
      : ((t.Isotope = t.Isotope || {}), (t.Isotope.Item = e(t.Outlayer)));
  })(window, function (t) {
    "use strict";
    function e() {
      t.Item.apply(this, arguments);
    }
    (e.prototype = new t.Item()),
      (e.prototype._create = function () {
        (this.id = this.layout.itemGUID++), t.Item.prototype._create.call(this), (this.sortData = {});
      }),
      (e.prototype.updateSortData = function () {
        if (!this.isIgnored) {
          (this.sortData.id = this.id), (this.sortData["original-order"] = this.id), (this.sortData.random = Math.random());
          var t = this.layout.options.getSortData,
            e = this.layout._sorters;
          for (var i in t) {
            var o = e[i];
            this.sortData[i] = o(this.element, this);
          }
        }
      });
    var i = e.prototype.destroy;
    return (
      (e.prototype.destroy = function () {
        i.apply(this, arguments),
          this.css({
            display: "",
          });
      }),
        e
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("isotope/js/layout-mode", ["get-size/get-size", "outlayer/outlayer"], e)
      : "object" == typeof exports
      ? (module.exports = e(require("get-size"), require("outlayer")))
      : ((t.Isotope = t.Isotope || {}), (t.Isotope.LayoutMode = e(t.getSize, t.Outlayer)));
  })(window, function (t, e) {
    "use strict";
    function i(t) {
      (this.isotope = t), t && ((this.options = t.options[this.namespace]), (this.element = t.element), (this.items = t.filteredItems), (this.size = t.size));
    }
    return (
      (function () {
        function t(t) {
          return function () {
            return e.prototype[t].apply(this.isotope, arguments);
          };
        }
        for (var o = ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout"], n = 0, s = o.length; s > n; n++) {
          var r = o[n];
          i.prototype[r] = t(r);
        }
      })(),
        (i.prototype.needsVerticalResizeLayout = function () {
          var e = t(this.isotope.element),
            i = this.isotope.size && e;
          return i && e.innerHeight != this.isotope.size.innerHeight;
        }),
        (i.prototype._getMeasurement = function () {
          this.isotope._getMeasurement.apply(this, arguments);
        }),
        (i.prototype.getColumnWidth = function () {
          this.getSegmentSize("column", "Width");
        }),
        (i.prototype.getRowHeight = function () {
          this.getSegmentSize("row", "Height");
        }),
        (i.prototype.getSegmentSize = function (t, e) {
          var i = t + e,
            o = "outer" + e;
          if ((this._getMeasurement(i, o), !this[i])) {
            var n = this.getFirstItemSize();
            this[i] = (n && n[o]) || this.isotope.size["inner" + e];
          }
        }),
        (i.prototype.getFirstItemSize = function () {
          var e = this.isotope.filteredItems[0];
          return e && e.element && t(e.element);
        }),
        (i.prototype.layout = function () {
          this.isotope.layout.apply(this.isotope, arguments);
        }),
        (i.prototype.getSize = function () {
          this.isotope.getSize(), (this.size = this.isotope.size);
        }),
        (i.modes = {}),
        (i.create = function (t, e) {
          function o() {
            i.apply(this, arguments);
          }
          return (o.prototype = new i()), e && (o.options = e), (o.prototype.namespace = t), (i.modes[t] = o), o;
        }),
        i
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("masonry/masonry", ["outlayer/outlayer", "get-size/get-size", "fizzy-ui-utils/utils"], e)
      : "object" == typeof exports
      ? (module.exports = e(require("outlayer"), require("get-size"), require("fizzy-ui-utils")))
      : (t.Masonry = e(t.Outlayer, t.getSize, t.fizzyUIUtils));
  })(window, function (t, e, i) {
    var o = t.create("masonry");
    return (
      (o.prototype._resetLayout = function () {
        this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns();
        var t = this.cols;
        for (this.colYs = []; t--; ) this.colYs.push(0);
        this.maxY = 0;
      }),
        (o.prototype.measureColumns = function () {
          if ((this.getContainerWidth(), !this.columnWidth)) {
            var t = this.items[0],
              i = t && t.element;
            this.columnWidth = (i && e(i).outerWidth) || this.containerWidth;
          }
          var o = (this.columnWidth += this.gutter),
            n = this.containerWidth + this.gutter,
            s = n / o,
            r = o - (n % o),
            a = r && 1 > r ? "round" : "floor";
          (s = Math[a](s)), (this.cols = Math.max(s, 1));
        }),
        (o.prototype.getContainerWidth = function () {
          var t = this.options.isFitWidth ? this.element.parentNode : this.element,
            i = e(t);
          this.containerWidth = i && i.innerWidth;
        }),
        (o.prototype._getItemLayoutPosition = function (t) {
          t.getSize();
          var e = t.size.outerWidth % this.columnWidth,
            o = e && 1 > e ? "round" : "ceil",
            n = Math[o](t.size.outerWidth / this.columnWidth);
          n = Math.min(n, this.cols);
          for (
            var s = this._getColGroup(n),
              r = Math.min.apply(Math, s),
              a = i.indexOf(s, r),
              l = {
                x: this.columnWidth * a,
                y: r,
              },
              d = r + t.size.outerHeight,
              c = this.cols + 1 - s.length,
              u = 0;
            c > u;
            u++
          )
            this.colYs[a + u] = d;
          return l;
        }),
        (o.prototype._getColGroup = function (t) {
          if (2 > t) return this.colYs;
          for (var e = [], i = this.cols + 1 - t, o = 0; i > o; o++) {
            var n = this.colYs.slice(o, o + t);
            e[o] = Math.max.apply(Math, n);
          }
          return e;
        }),
        (o.prototype._manageStamp = function (t) {
          var i = e(t),
            o = this._getElementOffset(t),
            n = this.options.isOriginLeft ? o.left : o.right,
            s = n + i.outerWidth,
            r = Math.floor(n / this.columnWidth);
          r = Math.max(0, r);
          var a = Math.floor(s / this.columnWidth);
          (a -= s % this.columnWidth ? 0 : 1), (a = Math.min(this.cols - 1, a));
          for (var l = (this.options.isOriginTop ? o.top : o.bottom) + i.outerHeight, d = r; a >= d; d++) this.colYs[d] = Math.max(l, this.colYs[d]);
        }),
        (o.prototype._getContainerSize = function () {
          this.maxY = Math.max.apply(Math, this.colYs);
          var t = {
            height: this.maxY,
          };
          return this.options.isFitWidth && (t.width = this._getContainerFitWidth()), t;
        }),
        (o.prototype._getContainerFitWidth = function () {
          for (var t = 0, e = this.cols; --e && 0 === this.colYs[e]; ) t++;
          return (this.cols - t) * this.columnWidth - this.gutter;
        }),
        (o.prototype.needsResizeLayout = function () {
          var t = this.containerWidth;
          return this.getContainerWidth(), t !== this.containerWidth;
        }),
        o
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define("isotope/js/layout-modes/masonry", ["../layout-mode", "masonry/masonry"], e)
      : "object" == typeof exports
      ? (module.exports = e(require("../layout-mode"), require("masonry-layout")))
      : e(t.Isotope.LayoutMode, t.Masonry);
  })(window, function (t, e) {
    "use strict";
    function i(t, e) {
      for (var i in e) t[i] = e[i];
      return t;
    }
    var o = t.create("masonry"),
      n = o.prototype._getElementOffset,
      s = o.prototype.layout,
      r = o.prototype._getMeasurement;
    i(o.prototype, e.prototype), (o.prototype._getElementOffset = n), (o.prototype.layout = s), (o.prototype._getMeasurement = r);
    var a = o.prototype.measureColumns;
    o.prototype.measureColumns = function () {
      (this.items = this.isotope.filteredItems), a.call(this);
    };
    var l = o.prototype._manageStamp;
    return (
      (o.prototype._manageStamp = function () {
        (this.options.isOriginLeft = this.isotope.options.isOriginLeft), (this.options.isOriginTop = this.isotope.options.isOriginTop), l.apply(this, arguments);
      }),
        o
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/layout-modes/fit-rows", ["../layout-mode"], e) : "object" == typeof exports ? (module.exports = e(require("../layout-mode"))) : e(t.Isotope.LayoutMode);
  })(window, function (t) {
    "use strict";
    var e = t.create("fitRows");
    return (
      (e.prototype._resetLayout = function () {
        (this.x = 0), (this.y = 0), (this.maxY = 0), this._getMeasurement("gutter", "outerWidth");
      }),
        (e.prototype._getItemLayoutPosition = function (t) {
          t.getSize();
          var e = t.size.outerWidth + this.gutter,
            i = this.isotope.size.innerWidth + this.gutter;
          0 !== this.x && e + this.x > i && ((this.x = 0), (this.y = this.maxY));
          var o = {
            x: this.x,
            y: this.y,
          };
          return (this.maxY = Math.max(this.maxY, this.y + t.size.outerHeight)), (this.x += e), o;
        }),
        (e.prototype._getContainerSize = function () {
          return {
            height: this.maxY,
          };
        }),
        e
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define("isotope/js/layout-modes/vertical", ["../layout-mode"], e) : "object" == typeof exports ? (module.exports = e(require("../layout-mode"))) : e(t.Isotope.LayoutMode);
  })(window, function (t) {
    "use strict";
    var e = t.create("vertical", {
      horizontalAlignment: 0,
    });
    return (
      (e.prototype._resetLayout = function () {
        this.y = 0;
      }),
        (e.prototype._getItemLayoutPosition = function (t) {
          t.getSize();
          var e = (this.isotope.size.innerWidth - t.size.outerWidth) * this.options.horizontalAlignment,
            i = this.y;
          return (
            (this.y += t.size.outerHeight),
              {
                x: e,
                y: i,
              }
          );
        }),
        (e.prototype._getContainerSize = function () {
          return {
            height: this.y,
          };
        }),
        e
    );
  }),
  (function (t, e) {
    "use strict";
    "function" == typeof define && define.amd
      ? define([
        "outlayer/outlayer",
        "get-size/get-size",
        "matches-selector/matches-selector",
        "fizzy-ui-utils/utils",
        "isotope/js/item",
        "isotope/js/layout-mode",
        "isotope/js/layout-modes/masonry",
        "isotope/js/layout-modes/fit-rows",
        "isotope/js/layout-modes/vertical",
      ], function (i, o, n, s, r, a) {
        return e(t, i, o, n, s, r, a);
      })
      : "object" == typeof exports
      ? (module.exports = e(
        t,
        require("outlayer"),
        require("get-size"),
        require("desandro-matches-selector"),
        require("fizzy-ui-utils"),
        require("./item"),
        require("./layout-mode"),
        require("./layout-modes/masonry"),
        require("./layout-modes/fit-rows"),
        require("./layout-modes/vertical")
      ))
      : (t.Isotope = e(t, t.Outlayer, t.getSize, t.matchesSelector, t.fizzyUIUtils, t.Isotope.Item, t.Isotope.LayoutMode));
  })(window, function (t, e, i, o, n, s, r) {
    function a(t, e) {
      return function (i, o) {
        for (var n = 0, s = t.length; s > n; n++) {
          var r = t[n],
            a = i.sortData[r],
            l = o.sortData[r];
          if (a > l || l > a) {
            var d = void 0 !== e[r] ? e[r] : e,
              c = d ? 1 : -1;
            return (a > l ? 1 : -1) * c;
          }
        }
        return 0;
      };
    }
    var l = t.jQuery,
      d = String.prototype.trim
        ? function (t) {
          return t.trim();
        }
        : function (t) {
          return t.replace(/^\s+|\s+$/g, "");
        },
      c = document.documentElement,
      u = c.textContent
        ? function (t) {
          return t.textContent;
        }
        : function (t) {
          return t.innerText;
        },
      p = e.create("isotope", {
        layoutMode: "masonry",
        isJQueryFiltering: !0,
        sortAscending: !0,
      });
    (p.Item = s),
      (p.LayoutMode = r),
      (p.prototype._create = function () {
        (this.itemGUID = 0), (this._sorters = {}), this._getSorters(), e.prototype._create.call(this), (this.modes = {}), (this.filteredItems = this.items), (this.sortHistory = ["original-order"]);
        for (var t in r.modes) this._initLayoutMode(t);
      }),
      (p.prototype.reloadItems = function () {
        (this.itemGUID = 0), e.prototype.reloadItems.call(this);
      }),
      (p.prototype._itemize = function () {
        for (var t = e.prototype._itemize.apply(this, arguments), i = 0, o = t.length; o > i; i++) {
          var n = t[i];
          n.id = this.itemGUID++;
        }
        return this._updateItemsSortData(t), t;
      }),
      (p.prototype._initLayoutMode = function (t) {
        var e = r.modes[t],
          i = this.options[t] || {};
        (this.options[t] = e.options ? n.extend(e.options, i) : i), (this.modes[t] = new e(this));
      }),
      (p.prototype.layout = function () {
        return !this._isLayoutInited && this.options.isInitLayout ? void this.arrange() : void this._layout();
      }),
      (p.prototype._layout = function () {
        var t = this._getIsInstant();
        this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, t), (this._isLayoutInited = !0);
      }),
      (p.prototype.arrange = function (t) {
        function e() {
          o.reveal(i.needReveal), o.hide(i.needHide);
        }
        this.option(t), this._getIsInstant();
        var i = this._filter(this.items);
        this.filteredItems = i.matches;
        var o = this;
        this._bindArrangeComplete(), this._isInstant ? this._noTransition(e) : e(), this._sort(), this._layout();
      }),
      (p.prototype._init = p.prototype.arrange),
      (p.prototype._getIsInstant = function () {
        var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
        return (this._isInstant = t), t;
      }),
      (p.prototype._bindArrangeComplete = function () {
        function t() {
          e && i && o && n.dispatchEvent("arrangeComplete", null, [n.filteredItems]);
        }
        var e,
          i,
          o,
          n = this;
        this.once("layoutComplete", function () {
          (e = !0), t();
        }),
          this.once("hideComplete", function () {
            (i = !0), t();
          }),
          this.once("revealComplete", function () {
            (o = !0), t();
          });
      }),
      (p.prototype._filter = function (t) {
        var e = this.options.filter;
        e = e || "*";
        for (var i = [], o = [], n = [], s = this._getFilterTest(e), r = 0, a = t.length; a > r; r++) {
          var l = t[r];
          if (!l.isIgnored) {
            var d = s(l);
            d && i.push(l), d && l.isHidden ? o.push(l) : d || l.isHidden || n.push(l);
          }
        }
        return {
          matches: i,
          needReveal: o,
          needHide: n,
        };
      }),
      (p.prototype._getFilterTest = function (t) {
        return l && this.options.isJQueryFiltering
          ? function (e) {
            return l(e.element).is(t);
          }
          : "function" == typeof t
            ? function (e) {
              return t(e.element);
            }
            : function (e) {
              return o(e.element, t);
            };
      }),
      (p.prototype.updateSortData = function (t) {
        var e;
        t ? ((t = n.makeArray(t)), (e = this.getItems(t))) : (e = this.items), this._getSorters(), this._updateItemsSortData(e);
      }),
      (p.prototype._getSorters = function () {
        var t = this.options.getSortData;
        for (var e in t) {
          var i = t[e];
          this._sorters[e] = h(i);
        }
      }),
      (p.prototype._updateItemsSortData = function (t) {
        for (var e = t && t.length, i = 0; e && e > i; i++) {
          var o = t[i];
          o.updateSortData();
        }
      });
    var h = (function () {
      function t(t) {
        if ("string" != typeof t) return t;
        var i = d(t).split(" "),
          o = i[0],
          n = o.match(/^\[(.+)\]$/),
          s = n && n[1],
          r = e(s, o),
          a = p.sortDataParsers[i[1]];
        return (t = a
          ? function (t) {
            return t && a(r(t));
          }
          : function (t) {
            return t && r(t);
          });
      }
      function e(t, e) {
        var i;
        return (i = t
          ? function (e) {
            return e.getAttribute(t);
          }
          : function (t) {
            var i = t.querySelector(e);
            return i && u(i);
          });
      }
      return t;
    })();
    (p.sortDataParsers = {
      parseInt: function (t) {
        return parseInt(t, 10);
      },
      parseFloat: function (t) {
        return parseFloat(t);
      },
    }),
      (p.prototype._sort = function () {
        var t = this.options.sortBy;
        if (t) {
          var e = [].concat.apply(t, this.sortHistory),
            i = a(e, this.options.sortAscending);
          this.filteredItems.sort(i), t != this.sortHistory[0] && this.sortHistory.unshift(t);
        }
      }),
      (p.prototype._mode = function () {
        var t = this.options.layoutMode,
          e = this.modes[t];
        if (!e) throw new Error("No layout mode: " + t);
        return (e.options = this.options[t]), e;
      }),
      (p.prototype._resetLayout = function () {
        e.prototype._resetLayout.call(this), this._mode()._resetLayout();
      }),
      (p.prototype._getItemLayoutPosition = function (t) {
        return this._mode()._getItemLayoutPosition(t);
      }),
      (p.prototype._manageStamp = function (t) {
        this._mode()._manageStamp(t);
      }),
      (p.prototype._getContainerSize = function () {
        return this._mode()._getContainerSize();
      }),
      (p.prototype.needsResizeLayout = function () {
        return this._mode().needsResizeLayout();
      }),
      (p.prototype.appended = function (t) {
        var e = this.addItems(t);
        if (e.length) {
          var i = this._filterRevealAdded(e);
          this.filteredItems = this.filteredItems.concat(i);
        }
      }),
      (p.prototype.prepended = function (t) {
        var e = this._itemize(t);
        if (e.length) {
          this._resetLayout(), this._manageStamps();
          var i = this._filterRevealAdded(e);
          this.layoutItems(this.filteredItems), (this.filteredItems = i.concat(this.filteredItems)), (this.items = e.concat(this.items));
        }
      }),
      (p.prototype._filterRevealAdded = function (t) {
        var e = this._filter(t);
        return this.hide(e.needHide), this.reveal(e.matches), this.layoutItems(e.matches, !0), e.matches;
      }),
      (p.prototype.insert = function (t) {
        var e = this.addItems(t);
        if (e.length) {
          var i,
            o,
            n = e.length;
          for (i = 0; n > i; i++) (o = e[i]), this.element.appendChild(o.element);
          var s = this._filter(e).matches;
          for (i = 0; n > i; i++) e[i].isLayoutInstant = !0;
          for (this.arrange(), i = 0; n > i; i++) delete e[i].isLayoutInstant;
          this.reveal(s);
        }
      });
    var f = p.prototype.remove;
    return (
      (p.prototype.remove = function (t) {
        t = n.makeArray(t);
        var e = this.getItems(t);
        f.call(this, t);
        var i = e && e.length;
        if (i)
          for (var o = 0; i > o; o++) {
            var s = e[o];
            n.removeFrom(this.filteredItems, s);
          }
      }),
        (p.prototype.shuffle = function () {
          for (var t = 0, e = this.items.length; e > t; t++) {
            var i = this.items[t];
            i.sortData.random = Math.random();
          }
          (this.options.sortBy = "random"), this._sort(), this._layout();
        }),
        (p.prototype._noTransition = function (t) {
          var e = this.options.transitionDuration;
          this.options.transitionDuration = 0;
          var i = t.call(this);
          return (this.options.transitionDuration = e), i;
        }),
        (p.prototype.getFilteredItemElements = function () {
          for (var t = [], e = 0, i = this.filteredItems.length; i > e; e++) t.push(this.filteredItems[e].element);
          return t;
        }),
        p
    );
  }),
  jQuery(document).ready(function (t) {
    var e = t(".jobs-hero__listing");
    e.length &&
    (e.on("afterChange", function (i, o, n) {
      var s = t("[data-slick-index='" + n + "']"),
        r = s.data("theme"),
        a = t("#masthead, #site-navigation, .sliding-panel-button");
      "light" === r ? (t("body").attr("data-nav-color", ""), a.removeClass("alt"), e.addClass("alt")) : (t("body").attr("data-nav-color", "alt"), a.addClass("alt"), e.removeClass("alt"));
    }),
      e.slick({
        dots: !0,
        arrows: !1,
        autoplay: !0,
        autoplaySpeed: 4e3,
      }));
  }),
  (function (t) {
    (t.fn.quovolver = function (e) {
      "use strict";
      var i = t.extend({}, t.fn.quovolver.defaults, e);
      return this.each(function () {
        function e(e) {
          if (g.is(":animated") || h.is(":animated")) return !1;
          if (v.is(":hidden")) return !1;
          e < 1 ? (e = w) : e > w && (e = 1);
          var i = {
            current: t(g[y - 1]),
            upcoming: t(g[e - 1]),
          };
          (i.currentHeight = n(i.current, "height")),
            (i.upcomingHeight = n(i.upcoming, "height")),
            (i.currentOuterHeight = n(i.current, "outerHeight")),
            (i.upcomingOuterHeight = n(i.upcoming, "outerHeight")),
            (i.currentWidth = n(i.current, "width")),
            (i.upcomingWidth = n(i.upcoming, "width")),
            (i.currentOuterWidth = n(i.current, "outerWidth")),
            (i.upcomingOuterWidth = n(i.upcoming, "outerWidth"));
          var o = typeof window[f.transition];
          return "basic" !== f.transition && "string" == typeof f.transition && "function" === o ? f.transition(goToData) : p(i), (y = e), r(b), a(b), !1;
        }
        function o() {
          var e;
          if (
            (("above" !== f.navPosition && "both" !== f.navPosition) || (v.prepend('<div class="quovolve-nav quovolve-nav-above"></div>'), (e = v.find(".quovolve-nav"))),
            ("below" !== f.navPosition && "both" !== f.navPosition) || (v.append('<div class="quovolve-nav quovolve-nav-below"></div>'), (e = v.find(".quovolve-nav"))),
            "custom" === f.navPosition &&
            "" !== f.navPositionCustom &&
            0 !== t(f.navPositionCustom).length &&
            (t(f.navPositionCustom).append('<div class="quovolve-nav quovolve-nav-custom"></div>'), (e = t(f.navPositionCustom).find(".quovolve-nav"))),
            f.navPrev && e.append('<span class="nav-prev"><a href="#">' + f.navPrevText + "</a></span>"),
            f.navNext && e.append('<span class="nav-next"><a href="#">' + f.navNextText + "</a></span>"),
              f.navNum)
          ) {
            e.append('<ol class="nav-numbers"></ol>');
            for (var i = 1; i < w + 1; i++) e.find(".nav-numbers").append('<li><a href="#item-' + i + '">' + i + "</a></li>");
            r(e);
          }
          return f.navText && (e.append('<span class="nav-text"></span>'), a(e)), e;
        }
        function n(e, i) {
          var o = e[i]();
          if (!o || 0 === o) {
            var n = e.parents().andSelf().filter(":hidden");
            n.each(function () {
              (this.oDisplay = this.style.display), t(this).show();
            }),
              (o = e[i]()),
              n.each(function () {
                this.style.display = this.oDisplay;
              });
          }
          return o;
        }
        function s(e) {
          var i = 0;
          e.height("auto"),
            e.each(function () {
              var e;
              (e = t(this).is(":visible") ? t(this).height() : n(t(this), "height")), e > i && (i = e);
            }),
            e.height(i);
        }
        function r(t) {
          f.navEnabled &&
          (t.find(".nav-numbers li").removeClass("active"),
            t
            .find('.nav-numbers a[href="#item-' + y + '"]')
            .parent()
            .addClass("active"));
        }
        function a(t) {
          if (f.navEnabled) {
            var e = f.navTextContent.replace("@a", y).replace("@b", w);
            t.find(".nav-text").text(e);
          }
        }
        function l() {
          var t = "auto" == f.autoPlaySpeed ? 25 * g[y - 1].textLength + 2e3 : f.autoPlaySpeed;
          v.addClass("play"),
            clearTimeout(k),
            (k = setTimeout(function () {
              e(y + 1), l();
            }, t));
        }
        function d() {
          f.stopAutoPlay !== !0 &&
          v.hover(
            function () {
              v.addClass("pause").removeClass("play"), clearTimeout(k);
            },
            function () {
              v.removeClass("pause").addClass("play"), clearTimeout(k), l();
            }
          );
        }
        function c() {
          v.hover(
            function () {
              v.addClass("stop").removeClass("play"), clearTimeout(k);
            },
            function () {}
          );
        }
        function u(t) {
          clearTimeout(k), e(t), f.autoPlay && l();
        }
        function p(t) {
          h.css("height", t.upcomingOuterHeight), t.current.hide(), t.upcoming.show(), f.equalHeight === !1 && h.css("height", "auto");
        }
        var h = t(this),
          f = t.meta ? t.extend({}, i, h.data()) : i;
        h.addClass("quovolve")
        .css({
          position: "relative",
        })
        .wrap('<div class="quovolve-box"></div>');
        var m;
        m = f.children ? "find" : "children";
        var v = h.parent(".quovolve-box"),
          g = h[m](f.children),
          y = 1,
          w = g.length;
        if ((g.hide().filter(":first").show(), f.navPrev || f.navNext || f.navNum || f.navText)) {
          f.navEnabled = !0;
          var b = o();
        } else f.navEnabled = !1;
        if (
          (f.equalHeight &&
          (s(g),
            t(window).resize(function () {
              s(g), h.css("height", t(g[y - 1]).outerHeight());
            })),
            f.autoPlay)
        ) {
          "auto" == f.autoPlaySpeed &&
          g.each(function () {
            this.textLength = t(this).text().length;
          });
          var k;
          l(), f.stopOnHover ? c() : f.pauseOnHover && d();
        }
        t(".nav-prev a", v).click(function () {
          return u(y - 1), !1;
        }),
          t(".nav-next a", v).click(function () {
            return u(y + 1), !1;
          }),
          t(".nav-numbers a", v).click(function () {
            return u(t(this).text()), !1;
          }),
          t(this).bind("goto", function (t, e) {
            u(e);
          });
      });
    }),
      (t.fn.quovolver.defaults = {
        children: "",
        transition: "fade",
        transitionSpeed: 300,
        autoPlay: !0,
        autoPlaySpeed: "auto",
        pauseOnHover: !0,
        stopOnHover: !1,
        equalHeight: !0,
        navPosition: "above",
        navPositionCustom: "",
        navPrev: !1,
        navNext: !1,
        navNum: !1,
        navText: !1,
        navPrevText: "Prev",
        navNextText: "Next",
        navTextContent: "@a / @b",
      });
  })(jQuery),
  jQuery(document).ready(function (t) {
    t("div.quotes").quovolver(t);
  }),
  jQuery(document).ready(function (t) {
    t("div#page").removeClass("no-js");
  }),
  jQuery(document).ready(function (t) {
    function e() {
      return "ontouchstart" in window || navigator.MaxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
    }
    function i() {
      t("#page-front-page .corner-play-button").hide(),
        t("#bgvid-front-page").hide(),
        t("#page-front-page #mobile-video-replacement").show().addClass("visible"),
        t("#page-front-page .marietv-placeholder, #page-front-page .marietv-placeholder-sm").addClass("visible");
    }
    var o = t("#logo"),
      n = !1,
      s = [
        "make some noise",
        "make magic happen",
        "make yourself heard",
        "make time for you",
        "make history",
        "make it count",
        "make a bold move",
        "make mama some money",
        "make me a drank",
        "make that hotline bling",
        "make it rain",
        "make a difference",
        "make your dreams real",
        "make your move",
        "make it outfuckingstanding",
        "make it outstanding",
        "make mashed potatoes",
        "make connections",
        "make guacamole",
        "make your art",
        "make’m want more",
        "make success inevitable",
        "make a leap of faith",
        "make someone laugh",
        "make silly faces",
        "make misteaks mistakes often",
        "make work you love",
        "make money, change the world",
        "make dreams come true",
        "make peace",
        "make weird art",
        "make poo jokes",
        "make a ruckus",
        "make coffee",
        "make more art",
        "make it simple",
        "make the internet kinder",
        "make yourself proud",
        "make time for what matters",
        "make fart noises",
        "make memories",
        "make some magic",
        "make make-believe",
        "make work into play",
        "make new things",
        "make banana pancakes",
        "make it dirty",
        "make it matter",
        "make your soul happy",
        "make new friends",
        "make it happen",
        "make it unforgettable",
        "making meaning",
      ],
      r = 'make <span style="text-decoration: line-through;">misteaks</span> mistakes often.',
      a = "make misteaks mistakes often.",
      l = function (t) {
        if (n === !1) {
          o.off(), (n = Math.floor(Math.random() * s.length));
          var e = s[n];
          c(e);
        }
      },
      d = function (t) {
        c("marie forleo");
      };
    e() || o.on("mouseenter", l).on("mouseleave", d), t("#page-front-page").length > 0 && t(window).innerWidth() <= 768 && i();
    var c = function (t) {
        if ((o.html() == r && o.html(a), o.html().length > 2)) {
          o.html(o.html().slice(0, -2) + ".");
          setTimeout(function () {
            c(t);
          }, 5);
        } else u(t, 1);
      },
      u = function (t, e) {
        if (e < t.length) {
          o.html(o.html().slice(0, -1) + t.charAt(e) + "."), (e += 1);
          setTimeout(function () {
            u(t, e);
          }, 5);
        } else o.html() == a && o.html(r), (n = !1), o.on("mouseenter", l).on("mouseleave", d);
      };
  });
var Mailcheck = {
  domainThreshold: 2,
  secondLevelThreshold: 2,
  topLevelThreshold: 2,
  defaultDomains: [
    "msn.com",
    "bellsouth.net",
    "telus.net",
    "comcast.net",
    "optusnet.com.au",
    "earthlink.net",
    "qq.com",
    "sky.com",
    "icloud.com",
    "mac.com",
    "sympatico.ca",
    "googlemail.com",
    "att.net",
    "xtra.co.nz",
    "web.de",
    "cox.net",
    "gmail.com",
    "ymail.com",
    "aim.com",
    "rogers.com",
    "verizon.net",
    "rocketmail.com",
    "google.com",
    "optonline.net",
    "sbcglobal.net",
    "aol.com",
    "me.com",
    "btinternet.com",
    "charter.net",
    "shaw.ca",
  ],
  defaultSecondLevelDomains: ["yahoo", "hotmail", "mail", "live", "outlook", "gmx"],
  defaultTopLevelDomains: [
    "com",
    "com.au",
    "com.tw",
    "ca",
    "co.nz",
    "co.uk",
    "de",
    "fr",
    "it",
    "ru",
    "net",
    "org",
    "edu",
    "gov",
    "jp",
    "nl",
    "kr",
    "se",
    "eu",
    "ie",
    "co.il",
    "us",
    "at",
    "be",
    "dk",
    "hk",
    "es",
    "gr",
    "ch",
    "no",
    "cz",
    "in",
    "net",
    "net.au",
    "info",
    "biz",
    "mil",
    "co.jp",
    "sg",
    "hu",
    "uk",
  ],
  run: function (t) {
    (t.domains = t.domains || Mailcheck.defaultDomains),
      (t.secondLevelDomains = t.secondLevelDomains || Mailcheck.defaultSecondLevelDomains),
      (t.topLevelDomains = t.topLevelDomains || Mailcheck.defaultTopLevelDomains),
      (t.distanceFunction = t.distanceFunction || Mailcheck.sift4Distance);
    var e = function (t) {
        return t;
      },
      i = t.suggested || e,
      o = t.empty || e,
      n = Mailcheck.suggest(Mailcheck.encodeEmail(t.email), t.domains, t.secondLevelDomains, t.topLevelDomains, t.distanceFunction);
    return n ? i(n) : o();
  },
  suggest: function (t, e, i, o, n) {
    t = t.toLowerCase();
    var s = this.splitEmail(t);
    if (i && o && -1 !== i.indexOf(s.secondLevelDomain) && -1 !== o.indexOf(s.topLevelDomain)) return !1;
    var r = this.findClosestDomain(s.domain, e, n, this.domainThreshold);
    if (r)
      return (
        r != s.domain && {
          address: s.address,
          domain: r,
          full: s.address + "@" + r,
        }
      );
    var a = this.findClosestDomain(s.secondLevelDomain, i, n, this.secondLevelThreshold),
      l = this.findClosestDomain(s.topLevelDomain, o, n, this.topLevelThreshold);
    if (s.domain) {
      r = s.domain;
      var d = !1;
      if ((a && a != s.secondLevelDomain && ((r = r.replace(s.secondLevelDomain, a)), (d = !0)), l && l != s.topLevelDomain && "" !== s.secondLevelDomain && ((r = r.replace(new RegExp(s.topLevelDomain + "$"), l)), (d = !0)), d))
        return {
          address: s.address,
          domain: r,
          full: s.address + "@" + r,
        };
    }
    return !1;
  },
  findClosestDomain: function (t, e, i, o) {
    o = o || this.topLevelThreshold;
    var n,
      s = 1 / 0,
      r = null;
    if (!t || !e) return !1;
    i || (i = this.sift4Distance);
    for (var a = 0; a < e.length; a++) {
      if (t === e[a]) return t;
      (n = i(t, e[a])), s > n && ((s = n), (r = e[a]));
    }
    return o >= s && null !== r && r;
  },
  sift4Distance: function (t, e, i) {
    if ((void 0 === i && (i = 5), !t || !t.length)) return e ? e.length : 0;
    if (!e || !e.length) return t.length;
    for (var o = t.length, n = e.length, s = 0, r = 0, a = 0, l = 0, d = 0, c = []; o > s && n > r; ) {
      if (t.charAt(s) == e.charAt(r)) {
        l++;
        for (var u = !1, p = 0; p < c.length; ) {
          var h = c[p];
          if (s <= h.c1 || r <= h.c2) {
            (u = Math.abs(r - s) >= Math.abs(h.c2 - h.c1)), u ? d++ : h.trans || ((h.trans = !0), d++);
            break;
          }
          s > h.c2 && r > h.c1 ? c.splice(p, 1) : p++;
        }
        c.push({
          c1: s,
          c2: r,
          trans: u,
        });
      } else {
        (a += l), (l = 0), s != r && (s = r = Math.min(s, r));
        for (var f = 0; i > f && (o > s + f || n > r + f); f++) {
          if (o > s + f && t.charAt(s + f) == e.charAt(r)) {
            (s += f - 1), r--;
            break;
          }
          if (n > r + f && t.charAt(s) == e.charAt(r + f)) {
            s--, (r += f - 1);
            break;
          }
        }
      }
      s++, r++, (s >= o || r >= n) && ((a += l), (l = 0), (s = r = Math.min(s, r)));
    }
    return (a += l), Math.round(Math.max(o, n) - a + d);
  },
  splitEmail: function (t) {
    t = null !== t ? t.replace(/^\s*/, "").replace(/\s*$/, "") : null;
    var e = t.split("@");
    if (e.length < 2) return !1;
    for (var i = 0; i < e.length; i++) if ("" === e[i]) return !1;
    var o = e.pop(),
      n = o.split("."),
      s = "",
      r = "";
    if (0 === n.length) return !1;
    if (1 == n.length) r = n[0];
    else {
      s = n[0];
      for (var a = 1; a < n.length; a++) r += n[a] + ".";
      r = r.substring(0, r.length - 1);
    }
    return {
      topLevelDomain: r,
      secondLevelDomain: s,
      domain: o,
      address: e.join("@"),
    };
  },
  encodeEmail: function (t) {
    var e = encodeURI(t);
    return (e = e.replace("%20", " ").replace("%25", "%").replace("%5E", "^").replace("%60", "`").replace("%7B", "{").replace("%7C", "|").replace("%7D", "}"));
  },
};
"undefined" != typeof module && module.exports && (module.exports = Mailcheck),
"function" == typeof define &&
define.amd &&
define("mailcheck", [], function () {
  return Mailcheck;
}),
"undefined" != typeof window &&
window.jQuery &&
!(function (t) {
  t.fn.mailcheck = function (t) {
    var e = this;
    if (t.suggested) {
      var i = t.suggested;
      t.suggested = function (t) {
        i(e, t);
      };
    }
    if (t.empty) {
      var o = t.empty;
      t.empty = function () {
        o.call(null, e);
      };
    }
    (t.email = this.val()), Mailcheck.run(t);
  };
})(jQuery),
  jQuery(document).ready(function (t) {
    t(".search-marietv input").attr("placeholder", "Search MarieTV");
    var e = t(".marietv-questions-dropdown li:first-child a");
    if (e.length > 0) {
      var o = {
        action: "mf_get_marietv_category",
        cat_slug: e.data("slug"),
      };
      t.post(frontendajax.ajaxurl, o, function (e) {
        var o = jQuery.parseJSON(e),
          n = "";
        for (i = 0; i < o.length; i++)
          n +=
            '<div class="post"><a class="read-more" href="' +
            o[i].permalink +
            '"><div class="thumbnail">' +
            o[i].thumbnail +
            '</div><div class="content"><h4>' +
            o[i].title +
            '</h4><div class="arrow-link">Watch Now </div></div></a></div>';
        t(".have-a-question .posts-content").remove(),
          t(".have-a-question .header").after('<div class="posts-content">' + n + "</div>"),
        t(window).width() < 768 &&
        t("#page-marietv .have-a-question .posts-content").slick({
          arrows: !1,
          autoplay: !1,
          dots: !0,
        }),
          t(document).trigger("marietv-show-question-posts");
      }),
        t(".have-a-question .selected").html(e.text()),
        e.parent().hide();
    }
    t(".marietv-questions-dropdown .dropdown-trigger, .marietv-questions-dropdown .selected").on("click", function (e) {
      t(".marietv-questions-dropdown").toggleClass("open"), e.preventDefault();
    }),
      (window.MFHaveAQuestion = {
        _bindEvents: function () {
          var e = this;
          t(document).on("marietv-questions-close", function () {
            t(".marietv-questions-dropdown").removeClass("open");
          }),
            t(document).on("marietv-questions-fetchposts", function (t) {
              e.fetchPosts(t.questionLink);
            }),
            t(".marietv-questions-dropdown li a").on("click", function (e) {
              e.preventDefault(),
                t(document).trigger("marietv-questions-close"),
                t(document).trigger({
                  type: "marietv-questions-fetchposts",
                  questionLink: t(this),
                });
            });
        },
        adjustQuestionsDropdownList: function (e) {
          t(".marietv-questions-dropdown li").show(), t(".have-a-question .selected").html(e.text()), e.parent().hide();
        },
        buildPostHTML: function (t) {
          var e = "";
          for (i = 0; i < t.length; i++)
            e +=
              '<div class="post"><a class="read-more" href="' +
              t[i].permalink +
              '"><div class="thumbnail">' +
              t[i].thumbnail +
              '</div><div class="content"><h4>' +
              t[i].title +
              '</h4><div class="arrow-link">Watch Now </div></div></a></div>';
          return e;
        },
        ditchOldPosts: function () {
          t(".have-a-question .posts-content").remove();
        },
        fetchPosts: function (e) {
          var i = this,
            o = {
              action: "mf_get_marietv_category",
              cat_slug: e.data("slug"),
            };
          t.post(frontendajax.ajaxurl, o, function (o) {
            var n = t.parseJSON(o),
              s = i.buildPostHTML(n);
            i.ditchOldPosts(), i.adjustQuestionsDropdownList(e), i.showNewPosts(s), t(document).trigger("marietv-show-question-posts");
          });
        },
        showNewPosts: function (e) {
          t(".have-a-question .header").after('<div class="posts-content">' + e + "</div>");
        },
      }),
      window.MFHaveAQuestion._bindEvents(),
      t(".marietv-oracle").on("click", "a.pick-it", function (e) {
        var i = {
          action: "mf_get_marietv_random_post",
        };
        t.post(frontendajax.ajaxurl, i, function (e) {
          var i = jQuery.parseJSON(e),
            o = '<li><a href="' + i[0].permalink + '" target="_blank" class="button button-outline-white">This was it!</a></li><li><a href="#" class="pick-it button button-outline-white">Let\'s try this again</a></li>',
            n = '<h4><a href="' + i[0].permalink + '" target="_blank"><span>' + i[0].title + "</span></a></h4>",
            s = t(".marietv-oracle .magic-ball"),
            r = t(".marietv-oracle .oracle-actions");
          r.fadeOut(500),
            s.fadeOut(500, function () {
              window.setTimeout(function () {
                s.css("background-image", "url(" + i[0].thumbnail + ")"), s.html(n), s.fadeIn(500), r.fadeIn(500), r.html(o);
              }, 500);
            });
        }),
          e.preventDefault();
      });
  }),
  jQuery(document).ready(function (t) {
    t(".m-dot img").hover(
      function () {
        t(this).attr("src", "http://thecopycure.com/wp-content/uploads/2015/04/m.gif");
      },
      function () {
        t(this).attr("src", "http://thecopycure.com/wp-content/uploads/2015/04/m-dot.png");
      }
    );
  }),
  jQuery(document).ready(function (t) {
    function e() {
      t(".modal-state").attr("checked", !1), t(".modal-state:checked").prop("checked", !1).change();
    }
    function i(e) {
      var i = t('video[data-modal-id="' + e + '"]');
      i.length > 0 && i.get(0).play();
    }
    function o() {
      var e = t(".modal video");
      e.length > 0 && e.get(0).pause();
    }
    function n(t) {
      t = t.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var e = new RegExp("[\\?&]" + t + "=([^&#]*)"),
        i = e.exec(location.search);
      return null === i ? "" : decodeURIComponent(i[1].replace(/\+/g, " "));
    }
    t(".modal-trigger").on("click", function (e) {
      if (!t(this).data("optin-slug")) {
        var o,
          n = t(this).data("target");
        t("#" + n).attr("checked", !0),
          t("body").addClass("modal-open"),
          i(n),
          t(this).data("youtube-id")
            ? (o = "https://www.youtube.com/embed/" + t(this).data("youtube-id"))
            : t(this).data("vimeo-id") && (o = "https://player.vimeo.com/video/" + t(this).data("vimeo-id") + "?color=ffffff&title=0&byline=0&portrait=0"),
          t("#" + n)
          .next()
          .find("iframe")
          .attr("src", ""),
          t("#" + n)
          .next()
          .find("iframe")
          .attr("src", o + "?autoplay=1"),
          e.preventDefault();
      }
    }),
      t(".modal-close").on("click", function () {
        var i;
        e(),
          o(),
          t("body").removeClass("modal-open"),
          t(this).data("youtube-id")
            ? (i = "https://www.youtube.com/embed/" + t(this).data("youtube-id"))
            : t(this).data("vimeo-id") && (i = "https://player.vimeo.com/video/" + t(this).data("vimeo-id") + "?color=ffffff&title=0&byline=0&portrait=0"),
          t(this).prev().find("iframe").attr("src", ""),
          t(this).prev().find("iframe").attr("src", i);
      }),
      t(".modal-fade-screen").on("click", function () {
        var i;
        e(),
          o(),
          t("body").removeClass("modal-open"),
          t(this).data("youtube-id")
            ? (i = "https://www.youtube.com/embed/" + t(this).data("youtube-id"))
            : t(this).data("vimeo-id") && (i = "https://player.vimeo.com/video/" + t(this).data("vimeo-id") + "?color=ffffff&title=0&byline=0&portrait=0"),
          t(this).find("iframe").attr("src", ""),
          t(this).find("iframe").attr("src", i);
      }),
      t(".modal-inner").on("click", function (t) {
        t.stopPropagation();
      });
    var s = n("modal");
    s && (t("#modal-" + s).attr("checked", !0), t("body").addClass("modal-open"));
    var r = t(".modal-exit-intent");
    r.length &&
    ouibounce(r[0], {
      callback: function () {
        r.find(".modal-state").attr("checked", !0), t("body").addClass("modal-open");
      },
    });
  }),
  jQuery(document).ready(function (t) {
    t(".sliding-panel-content .search-field").attr("placeholder", "Search");
  }),
  jQuery(document).ready(function (t) {
    t(".dropdown-button").click(function () {
      t(".dropdown-button").toggleClass("active"),
        t(".dropdown-menu").toggleClass("show-menu"),
        t(".dropdown-menu > li, .dropdown-button-close").click(function () {
          t(".dropdown-menu").removeClass("show-menu");
        }),
        t(".dropdown-menu.dropdown-select > li").click(function () {
          t(".dropdown-button").html(t(this).html());
        });
    });
  }),
  !(function (t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? (module.exports = e(require, exports, module)) : (t.ouibounce = e());
  })(this, function (t, e, i) {
    return function (t, e) {
      "use strict";
      function i(t, e) {
        return "undefined" == typeof t ? e : t;
      }
      function o(t) {
        var e = 24 * t * 60 * 60 * 1e3,
          i = new Date();
        return i.setTime(i.getTime() + e), "; expires=" + i.toUTCString();
      }
      function n() {
        c() || (x.addEventListener("mouseleave", s), x.addEventListener("mouseenter", r), x.addEventListener("keydown", a));
      }
      function s(t) {
        t.clientY > m || (T = setTimeout(u, g));
      }
      function r() {
        T && (clearTimeout(T), (T = null));
      }
      function a(t) {
        C || (t.metaKey && 76 === t.keyCode && ((C = !0), (T = setTimeout(u, g))));
      }
      function l(t, e) {
        return d()[t] === e;
      }
      function d() {
        for (var t = document.cookie.split("; "), e = {}, i = t.length - 1; i >= 0; i--) {
          var o = t[i].split("=");
          e[o[0]] = o[1];
        }
        return e;
      }
      function c() {
        return l(k, "true") && !f;
      }
      function u() {
        c() || (t && (t.style.display = "block"), y(), p());
      }
      function p(t) {
        var e = t || {};
        "undefined" != typeof e.cookieExpire && (w = o(e.cookieExpire)),
        e.sitewide === !0 && (S = ";path=/"),
        "undefined" != typeof e.cookieDomain && (b = ";domain=" + e.cookieDomain),
        "undefined" != typeof e.cookieName && (k = e.cookieName),
          (document.cookie = k + "=true" + w + b + S),
          x.removeEventListener("mouseleave", s),
          x.removeEventListener("mouseenter", r),
          x.removeEventListener("keydown", a);
      }
      var h = e || {},
        f = h.aggressive || !1,
        m = i(h.sensitivity, 20),
        v = i(h.timer, 1e3),
        g = i(h.delay, 0),
        y = h.callback || function () {},
        w = o(h.cookieExpire) || "",
        b = h.cookieDomain ? ";domain=" + h.cookieDomain : "",
        k = h.cookieName ? h.cookieName : "viewedOuibounceModal",
        S = h.sitewide === !0 ? ";path=/" : "",
        T = null,
        x = document.documentElement;
      setTimeout(n, v);
      var C = !1;
      return {
        fire: u,
        disable: p,
        isDisabled: c,
      };
    };
  }),
  jQuery(document).ready(function (t) {
    t(window).scroll(function () {
      t(window).width() > 768 && window.requestAnimationFrame(scrollHandler), window.requestAnimationFrame(howWeRollScrollHandler);
    });
  }),
  (function () {
    for (var t = 0, e = ["ms", "moz", "webkit", "o"], i = 0; i < e.length && !window.requestAnimationFrame; ++i)
      (window.requestAnimationFrame = window[e[i] + "RequestAnimationFrame"]), (window.cancelAnimationFrame = window[e[i] + "CancelAnimationFrame"] || window[e[i] + "CancelRequestAnimationFrame"]);
    window.requestAnimationFrame ||
    (window.requestAnimationFrame = function (e, i) {
      var o = new Date().getTime(),
        n = Math.max(0, 16 - (o - t)),
        s = window.setTimeout(function () {
          e(o + n);
        }, n);
      return (t = o + n), s;
    }),
    window.cancelAnimationFrame ||
    (window.cancelAnimationFrame = function (t) {
      clearTimeout(t);
    });
  })(),
  jQuery(document).ready(function (t) {
    t(".image-rotator").rotateClass({
      direction: "backward",
      initialSlide: 1,
    });
  }),
  (function (t) {
    function e(e) {
      var i,
        o = t(this).children(),
        n = "forward" == e.direction ? o.eq(e.initialSlide - 1) : o.eq(e.initialSlide + 1),
        s = o.filter("." + e.activeClass).length > 0 ? o.filter("." + e.activeClass) : n;
      s.removeClass(e.activeClass), (i = "forward" === e.direction ? (s.next().length ? s.next() : o.eq(e.initialSlide)) : s.prev().length ? s.prev() : o.eq(o.length - 1)), i.addClass(e.activeClass);
    }
    t.fn.rotateClass = function (i) {
      var o = t.extend(
        {
          interval: 4e3,
          direction: "forward",
          activeClass: "active",
          initialSlide: 0,
        },
        i
        ),
        n = setInterval(e.bind(this, o), o.interval);
      return (
        t(this).hover(
          function () {
            clearInterval(n), t(this).find("figure").removeClass(o.activeClass);
          },
          function () {
            n = setInterval(e.bind(this, o), o.interval);
          }
        ),
          this
      );
    };
  })(jQuery),
  (function (t) {
    window.MFcomNavigation = {
      windowWidth: t(window).width(),
      _bindEvents: function () {
        var e = this;
        t(document).on("click", ".main-navigation .search a", function () {
          "true" == t("#search").attr("data-search-bar-closed") ? t(document).trigger("search-bar-opening") : t(document).trigger("search-bar-closing");
        }),
          t(document).on("click", "#side-navigation-button", function () {
            t("#side-navigation").hasClass("is-visible") ? t(document).trigger("side-navigation-closing") : t(document).trigger("side-navigation-opening");
          }),
          t(window).on("scroll", function () {
            t(window).scrollTop() > 0 && (t("#side-navigation").attr("data-search-bar-closed") || t(document).trigger("search-bar-closing")),
            t(".htgayw-text #htgayw-phone").length > 0 && e.windowWidth >= 480 && e.windowWidth <= 1024 && e.fakeScroll(window, ".htgayw-text #htgayw-phone", 170, "", 2);
          }),
          t(document).keyup(function (e) {
            27 == e.keyCode && (t("#side-navigation").attr("data-search-bar-closed") || t(document).trigger("search-bar-closing"));
          }),
          t(document).on("mouseup", function (e) {
            var i = t("#search");
            i.is(e.target) || 0 !== i.has(e.target).length || t(document).trigger("search-bar-closing");
          }),
          t(window).on("resize", function () {
            e.windowWidth = t(window).width();
          }),
          t(document).on("search-bar-opening", function () {
            t("body").addClass("search-bar-open"), t("#side-navigation").hasClass("is-visible") && t(document).trigger("side-navigation-closing"), e.toggleSearch(!1), e.toggleNavColor(!0);
          }),
          t(document).on("search-bar-closing", function () {
            t("body").removeClass("search-bar-open"), e.toggleSearch(!0), e.toggleNavColor(!1);
          }),
          t(document).on("side-navigation-closing", function () {
            t("body").removeClass("side-navigation-open"), e.toggleSideNavigation(!0), e.removeFakeScroll("#side-navigation-button"), e.windowWidth > 768 && e.removeFakeScroll("#masthead", "fixed");
          }),
          t(document).on("side-navigation-opening", function () {
            t("body").addClass("side-navigation-open"),
              e.toggleSideNavigation(!1),
            t("#side-navigation").attr("data-search-bar-closed") || t(document).trigger("search-bar-closing"),
              t("#side-navigation").on("scroll", function () {
                e.fakeScroll("#side-navigation", "#side-navigation-button", 0), e.windowWidth <= 768 && e.fakeScroll("#side-navigation", "#masthead", 0, "fixed");
              }),
              e.windowWidth <= 768 ? e.toggleNavColor(!0) : e.toggleNavColor(!1);
          });
      },
      fakeScroll: function (e, i, o, n, s) {
        (n = "undefined" != typeof n ? n : ""), (s = "undefined" != typeof s ? s : 1);
        var r = t(e).scrollTop();
        (r *= s), (i = t(i)), i.css("top", -(r - o)), i.addClass(n);
      },
      removeFakeScroll: function (e, i) {
        (e = t(e)), e.css("top", "none"), i && "undefined" !== i && e.removeClass(i);
      },
      toggleSearch: function (e) {
        e || t('.search-form input[type="search"]')[0].focus(), t("#search").attr("data-search-bar-closed", e);
      },
      toggleSideNavigation: function (e) {
        e
          ? (t("#side-navigation").removeClass("is-visible"), t("#side-navigation-button").attr("data-side-nav-visible", !1), t(".menu-to-close, .sliding-panel-button, .sliding-panel-button-text").toggleClass("active"))
          : (t("#side-navigation-button").attr("data-side-nav-visible", !0), t("#side-navigation").addClass("is-visible"), t(".menu-to-close, .sliding-panel-button, .sliding-panel-button-text").toggleClass("active"));
      },
      toggleNavColor: function (e) {
        e === !0
          ? t("#masthead, #site-navigation, .sliding-panel-button").removeClass("alt")
          : "alt" === t("body").attr("data-nav-color")
          ? t("#masthead, #site-navigation, .sliding-panel-button").addClass("alt")
          : t("#masthead, #site-navigation, .sliding-panel-button").removeClass("alt");
      },
    };
  })(jQuery),
  jQuery(document).ready(function (t) {
    "use strict";
    var e = t("body").width();
    e < 640 && t(".main-navigation #search").hide(),
      t(".main-navigation .search-glass").click(function () {
        t(this).css("opacity", "0"), t("#search-toggle").addClass("active");
      }),
      t(".sliding-panel-button").click(function () {
        t(".search-glass").css("opacity", "1"), t("#search-toggle").removeClass("active");
      }),
      window.MFcomNavigation._bindEvents();
  }),
  jQuery(window).resize(function () {
    "use strict";
    var t = jQuery("body").width();
    t < 640 && jQuery("#search").hide();
  }),
  jQuery(document).ready(function (t) {
    t(".bschool-side-nav-ad").length > 0 &&
    t("#side-navigation-button").click(function () {
      t(this).toggleClass("side-nav-ads-exist"), t("span.active").toggleClass("side-nav-ads-exist"), window.innerWidth < 800 && t("#masthead #logo").toggleClass("side-nav-ads-exist");
    });
  }),
  jQuery(document).ready(function (t) {
    t(".c-stickyBar .button").simpleScrollTo(),
      t("#page-about .down-arrow").simpleScrollTo(),
      t("#page-eif .arrow").simpleScrollTo(),
      t("#page-front-page .down-arrow").simpleScrollTo(),
      t(".page-secondary .down-arrow").simpleScrollTo(),
      t("#page-success-stories .down-arrow").simpleScrollTo(),
      t(".hero .icon-caret-down").simpleScrollTo(),
      t("#page-how-we-roll .down-arrow").simpleScrollTo(),
      t(".single-post .blog-jump").simpleScrollTo(),
      t(".blog .link-to-top").simpleScrollTo(),
      t(".down-arrow").simpleScrollTo();
  }),
  (function (t) {
    t.fn.simpleScrollTo = function (e) {
      var i = t.extend(
        {
          duration: 800,
          easing: "swing",
        },
        e
      );
      return (
        t(this).on(
          "click",
          {
            options: i,
          },
          function (e) {
            var i = t(this).attr("href");
            t("html, body").animate(
              {
                scrollTop: t(i).offset().top,
              },
              e.data.options.duration,
              e.data.options.easing
            ),
              e.preventDefault();
          }
        ),
          this
      );
    };
  })(jQuery),
  (function () {
    var t = navigator.userAgent.toLowerCase().indexOf("webkit") > -1,
      e = navigator.userAgent.toLowerCase().indexOf("opera") > -1,
      i = navigator.userAgent.toLowerCase().indexOf("msie") > -1;
    (t || e || i) &&
    document.getElementById &&
    window.addEventListener &&
    window.addEventListener(
      "hashchange",
      function () {
        var t,
          e = location.hash.substring(1);
        /^[A-z0-9_-]+$/.test(e) && ((t = document.getElementById(e)), t && (/^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus()));
      },
      !1
    );
  })(),
  !(function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof exports ? (module.exports = t(require("jquery"))) : t(jQuery);
  })(function (t) {
    "use strict";
    var e = window.Slick || {};
    (e = (function () {
      function e(e, o) {
        var n,
          s = this;
        (s.defaults = {
          accessibility: !0,
          adaptiveHeight: !1,
          appendArrows: t(e),
          appendDots: t(e),
          arrows: !0,
          asNavFor: null,
          prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
          nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
          autoplay: !1,
          autoplaySpeed: 3e3,
          centerMode: !1,
          centerPadding: "50px",
          cssEase: "ease",
          customPaging: function (t, e) {
            return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">' + (e + 1) + "</button>";
          },
          dots: !1,
          dotsClass: "slick-dots",
          draggable: !0,
          easing: "linear",
          edgeFriction: 0.35,
          fade: !1,
          focusOnSelect: !1,
          infinite: !0,
          initialSlide: 0,
          lazyLoad: "ondemand",
          mobileFirst: !1,
          pauseOnHover: !0,
          pauseOnDotsHover: !1,
          respondTo: "window",
          responsive: null,
          rows: 1,
          rtl: !1,
          slide: "",
          slidesPerRow: 1,
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 500,
          swipe: !0,
          swipeToSlide: !1,
          touchMove: !0,
          touchThreshold: 5,
          useCSS: !0,
          useTransform: !1,
          variableWidth: !1,
          vertical: !1,
          verticalSwiping: !1,
          waitForAnimate: !0,
          zIndex: 1e3,
        }),
          (s.initials = {
            animating: !1,
            dragging: !1,
            autoPlayTimer: null,
            currentDirection: 0,
            currentLeft: null,
            currentSlide: 0,
            direction: 1,
            $dots: null,
            listWidth: null,
            listHeight: null,
            loadIndex: 0,
            $nextArrow: null,
            $prevArrow: null,
            slideCount: null,
            slideWidth: null,
            $slideTrack: null,
            $slides: null,
            sliding: !1,
            slideOffset: 0,
            swipeLeft: null,
            $list: null,
            touchObject: {},
            transformsEnabled: !1,
            unslicked: !1,
          }),
          t.extend(s, s.initials),
          (s.activeBreakpoint = null),
          (s.animType = null),
          (s.animProp = null),
          (s.breakpoints = []),
          (s.breakpointSettings = []),
          (s.cssTransitions = !1),
          (s.hidden = "hidden"),
          (s.paused = !1),
          (s.positionProp = null),
          (s.respondTo = null),
          (s.rowCount = 1),
          (s.shouldClick = !0),
          (s.$slider = t(e)),
          (s.$slidesCache = null),
          (s.transformType = null),
          (s.transitionType = null),
          (s.visibilityChange = "visibilitychange"),
          (s.windowWidth = 0),
          (s.windowTimer = null),
          (n = t(e).data("slick") || {}),
          (s.options = t.extend({}, s.defaults, n, o)),
          (s.currentSlide = s.options.initialSlide),
          (s.originalSettings = s.options),
          "undefined" != typeof document.mozHidden
            ? ((s.hidden = "mozHidden"), (s.visibilityChange = "mozvisibilitychange"))
            : "undefined" != typeof document.webkitHidden && ((s.hidden = "webkitHidden"), (s.visibilityChange = "webkitvisibilitychange")),
          (s.autoPlay = t.proxy(s.autoPlay, s)),
          (s.autoPlayClear = t.proxy(s.autoPlayClear, s)),
          (s.changeSlide = t.proxy(s.changeSlide, s)),
          (s.clickHandler = t.proxy(s.clickHandler, s)),
          (s.selectHandler = t.proxy(s.selectHandler, s)),
          (s.setPosition = t.proxy(s.setPosition, s)),
          (s.swipeHandler = t.proxy(s.swipeHandler, s)),
          (s.dragHandler = t.proxy(s.dragHandler, s)),
          (s.keyHandler = t.proxy(s.keyHandler, s)),
          (s.autoPlayIterator = t.proxy(s.autoPlayIterator, s)),
          (s.instanceUid = i++),
          (s.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/),
          s.registerBreakpoints(),
          s.init(!0),
          s.checkResponsive(!0);
      }
      var i = 0;
      return e;
    })()),
      (e.prototype.addSlide = e.prototype.slickAdd = function (e, i, o) {
        var n = this;
        if ("boolean" == typeof i) (o = i), (i = null);
        else if (0 > i || i >= n.slideCount) return !1;
        n.unload(),
          "number" == typeof i
            ? 0 === i && 0 === n.$slides.length
            ? t(e).appendTo(n.$slideTrack)
            : o
              ? t(e).insertBefore(n.$slides.eq(i))
              : t(e).insertAfter(n.$slides.eq(i))
            : o === !0
            ? t(e).prependTo(n.$slideTrack)
            : t(e).appendTo(n.$slideTrack),
          (n.$slides = n.$slideTrack.children(this.options.slide)),
          n.$slideTrack.children(this.options.slide).detach(),
          n.$slideTrack.append(n.$slides),
          n.$slides.each(function (e, i) {
            t(i).attr("data-slick-index", e);
          }),
          (n.$slidesCache = n.$slides),
          n.reinit();
      }),
      (e.prototype.animateHeight = function () {
        var t = this;
        if (1 === t.options.slidesToShow && t.options.adaptiveHeight === !0 && t.options.vertical === !1) {
          var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
          t.$list.animate(
            {
              height: e,
            },
            t.options.speed
          );
        }
      }),
      (e.prototype.animateSlide = function (e, i) {
        var o = {},
          n = this;
        n.animateHeight(),
        n.options.rtl === !0 && n.options.vertical === !1 && (e = -e),
          n.transformsEnabled === !1
            ? n.options.vertical === !1
            ? n.$slideTrack.animate(
              {
                left: e,
              },
              n.options.speed,
              n.options.easing,
              i
            )
            : n.$slideTrack.animate(
              {
                top: e,
              },
              n.options.speed,
              n.options.easing,
              i
            )
            : n.cssTransitions === !1
            ? (n.options.rtl === !0 && (n.currentLeft = -n.currentLeft),
              t({
                animStart: n.currentLeft,
              }).animate(
                {
                  animStart: e,
                },
                {
                  duration: n.options.speed,
                  easing: n.options.easing,
                  step: function (t) {
                    (t = Math.ceil(t)), n.options.vertical === !1 ? ((o[n.animType] = "translate(" + t + "px, 0px)"), n.$slideTrack.css(o)) : ((o[n.animType] = "translate(0px," + t + "px)"), n.$slideTrack.css(o));
                  },
                  complete: function () {
                    i && i.call();
                  },
                }
              ))
            : (n.applyTransition(),
              (e = Math.ceil(e)),
              n.options.vertical === !1 ? (o[n.animType] = "translate3d(" + e + "px, 0px, 0px)") : (o[n.animType] = "translate3d(0px," + e + "px, 0px)"),
              n.$slideTrack.css(o),
            i &&
            setTimeout(function () {
              n.disableTransition(), i.call();
            }, n.options.speed));
      }),
      (e.prototype.asNavFor = function (e) {
        var i = this,
          o = i.options.asNavFor;
        o && null !== o && (o = t(o).not(i.$slider)),
        null !== o &&
        "object" == typeof o &&
        o.each(function () {
          var i = t(this).slick("getSlick");
          i.unslicked || i.slideHandler(e, !0);
        });
      }),
      (e.prototype.applyTransition = function (t) {
        var e = this,
          i = {};
        e.options.fade === !1 ? (i[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase) : (i[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase),
          e.options.fade === !1 ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i);
      }),
      (e.prototype.autoPlay = function () {
        var t = this;
        t.autoPlayTimer && clearInterval(t.autoPlayTimer), t.slideCount > t.options.slidesToShow && t.paused !== !0 && (t.autoPlayTimer = setInterval(t.autoPlayIterator, t.options.autoplaySpeed));
      }),
      (e.prototype.autoPlayClear = function () {
        var t = this;
        t.autoPlayTimer && clearInterval(t.autoPlayTimer);
      }),
      (e.prototype.autoPlayIterator = function () {
        var t = this;
        t.options.infinite === !1
          ? 1 === t.direction
          ? (t.currentSlide + 1 === t.slideCount - 1 && (t.direction = 0), t.slideHandler(t.currentSlide + t.options.slidesToScroll))
          : (t.currentSlide - 1 === 0 && (t.direction = 1), t.slideHandler(t.currentSlide - t.options.slidesToScroll))
          : t.slideHandler(t.currentSlide + t.options.slidesToScroll);
      }),
      (e.prototype.buildArrows = function () {
        var e = this;
        e.options.arrows === !0 &&
        ((e.$prevArrow = t(e.options.prevArrow).addClass("slick-arrow")),
          (e.$nextArrow = t(e.options.nextArrow).addClass("slick-arrow")),
          e.slideCount > e.options.slidesToShow
            ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
              e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
            e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows),
            e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows),
            e.options.infinite !== !0 && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"))
            : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
              "aria-disabled": "true",
              tabindex: "-1",
            }));
      }),
      (e.prototype.buildDots = function () {
        var e,
          i,
          o = this;
        if (o.options.dots === !0 && o.slideCount > o.options.slidesToShow) {
          for (i = '<ul class="' + o.options.dotsClass + '">', e = 0; e <= o.getDotCount(); e += 1) i += "<li>" + o.options.customPaging.call(this, o, e) + "</li>";
          (i += "</ul>"), (o.$dots = t(i).appendTo(o.options.appendDots)), o.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false");
        }
      }),
      (e.prototype.buildOut = function () {
        var e = this;
        (e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide")),
          (e.slideCount = e.$slides.length),
          e.$slides.each(function (e, i) {
            t(i)
            .attr("data-slick-index", e)
            .data("originalStyling", t(i).attr("style") || "");
          }),
          e.$slider.addClass("slick-slider"),
          (e.$slideTrack = 0 === e.slideCount ? t('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent()),
          (e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent()),
          e.$slideTrack.css("opacity", 0),
        (e.options.centerMode === !0 || e.options.swipeToSlide === !0) && (e.options.slidesToScroll = 1),
          t("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"),
          e.setupInfinite(),
          e.buildArrows(),
          e.buildDots(),
          e.updateDots(),
          e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0),
        e.options.draggable === !0 && e.$list.addClass("draggable");
      }),
      (e.prototype.buildRows = function () {
        var t,
          e,
          i,
          o,
          n,
          s,
          r,
          a = this;
        if (((o = document.createDocumentFragment()), (s = a.$slider.children()), a.options.rows > 1)) {
          for (r = a.options.slidesPerRow * a.options.rows, n = Math.ceil(s.length / r), t = 0; n > t; t++) {
            var l = document.createElement("div");
            for (e = 0; e < a.options.rows; e++) {
              var d = document.createElement("div");
              for (i = 0; i < a.options.slidesPerRow; i++) {
                var c = t * r + (e * a.options.slidesPerRow + i);
                s.get(c) && d.appendChild(s.get(c));
              }
              l.appendChild(d);
            }
            o.appendChild(l);
          }
          a.$slider.html(o),
            a.$slider
            .children()
            .children()
            .children()
            .css({
              width: 100 / a.options.slidesPerRow + "%",
              display: "inline-block",
            });
        }
      }),
      (e.prototype.checkResponsive = function (e, i) {
        var o,
          n,
          s,
          r = this,
          a = !1,
          l = r.$slider.width(),
          d = window.innerWidth || t(window).width();
        if (("window" === r.respondTo ? (s = d) : "slider" === r.respondTo ? (s = l) : "min" === r.respondTo && (s = Math.min(d, l)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive)) {
          n = null;
          for (o in r.breakpoints) r.breakpoints.hasOwnProperty(o) && (r.originalSettings.mobileFirst === !1 ? s < r.breakpoints[o] && (n = r.breakpoints[o]) : s > r.breakpoints[o] && (n = r.breakpoints[o]));
          null !== n
            ? null !== r.activeBreakpoint
            ? (n !== r.activeBreakpoint || i) &&
            ((r.activeBreakpoint = n),
              "unslick" === r.breakpointSettings[n] ? r.unslick(n) : ((r.options = t.extend({}, r.originalSettings, r.breakpointSettings[n])), e === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(e)),
              (a = n))
            : ((r.activeBreakpoint = n),
              "unslick" === r.breakpointSettings[n] ? r.unslick(n) : ((r.options = t.extend({}, r.originalSettings, r.breakpointSettings[n])), e === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(e)),
              (a = n))
            : null !== r.activeBreakpoint && ((r.activeBreakpoint = null), (r.options = r.originalSettings), e === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(e), (a = n)),
          e || a === !1 || r.$slider.trigger("breakpoint", [r, a]);
        }
      }),
      (e.prototype.changeSlide = function (e, i) {
        var o,
          n,
          s,
          r = this,
          a = t(e.target);
        switch ((a.is("a") && e.preventDefault(), a.is("li") || (a = a.closest("li")), (s = r.slideCount % r.options.slidesToScroll !== 0), (o = s ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll), e.data.message)) {
          case "previous":
            (n = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o), r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - n, !1, i);
            break;
          case "next":
            (n = 0 === o ? r.options.slidesToScroll : o), r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + n, !1, i);
            break;
          case "index":
            var l = 0 === e.data.index ? 0 : e.data.index || a.index() * r.options.slidesToScroll;
            r.slideHandler(r.checkNavigable(l), !1, i), a.children().trigger("focus");
            break;
          default:
            return;
        }
      }),
      (e.prototype.checkNavigable = function (t) {
        var e,
          i,
          o = this;
        if (((e = o.getNavigableIndexes()), (i = 0), t > e[e.length - 1])) t = e[e.length - 1];
        else
          for (var n in e) {
            if (t < e[n]) {
              t = i;
              break;
            }
            i = e[n];
          }
        return t;
      }),
      (e.prototype.cleanUpEvents = function () {
        var e = this;
        e.options.dots &&
        null !== e.$dots &&
        (t("li", e.$dots).off("click.slick", e.changeSlide),
        e.options.pauseOnDotsHover === !0 && e.options.autoplay === !0 && t("li", e.$dots).off("mouseenter.slick", t.proxy(e.setPaused, e, !0)).off("mouseleave.slick", t.proxy(e.setPaused, e, !1))),
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)),
          e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler),
          e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler),
          e.$list.off("touchend.slick mouseup.slick", e.swipeHandler),
          e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler),
          e.$list.off("click.slick", e.clickHandler),
          t(document).off(e.visibilityChange, e.visibility),
          e.$list.off("mouseenter.slick", t.proxy(e.setPaused, e, !0)),
          e.$list.off("mouseleave.slick", t.proxy(e.setPaused, e, !1)),
        e.options.accessibility === !0 && e.$list.off("keydown.slick", e.keyHandler),
        e.options.focusOnSelect === !0 && t(e.$slideTrack).children().off("click.slick", e.selectHandler),
          t(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange),
          t(window).off("resize.slick.slick-" + e.instanceUid, e.resize),
          t("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault),
          t(window).off("load.slick.slick-" + e.instanceUid, e.setPosition),
          t(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition);
      }),
      (e.prototype.cleanUpRows = function () {
        var t,
          e = this;
        e.options.rows > 1 && ((t = e.$slides.children().children()), t.removeAttr("style"), e.$slider.html(t));
      }),
      (e.prototype.clickHandler = function (t) {
        var e = this;
        e.shouldClick === !1 && (t.stopImmediatePropagation(), t.stopPropagation(), t.preventDefault());
      }),
      (e.prototype.destroy = function (e) {
        var i = this;
        i.autoPlayClear(),
          (i.touchObject = {}),
          i.cleanUpEvents(),
          t(".slick-cloned", i.$slider).detach(),
        i.$dots && i.$dots.remove(),
        i.$prevArrow &&
        i.$prevArrow.length &&
        (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()),
        i.$nextArrow &&
        i.$nextArrow.length &&
        (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()),
        i.$slides &&
        (i.$slides
        .removeClass("slick-slide slick-active slick-center slick-visible slick-current")
        .removeAttr("aria-hidden")
        .removeAttr("data-slick-index")
        .each(function () {
          t(this).attr("style", t(this).data("originalStyling"));
        }),
          i.$slideTrack.children(this.options.slide).detach(),
          i.$slideTrack.detach(),
          i.$list.detach(),
          i.$slider.append(i.$slides)),
          i.cleanUpRows(),
          i.$slider.removeClass("slick-slider"),
          i.$slider.removeClass("slick-initialized"),
          (i.unslicked = !0),
        e || i.$slider.trigger("destroy", [i]);
      }),
      (e.prototype.disableTransition = function (t) {
        var e = this,
          i = {};
        (i[e.transitionType] = ""), e.options.fade === !1 ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i);
      }),
      (e.prototype.fadeSlide = function (t, e) {
        var i = this;
        i.cssTransitions === !1
          ? (i.$slides.eq(t).css({
            zIndex: i.options.zIndex,
          }),
            i.$slides.eq(t).animate(
              {
                opacity: 1,
              },
              i.options.speed,
              i.options.easing,
              e
            ))
          : (i.applyTransition(t),
            i.$slides.eq(t).css({
              opacity: 1,
              zIndex: i.options.zIndex,
            }),
          e &&
          setTimeout(function () {
            i.disableTransition(t), e.call();
          }, i.options.speed));
      }),
      (e.prototype.fadeSlideOut = function (t) {
        var e = this;
        e.cssTransitions === !1
          ? e.$slides.eq(t).animate(
          {
            opacity: 0,
            zIndex: e.options.zIndex - 2,
          },
          e.options.speed,
          e.options.easing
          )
          : (e.applyTransition(t),
            e.$slides.eq(t).css({
              opacity: 0,
              zIndex: e.options.zIndex - 2,
            }));
      }),
      (e.prototype.filterSlides = e.prototype.slickFilter = function (t) {
        var e = this;
        null !== t && ((e.$slidesCache = e.$slides), e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(t).appendTo(e.$slideTrack), e.reinit());
      }),
      (e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
        var t = this;
        return t.currentSlide;
      }),
      (e.prototype.getDotCount = function () {
        var t = this,
          e = 0,
          i = 0,
          o = 0;
        if (t.options.infinite === !0) for (; e < t.slideCount; ) ++o, (e = i + t.options.slidesToScroll), (i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow);
        else if (t.options.centerMode === !0) o = t.slideCount;
        else for (; e < t.slideCount; ) ++o, (e = i + t.options.slidesToScroll), (i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow);
        return o - 1;
      }),
      (e.prototype.getLeft = function (t) {
        var e,
          i,
          o,
          n = this,
          s = 0;
        return (
          (n.slideOffset = 0),
            (i = n.$slides.first().outerHeight(!0)),
            n.options.infinite === !0
              ? (n.slideCount > n.options.slidesToShow && ((n.slideOffset = n.slideWidth * n.options.slidesToShow * -1), (s = i * n.options.slidesToShow * -1)),
              n.slideCount % n.options.slidesToScroll !== 0 &&
              t + n.options.slidesToScroll > n.slideCount &&
              n.slideCount > n.options.slidesToShow &&
              (t > n.slideCount
                ? ((n.slideOffset = (n.options.slidesToShow - (t - n.slideCount)) * n.slideWidth * -1), (s = (n.options.slidesToShow - (t - n.slideCount)) * i * -1))
                : ((n.slideOffset = (n.slideCount % n.options.slidesToScroll) * n.slideWidth * -1), (s = (n.slideCount % n.options.slidesToScroll) * i * -1))))
              : t + n.options.slidesToShow > n.slideCount && ((n.slideOffset = (t + n.options.slidesToShow - n.slideCount) * n.slideWidth), (s = (t + n.options.slidesToShow - n.slideCount) * i)),
          n.slideCount <= n.options.slidesToShow && ((n.slideOffset = 0), (s = 0)),
            n.options.centerMode === !0 && n.options.infinite === !0
              ? (n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2) - n.slideWidth)
              : n.options.centerMode === !0 && ((n.slideOffset = 0), (n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2))),
            (e = n.options.vertical === !1 ? t * n.slideWidth * -1 + n.slideOffset : t * i * -1 + s),
          n.options.variableWidth === !0 &&
          ((o = n.slideCount <= n.options.slidesToShow || n.options.infinite === !1 ? n.$slideTrack.children(".slick-slide").eq(t) : n.$slideTrack.children(".slick-slide").eq(t + n.options.slidesToShow)),
            (e = n.options.rtl === !0 ? (o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0) : o[0] ? -1 * o[0].offsetLeft : 0),
          n.options.centerMode === !0 &&
          ((o = n.slideCount <= n.options.slidesToShow || n.options.infinite === !1 ? n.$slideTrack.children(".slick-slide").eq(t) : n.$slideTrack.children(".slick-slide").eq(t + n.options.slidesToShow + 1)),
            (e = n.options.rtl === !0 ? (o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0) : o[0] ? -1 * o[0].offsetLeft : 0),
            (e += (n.$list.width() - o.outerWidth()) / 2))),
            e
        );
      }),
      (e.prototype.getOption = e.prototype.slickGetOption = function (t) {
        var e = this;
        return e.options[t];
      }),
      (e.prototype.getNavigableIndexes = function () {
        var t,
          e = this,
          i = 0,
          o = 0,
          n = [];
        for (e.options.infinite === !1 ? (t = e.slideCount) : ((i = -1 * e.options.slidesToScroll), (o = -1 * e.options.slidesToScroll), (t = 2 * e.slideCount)); t > i; )
          n.push(i), (i = o + e.options.slidesToScroll), (o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow);
        return n;
      }),
      (e.prototype.getSlick = function () {
        return this;
      }),
      (e.prototype.getSlideCount = function () {
        var e,
          i,
          o,
          n = this;
        return (
          (o = n.options.centerMode === !0 ? n.slideWidth * Math.floor(n.options.slidesToShow / 2) : 0),
            n.options.swipeToSlide === !0
              ? (n.$slideTrack.find(".slick-slide").each(function (e, s) {
                return s.offsetLeft - o + t(s).outerWidth() / 2 > -1 * n.swipeLeft ? ((i = s), !1) : void 0;
              }),
                (e = Math.abs(t(i).attr("data-slick-index") - n.currentSlide) || 1))
              : n.options.slidesToScroll
        );
      }),
      (e.prototype.goTo = e.prototype.slickGoTo = function (t, e) {
        var i = this;
        i.changeSlide(
          {
            data: {
              message: "index",
              index: parseInt(t),
            },
          },
          e
        );
      }),
      (e.prototype.init = function (e) {
        var i = this;
        t(i.$slider).hasClass("slick-initialized") || (t(i.$slider).addClass("slick-initialized"), i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), i.updateArrows(), i.updateDots()),
        e && i.$slider.trigger("init", [i]),
        i.options.accessibility === !0 && i.initADA();
      }),
      (e.prototype.initArrowEvents = function () {
        var t = this;
        t.options.arrows === !0 &&
        t.slideCount > t.options.slidesToShow &&
        (t.$prevArrow.on(
          "click.slick",
          {
            message: "previous",
          },
          t.changeSlide
        ),
          t.$nextArrow.on(
            "click.slick",
            {
              message: "next",
            },
            t.changeSlide
          ));
      }),
      (e.prototype.initDotEvents = function () {
        var e = this;
        e.options.dots === !0 &&
        e.slideCount > e.options.slidesToShow &&
        t("li", e.$dots).on(
          "click.slick",
          {
            message: "index",
          },
          e.changeSlide
        ),
        e.options.dots === !0 && e.options.pauseOnDotsHover === !0 && e.options.autoplay === !0 && t("li", e.$dots).on("mouseenter.slick", t.proxy(e.setPaused, e, !0)).on("mouseleave.slick", t.proxy(e.setPaused, e, !1));
      }),
      (e.prototype.initializeEvents = function () {
        var e = this;
        e.initArrowEvents(),
          e.initDotEvents(),
          e.$list.on(
            "touchstart.slick mousedown.slick",
            {
              action: "start",
            },
            e.swipeHandler
          ),
          e.$list.on(
            "touchmove.slick mousemove.slick",
            {
              action: "move",
            },
            e.swipeHandler
          ),
          e.$list.on(
            "touchend.slick mouseup.slick",
            {
              action: "end",
            },
            e.swipeHandler
          ),
          e.$list.on(
            "touchcancel.slick mouseleave.slick",
            {
              action: "end",
            },
            e.swipeHandler
          ),
          e.$list.on("click.slick", e.clickHandler),
          t(document).on(e.visibilityChange, t.proxy(e.visibility, e)),
          e.$list.on("mouseenter.slick", t.proxy(e.setPaused, e, !0)),
          e.$list.on("mouseleave.slick", t.proxy(e.setPaused, e, !1)),
        e.options.accessibility === !0 && e.$list.on("keydown.slick", e.keyHandler),
        e.options.focusOnSelect === !0 && t(e.$slideTrack).children().on("click.slick", e.selectHandler),
          t(window).on("orientationchange.slick.slick-" + e.instanceUid, t.proxy(e.orientationChange, e)),
          t(window).on("resize.slick.slick-" + e.instanceUid, t.proxy(e.resize, e)),
          t("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault),
          t(window).on("load.slick.slick-" + e.instanceUid, e.setPosition),
          t(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition);
      }),
      (e.prototype.initUI = function () {
        var t = this;
        t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow.show(), t.$nextArrow.show()),
        t.options.dots === !0 && t.slideCount > t.options.slidesToShow && t.$dots.show(),
        t.options.autoplay === !0 && t.autoPlay();
      }),
      (e.prototype.keyHandler = function (t) {
        var e = this;
        t.target.tagName.match("TEXTAREA|INPUT|SELECT") ||
        (37 === t.keyCode && e.options.accessibility === !0
          ? e.changeSlide({
            data: {
              message: "previous",
            },
          })
          : 39 === t.keyCode &&
          e.options.accessibility === !0 &&
          e.changeSlide({
            data: {
              message: "next",
            },
          }));
      }),
      (e.prototype.lazyLoad = function () {
        function e(e) {
          t("img[data-lazy]", e).each(function () {
            var e = t(this),
              i = t(this).attr("data-lazy"),
              o = document.createElement("img");
            (o.onload = function () {
              e.animate(
                {
                  opacity: 0,
                },
                100,
                function () {
                  e.attr("src", i).animate(
                    {
                      opacity: 1,
                    },
                    200,
                    function () {
                      e.removeAttr("data-lazy").removeClass("slick-loading");
                    }
                  );
                }
              );
            }),
              (o.src = i);
          });
        }
        var i,
          o,
          n,
          s,
          r = this;
        r.options.centerMode === !0
          ? r.options.infinite === !0
          ? ((n = r.currentSlide + (r.options.slidesToShow / 2 + 1)), (s = n + r.options.slidesToShow + 2))
          : ((n = Math.max(0, r.currentSlide - (r.options.slidesToShow / 2 + 1))), (s = 2 + (r.options.slidesToShow / 2 + 1) + r.currentSlide))
          : ((n = r.options.infinite ? r.options.slidesToShow + r.currentSlide : r.currentSlide), (s = n + r.options.slidesToShow), r.options.fade === !0 && (n > 0 && n--, s <= r.slideCount && s++)),
          (i = r.$slider.find(".slick-slide").slice(n, s)),
          e(i),
          r.slideCount <= r.options.slidesToShow
            ? ((o = r.$slider.find(".slick-slide")), e(o))
            : r.currentSlide >= r.slideCount - r.options.slidesToShow
            ? ((o = r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow)), e(o))
            : 0 === r.currentSlide && ((o = r.$slider.find(".slick-cloned").slice(-1 * r.options.slidesToShow)), e(o));
      }),
      (e.prototype.loadSlider = function () {
        var t = this;
        t.setPosition(),
          t.$slideTrack.css({
            opacity: 1,
          }),
          t.$slider.removeClass("slick-loading"),
          t.initUI(),
        "progressive" === t.options.lazyLoad && t.progressiveLazyLoad();
      }),
      (e.prototype.next = e.prototype.slickNext = function () {
        var t = this;
        t.changeSlide({
          data: {
            message: "next",
          },
        });
      }),
      (e.prototype.orientationChange = function () {
        var t = this;
        t.checkResponsive(), t.setPosition();
      }),
      (e.prototype.pause = e.prototype.slickPause = function () {
        var t = this;
        t.autoPlayClear(), (t.paused = !0);
      }),
      (e.prototype.play = e.prototype.slickPlay = function () {
        var t = this;
        (t.paused = !1), t.autoPlay();
      }),
      (e.prototype.postSlide = function (t) {
        var e = this;
        e.$slider.trigger("afterChange", [e, t]), (e.animating = !1), e.setPosition(), (e.swipeLeft = null), e.options.autoplay === !0 && e.paused === !1 && e.autoPlay(), e.options.accessibility === !0 && e.initADA();
      }),
      (e.prototype.prev = e.prototype.slickPrev = function () {
        var t = this;
        t.changeSlide({
          data: {
            message: "previous",
          },
        });
      }),
      (e.prototype.preventDefault = function (t) {
        t.preventDefault();
      }),
      (e.prototype.progressiveLazyLoad = function () {
        var e,
          i,
          o = this;
        (e = t("img[data-lazy]", o.$slider).length),
        e > 0 &&
        ((i = t("img[data-lazy]", o.$slider).first()),
          i.attr("src", null),
          i
          .attr("src", i.attr("data-lazy"))
          .removeClass("slick-loading")
          .load(function () {
            i.removeAttr("data-lazy"), o.progressiveLazyLoad(), o.options.adaptiveHeight === !0 && o.setPosition();
          })
          .error(function () {
            i.removeAttr("data-lazy"), o.progressiveLazyLoad();
          }));
      }),
      (e.prototype.refresh = function (e) {
        var i,
          o,
          n = this;
        (o = n.slideCount - n.options.slidesToShow),
        n.options.infinite || (n.slideCount <= n.options.slidesToShow ? (n.currentSlide = 0) : n.currentSlide > o && (n.currentSlide = o)),
          (i = n.currentSlide),
          n.destroy(!0),
          t.extend(n, n.initials, {
            currentSlide: i,
          }),
          n.init(),
        e ||
        n.changeSlide(
          {
            data: {
              message: "index",
              index: i,
            },
          },
          !1
        );
      }),
      (e.prototype.registerBreakpoints = function () {
        var e,
          i,
          o,
          n = this,
          s = n.options.responsive || null;
        if ("array" === t.type(s) && s.length) {
          n.respondTo = n.options.respondTo || "window";
          for (e in s)
            if (((o = n.breakpoints.length - 1), (i = s[e].breakpoint), s.hasOwnProperty(e))) {
              for (; o >= 0; ) n.breakpoints[o] && n.breakpoints[o] === i && n.breakpoints.splice(o, 1), o--;
              n.breakpoints.push(i), (n.breakpointSettings[i] = s[e].settings);
            }
          n.breakpoints.sort(function (t, e) {
            return n.options.mobileFirst ? t - e : e - t;
          });
        }
      }),
      (e.prototype.reinit = function () {
        var e = this;
        (e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide")),
          (e.slideCount = e.$slides.length),
        e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll),
        e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0),
          e.registerBreakpoints(),
          e.setProps(),
          e.setupInfinite(),
          e.buildArrows(),
          e.updateArrows(),
          e.initArrowEvents(),
          e.buildDots(),
          e.updateDots(),
          e.initDotEvents(),
          e.checkResponsive(!1, !0),
        e.options.focusOnSelect === !0 && t(e.$slideTrack).children().on("click.slick", e.selectHandler),
          e.setSlideClasses(0),
          e.setPosition(),
          e.$slider.trigger("reInit", [e]),
        e.options.autoplay === !0 && e.focusHandler();
      }),
      (e.prototype.resize = function () {
        var e = this;
        t(window).width() !== e.windowWidth &&
        (clearTimeout(e.windowDelay),
          (e.windowDelay = window.setTimeout(function () {
            (e.windowWidth = t(window).width()), e.checkResponsive(), e.unslicked || e.setPosition();
          }, 50)));
      }),
      (e.prototype.removeSlide = e.prototype.slickRemove = function (t, e, i) {
        var o = this;
        return (
          "boolean" == typeof t ? ((e = t), (t = e === !0 ? 0 : o.slideCount - 1)) : (t = e === !0 ? --t : t),
          !(o.slideCount < 1 || 0 > t || t > o.slideCount - 1) &&
          (o.unload(),
            i === !0 ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(t).remove(),
            (o.$slides = o.$slideTrack.children(this.options.slide)),
            o.$slideTrack.children(this.options.slide).detach(),
            o.$slideTrack.append(o.$slides),
            (o.$slidesCache = o.$slides),
            void o.reinit())
        );
      }),
      (e.prototype.setCSS = function (t) {
        var e,
          i,
          o = this,
          n = {};
        o.options.rtl === !0 && (t = -t),
          (e = "left" == o.positionProp ? Math.ceil(t) + "px" : "0px"),
          (i = "top" == o.positionProp ? Math.ceil(t) + "px" : "0px"),
          (n[o.positionProp] = t),
          o.transformsEnabled === !1
            ? o.$slideTrack.css(n)
            : ((n = {}), o.cssTransitions === !1 ? ((n[o.animType] = "translate(" + e + ", " + i + ")"), o.$slideTrack.css(n)) : ((n[o.animType] = "translate3d(" + e + ", " + i + ", 0px)"), o.$slideTrack.css(n)));
      }),
      (e.prototype.setDimensions = function () {
        var t = this;
        t.options.vertical === !1
          ? t.options.centerMode === !0 &&
          t.$list.css({
            padding: "0px " + t.options.centerPadding,
          })
          : (t.$list.height(t.$slides.first().outerHeight(!0) * t.options.slidesToShow),
          t.options.centerMode === !0 &&
          t.$list.css({
            padding: t.options.centerPadding + " 0px",
          })),
          (t.listWidth = t.$list.width()),
          (t.listHeight = t.$list.height()),
          t.options.vertical === !1 && t.options.variableWidth === !1
            ? ((t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow)), t.$slideTrack.width(Math.ceil(t.slideWidth * t.$slideTrack.children(".slick-slide").length)))
            : t.options.variableWidth === !0
            ? t.$slideTrack.width(5e3 * t.slideCount)
            : ((t.slideWidth = Math.ceil(t.listWidth)), t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0) * t.$slideTrack.children(".slick-slide").length)));
        var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
        t.options.variableWidth === !1 && t.$slideTrack.children(".slick-slide").width(t.slideWidth - e);
      }),
      (e.prototype.setFade = function () {
        var e,
          i = this;
        i.$slides.each(function (o, n) {
          (e = i.slideWidth * o * -1),
            i.options.rtl === !0
              ? t(n).css({
                position: "relative",
                right: e,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0,
              })
              : t(n).css({
                position: "relative",
                left: e,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0,
              });
        }),
          i.$slides.eq(i.currentSlide).css({
            zIndex: i.options.zIndex - 1,
            opacity: 1,
          });
      }),
      (e.prototype.setHeight = function () {
        var t = this;
        if (1 === t.options.slidesToShow && t.options.adaptiveHeight === !0 && t.options.vertical === !1) {
          var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
          t.$list.css("height", e);
        }
      }),
      (e.prototype.setOption = e.prototype.slickSetOption = function (e, i, o) {
        var n,
          s,
          r = this;
        if ("responsive" === e && "array" === t.type(i))
          for (s in i)
            if ("array" !== t.type(r.options.responsive)) r.options.responsive = [i[s]];
            else {
              for (n = r.options.responsive.length - 1; n >= 0; ) r.options.responsive[n].breakpoint === i[s].breakpoint && r.options.responsive.splice(n, 1), n--;
              r.options.responsive.push(i[s]);
            }
        else r.options[e] = i;
        o === !0 && (r.unload(), r.reinit());
      }),
      (e.prototype.setPosition = function () {
        var t = this;
        t.setDimensions(), t.setHeight(), t.options.fade === !1 ? t.setCSS(t.getLeft(t.currentSlide)) : t.setFade(), t.$slider.trigger("setPosition", [t]);
      }),
      (e.prototype.setProps = function () {
        var t = this,
          e = document.body.style;
        (t.positionProp = t.options.vertical === !0 ? "top" : "left"),
          "top" === t.positionProp ? t.$slider.addClass("slick-vertical") : t.$slider.removeClass("slick-vertical"),
        (void 0 !== e.WebkitTransition || void 0 !== e.MozTransition || void 0 !== e.msTransition) && t.options.useCSS === !0 && (t.cssTransitions = !0),
        t.options.fade && ("number" == typeof t.options.zIndex ? t.options.zIndex < 3 && (t.options.zIndex = 3) : (t.options.zIndex = t.defaults.zIndex)),
        void 0 !== e.OTransform && ((t.animType = "OTransform"), (t.transformType = "-o-transform"), (t.transitionType = "OTransition"), void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)),
        void 0 !== e.MozTransform &&
        ((t.animType = "MozTransform"), (t.transformType = "-moz-transform"), (t.transitionType = "MozTransition"), void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (t.animType = !1)),
        void 0 !== e.webkitTransform &&
        ((t.animType = "webkitTransform"), (t.transformType = "-webkit-transform"), (t.transitionType = "webkitTransition"), void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)),
        void 0 !== e.msTransform && ((t.animType = "msTransform"), (t.transformType = "-ms-transform"), (t.transitionType = "msTransition"), void 0 === e.msTransform && (t.animType = !1)),
        void 0 !== e.transform && t.animType !== !1 && ((t.animType = "transform"), (t.transformType = "transform"), (t.transitionType = "transition")),
          (t.transformsEnabled = t.options.useTransform && null !== t.animType && t.animType !== !1);
      }),
      (e.prototype.setSlideClasses = function (t) {
        var e,
          i,
          o,
          n,
          s = this;
        (i = s.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true")),
          s.$slides.eq(t).addClass("slick-current"),
          s.options.centerMode === !0
            ? ((e = Math.floor(s.options.slidesToShow / 2)),
            s.options.infinite === !0 &&
            (t >= e && t <= s.slideCount - 1 - e
              ? s.$slides
              .slice(t - e, t + e + 1)
              .addClass("slick-active")
              .attr("aria-hidden", "false")
              : ((o = s.options.slidesToShow + t),
                i
                .slice(o - e + 1, o + e + 2)
                .addClass("slick-active")
                .attr("aria-hidden", "false")),
              0 === t ? i.eq(i.length - 1 - s.options.slidesToShow).addClass("slick-center") : t === s.slideCount - 1 && i.eq(s.options.slidesToShow).addClass("slick-center")),
              s.$slides.eq(t).addClass("slick-center"))
            : t >= 0 && t <= s.slideCount - s.options.slidesToShow
            ? s.$slides
            .slice(t, t + s.options.slidesToShow)
            .addClass("slick-active")
            .attr("aria-hidden", "false")
            : i.length <= s.options.slidesToShow
              ? i.addClass("slick-active").attr("aria-hidden", "false")
              : ((n = s.slideCount % s.options.slidesToShow),
                (o = s.options.infinite === !0 ? s.options.slidesToShow + t : t),
                s.options.slidesToShow == s.options.slidesToScroll && s.slideCount - t < s.options.slidesToShow
                  ? i
                  .slice(o - (s.options.slidesToShow - n), o + n)
                  .addClass("slick-active")
                  .attr("aria-hidden", "false")
                  : i
                  .slice(o, o + s.options.slidesToShow)
                  .addClass("slick-active")
                  .attr("aria-hidden", "false")),
        "ondemand" === s.options.lazyLoad && s.lazyLoad();
      }),
      (e.prototype.setupInfinite = function () {
        var e,
          i,
          o,
          n = this;
        if ((n.options.fade === !0 && (n.options.centerMode = !1), n.options.infinite === !0 && n.options.fade === !1 && ((i = null), n.slideCount > n.options.slidesToShow))) {
          for (o = n.options.centerMode === !0 ? n.options.slidesToShow + 1 : n.options.slidesToShow, e = n.slideCount; e > n.slideCount - o; e -= 1)
            (i = e - 1),
              t(n.$slides[i])
              .clone(!0)
              .attr("id", "")
              .attr("data-slick-index", i - n.slideCount)
              .prependTo(n.$slideTrack)
              .addClass("slick-cloned");
          for (e = 0; o > e; e += 1)
            (i = e),
              t(n.$slides[i])
              .clone(!0)
              .attr("id", "")
              .attr("data-slick-index", i + n.slideCount)
              .appendTo(n.$slideTrack)
              .addClass("slick-cloned");
          n.$slideTrack
          .find(".slick-cloned")
          .find("[id]")
          .each(function () {
            t(this).attr("id", "");
          });
        }
      }),
      (e.prototype.setPaused = function (t) {
        var e = this;
        e.options.autoplay === !0 && e.options.pauseOnHover === !0 && ((e.paused = t), t ? e.autoPlayClear() : e.autoPlay());
      }),
      (e.prototype.selectHandler = function (e) {
        var i = this,
          o = t(e.target).is(".slick-slide") ? t(e.target) : t(e.target).parents(".slick-slide"),
          n = parseInt(o.attr("data-slick-index"));
        return n || (n = 0), i.slideCount <= i.options.slidesToShow ? (i.setSlideClasses(n), void i.asNavFor(n)) : void i.slideHandler(n);
      }),
      (e.prototype.slideHandler = function (t, e, i) {
        var o,
          n,
          s,
          r,
          a = null,
          l = this;
        return (
          (e = e || !1),
            (l.animating === !0 && l.options.waitForAnimate === !0) || (l.options.fade === !0 && l.currentSlide === t) || l.slideCount <= l.options.slidesToShow
              ? void 0
              : (e === !1 && l.asNavFor(t),
                (o = t),
                (a = l.getLeft(o)),
                (r = l.getLeft(l.currentSlide)),
                (l.currentLeft = null === l.swipeLeft ? r : l.swipeLeft),
                l.options.infinite === !1 && l.options.centerMode === !1 && (0 > t || t > l.getDotCount() * l.options.slidesToScroll)
                  ? void (
                    l.options.fade === !1 &&
                    ((o = l.currentSlide),
                      i !== !0
                        ? l.animateSlide(r, function () {
                          l.postSlide(o);
                        })
                        : l.postSlide(o))
                  )
                  : l.options.infinite === !1 && l.options.centerMode === !0 && (0 > t || t > l.slideCount - l.options.slidesToScroll)
                  ? void (
                    l.options.fade === !1 &&
                    ((o = l.currentSlide),
                      i !== !0
                        ? l.animateSlide(r, function () {
                          l.postSlide(o);
                        })
                        : l.postSlide(o))
                  )
                  : (l.options.autoplay === !0 && clearInterval(l.autoPlayTimer),
                    (n =
                      0 > o
                        ? l.slideCount % l.options.slidesToScroll !== 0
                        ? l.slideCount - (l.slideCount % l.options.slidesToScroll)
                        : l.slideCount + o
                        : o >= l.slideCount
                        ? l.slideCount % l.options.slidesToScroll !== 0
                          ? 0
                          : o - l.slideCount
                        : o),
                    (l.animating = !0),
                    l.$slider.trigger("beforeChange", [l, l.currentSlide, n]),
                    (s = l.currentSlide),
                    (l.currentSlide = n),
                    l.setSlideClasses(l.currentSlide),
                    l.updateDots(),
                    l.updateArrows(),
                    l.options.fade === !0
                      ? (i !== !0
                      ? (l.fadeSlideOut(s),
                        l.fadeSlide(n, function () {
                          l.postSlide(n);
                        }))
                      : l.postSlide(n),
                        void l.animateHeight())
                      : void (i !== !0
                      ? l.animateSlide(a, function () {
                        l.postSlide(n);
                      })
                      : l.postSlide(n))))
        );
      }),
      (e.prototype.startLoad = function () {
        var t = this;
        t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow.hide(), t.$nextArrow.hide()),
        t.options.dots === !0 && t.slideCount > t.options.slidesToShow && t.$dots.hide(),
          t.$slider.addClass("slick-loading");
      }),
      (e.prototype.swipeDirection = function () {
        var t,
          e,
          i,
          o,
          n = this;
        return (
          (t = n.touchObject.startX - n.touchObject.curX),
            (e = n.touchObject.startY - n.touchObject.curY),
            (i = Math.atan2(e, t)),
            (o = Math.round((180 * i) / Math.PI)),
          0 > o && (o = 360 - Math.abs(o)),
            45 >= o && o >= 0
              ? n.options.rtl === !1
              ? "left"
              : "right"
              : 360 >= o && o >= 315
              ? n.options.rtl === !1
                ? "left"
                : "right"
              : o >= 135 && 225 >= o
                ? n.options.rtl === !1
                  ? "right"
                  : "left"
                : n.options.verticalSwiping === !0
                  ? o >= 35 && 135 >= o
                    ? "left"
                    : "right"
                  : "vertical"
        );
      }),
      (e.prototype.swipeEnd = function (t) {
        var e,
          i = this;
        if (((i.dragging = !1), (i.shouldClick = !(i.touchObject.swipeLength > 10)), void 0 === i.touchObject.curX)) return !1;
        if ((i.touchObject.edgeHit === !0 && i.$slider.trigger("edge", [i, i.swipeDirection()]), i.touchObject.swipeLength >= i.touchObject.minSwipe))
          switch (i.swipeDirection()) {
            case "left":
              (e = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide + i.getSlideCount()) : i.currentSlide + i.getSlideCount()),
                i.slideHandler(e),
                (i.currentDirection = 0),
                (i.touchObject = {}),
                i.$slider.trigger("swipe", [i, "left"]);
              break;
            case "right":
              (e = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide - i.getSlideCount()) : i.currentSlide - i.getSlideCount()),
                i.slideHandler(e),
                (i.currentDirection = 1),
                (i.touchObject = {}),
                i.$slider.trigger("swipe", [i, "right"]);
          }
        else i.touchObject.startX !== i.touchObject.curX && (i.slideHandler(i.currentSlide), (i.touchObject = {}));
      }),
      (e.prototype.swipeHandler = function (t) {
        var e = this;
        if (!(e.options.swipe === !1 || ("ontouchend" in document && e.options.swipe === !1) || (e.options.draggable === !1 && -1 !== t.type.indexOf("mouse"))))
          switch (
            ((e.touchObject.fingerCount = t.originalEvent && void 0 !== t.originalEvent.touches ? t.originalEvent.touches.length : 1),
              (e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold),
            e.options.verticalSwiping === !0 && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold),
              t.data.action)
            ) {
            case "start":
              e.swipeStart(t);
              break;
            case "move":
              e.swipeMove(t);
              break;
            case "end":
              e.swipeEnd(t);
          }
      }),
      (e.prototype.swipeMove = function (t) {
        var e,
          i,
          o,
          n,
          s,
          r = this;
        return (
          (s = void 0 !== t.originalEvent ? t.originalEvent.touches : null),
          !(!r.dragging || (s && 1 !== s.length)) &&
          ((e = r.getLeft(r.currentSlide)),
            (r.touchObject.curX = void 0 !== s ? s[0].pageX : t.clientX),
            (r.touchObject.curY = void 0 !== s ? s[0].pageY : t.clientY),
            (r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curX - r.touchObject.startX, 2)))),
          r.options.verticalSwiping === !0 && (r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curY - r.touchObject.startY, 2)))),
            (i = r.swipeDirection()),
            "vertical" !== i
              ? (void 0 !== t.originalEvent && r.touchObject.swipeLength > 4 && t.preventDefault(),
                (n = (r.options.rtl === !1 ? 1 : -1) * (r.touchObject.curX > r.touchObject.startX ? 1 : -1)),
              r.options.verticalSwiping === !0 && (n = r.touchObject.curY > r.touchObject.startY ? 1 : -1),
                (o = r.touchObject.swipeLength),
                (r.touchObject.edgeHit = !1),
              r.options.infinite === !1 &&
              ((0 === r.currentSlide && "right" === i) || (r.currentSlide >= r.getDotCount() && "left" === i)) &&
              ((o = r.touchObject.swipeLength * r.options.edgeFriction), (r.touchObject.edgeHit = !0)),
                r.options.vertical === !1 ? (r.swipeLeft = e + o * n) : (r.swipeLeft = e + o * (r.$list.height() / r.listWidth) * n),
              r.options.verticalSwiping === !0 && (r.swipeLeft = e + o * n),
              r.options.fade !== !0 && r.options.touchMove !== !1 && (r.animating === !0 ? ((r.swipeLeft = null), !1) : void r.setCSS(r.swipeLeft)))
              : void 0)
        );
      }),
      (e.prototype.swipeStart = function (t) {
        var e,
          i = this;
        return 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow
          ? ((i.touchObject = {}), !1)
          : (void 0 !== t.originalEvent && void 0 !== t.originalEvent.touches && (e = t.originalEvent.touches[0]),
            (i.touchObject.startX = i.touchObject.curX = void 0 !== e ? e.pageX : t.clientX),
            (i.touchObject.startY = i.touchObject.curY = void 0 !== e ? e.pageY : t.clientY),
            void (i.dragging = !0));
      }),
      (e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
        var t = this;
        null !== t.$slidesCache && (t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.appendTo(t.$slideTrack), t.reinit());
      }),
      (e.prototype.unload = function () {
        var e = this;
        t(".slick-cloned", e.$slider).remove(),
        e.$dots && e.$dots.remove(),
        e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(),
        e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(),
          e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
      }),
      (e.prototype.unslick = function (t) {
        var e = this;
        e.$slider.trigger("unslick", [e, t]), e.destroy();
      }),
      (e.prototype.updateArrows = function () {
        var t,
          e = this;
        (t = Math.floor(e.options.slidesToShow / 2)),
        e.options.arrows === !0 &&
        e.slideCount > e.options.slidesToShow &&
        !e.options.infinite &&
        (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
          e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
          0 === e.currentSlide
            ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"))
            : e.currentSlide >= e.slideCount - e.options.slidesToShow && e.options.centerMode === !1
            ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"))
            : e.currentSlide >= e.slideCount - 1 &&
            e.options.centerMode === !0 &&
            (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
      }),
      (e.prototype.updateDots = function () {
        var t = this;
        null !== t.$dots &&
        (t.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"),
          t.$dots
          .find("li")
          .eq(Math.floor(t.currentSlide / t.options.slidesToScroll))
          .addClass("slick-active")
          .attr("aria-hidden", "false"));
      }),
      (e.prototype.visibility = function () {
        var t = this;
        document[t.hidden] ? ((t.paused = !0), t.autoPlayClear()) : t.options.autoplay === !0 && ((t.paused = !1), t.autoPlay());
      }),
      (e.prototype.initADA = function () {
        var e = this;
        e.$slides
        .add(e.$slideTrack.find(".slick-cloned"))
        .attr({
          "aria-hidden": "true",
          tabindex: "-1",
        })
        .find("a, input, button, select")
        .attr({
          tabindex: "-1",
        }),
          e.$slideTrack.attr("role", "listbox"),
          e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function (i) {
            t(this).attr({
              role: "option",
              "aria-describedby": "slick-slide" + e.instanceUid + i,
            });
          }),
        null !== e.$dots &&
        e.$dots
        .attr("role", "tablist")
        .find("li")
        .each(function (i) {
          t(this).attr({
            role: "presentation",
            "aria-selected": "false",
            "aria-controls": "navigation" + e.instanceUid + i,
            id: "slick-slide" + e.instanceUid + i,
          });
        })
        .first()
        .attr("aria-selected", "true")
        .end()
        .find("button")
        .attr("role", "button")
        .end()
        .closest("div")
        .attr("role", "toolbar"),
          e.activateADA();
      }),
      (e.prototype.activateADA = function () {
        var t = this;
        t.$slideTrack
        .find(".slick-active")
        .attr({
          "aria-hidden": "false",
        })
        .find("a, input, button, select")
        .attr({
          tabindex: "0",
        });
      }),
      (e.prototype.focusHandler = function () {
        var e = this;
        e.$slider.on("focus.slick blur.slick", "*", function (i) {
          i.stopImmediatePropagation();
          var o = t(this);
          setTimeout(function () {
            e.isPlay && (o.is(":focus") ? (e.autoPlayClear(), (e.paused = !0)) : ((e.paused = !1), e.autoPlay()));
          }, 0);
        });
      }),
      (t.fn.slick = function () {
        var t,
          i,
          o = this,
          n = arguments[0],
          s = Array.prototype.slice.call(arguments, 1),
          r = o.length;
        for (t = 0; r > t; t++) if (("object" == typeof n || "undefined" == typeof n ? (o[t].slick = new e(o[t], n)) : (i = o[t].slick[n].apply(o[t].slick, s)), "undefined" != typeof i)) return i;
        return o;
      });
  }),
  jQuery(document).ready(function (t) {
    function e(e) {
      t(".slider-success-stories").length > 0 &&
      (e < 768
        ? (t(document).trigger("slider-color-change-to-dark-text"), t(".page-template-success-stories .slider-success-stories").slick("slickPause"))
        : t(".page-template-success-stories .slider-success-stories").slick("slickPlay"));
    }
    t(".slider-about").slick({
      arrows: !1,
      autoplay: !0,
      cssEase: "linear",
      fade: !0,
    }),
      t(".slider-eif").slick({
        arrows: !1,
        dots: !0,
        autoplay: !0,
        cssEase: "linear",
        dots: !0,
        fade: !0,
        mobileFirst: !0,
        responsive: [
          {
            breakpoint: 639,
            settings: {
              arrows: !0,
              nextArrow: '<a class="slick-next icon-angle-right have-a-question-arrows"></i></a>',
              prevArrow: '<a class="slick-prev icon-angle-left have-a-question-arrows"></i></a>',
              dots: !0,
              slidesToScroll: 1,
              slidesToShow: 1,
            },
          },
          {
            breakpoint: 1279,
            settings: "unslick",
          },
        ],
      }),
      t(".slider-visual-designer").slick({
        arrows: !1,
        autoplay: !0,
        autoplaySpeed: 6e3,
        cssEase: "linear",
        dots: !0,
        fade: !0,
        lazyLoad: "ondemand",
        slidesToShow: 1,
      }),
    t("#page-marietv").length > 0 &&
    t(document).on("marietv-show-question-posts", function () {
      t(".posts-content").slick({
        arrows: !0,
        autoplay: !1,
        dots: !1,
        nextArrow: '<a class="slick-next icon-angle-right have-a-question-arrows"></i></a>',
        prevArrow: '<a class="slick-prev icon-angle-left have-a-question-arrows"></i></a>',
        slidesToScroll: 5,
        slidesToShow: 5,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              arrows: !1,
              autoplay: !0,
              autoplaySpeed: 2e3,
              dots: !1,
              slidesToScroll: 1,
              slidesToShow: 1,
            },
          },
        ],
      });
    }),
      t(".slider-get-started").on("afterChange", function (e, i, o, n) {
        var s = t(this).find("[data-slick-index=" + o + "]");
        t(this).find("[data-slick-index=" + n + "]");
        t(s)
        .delay(100)
        .queue(function (e) {
          t(this).children(".slide-content").addClass("active"), t(this).children("h6").addClass("active"), e();
        }),
          t(s).hasClass("play-video") && t(window).width() > 768 ? t("#slide-video-marietv").get(0).play() : t("#slide-video-marietv").get(0).pause();
      }),
      t(".slider-get-started").on("beforeChange", function (e, i, o, n) {
        t(".slider-get-started").find(".slide-content").removeClass("active"), t(".slider-get-started").find("h6").removeClass("active");
      }),
      t(".fs_video_slider").slick({
        arrows: !0,
        prevArrow: '<a class="slick-prev fs-slick-arrow"><div class="fs-arrow-wrapper flex-vcenter"><img src="/wp-content/themes/marieforleo/assets/images/icon-caret-left.png"/><span>previous clip</span></div></a>',
        nextArrow: '<a class="slick-next fs-slick-arrow"><div class="fs-arrow-wrapper flex-vcenter"><span>next clip</span><img src="/wp-content/themes/marieforleo/assets/images/icon-caret-right.png"/></div></a>',
        autoplay: !1,
        dots: !0,
        mobileFirst: !0,
      }),
      t(".ymal-slick").slick({
        arrows: !1,
        autoplay: !0,
        dots: !0,
        infinite: !0,
        slidesToShow: 1,
      }),
      t(".first-time-here-testimonials").slick({
        arrows: !1,
        autoplay: !0,
        dots: !0,
        infinite: !0,
        slidesToShow: 1,
        speed: 500,
        autoplaySpeed: 6e3,
      }),
      t(".home .slider-success-stories-images").slick({
        arrows: !0,
        asNavFor: ".home .slider-success-stories",
        dots: !0,
        slidesToShow: 2,
      }),
      t(".home .slider-success-stories").slick({
        arrows: !1,
        asNavFor: ".slider-success-stories-images",
        autoplay: !0,
        autoplaySpeed: 6e3,
        dots: !1,
        fade: !0,
      }),
      t(".page-template-success-stories .slider-success-stories").slick({
        arrows: !1,
        autoplay: !0,
        dots: !0,
        fade: !0,
      }),
      t(".shop-quotes").slick({
        arrows: !1,
        autoplay: !0,
        dots: !1,
        fade: !0,
        autoplaySpeed: 2e3,
      }),
      t(".page-template-success-stories .slider-success-stories").on("beforeChange", function (e, i, o, n) {
        var s = n,
          r = i.$slides,
          a = t(r[s]).data("color");
        t("#page-success-stories .entry-banner-title").removeClass("dark-text light-text"), t("#page-success-stories .entry-banner-title").addClass(a), t(document).trigger("slider-color-change-to-" + a);
      }),
      t(document).on("slider-color-change-to-dark-text", function () {
        t("body").attr("data-nav-color", ""), t("#masthead, #site-navigation, .sliding-panel-button").removeClass("alt");
      }),
      t(document).on("slider-color-change-to-light-text", function () {
        t("body").attr("data-nav-color", "alt"), t("#masthead, #site-navigation, .sliding-panel-button").addClass("alt");
      }),
      t(".slider-podcast-reviews").slick({
        arrows: !1,
        centerMode: !0,
        centerPadding: "28px",
        dots: !0,
        focusOnSelect: !0,
        mobileFirst: !0,
        responsive: [
          {
            breakpoint: 413,
            settings: {
              centerPadding: "34px",
            },
          },
          {
            breakpoint: 639,
            settings: {
              arrows: !0,
              centerPadding: "159px",
              focusOnSelect: !1,
              nextArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--next"><span class="visuallyhidden">Next</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--next" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
              prevArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--prev"><span class="visuallyhidden">Prev</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--prev" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
            },
          },
          {
            breakpoint: 1023,
            settings: {
              arrows: !0,
              centerPadding: "238px",
              focusOnSelect: !1,
              nextArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--next"><span class="visuallyhidden">Next</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--next" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
              prevArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--prev"><span class="visuallyhidden">Prev</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--prev" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
            },
          },
          {
            breakpoint: 1439,
            settings: {
              arrows: !0,
              centerPadding: "287px",
              focusOnSelect: !1,
              nextArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--next"><span class="visuallyhidden">Next</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--next" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
              prevArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--prev"><span class="visuallyhidden">Prev</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--prev" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
            },
          },
          {
            breakpoint: 1919,
            settings: {
              arrows: !0,
              centerPadding: "360px",
              focusOnSelect: !1,
              nextArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--next"><span class="visuallyhidden">Next</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--next" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
              prevArrow:
                '<button type="button" class="slider-podcast-reviews__arrow slider-podcast-reviews__arrow--prev"><span class="visuallyhidden">Prev</span><img class="slider-podcast-reviews__arrow-icon slider-podcast-reviews__arrow-icon--prev" src="../wp-content/themes/marieforleo/assets/images/icon-podcast-page-slider-arrow.png"></button>',
            },
          },
        ],
        slidesToShow: 1,
      }),
      t(window).on("resize", function () {
        e(window.innerWidth);
      }),
      e(window.innerWidth);
  }),
  jQuery(function (t) {
    var e = t("body"),
      i = t(".c-sticky-ad"),
      o = "transitionend webkitTransitionEnd otransitionend MSTransitionEnd";
    t(".js-toggle-sticky-ad").on("click", function () {
      e.hasClass("animating") ||
      (e.addClass("animating"),
        i.toggleClass("active"),
        e.on(o, function () {
          e.removeClass("animating"), e.off(o);
        }));
    });
  }),
  jQuery(document).ready(function (t) {
    t(".success-stories-filter h3").on("click", function (e) {
      t(this).find(".menu-to-close").toggleClass("active"), t(this).next(".success-stories-filter-menu").toggleClass("active"), e.preventDefault();
    }),
      t(".success-stories-filter-menu li")
      .not(".special")
      .not(".external-link")
      .on("click", function (e) {
        var i = t(this).children("a").data("target");
        t(".success-stories-filter h3").trigger("click"),
          t(".stories").removeClass("success-stories marietv-love praise all"),
          t(".stories").addClass(i),
          t(".tab").removeClass("visible"),
          t(".tab").addClass("hidden"),
          t(".tab-" + i).removeClass("hidden"),
          t(".tab-" + i).addClass("visible"),
        "all" == i && (t(".tab").removeClass("hidden"), t(".tab").addClass("visible")),
        t(".stories").hasClass("closed") && (t(".stories").removeClass("closed").slideDown(500), t(".success-stories-filter.secondary").show()),
        t(".success-stories-form-wrap").hasClass("active") && t(".success-stories-form-wrap").removeClass("active").slideUp(500),
          t(".success-stories-filter-menu li").removeClass("current"),
          t(this).addClass("current"),
          e.preventDefault();
      }),
      t(".success-stories-filter-menu li.special").on("click", function (e) {
        t(".success-stories-form-wrap").hasClass("active") || t(".success-stories-filter h3").trigger("click"), e.preventDefault();
      }),
      t(".success-stories-filter-menu li.special, .add-your-story").on("click", function (e) {
        t(".success-stories-form-wrap").hasClass("active") ||
        (t(".success-stories-form-wrap").addClass("active").slideDown(500),
          t(".stories").addClass("closed").slideUp(500),
          t(".success-stories-filter-menu li").removeClass("current"),
          t(".success-stories-filter.secondary").hide(),
          t("html, body").animate(
            {
              scrollTop: t("#first").offset().top,
            },
            500
          )),
          e.preventDefault();
      }),
      t("#page-success-stories .story").on("click", function (e) {
        if (t(window).width() >= 768) {
          t(".full-width-story").remove();
          var i = t(this).data("target"),
            o = t(this).children("picture").html(),
            n = t(this).find(".intro").data("text"),
            s = t(this).find(".content").html(),
            r = '<div class="full-width-story"><a href="#" class="close" data-target="' + i + '">Close</a><picture>' + o + '</picture><p class="intro">' + n + "</p>" + s + "</div>",
            a = t(r)
            .clone()
            .css({
              height: "auto",
              width: "auto",
            })
            .appendTo("#page-success-stories .stories"),
            l = a.css("height");
          a.remove(),
            t(r)
            .insertAfter("#" + i)
            .animate(
              {
                height: l,
              },
              500,
              "swing"
            ),
            t("html, body")
            .stop()
            .animate(
              {
                scrollTop: t("#" + i).offset().top + t("#" + i).height(),
              },
              500,
              "swing",
              function () {
                t(".full-width-story").css("height", "auto");
              }
            );
        } else {
          var d = t(this).offset().top;
          t("html, body").stop().animate(
            {
              scrollTop: d,
            },
            500,
            "swing"
          ),
            t(this).hasClass("active") ? t(this).removeClass("active") : t(this).addClass("active");
        }
        e.preventDefault();
      }),
      t("#page-success-stories").on("click", ".full-width-story .close", function (e) {
        var i = t(this).parent();
        if (
          (i.stop().animate(
            {
              height: 0,
            },
            400,
            "linear",
            function () {
              t(this).remove();
            }
          ),
          t(window).width() >= 768)
        ) {
          var o = t(this).data("target");
          t("html, body")
          .stop()
          .animate(
            {
              scrollTop: t("#" + o).offset().top,
            },
            500,
            "swing"
          );
        }
        e.preventDefault();
      });
  }),
  (function (t) {
    t(document).ready(function () {
      function e(t, e, i) {
        var o = Number(new Date()) + (e || 2e3);
        i = i || 100;
        var n = function (e, s) {
          var r = t();
          r ? e(r) : Number(new Date()) < o ? setTimeout(n, i, e, s) : s(new Error("timed out for " + t + ": " + arguments));
        };
        return new Promise(n);
      }
      if ("undefined" != typeof __gaTracker) {
        t("body").on("click", 'a[title="Facebook"]', function () {
          "facebook" == t(this).data("sumome-share") && __gaTracker("send", "event", "Social Tracking", "facebook", "facebook share");
        }),
          t("body").on("click", 'a[title="Facebook Like"]', function () {
            "facebooklike" == t(this).data("sumome-share") && __gaTracker("send", "event", "Social Tracking", "facebooklike", "facebook like");
          }),
          t("body").on("click", 'a[title="Twitter"]', function () {
            "twitter" == t(this).data("sumome-share") && __gaTracker("send", "event", "Social Tracking", "twitter", "twitter share");
          }),
          t("body").on("click", 'a[title="LinkedIn"]', function () {
            "linkedin" == t(this).data("sumome-share") && __gaTracker("send", "event", "Social Tracking", "linkdedin", "linkedin share");
          });
      }
      t("body").on("submit", ".js-ga_optin_form", function (e) {
        var i = e;
        t(this)
        .find('input[data-required="true"]')
        .each(function (e, o) {
          t(o).val() || i.preventDefault();
        });
      }),
        e(
          function () {
            return window.om_loaded;
          },
          5e3,
          500
        )
        .then(function () {
          t(".seattle-background").find("style").remove();
        })
          ["catch"](function () {
          t(".seattle-background").find("style").remove();
        });
    });
  })(jQuery),
  jQuery(document).ready(function (t) {
    viewportRatio();
  }),
  jQuery(window).resize(function () {
    viewportRatio();
  }),
  (function (t) {
    t(document).ready(function () {
      var e = new Date(),
        i = t("#js_single_page_startTime").data("start_time"),
        o = e.getTime();
      (o = t.trim(o).substring(0, 10)), i < o && t("#js_single_page_startTime").trigger("click");
    });
  })(jQuery),
  jQuery(document).ready(function (t) {
    t(".word-stream .anything").wordStream({
      words: [
        "more confidence.",
        "a new business.",
        "total clarity.",
        "a new kitchen.",
        "a life of meaning.",
        "more courage.",
        "a creative career.",
        "new dance moves.",
        "an open mind.",
        "a trip to Italy.",
        "a country house.",
        "a simpler life.",
        "your first book.",
        "an island retreat.",
        "greater freedom.",
        "more time off.",
        "deeper joy.",
        "loads of laughs.",
        "world travel.",
        "higher profit margins.",
        "happier customers.",
        "a stronger body.",
        "more peace.",
        "greater happiness.",
        "joy.",
        "fulfillment.",
        "mo’ money.",
        "peace of mind.",
        "financial freedom.",
        "anything.",
      ],
      interval: 100,
      showingDelay: 600,
      stopOnHoverEnd: !0,
    }),
      t(".contact-stream").wordStream({
        words: [
          "goodie",
          "unicorn",
          "surfboard",
          "mixtape",
          "chia pet",
          "stuffed crocodile",
          "llama",
          "leprachaun",
          "soup spoon",
          "sewing kit",
          "baby chick",
          "spare tire",
          "book",
          "green juice",
          "llama",
          "basil guacamole",
          "smurf",
        ],
        interval: 500,
        stopOnHoverEnd: !0,
      });
  }),
  (function (t) {
    function e(e, i) {
      function o() {
        t(e)
        .delay(s)
        .animate(
          {
            opacity: 0,
          },
          i.interval,
          function () {
            t(e)
            .text(i.words[i.words.length - 1])
            .delay(i.hidingDelay)
            .animate(
              {
                opacity: 1,
              },
              i.interval
            );
          }
        );
      }
      function n() {
        (s = r === -1 ? 0 : i.showingDelay),
          r++,
          t(e)
          .delay(s)
          .animate(
            {
              opacity: 0,
            },
            i.interval,
            function () {
              r < i.words.length - 1
                ? t(e)
                .text(i.words[r])
                .delay(i.hidingDelay)
                .animate(
                  {
                    opacity: 1,
                  },
                  i.interval,
                  function () {
                    console.log(e.running), e.running ? n() : o();
                  }
                )
                : o();
            }
          );
      }
      var s,
        r = -1;
      (e.running = !0), n();
    }
    t.fn.wordStream = function (i) {
      var o = t.extend(
        {
          interval: 1e3,
          showingDelay: 0,
          hidingDelay: 0,
          stopOnHoverEnd: !1,
        },
        i
      );
      return (
        (this.running = !1),
          t(this).hover(
            function () {
              e(this, o);
            },
            function () {
              o.stopOnHoverEnd && (this.running = !1);
            }
          ),
          this
      );
    };
  })(jQuery);
