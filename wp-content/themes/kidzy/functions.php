<?php
  define('KIDZY_NAME', wp_get_theme()->get( 'Name' ));
  define('KIDZY_CSS', get_template_directory_uri().'/css/');
  define('KIDZY_JS', get_template_directory_uri().'/js/');
  
  define( 'KIDZY_HELPERS', 			__DIR__ . '/lib/helpers/' );
  define( 'KIDZY_ASSETS', 			__DIR__ . '/assets/' );
  
  require_once( KIDZY_HELPERS . 'api.php' );
  require_once( KIDZY_HELPERS . 'metaxbox-categories.php' );
  require_once( KIDZY_HELPERS . 'widget-categories.php' );
  require_once( KIDZY_HELPERS . 'widget-recent-posts.php' );
  require_once( KIDZY_HELPERS . 'media.php' );
  require_once( KIDZY_HELPERS . 'wordpress.php' );
  /*-----------------------------------------------------
   * 				SEO FACEBOOK
  *----------------------------------------------------*/
  //Adding the Open Graph in the Language Attributes
  function add_opengraph_doctype($output) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
  }
  
  add_filter('language_attributes', 'add_opengraph_doctype');
  
  //add Open Graph Meta Info
  function insert_fb_in_head() {
    global $post;
    if (!is_singular()) //if it is not a post or a page
      return;
    
    if ($excerpt = $post->post_excerpt) {
      $excerpt = strip_tags($post->post_excerpt);
    }
    else {
      $excerpt = get_bloginfo('description');
    }
    
    ob_start();
    ?>
    <meta property="fb:app_id" content="YOUR APPID"/>
    <meta property="og:title" content="<?php echo get_the_title(); ?>"/>
    <meta property="og:description" content="<?php echo $excerpt; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo get_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta name="twitter:title" content="<?php echo get_the_title(); ?>"/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="<?php echo $excerpt; ?>" />
    <meta name="twitter:url" content="<?php echo get_permalink(); ?>"/>
    
    <?php if (!has_post_thumbnail($post->ID)) : ?>
      <?php $default_image = "http://example.com/image.jpg"; //<--replace this with a default image on your server or an image in your media library ?>
      <meta property="og:image" content="<?php echo $default_image; ?>"/>
      <meta name="twitter:image" content="<?php echo $default_image; ?>"/>
    <?php else: ?>
      <?php $thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); ?>
      <meta property="og:image" content="<?php echo esc_attr($thumbnail_src[0]); ?>"/>
      <meta name="twitter:image" content="<?php echo esc_attr($thumbnail_src[0]); ?>"/>
    <?php endif; ?>
    <?php
    echo ob_get_clean();
  }
  
  add_action('wp_head', 'insert_fb_in_head', 5);
  /*-----------------------------------------------------
   * 				KIDZY ShortCode
  *----------------------------------------------------*/
  add_shortcode( 'kidzy_get_doitac', 'kidzy_get_doitac' );
  function kidzy_get_doitac($attributes) {
    
    extract( shortcode_atts( array(), $attributes ) );
    
    $args = array(
      'post_type' => 'doitac',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby' => 'title',
      'order' => 'ASC'
    );
    
    $query = new WP_Query( $args );
    $items = $query->posts;
    $true_image = get_site_url().'/wp-content/themes/kidzy/assets/img/truecolors-logo.png';
    
    ob_start();
    ?>
    <?php if (!empty($items)) : ?>
      <div class="doitac">
        <div class="doitac__inner relative">
          <?php if (!empty($true_image)) : ?>
            <div class="doitac__image">
              <figure class="doitac__image-wrapper" style="background-image: url(<?php echo $true_image; ?>);">
                <span class="image-alt" role="img" aria-label="<?php echo esc_attr( 'truecolors logo' ); ?>"></span>
              </figure>
            </div>
          <?php endif; ?>
          <div class="doitac__list">
            <?php foreach($items as $item) : ?>
              <div class="doitac__item">
                <div class="doitac__item-inner">
                  <div class="doitac__item-icon">
                    <?php $image = wp_get_attachment_url( get_post_thumbnail_id($item->ID), 'thumbnail' ); ?>
                    <?php if (!empty($image)) : ?>
                      <div class="doitac__item-image">
                        <figure class="doitac__item-image-wrapper" style="background-image: url(<?php echo $image; ?>);">
                          <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $item->post_title ); ?>"></span>
                        </figure>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div class="doitac__item-content">
                    <h4 class="doitac__item-title block relative"><?php echo $item->post_title; ?></h4>
                    <div class="doitac__item-description"><?php echo wpautop($item->post_content); ?></div>
                  </div>
                </div>
              </div>
            <?php
            endforeach;
            ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
  }
  
  add_shortcode( 'kidzy_get_locations', 'kidzy_get_locations' );
  function kidzy_get_locations($attributes) {
    
    extract( shortcode_atts( array(
      'title' => __('Thông tin các cơ sở', 'kidzy'),
    ), $attributes ) );
    
    $args = array(
      'post_type' => 'location',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby' => 'title',
      'order' => 'ASC'
    );
    
    $query = new WP_Query( $args );
    $locations = $query->posts;
    
    ob_start();
    ?>
    <?php if (!empty($locations)) : ?>
      <div class="locations">
        <div class="locations__inner">
          <h2 class="locations__title"><?php echo $title; ?></h2>
          <div class="locations__list">
            <?php
              foreach($locations as $location) :
                $is_new = get_post_meta($location->ID,'themeum_location_new',true);
                ?>
                <div class="locations__item">
                  <div class="locations__item-inner">
                    <div class="locations__item-icon">
                      <svg viewBox="0 -20 464 464" xmlns="http://www.w3.org/2000/svg"><path d="m340 0c-44.773438.00390625-86.066406 24.164062-108 63.199219-21.933594-39.035157-63.226562-63.19531275-108-63.199219-68.480469 0-124 63.519531-124 132 0 172 232 292 232 292s232-120 232-292c0-68.480469-55.519531-132-124-132zm0 0" fill="#ff6243"/><path d="m32 132c0-63.359375 47.550781-122.359375 108.894531-130.847656-5.597656-.769532-11.242187-1.15625025-16.894531-1.152344-68.480469 0-124 63.519531-124 132 0 172 232 292 232 292s6-3.113281 16-8.992188c-52.414062-30.824218-216-138.558593-216-283.007812zm0 0" fill="#ff5023"/></svg>
                    </div>
                    <div class="locations__item-content">
                      <a target="_blank" href="http://maps.google.com/?q=truecolors-<?php echo $location->post_excerpt; ?>" class="locations__item-title block relative">
                        <?php echo $location->post_title; ?>
                        <?php if (!empty($is_new)) : ?>
                          <div class="locations__item-content-new">
                            <?= _get_svg('icon-new');?>
                          </div>
                        <?php endif; ?>
                      </a>
                      <div class="locations__item-description"><?php echo wpautop($location->post_content); ?></div>
                    </div>
                  </div>
                </div>
              <?php
              endforeach;
            ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
  }
  
  add_shortcode( 'kidzy_get_grapeseed', 'kidzy_get_grapeseed' );
  function kidzy_get_grapeseed($attributes) {
    
    extract( shortcode_atts( array(
      'title' => __('GrapeSEED quốc tế nói gì về True Colors? ', 'kidzy'),
    ), $attributes ) );
    
    $args = array(
      'post_type' => 'grapeseed',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby' => 'title',
      'order' => 'ASC'
    );
    
    $query = new WP_Query( $args );
    $grapeseeds = $query->posts;
    
    ob_start();
    ?>
    <?php if (!empty($grapeseeds)) : ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
      <script>
        jQuery(document).ready(function($) {
          jQuery('#grapeseed-courses').slick({
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            centerMode: true,
            variableWidth: true,
            prevArrow:
              "<button type=\"button\" class=\"slick-prev\"><i class=\"fa fa-angle-left\"></button>",
            nextArrow:
              "<button type=\"button\" class=\"slick-next\"><i class=\"fa fa-angle-right\"></button>"
          });
        });
      </script>
      <div class="grapeseed">
        <div class="grapeseed__inner">
          <h2 class="grapeseed__title"><?php echo $title; ?></h2>
          <div id="grapeseed-courses" class="relative grapeseed__list">
            <?php
              foreach($grapeseeds as $grapeseed) :
                $image = wp_get_attachment_url( get_post_thumbnail_id($grapeseed->ID), 'thumbnail' );
                ?>
                <div class="grapeseed__item">
                  <div class="grapeseed__item-inner">
                    <div class="grapeseed__item-content">
                      <?php if (!empty($image)) : ?>
                        <div class="grapeseed__item-image">
                          <figure class="grapeseed__item-image-wrapper" style="background-image: url(<?php echo $image; ?>);">
                            <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $grapeseed->post_title ); ?>"></span>
                          </figure>
                        </div>
                      <?php endif; ?>
                      <div class="grapeseed__item-info">
                        <div class="grapeseed__item-title">
                          <i class="fa fa-quote-left grapeseed__item-quote-left"></i>
                          <?php echo $grapeseed->post_title; ?>
                          <i class="fa fa-quote-right grapeseed__item-quote-right"></i>
                        </div>
                        <div class="grapeseed__item-description"><?php echo wpautop($grapeseed->post_content); ?></div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
              endforeach;
            ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
  }
  
  add_shortcode( 'kidzy_get_gallery', 'kidzy_get_gallery' );
  function kidzy_get_gallery($attributes) {
    
    extract( shortcode_atts( array(
      'title' => __('TRUE COLORS GALLERY', 'kidzy'),
      'description' => __('Ngay từ những ngày đầu thành lập, True Colors đã tổ chức rất nhiều các hoạt động ý nghĩa để tạo môi trường phát triển khả năng Tiếng Anh cũng như phát triển nhân cách của các con. Hãy cùng nhìn lại xem True Colors đã làm được những gì nhé!!!!', 'kidzy'),
    ), $attributes ) );
    
    $args = array(
      'post_type' => '$gallery',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby' => 'title',
      'order' => 'ASC'
    );
    
    $query = new WP_Query( $args );
    $gallerys = $query->posts;
    
    ob_start();
    ?>
    <?php if (!empty($gallerys)) : ?>
      <div class="gallery">
        <div class="gallery__inner">
          <h2 class="gallery__title"><?php echo $title; ?></h2>
          <p class="gallery__description"><?php echo $description; ?></p>
          <div class="gallery__list">
            <?php
              foreach($gallerys as $gallery) :
                $gallery_facebook_link = get_post_meta($gallery->ID,'themeum__gallery_facebook_link',true);
                $image = wp_get_attachment_url( get_post_thumbnail_id($gallery->ID), 'thumbnail' );
                ?>
                <div class="gallery__item">
                  <div class="gallery__item-inner">
                    <div class="gallery__item-content relative">
                      <?php if (!empty($image)) : ?>
                        <a href="<?php echo $gallery_facebook_link; ?>" class="gallery__item-link block">
                          <div class="gallery__item-image">
                            <figure class="gallery__item-image-wrapper" style="background-image: url(<?php echo $image; ?>);">
                              <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $gallery->post_title ); ?>"></span>
                            </figure>
                          </div>
                        </a>
                      <?php endif; ?>
                      <div class="gallery__item-bottom">
                        <a href="<?php echo $gallery_facebook_link; ?>" class="gallery__item-title white">
                          <?php echo $gallery->post_title; ?>
                        </a>
                        <div class="gallery__item-description"><?php echo wpautop($gallery->post_content); ?></div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
              endforeach;
            ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
  }
  
  add_shortcode( 'kidzy_get_testimonials', 'kidzy_get_testimonials' );
  function kidzy_get_testimonials($attributes) {
    
    extract( shortcode_atts( array(
      'title' => __('Phụ huynh nói gì về True Colors', 'kidzy'),
    ), $attributes ) );
    
    $args = array(
      'post_type' => 'testimonial',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby' => 'title',
      'order' => 'ASC'
    );
    
    $query = new WP_Query( $args );
    $testimonials = $query->posts;
    
    ob_start();
    ?>
    <?php if (!empty($testimonials)) : ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
      <script>
        jQuery(document).ready(function($) {
          jQuery('#testimonials').slick({
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            centerMode: true,
            variableWidth: true,
            prevArrow:
              "<button type=\"button\" class=\"slick-prev\"><i class=\"fa fa-angle-left\"></button>",
            nextArrow:
              "<button type=\"button\" class=\"slick-next\"><i class=\"fa fa-angle-right\"></button>"
          });
        });
      </script>
      <h2 class="testimonials__title"><?php echo $title; ?></h2>
      <div id="testimonials" class="carousel-slick relative testimonials">
        <?php
          foreach($testimonials as $testimonial) :
            $sub_title = get_post_meta($testimonial->ID,'themeum__testimonial_sub_title',true);
            $image = wp_get_attachment_url( get_post_thumbnail_id($testimonial->ID), 'thumbnail' );
            ?>
            <div class="testimonials__item">
              <div class="testimonials__item-inner">
                <div class="testimonials__item-bg"></div>
                <div class="testimonials__item-image">
                  <figure class="testimonials__item-image-wrapper" style="background-image: url(<?php echo $image; ?>);">
                    <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $testimonial->post_title ); ?>"></span>
                  </figure>
                </div>
                <div class="testimonials__item-content">
                  <h4 class="testimonials__item-title"><?php echo $testimonial->post_title; ?></h4>
                  <h5 class="testimonials__item-sub-title"><?php echo $sub_title; ?></h5>
                  <div class="testimonials__item-description"><?php echo wpautop($testimonial->post_content); ?></div>
                </div>
              </div>
            </div>
          <?php
          endforeach;
        ?>
      </div>
    <?php endif; ?>
    <?php
    return ob_get_clean();
  }
  
  /*-----------------------------------------------------
   * 				KIDZY Options
  *----------------------------------------------------*/
  if(!function_exists('kidzy_options')):
    function kidzy_options($arg) {
      global $themeum_options;
      if (isset($themeum_options[$arg])) {
        return $themeum_options[$arg];
      } else {
        return false;
      }
    }
  endif;
  
  if(!function_exists('kidzy_options_url')):
    function kidzy_options_url($arg,$arg2) {
      global $themeum_options;
      if (isset($themeum_options[$arg][$arg2])) {
        return $themeum_options[$arg][$arg2];
      } else {
        return false;
      }
    }
  endif;
  
  
  /*-------------------------------------------*
   *				Register Navigation
   *------------------------------------------*/
  register_nav_menus( array(
    'primary' 	=> esc_html__( 'Primary Menu', 'kidzy' )
  ) );
  
  /*-------------------------------------------*
   *				title tag
   *------------------------------------------*/
  /*Used only here*/
  
  add_action( 'after_setup_theme', 'kidzy_slug_setup' );
  if(!function_exists( 'kidzy_slug_setup' )):
    function kidzy_slug_setup() {
      add_theme_support( 'title-tag' );
      add_theme_support( 'post-formats', array('image','video','quote','link','gallery','audio'));
    }
  endif;
  
  /*-------------------------------------------*
   *				Navwalker
   *------------------------------------------*/
  require_once( get_template_directory()  . '/lib/menu/admin-megamenu-walker.php');
  require_once( get_template_directory()  . '/lib/menu/meagmenu-walker.php');
  require_once( get_template_directory()  . '/lib/menu/mobile-navwalker.php');
  add_filter( 'wp_edit_nav_menu_walker', function( $class, $menu_id ){
    return 'Themeum_Megamenu_Walker';
  }, 10, 2 );
  
  
  /*-------------------------------------------*
   *				Kidzy Register
   *------------------------------------------*/
  require_once( get_template_directory()  . '/lib/main-function/themeum-register.php');
  
  
  /*-------------------------------------------------------
   *			Themeum Core
   *-------------------------------------------------------*/
  require_once( get_template_directory()  . '/lib/main-function/themeum-core.php');
  
  
  /*-----------------------------------------------------
   * 				Custom Excerpt Length
   *----------------------------------------------------*/
  
  if(!function_exists('kidzy_excerpt_max_charlength')):
    function kidzy_excerpt_max_charlength($charlength) {
      $excerpt = get_the_excerpt();
      $charlength++;
      
      if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
          return mb_substr( $subex, 0, $excut );
        } else {
          return $subex;
        }
        
      } else {
        return $excerpt;
      }
    }
  endif;
  
  
  /*-----------------------------------------------------
   * 				Custom body class
   *----------------------------------------------------*/
  add_filter( 'body_class', 'kidzy_body_class' );
  function kidzy_body_class( $classes ) {
    $menu_style = 'none';
    
    if ( kidzy_options('boxfull-en') ) {
      $layout = esc_attr(kidzy_options('boxfull-en'));
    }else{
      $layout = 'fullwidth';
    }
    
    $classes[] = $layout.'-bg';
    return $classes;
  }
  
  
  /*-------------------------------------------------------
   *			Themeum Pagination
   *-------------------------------------------------------*/
  
  if(!function_exists('themeum_pagination')):
    
    function themeum_pagination($pages = '', $range = 2)
    {
      $showitems = ($range * 1)+1;
      
      global $paged;
      
      if(empty($paged)) $paged = 1;
      
      if($pages == '')
      {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        
        if(!$pages)
        {
          $pages = 1;
        }
      }
      
      if(1 != $pages)
      {
        echo "<div class='row'><div class='col-sm-12 col-xs-12 text-center'><ul class='pagination'>";
        
        if($paged > 2 && $paged > $range+1 && $showitems < $pages){
          echo "<li><a href='".get_pagenum_link(1)."'>&laquo;</a></li>";
        }
        
        if($paged > 1 && $showitems < $pages){
          echo '<li>';
          previous_posts_link(esc_html__("Previous","kidzy"));
          echo '</li>';
        }
        
        for ($i=1; $i <= $pages; $i++)
        {
          if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
          {
            echo ($paged == $i)? "<li class='active'><a href='#'>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."' >".$i."</a></li>";
          }
        }
        
        if ($paged < $pages && $showitems < $pages){
          echo '<li>';
          next_posts_link(esc_html__("Next","kidzy"));
          echo '</li>';
        }
        
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
          echo "<li><a href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
        }
        
        echo "</ul></div></div>";
      }
    }
  
  endif;
  
  
  /*-------------------------------------------------------
  *			Custom Widgets Include
  *-------------------------------------------------------*/
// all custom widgets
  require_once( get_template_directory()  . '/lib/widgets/themeum_about_widget.php');
  require_once( get_template_directory()  . '/lib/widgets/popular-post.php');
  require_once( get_template_directory()  . '/lib/widgets/class-list-widget.php');
  
  
  /*-------------------------------------------*
   *				woocommerce support
   *------------------------------------------*/
  add_action( 'after_setup_theme', 'kidzy_woocommerce_support' );
  function kidzy_woocommerce_support() {
    add_theme_support( 'woocommerce' );
  }
  
  
  /*-----------------------------------------------------
   * 				Custom body class
   *----------------------------------------------------*/
  add_filter( 'body_class', 'kidzy_body_classs' );
  function kidzy_body_classs( $classes ) {
    $menu_style = 'none';
    if ( kidzy_options('boxfull-en') ) {
      $layout = esc_attr(kidzy_options('boxfull-en'));
    }else{
      $layout = 'fullwidth';
    }
    $classes[] = $layout.'-bg';
    return $classes;
  }
