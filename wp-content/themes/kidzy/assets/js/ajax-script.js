jQuery(document).ready(function($) {
  jQuery('.js-handle-click-like').click(function(e) {
    e.preventDefault();
    var element = e.target;
    var postId = element.getAttribute('data-id');
    var data = {
      'action': 'handle_like',
      'post_id': postId
    };
    jQuery.ajax({
      type: "get",
      dataType: "json",
      url: ajax_object.ajax_url,
      data: data,
      success: function(response){
        if (response) {
          var success = response['success'];
          var numberLike = response['like'];
          if (success) {
            jQuery(element).addClass('single-news__item-like--is-active');
            jQuery(element).find('.js-like-count').html(numberLike);
          }
        }
      }
    });
  });

});
