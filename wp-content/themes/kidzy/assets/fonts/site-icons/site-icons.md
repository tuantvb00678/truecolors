## Site Icons

This folder contains auto-generated font files via the grunt-webfont package. Place all required icons as SVG documents in the `src/icons/` and do not edit these files directly!