<?php 
/*
 * Template Name: Coming Soon
 */
get_header('alternative'); ?>

<?php
    $comingsoon_date = '';
    if ( kidzy_options('comingsoon-date') ) {
        $comingsoon_date = esc_attr( kidzy_options('comingsoon-date') );
    }
?>

    <section class="coming-soon-section text-center">

        <?php if( kidzy_options_url('comingsoon-logo','url')): ?>
            <div class="coming-soon-logo">
                <img src="<?php echo esc_url( kidzy_options_url('comingsoon-logo','url')); ?>" alt="<?php  esc_html_e( '404 error', 'kidzy' ); ?>">
            </div>
        <?php endif; ?>

        <?php
            if($comingsoon_date > date('m-d-Y h:i')){ 
                $sdate = str_replace('-', '/', $comingsoon_date); 
            } 
        ?>
        
        <!-- Coiming Soon Page -->
        <ul id="count-down" class="coming-soon-counter" data-datedata="<?php echo $sdate; ?>">
            <li><p class="days"></p><span><?php esc_html_e( 'Days', 'kidzy' ); ?></span></li>
            <li><p class="hours"></p><span><?php esc_html_e( 'Hours', 'kidzy' ); ?></span></li>
            <li><p class="minutes"></p><span><?php esc_html_e( 'Minutes', 'kidzy' ); ?></span></li>
            <li><p class="seconds"></p><span><?php esc_html_e( 'Seconds', 'kidzy' ); ?></span></li>
        </ul>

        <ul class="social-icon">
            <li>
                <?php if ( kidzy_options('topbar-social')) { ?>
                    <?php get_template_part( 'lib/social-icons'); ?>
                <?php } ?>  
            </li>
        </ul>

        <?php if ( kidzy_options('comingsoon-subtitle') ){ ?>
            <p><?php echo esc_html( kidzy_options('comingsoon-subtitle' )); ?></p>
        <?php }?>

        <form action="#" method="post" class="coming-soon-subs-form">
            <input type="email" placeholder="Enter your email here" name="email">
            <button type="submit" class="primary-btn"><span><?php esc_html_e( 'Submit', 'kidzy' ); ?></span></button>
        </form>

        <div class="text-center comingsoon-footer">
            
            <?php if ( kidzy_options('comingsoon-copyright') ){?>
                <div class="copyright-text">
                    <p><?php echo esc_html(kidzy_options('comingsoon-copyright')); ?></p>
                </div>
            <?php }?>   
        </div><!--/.comingsoon-footer-->
    </section><!--/.comingsoon-->


<?php get_footer('alternative');