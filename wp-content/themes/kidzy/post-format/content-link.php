<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <?php 
    if(is_single()){ #Blog details page
    ?>

    <?php if(function_exists('rwmb_meta')){ ?>
    <div class="featured-wrap">
        <div class="entry-link-post-format single-post-link">  
            <?php  if ( get_post_meta( get_the_ID(), 'themeum_link',true ) ) { ?>
                <h4><?php echo esc_url( get_post_meta( get_the_ID(), 'themeum_link',true ) ); ?></h4>
            <?php } ?>
        </div>  
    </div>
    <?php } ?>

    <?php } 
    else{ ?>
        <?php if(function_exists('rwmb_meta')){ ?>
        <div class="post-thumb post-thumb-link pull-left">
            <?php  if ( get_post_meta( get_the_ID(), 'themeum_link',true ) ) { ?>
                <h4><?php echo esc_url( get_post_meta( get_the_ID(), 'themeum_link',true ) ); ?></h4>
            <?php } ?>
        </div>
        <?php } ?>
    <?php } ?>

    <?php get_template_part( 'post-format/entry-content' ); ?> 

<div class="clearfix"></div>    
</article> <!--/#post -->