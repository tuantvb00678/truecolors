<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php if ( has_post_thumbnail() ){ ?>
    <?php $post_new = get_post_meta( the_ID(), 'themeum__post_new',true ); ?>
    <?php
    if(is_single()){ ?>
      <div class="blog-details-img relative 2">
        <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail('kidzy-blog-single', array('class' => 'img-responsive')); ?>
        </a>
  
        <?php $post_new = get_post_meta( the_ID(), 'themeum__post_new',true ); ?>
        <?php if (!empty($post_new)) : ?>
          <div class="single-news__item-new">
            <?= _get_svg('icon-new');?>
          </div>
        <?php endif; ?>
      </div>
    <?php } //End of if
    
    else{ ?>
      <div class="post-thumb pull-left relative 1">
        <?php the_post_thumbnail('kidzy-blog-thumbnails', array('class' => 'img-responsive')); ?>
  
        <?php if (!empty($post_new)) : ?>
          <div class="single-news__item-new">
            <?= _get_svg('icon-new');?>
          </div>
        <?php endif; ?>
      </div>
    <?php }//End of else
    ?>
  
  
  <?php }
    get_template_part( 'post-format/entry-content' ); ?>


  <div class="clearfix"></div>
</article> <!--/#post-->
