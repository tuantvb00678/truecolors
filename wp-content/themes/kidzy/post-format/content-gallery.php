<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php 
    if(is_single()){ #Blog details page
    ?> 
    <?php if(function_exists('rwmb_meta')){ ?>
    <div class="featured-wrap">
        <div class="entry-content-gallery">
            
                <?php  if ( rwmb_meta('themeum_gallery_images','type=image_advanced') ) { ?>
                    <?php $slides = rwmb_meta('themeum_gallery_images','type=image_advanced'); ?>
                        <?php if(count($slides) > 0) { ?>
                            <div id="blog-gallery-slider<?php echo get_the_ID(); ?>" class="carousel slide blog-gallery-slider">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php $slide_no = 1; ?>
                                    <?php foreach( $slides as $slide ) { ?>
                                    <div class="item <?php if($slide_no == 1) echo 'active'; ?>">
                                       <?php $images = wp_get_attachment_image_src( esc_attr($slide['ID']), 'kidzy-blog-single' ); ?>
                                        <img class="img-responsive" src="<?php echo esc_url($images[0]); ?>" alt="<?php  esc_html_e( 'image', 'kidzy' ); ?>">
                                    </div>
                                    <?php $slide_no++; ?>
                                    <?php } ?>
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-left" href="#blog-gallery-slider<?php echo get_the_ID(); ?>" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right carousel-right" href="#blog-gallery-slider<?php echo get_the_ID(); ?>" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                    <?php } ?>
                <?php } ?>
            
        </div><!--/.entry-content-gallery-->
    </div>
    <?php }//function exists ?>
    <?php } //End of if
    else{ ?>
        <?php if(function_exists('rwmb_meta')){ ?>
            <div class="post-thumb post-thumb-quote pull-left">
                <?php  if ( rwmb_meta('themeum_gallery_images','type=image_advanced') ) { ?>
                    <?php $slides = rwmb_meta('themeum_gallery_images','type=image_advanced'); ?>
                        <?php if(count($slides) > 0) { ?>
                            <div id="blog-gallery-slider<?php echo get_the_ID(); ?>" class="carousel slide blog-gallery-slider">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php $slide_no = 1; ?>
                                    <?php foreach( $slides as $slide ) { ?>
                                    <div class="item <?php if($slide_no == 1) echo 'active'; ?>">
                                       <?php $images = wp_get_attachment_image_src( esc_attr($slide['ID']), 'kidzy-blog-thumbnails' ); ?>
                                        <img class="img-responsive" src="<?php echo esc_url($images[0]); ?>" alt="<?php  esc_html_e( 'image', 'kidzy' ); ?>">
                                    </div>
                                    <?php $slide_no++; ?>
                                    <?php } ?>
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-left" href="#blog-gallery-slider<?php echo get_the_ID(); ?>" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right carousel-right" href="#blog-gallery-slider<?php echo get_the_ID(); ?>" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <?php }//function exists ?>
    <?php }//End of else ?>

    <?php get_template_part( 'post-format/entry-content' ); ?> 

<div class="clearfix"></div>
</article> <!--/#post -->



