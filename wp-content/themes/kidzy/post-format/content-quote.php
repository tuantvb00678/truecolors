<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    

    <?php 
    if(is_single()){ #Blog details page
    ?>   
        
        <div class="featured-wrap single-post-quote">
            <div class="entry-quote-post-format">
                <blockquote>
                <?php if(function_exists('rwmb_meta')){ ?>
                    <?php  if ( get_post_meta( get_the_ID(), 'themeum_qoute',true ) ) { ?>
                        <p><?php echo esc_html(get_post_meta( get_the_ID(), 'themeum_qoute',true )); ?></p>
                        <small><?php echo esc_html(get_post_meta( get_the_ID(), 'themeum_qoute_author',true )); ?></small>
                    <?php } ?>
                <?php } ?>
                </blockquote>
            </div>    
        </div>

    <?php } //End of if

    else{ #Blog summary page
    ?>
        <div class="post-thumb post-thumb-quote pull-left">
            <blockquote>
                <?php if(function_exists('rwmb_meta')){ ?>
                    <?php  if ( get_post_meta( get_the_ID(), 'themeum_qoute',true ) ) { ?>
                        <p><?php echo esc_html(get_post_meta( get_the_ID(), 'themeum_qoute',true )); ?></p>
                        <small><?php echo esc_html(rwmb_meta( 'themeum_qoute_author' )); ?></small>
                    <?php } ?>
                <?php } ?>
            </blockquote>
        </div>

    <?php }//End of else
    ?>

    <?php get_template_part( 'post-format/entry-content' ); ?>

<div class="clearfix"></div>     
</article> <!--/#post -->