<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
  <div class="article__inner">
  
    <?php $post_id = get_the_ID(); ?>
    <?php $post_new = get_post_meta( get_the_ID(), 'themeum__post_new',true ); ?>
    <?php if(!is_single()) : ?>
      <?php
        $views = get_post_meta( get_the_ID(), 'themeum__post_view_number',true );
        $comments = get_post_meta( get_the_ID(), 'themeum__post_comment_number',true );
        $likes = get_post_meta( get_the_ID(), 'themeum__post_like_number',true );
      
        $comments_count = get_comments_number( get_the_ID() );
        $total_comment = ($comments_count <= 1) ? 0 : $comments_count;
        $total_views = ($views <= 1) ? 0 : $views;
        $total_likes = ($likes <= 1) ? 0 : $likes;
      
      ?>
      <div class="article__action f jcb aic">
        <div class="article__action-group f jcc aic">
          <a href="<?php the_permalink(); ?>" class="article__view f jcc aic black">
            <?= _get_svg('icon-view');?> <?php echo $total_views; ?>
          </a>
          <a href="<?php the_permalink(); ?>" class="article__comment f jcc aic black">
              <?= _get_svg('icon-comment');?> <?php echo $total_comment; ?>
          </a>
        </div>
        <div class="article__like f jcc aic js-handle-click-like" data-id="<?php echo $post_id?>">
          <?= _get_svg('icon-heart');?> <span class="js-like-count"><?php echo $total_likes; ?></span>
        </div>
      </div>
    <?php endif; ?>
    
    <?php if ( has_post_thumbnail() ){ ?>
      
      <?php
      if(is_single()){ ?>
        <div class="blog-details-img relative 4">
          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('kidzy-blog-single', array('class' => 'img-responsive')); ?>
          </a>
  
          <?php if (!empty($post_new)) : ?>
            <div class="single-news__item-new">
              <?= _get_svg('icon-new');?>
            </div>
          <?php endif; ?>
        </div>
      <?php } //End of if
      
      else{ ?>
        <div class="post-thumb pull-left relative">
          <?php the_post_thumbnail('kidzy-blog-thumbnails', array('class' => 'img-responsive')); ?>
  
          <?php if (!empty($post_new)) : ?>
            <div class="single-news__item-new">
              <?= _get_svg('icon-new');?>
            </div>
          <?php endif; ?>
        </div>
      <?php }//End of else
      ?>
    
    <?php }
      get_template_part( 'post-format/entry-content' ); ?>
    
    <div class="clearfix"></div>
  </div>
</article><!--/#post-->
