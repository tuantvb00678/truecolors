<?php
  if(!is_single()){ ?>
<div class="post-details pull-left">
  <?php }
    
    if(!is_single()){ ?>
  <h3>
    <?php }
      else{ ?>
    <h2>
      <?php } ?>

      <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
      <?php if ( is_sticky() && is_home() && ! is_paged() ) { ?>
        <sup class="featured-post"><i class="fa fa-star-o"></i><?php esc_html_e( 'Sticky', 'kidzy' ) ?></sup>
      <?php } ?>
      
      <?php if(!is_single()){ ?>
  </h3>
<?php }
  else{ ?>
    </h2>
  <?php } ?>
  <!-- //.entry-title -->

  <ul class="author-tag">
    <?php
      if ( class_exists( 'ReduxFramework' ) ) {
        if (  kidzy_options('blog-date') ) { ?>
          <li class="author-tag__date"><i class="fa fa-clock-o" aria-hidden="true"></i>
            <time datetime="<?php echo get_the_time('Y-m-d H:i') ?>"><?php echo get_the_time('l, j F, Y'); ?></time></li>
        <?php }
        else{}
      }//End of Redux Framwork
      else{ ?>
        <li class="author-tag__date"><i class="fa fa-clock-o" aria-hidden="true"></i>
          <time datetime="<?php echo get_the_time('Y-m-d H:i') ?>"><?php echo get_the_time('l,j F, Y'); ?></time></li>
      <?php }
    ?>
    
    
    
    <?php
      # START: Blog Author
      if ( class_exists( 'ReduxFramework' ) ) {
        if (kidzy_options('blog-author') && kidzy_options('blog-author') ) { ?>
          <?php if ( get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "" ) { ?>
            <li class="author-tag__user"> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><i class="fa fa-user" aria-hidden="true"></i>
                <?php echo get_the_author_meta('first_name');?> <?php echo get_the_author_meta('last_name');?></a></li>
          <?php } else { ?>
            <li class="author-tag__user"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><i class="fa fa-user" aria-hidden="true"></i>
                <?php the_author(); ?></a></li>
          <?php }?>
        <?php }
        else{}
      }//End of Redux Framework
      else{ ?>
        <li class="author-tag__user"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><i class="fa fa-user" aria-hidden="true"></i>
            <?php the_author(); ?></a></li>
      <?php }
      #END: Blog Author
    ?>
    
    
    
    
    <?php
      # START: Blog Category
      if ( class_exists( 'ReduxFramework' ) ) {
        if (kidzy_options('blog-category') && kidzy_options('blog-category') ) { ?>
          <li class="author-tag__tag"><i class="fa fa-tag" aria-hidden="true"></i><?php echo get_the_category_list(", "); ?></li>
        <?php }
        else{}
      }
      else{?>
        <li class="author-tag__tag"><i class="fa fa-tag" aria-hidden="true"></i><?php echo get_the_category_list(", "); ?></li>
      <?php }
    ?>
    
    
    
    
    <?php
      #START: Blog Tags
      if ( class_exists( 'ReduxFramework' ) ) {
        if (kidzy_options('blog-tag') && kidzy_options('blog-tag') ) { ?>
          <li class="author-tag__tag"><i class="fa fa-tags" aria-hidden="true"></i> <?php the_tags('', ', ', '<br />'); ?></li>
        <?php }else{}
      }//End of Redux Framwork
      else{}
    ?>
    
    
    
    
    <?php
      #START: Numbers of Comment
      if ( class_exists( 'ReduxFramework' ) ) {
        if (kidzy_options('blog-comment') && kidzy_options('blog-comment') ) { ?>
          <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
            <li>
              <i class="fa fa-comment" aria-hidden="true"></i>
              <?php comments_popup_link( '<span class="leave-reply">' . esc_html__( 'No comment', 'kidzy' ) . '</span>', esc_html__( 'One comment', 'kidzy' ), esc_html__( '% comments', 'kidzy' ) ); ?>
            </li>
          <?php endif; ?>
        <?php }
        else{}
      }//End of ReduxFramework
      else{ ?>
        <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
          <li>
            <i class="fa fa-comment" aria-hidden="true"></i>
            <?php comments_popup_link( '<span class="leave-reply">' . esc_html__( 'No comment', 'kidzy' ) . '</span>', esc_html__( 'One comment', 'kidzy' ), esc_html__( '% comments', 'kidzy' ) ); ?>
          </li>
        <?php endif; ?>
      <?php }//End of else
    ?>
  </ul>
  
  <?php
    if ( is_single() ) {
      the_content();
    } else {
      echo kidzy_excerpt_max_charlength(200);
      
      if ( class_exists( 'ReduxFramework' ) ) {
        if ( kidzy_options('blog-continue-en') && kidzy_options('blog-continue-en')==1 ) {
          if ( kidzy_options('blog-continue') && kidzy_options('blog-continue') ) {
            $continue = esc_html(kidzy_options('blog-continue') );
            echo '<p class="wrap-btn-style"><a class="btn-blog" href="'.get_permalink().'">'. $continue .' <i class="fa fa-long-arrow-right"></i></a></p>';
          } else {
            echo '<p class="wrap-btn-style"><a class="btn-blog" href="'.get_permalink().'">'. esc_html__( 'Read More', 'kidzy' ) .' <i class="fa fa-long-arrow-right"></i></a></p>';
          }
        }//End of if themum options
      }//End of ReduxFramework
      
    }
    wp_link_pages( array(
      'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'kidzy' ) . '</span>',
      'after'       => '</div>',
      'link_before' => '<span>',
      'link_after'  => '</span>',
    ) );
  ?>
  
  
  <?php
    if(!is_single()){ ?>
</div> <!--/.entry-meta -->
<?php } ?>



