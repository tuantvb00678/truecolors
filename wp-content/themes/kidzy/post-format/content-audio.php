<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <?php 
    #get_post_meta(get_the_ID(),'themeum_designation',true);
    if(is_single()){ ?>
        <div class="featured-wrap">
            <?php if(function_exists('rwmb_meta')){ ?>
                <?php  if ( get_post_meta( get_the_ID(), 'themeum_audio_code',true )) { ?>
        	        <div class="entry-audio embed-responsive embed-responsive-16by9">
        	            <?php echo get_post_meta( get_the_ID(), 'themeum_audio_code',true ); ?>
        	        </div> <!--/.audio-content -->
        	    <?php } ?>
            <?php } ?>
        </div>
    <?php }//End of IF

    else{ ?>
        <div class="featured-wrap post-thumb-audio">
            <?php if(function_exists('rwmb_meta')){ ?>
                <?php  if ( get_post_meta( get_the_ID(), 'themeum_audio_code',true ) ) { ?>
                    <div class="entry-audio embed-responsive embed-responsive-16by9">
                        <?php echo get_post_meta( get_the_ID(), 'themeum_audio_code',true ); ?>
                    </div> <!--/.audio-content -->
                <?php } ?>
            <?php } ?>
        </div>
    <?php } //End of ELSE
    ?>

    <?php get_template_part( 'post-format/entry-content' ); ?>
  <div class="clearfix"></div>  
</article> <!--/#post -->
