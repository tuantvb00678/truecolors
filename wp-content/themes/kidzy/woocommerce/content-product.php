<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     5.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; # Exit if accessed directly
}

global $product;

# Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<li <?php post_class(); ?>>
<a href="<?php the_permalink(); ?>">
	<div class="product-thumbnail-outer">
		<div class="product-thumbnail-outer-inner">
			<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
			<div class="addtocart-btn">
				<div class="svg-overlay-shop"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none"><path d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path></svg></div>	
				<?php echo kidzy_more_info_button(); ?>	
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>

		</div>
		<div class="product-content-inner">
			<a href="<?php the_permalink(); ?>"><?php do_action( 'woocommerce_shop_loop_item_title' ); ?></a>
			<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
		</div>
	</div>
	</a>
</li>