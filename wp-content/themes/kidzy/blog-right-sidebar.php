<?php 
/**
* Template Name: Blog Right Sidebar 
*/
get_header();?>

<section id="main">
    <?php get_template_part('lib/sub-header')?>

        <!-- Blog Post Section -->
        <section class="blog-post-section">
            <div class="container">
                <div class="row">

                    <!-- Start: Post Summery -->                
                    <div class="col-sm-8 col-md-9 col-xs-12">

                        <?php
                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

                            $args = array(
                                    'post_type'         => 'post',
                                    'order'             => 'DESC',
                                    'paged' => $paged
                                );
                            $the_post = new WP_Query($args);

                            if ( $the_post->have_posts() ) :
                                while ( $the_post->have_posts() ) : $the_post->the_post();
                                    get_template_part( 'post-format/content', get_post_format() );
                                endwhile;
                            else:
                                get_template_part( 'post-format/content', 'none' );
                            endif;
                        ?>
                        

                        <?php                                 
                            $page_numb = max( 1, get_query_var('paged') );
                            $max_page = $the_post->max_num_pages;
                            echo kidzy_pagination( $page_numb, $max_page ); 
                        ?>

                    </div>
                    <!-- End: Post Summery -->

                    <!-- Start: Sidebar -->
                    <?php get_sidebar(); ?>
                    <!-- End: Sidebar -->                    
                    
                </div>
            </div>
        </section>


</section> 

<?php get_footer(); ?>