<?php

get_header(); ?>

<section id="main">
   
   <?php get_template_part('lib/sub-header')?>

    <div class="container">
        <div id="content" class="site-content kidzy-archive-page row" role="main">

            <?php while ( have_posts() ): the_post(); ?>

                <?php
                    $class_status = get_post_meta(get_the_ID(),'themeum_class_status',true);
                    $class_link = get_post_meta(get_the_ID(),'themeum_class_link',true);
                    $years_old = get_post_meta(get_the_ID(),'themeum_years_old',true);
                    $class_size = get_post_meta(get_the_ID(),'themeum_class_size',true);
                    $tution_fee = get_post_meta(get_the_ID(),'themeum_tution_fee',true);
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'kidzy-blog-single' );
                ?>
                <div class="col-sm-4 col-xs-12">
                <div class="single-class">

                <?php if($image): ?>
                    <div class="class-img">
                        <img class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo get_the_title(); ?>">
                        <div class="class-hover">
                            <a href="<?php echo get_the_permalink(); ?>" class="popup"><i class="icon-link"></i></a>
                        </div>
                    </div>
                <?php endif; ?>
                
                    <div class="class-details">
                        <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                        <a href="<?php echo $class_link; ?>" class="black"><?php echo $class_status; ?></a>
                        <div class="clearfix">
                            <div class="class-meta pull-left">
                                <span><?php esc_html_e('Lứa tuổi', 'kidzy'); ?></span>
                                <p><?php echo esc_html($years_old); ?></p>
                            </div>
                            <div class="class-meta pull-left">
                                <span><?php esc_html_e('Sĩ số', 'kidzy'); ?></span>
                                <p><?php echo esc_html($class_size); ?></p>
                            </div>
                            <div class="class-meta pull-left">
                                <span><?php esc_html_e('Học phí', 'kidzy'); ?></span>
                                <a href="/contact" class="block"><?php echo esc_html($tution_fee); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div> <!--/#content-->
    </div>

</section> <!--/#main-->
<?php get_footer();
