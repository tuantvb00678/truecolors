<?php
/**
 * Display Single Teacher
 *
 * @author      Themeum
 * @category    Template
 * @package     Kidzy
 * @version     1.0
 *-------------------------------------------------------------*/

if ( ! defined( 'ABSPATH' ) )
    exit; // Exit if accessed directly

get_header(); ?>

<section id="main">
<?php get_template_part('lib/sub-header'); ?>


    <section class="teacher-details-section">
        <div class="container">
        <?php

        if ( have_posts() ){
            while ( have_posts() ) {
                the_post();

                $teacher_designation = get_post_meta(get_the_ID(),'themeum_designation',true);
                $teacher_facebook = get_post_meta(get_the_ID(),'themeum_facebook',true);
                $teacher_twitter = get_post_meta(get_the_ID(),'themeum_twitter',true);
                $teacher_gplus = get_post_meta(get_the_ID(),'themeum_gplus',true);
                $teacher_linkedin = get_post_meta(get_the_ID(),'themeum_linkedin',true);
                $teacher_instagram = get_post_meta(get_the_ID(),'themeum_instagram',true);
                $teacher_flickr = get_post_meta(get_the_ID(),'themeum_flickr',true);
                $teacher_timeline = rwmb_meta('themeum_teacher_timeline');
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-medium' );
                ?>

                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-xs-12 text-center">
                        <div class="single-teacher">

                        <?php if($image){ ?>
                            <div class="teacher-img">
                                <?php echo '<img class="img-responsive" src="'.esc_url( $image[0] ).'" alt="'.get_the_title().'">'; ?>
                            </div>
                        <?php } ?>
                        
                            <h2 class="title-2"><?php echo get_the_title() ?></h2>
                            <p class="subtitle color"><?php echo $teacher_designation ?></p>
                            <ul class="tsocial-icon">
                            <?php
                                $output = '';
                                if($teacher_facebook){
                                    $output .= '<li><a href="'.$teacher_facebook.'" class="facebook"><i class="fa fa-facebook"></i></a></li>';
                                }
                                if($teacher_twitter){
                                    $output .= '<li><a href="'.$teacher_twitter.'" class="twitter"><i class="fa fa-twitter"></i></a></li>';
                                }
                                if($teacher_gplus){
                                    $output .= '<li><a href="'.$teacher_gplus.'" class="gplus"><i class="fa fa-google-plus"></i></a></li>';
                                }
                                if($teacher_linkedin){
                                    $output .= '<li><a href="'.$teacher_linkedin.'" class="linkedin"><i class="fa fa-linkedin"></i></a></li>';
                                }
                                if($teacher_instagram){
                                    $output .= '<li><a href="'.$teacher_instagram.'" class="gplus"><i class="fa fa-instagram"></i></a></li>';
                                }
                                if($teacher_flickr){
                                    $output .= '<li><a href="'.$teacher_flickr.'" class="flickr"><i class="fa fa-flickr"></i></a></li>';
                                }
                                echo $output;
                            ?>
                            </ul>
                        </div>
                        <div class="teacher-content">
                            <div>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if(!empty($teacher_timeline)) { ?>
                <div class="row timeline-wrap">
                    <div class="col-sm-12 col-xs-12">
                        <?php
                        $i=0;
                        foreach ($teacher_timeline as $value) {
                            if($i%2==0){ ?>
                            <div class="bio-content">
                                <div class="bio-date text-right pull-left">
                                    <p><?php echo esc_html($value['themeum_timeline_date']); ?></p>
                                </div>

                                <div class="bio-details text-left pull-right">
                                    <h5><?php echo esc_html($value['themeum_timeline_designation']); ?></h5>
                                    <p><?php echo esc_html($value['themeum_timeline_school']); ?></p>
                                </div>
                            <div class="clearfix"></div>
                            </div>
                            <?php
                            }//End of if i%2=0

                            if($i%2==1){ ?>
                            <div class="bio-content">
                                <div class="bio-details text-right pull-left">
                                    <h5><?php echo esc_html($value['themeum_timeline_designation']); ?></h5>
                                    <p><?php echo esc_html($value['themeum_timeline_school']); ?></p>
                                </div>

                                <div class="bio-date four text-left pull-right">
                                    <p><?php echo esc_html($value['themeum_timeline_date']); ?></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
                            }//End of if i%2=1
                            
                            $i++;
                        }//End of foreach ?>
                    </div>
                </div>
                <?php } ?>


        <?php }//End of While
        }//End of if
        ?>
        </div>
    </section>

</section>

<?php get_footer();
