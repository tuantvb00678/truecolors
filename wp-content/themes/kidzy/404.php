<?php get_header('alternative'); 
/*
*Template Name: 404 Page Template
*/
?>

<section class="four-zero-four-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <div class="four-zero-content">
                    <?php if( kidzy_options_url('errorpage','url') ): ?>
                        <div class="logo-top">
                            <img class="img-responsive" src="<?php echo esc_url( kidzy_options_url('errorpage','url') ); ?>" alt="<?php  esc_html_e( '404 error', 'kidzy' ); ?>">
                        </div>
                    <?php endif; ?>

                     <h3><?php  esc_html_e( 'Page not found.', 'kidzy' ); ?></h3>
                    <p>
                        <?php esc_html_e( 'The page you are looking for was moved, removed, renamed <br>or might never existed.', 'kidzy' ); ?>
                    </p>
                    <a href="<?php echo esc_url( home_url('/') ); ?>" class="primary-btn"><span><?php  esc_html_e( 'Go Back Home', 'kidzy' ); ?></span></a>
                </div>

                <?php if(kidzy_options('copyright-text')){ ?>                      
                <p class="four-zero-copy"><?php echo esc_textarea(kidzy_options('copyright-text')); ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer('alternative'); ?>
