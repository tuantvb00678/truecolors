<?php
/**
 * Display Single Event
 *
 * @author      Themeum
 * @category    Template
 * @package     Kidzy
 * @version     1.0
 *-------------------------------------------------------------*/

if ( ! defined( 'ABSPATH' ) )
    exit; // Exit if accessed directly


get_header();
?>

<section id="main">
<?php

get_template_part('lib/sub-header');

if ( have_posts() ){
    while ( have_posts() ) {
        the_post();

        $start_date         = get_post_meta(get_the_ID(),'themeum_event_start_datetime',true);
        $end_date           = get_post_meta(get_the_ID(),'themeum_event_end_datetime',true);
        $about_speaker      = get_post_meta(get_the_ID(),'themeum_event_about_speaker',true);
        $event_address      = get_post_meta(get_the_ID(),'themeum_event_location',true);
        $performers         = rwmb_meta('themeum_event_performers','type=checkbox_list');
        $image              = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-medium' );

        # Map
        $map = rwmb_meta('themeum_event_location_map');
        $map_trigger = get_post_meta(get_the_ID(), 'themeum_event_map_trigger', true);
?>


        <!-- Event Details Section -->
        <section class="event-details-section">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-xs-12">
                        <div class="event-img">
                        <?php if($image){ ?>
                            <?php echo '<img class="img-responsive" src="'.esc_url( $image[0] ).'" alt="'.get_the_title().'">'; ?>
                        <?php } ?>
                        </div>
                    </div>
                    <?php
                        if($start_date > date('Y-m-d h:i')){
                            $sdate = str_replace('-', '/', $start_date);
                        }
                    ?>
                    <div class="col-sm-6 col-xs-12">
                        <ul id="count-down" data-datedata="<?php echo $sdate; ?>" class="event-count">
                            <li>
                                <p class="days">00</p>
                                <span><?php esc_html_e('Days','kidzy') ?></span>
                            </li>
                            <li>
                                <p class="hours">00</p>
                                <span><?php esc_html_e('Hours','kidzy') ?></span>
                            </li>
                            <li>
                                <p class="minutes">00</p>
                                <span><?php esc_html_e('Minutes','kidzy') ?></span>
                            </li>
                            <li>
                                <p class="seconds">00</p>
                                <span><?php esc_html_e('Seconds','kidzy') ?></span>
                            </li>
                        </ul>

                        <div class="single-event">
                            <h3><?php echo get_the_title() ?></h3>
                            <span><?php echo date_i18n("F j, Y, h:i", strtotime($start_date)).' - '.date_i18n("F j, Y, h:i", strtotime($end_date)) ?></span>
                            <span><?php echo $event_address ?></span>
                            <p>
                                <?php echo get_the_content(); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.event-section -->

        <!-- Speaker Section -->
        <section class="speaker-section">
            <div class="container">

                <?php
                if(!empty($performers)) { ?>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-center">
                        <h2 class="title-2"><?php esc_html_e('Speaker & Guest of Honor','kidzy') ?></h2>
                        <p class="subtitle-2">
                            <?php echo $about_speaker ?>
                        </p>
                    </div>
                </div>
                <?php } ?>


                <div class="row">
                <?php
                if(!empty($performers)) {
                    $posts_id = array();
                    foreach ($performers as $value) {
                        $posts = get_posts(array('post_type' => 'teacher', 'name' => $value));
                        $posts_id[] = $posts[0]->ID;
                    }
                    $performers = get_posts( array( 'post_type' => 'teacher', 'post__in' => $posts_id, 'posts_per_page' => -1) );


                    foreach ($performers as $key=>$post) {
                        setup_postdata( $post );
                        $thumb_src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );

                        $teacher_facebook = rwmb_meta('themeum_facebook');
                        $teacher_twitter = rwmb_meta('themeum_twitter');
                        $teacher_gplus = rwmb_meta('themeum_gplus');
                        $teacher_linkedin = rwmb_meta('themeum_linkedin');
                        $teacher_instagram = rwmb_meta('themeum_instagram');
                        $teacher_flickr = rwmb_meta('themeum_flickr');
                        ?>


                        <div class="col-sm-3 col-xs-12 text-center">
                        <div class="single-team">
                            <div class="team-img">
                                <a href="<?php echo get_the_permalink() ?>">
                                  <img alt="<?php echo get_the_title(); ?>" src="<?php echo $thumb_src; ?>">
                                </a>
                                <ul class="team-social">
                                    <?php if($teacher_facebook){
                                        echo '<li><a href="'.$teacher_facebook.'" class="facebook"><i class="fa fa-facebook"></i></a></li>';
                                    }
                                    if($teacher_twitter){
                                        echo '<li><a href="'.$teacher_twitter.'" class="twitter"><i class="fa fa-twitter"></i></a></li>';
                                    }
                                    if($teacher_gplus){
                                        echo '<li><a href="'.$teacher_gplus.'" class="gplus"><i class="fa fa-google-plus"></i></a></li>';
                                    }
                                    if($teacher_linkedin){
                                        echo '<li><a href="'.$teacher_linkedin.'" class="linkedin"><i class="fa fa-linkedin"></i></a></li>';
                                    }
                                    if($teacher_instagram){
                                        echo '<li><a href="'.$teacher_instagram.'" class="gplus"><i class="fa fa-instagram"></i></a></li>';
                                    }
                                    if($teacher_flickr){
                                        echo '<li><a href="'.$teacher_flickr.'" class="flickr"><i class="fa fa-flickr"></i></a></li>';
                                    } ?>
                                </ul>
                            </div>
                            <h3><?php echo get_the_title() ?></h3>
                            <p><?php echo rwmb_meta( 'themeum_designation' ); ?></p>
                        </div>
                    </div>

                    <?php   }
                    wp_reset_postdata();
                }
                ?>
                </div>

            </div>
        </section>
        <!-- /.speaker-section -->

        <?php if (!$map_trigger) : ?>
        <!-- Map Section -->
        <section class="map-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-center">
                        <?php if ($map) { ?>
                            <h2 class="title-2"><?php esc_html_e('Event Location','kidzy') ?></h2>
                            <p class="subtitle"><?php echo $event_address; ?></p>
                        <?php echo $map; } ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

<?php }//End of While
}//End of if have post
?>

</section>

<?php get_footer(); ?>
