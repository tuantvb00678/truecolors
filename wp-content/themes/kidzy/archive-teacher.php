<?php

get_header(); ?>

<section id="main">
   
   <?php get_template_part('lib/sub-header')?>

    <div class="container">
        <div id="content" class="site-content row" role="main">

            <?php while ( have_posts() ): the_post(); ?>

                <?php
                    $teacher_designation = get_post_meta(get_the_ID(),'themeum_designation',true);
                    $teacher_facebook = get_post_meta(get_the_ID(),'themeum_facebook',true);
                    $teacher_twitter = get_post_meta(get_the_ID(),'themeum_twitter',true);
                    $teacher_gplus = get_post_meta(get_the_ID(),'themeum_gplus',true);
                    $teacher_linkedin = get_post_meta(get_the_ID(),'themeum_linkedin',true);
                    $teacher_instagram = get_post_meta(get_the_ID(),'themeum_instagram',true);
                    $teacher_flickr = get_post_meta(get_the_ID(),'themeum_flickr',true);
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'kidzy-medium' );
                ?>


                <div class="col-sm-3 col-xs-12 text-center">
                    <div class="single-team">
                        <div class="team-img">
                        <?php if($image): ?>
                          <a href="<?php echo get_the_permalink() ?>">
                            <img class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo get_the_title(); ?>">
                          </a>
                        <?php  endif; ?>
                            <ul class="team-social">
                                <?php if($teacher_facebook): ?>
                                    <li><a href="<?php echo esc_url($teacher_facebook); ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <?php endif; ?>
                                <?php if($teacher_twitter): ?>
                                    <li><a href="<?php echo esc_url($teacher_twitter); ?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <?php endif; ?>
                                <?php if($teacher_gplus): ?>
                                    <li><a href="<?php echo esc_url($teacher_gplus); ?>" class="gplus"><i class="fa fa-google-plus"></i></a></li>
                                <?php endif; ?>
                                <?php if($teacher_linkedin): ?>
                                    <li><a href="<?php echo esc_url($teacher_linkedin); ?>" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <?php endif; ?>
                                <?php if($teacher_instagram): ?>
                                    <li><a href="<?php echo esc_url($teacher_instagram); ?>" class="gplus"><i class="fa fa-instagram"></i></a></li>
                                <?php endif; ?>
                                <?php if($teacher_flickr): ?>
                                    <li><a href="<?php echo esc_url($teacher_flickr); ?>" class="flickr"><i class="fa fa-flickr"></i></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                        <p><?php echo esc_html($teacher_designation); ?></p>
                    </div>
                </div>
            <?php endwhile; ?>
        </div> <!--/#content-->
    </div>

</section> <!--/#main-->
<?php get_footer();
