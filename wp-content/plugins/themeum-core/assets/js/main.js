/*
* Plugin Name: Themeum Core
* Plugin URI: http://www.themeum.com/item/core
* Author: Themeum
* Author URI: http://www.themeum.com
* License - GNU/GPL V2 or Later
* Description: Themeum Core is a required plugin for this theme.
* Version: 1.0
*/

jQuery(document).ready(function($){'use strict';

        //video carosuel
        var $viddeocarosuel = $('.themeum-video-carosuel');
        $viddeocarosuel.owlCarousel({
            loop:true,
            dots:false,
            nav:false,
            margin:30,
            autoplay:false,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            autoHeight: false,
            lazyLoad:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:4
                }
            }
        });

        $('.videoPrev').click(function(){
        $viddeocarosuel.trigger('prev.owl.carousel', [400]);
        });

        $('.videoNext').click(function(){
        $viddeocarosuel.trigger('next.owl.carousel',[400]);
        });

        //-------------------------------------------------------
        // Popups
        //-------------------------------------------------------
        if ($(".champ-video").length > 0) {
            $(".champ-video a").magnificPopup({
                disableOn: 0,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 300,
                preloader: false,
                fixedContentPos: false,
            });
        }
        $('.plus-icon').magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom',
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
                opener: function (openerElement) {
                    return openerElement.next('img') ? openerElement : openerElement.find('img');
                }
            },
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            }

        });

    // prettySocial
    $('.prettySocial').prettySocial();

    //Animated Number
    $('.themeum-counter-number').each(function(){
      var $this = $(this);
      $({ Counter: 0 }).animate({ Counter: $this.data('digit') }, {
        duration: $this.data('duration'),
        step: function () {
          $this.text(Math.ceil(this.Counter));
        }
      });
    });

          //client fedback
    var $clientfeedback = $('#client-feedback-message');

    $clientfeedback.owlCarousel({
      loop: true,
      autoplay:true,
      rtl:true,
      margin:30,
      autoplayTimeout:8000, 
      dots:true,
      nav:false,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 3
        }
      },
    });

});