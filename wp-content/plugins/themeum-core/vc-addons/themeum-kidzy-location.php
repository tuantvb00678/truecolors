<?php
	add_shortcode( 'themeum_kidzy_location', 'themeum_kidzy_location_function');
	
	function themeum_kidzy_location_function($atts, $content = null) {
		
		$heading 		= '';
		$class_category = '';
		$count_post 	= '';
		$column 		= '';
		$class  		= '';
		
		extract(shortcode_atts(array(
			'heading' 		=> '',
			'class_category'=> '',
			'count_post' 	=>	7,
			'column' 		=> '',
			'class' 		=> '',
		), $atts));
		
		
		global $wpdb;
		global $post;
		
		$args = array(
			'post_type' 	=> 'location',
			'tax_query'  => array(
				array(
					'taxonomy' => 'location_category',
					'field'    => 'slug',
					'terms'    => $class_category,
				),
			),
			'order' 			=> 'ASC',
			'posts_per_page'	=> esc_attr($count_post)
		);
		
		$show_class = new WP_Query($args);
		
		$output = '';
		
		if ( $show_class->have_posts() ){
			while($show_class->have_posts()) {
				$show_class->the_post();
				
				$class_status = get_post_meta(get_the_ID(),'themeum_class_status',true);
        $class_link = get_post_meta(get_the_ID(),'themeum_class_link',true);
				$years_old = get_post_meta(get_the_ID(),'themeum_years_old',true);
				$class_size = get_post_meta(get_the_ID(),'themeum_class_size',true);
				$tution_fee = get_post_meta(get_the_ID(),'themeum_tution_fee',true);
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-blog-single' );
				
				ob_start();
				?>
				<div class="col-sm-4 col-xs-12">
					<div class="single-class">
						<?php if($image) : ?>
							<div class="class-img">
								<img class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo get_the_title(); ?>">
								<a class="class-hover" href="<?php echo get_the_permalink(); ?>">
									<span class="popup"><i class="icon-link"></i></span>
									<div class="class__short-description"><?php echo wp_trim_words( get_the_content(), 30, '...' ); ?></div>
								</a>
							</div>
						<?php endif; ?>
						
						<div class="class-details">
							<h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
							<a href="<?php echo $class_link; ?>" class="black"><?php echo $class_status; ?></a>
							<div class="clearfix">
								<div class="class-meta pull-left">
									<span>Lứa tuổi</span>
									<p><?php echo $years_old; ?></p>
								</div>
								<div class="class-meta pull-left">
									<span>Sĩ số</span>
									<p><?php echo $class_size; ?></p>
								</div>
								<div class="class-meta pull-left">
									<span>Học phí</span>
									<a href="/contact" class="block"><?php echo $tution_fee; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				$output = ob_get_clean();
			}//End of while
		}//End of IF
		
		return $output;
	}
	
	
	//Visual Composer
	if (class_exists('WPBakeryVisualComposerAbstract')) {
		vc_map(array(
			"name" => esc_html__("Display Classes", 'themeum-core'),
			"base" => "themeum_kidzy_location",
			'icon' => 'icon-thm-latest-news',
			"class" => "",
			"description" => esc_html__("All Class display by Category", 'themeum-core'),
			"category" => esc_html__('Kidzy', 'themeum-core'),
			"params" => array(
				
				array(
					"type" 		=> "dropdown",
					"heading" 	=> esc_html__("Category Filter", 'themeum-core'),
					"param_name" => "class_category",
					"value" 	=> themeum_cat_list('class_category'),
				),
				
				array(
					"type" 		=> "textfield",
					"heading" 	=> esc_html__("Number Of Post Show", "themeum-core"),
					"param_name" => "count_post",
					"value" 	=> "6",
				),
				
				array(
					"type" 		=> "dropdown",
					"heading" 	=> esc_html__("Number Of Column", "themeum-core"),
					"param_name" => "column",
					"value" 	=> array('No Column'=>'','column 2'=>'6','column 3'=>'4','column 4'=>'3'),
				),
				
				array(
					"type" 		=> "textfield",
					"heading" 	=> esc_html__("Custom Class", "themeum-core"),
					"param_name" => "class",
					"value" 	=> "",
				),
			
			)
		
		));
	}?>
