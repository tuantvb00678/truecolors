<?php
if ( ! function_exists( 'getIconsLists' ) ) {

	function getIconsLists(){
		return array(
		'',
		'icon-call',
		'icon-class',
		'icon-clock',
		'icon-color',
		'icon-food',
		'icon-lay',
		'icon-link',
		'icon-map',
		'icon-music-class',
		'icon-next',
		'icon-parents',
		'icon-prev',
		'icon-reader',
		'icon-teacher',
		'icon-testimonial',
		'icon-translator',
		'icon-trophy',
		'icon-user'
		);
	}
}