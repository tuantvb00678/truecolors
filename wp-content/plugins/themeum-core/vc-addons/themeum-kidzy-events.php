<?php

if ( ! defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly

$event_category = '';

//shortocde
add_shortcode( 'themeum_events_news', function($atts, $content = null){
  	extract(shortcode_atts(array(
    	'count_post' 	=>	6,
    	'column'		=>	6,
    	'class' 		=>	'',
    	'events_cat'	=>  'themeumall',
    	'custom_style'  =>  '',
    	), $atts));


  	global $post;
  	$args = array();

	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

  	if( $events_cat == 'themeumall' ){
  		$args = array(
			    'post_type' 		=> 'event',
			    'order' 			=> 'DESC',
			    'posts_per_page' 	=> esc_attr($count_post),
				'paged' 			=> $paged			      
			    );
  	}else{
	  		$args = array(
			    'post_type' => 'event',
			    'order' 	=> 'DESC',
			    'tax_query' => array(
			            array(
			                'taxonomy' => 'event_cat',
			                'field'    => 'slug',
			                'terms'    => esc_attr($events_cat),
			                ),
			            ),
			    'posts_per_page'=> esc_attr($count_post),
				'paged' 		=> $paged			      
			    );
  	}

  	$eventsnews = new WP_Query($args);

  	$output = '';
  	$output .= '<div class="event-section ' . esc_attr($class) .'">';
  	$output .= '<div class="container">';

  		if ( $eventsnews->have_posts() ){
			
			$x = 1;

			while($eventsnews->have_posts()) {
				$eventsnews->the_post();
				
			    $start_date = get_post_meta(get_the_ID(),'themeum_event_start_datetime',true);
			    $end_date  	= get_post_meta(get_the_ID(),'themeum_event_end_datetime',true);			    
			    $event_address = get_post_meta(get_the_ID(),'themeum_event_location',true);
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-medium' );

				if( $x == 1 ){
			    	$output .= '<div class="row">';	
			    }

				$output .= '<div class="col-xs-12 col-sm-6 col-md-'.esc_attr( $column ).'">';

				$output .= '<div class="single-event">';

				if($image){
					$output .= '<div class="event-img">';
                        if (has_post_thumbnail( $post->ID ) ): 
						  $output .= '<img class="img-responsive" src="'.esc_url( $image[0] ).'" alt="'.get_the_title().'">';
						endif;

                        $output .= '<div class="class-hover">';
                            $output .= '<a class="popup" href="'.get_permalink().'"><i class="icon-link"></i></a>';
                        $output .= '</div>'; // class-hover
                        $output .= '<a href="'.get_permalink().'">'. date_i18n("j", strtotime($start_date)) .'<span>'. date_i18n("M", strtotime($start_date)) .'</span></a>';
                    $output .= '</div>'; //event-img
                }

                    $output .= '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
                    $output .= '<span>'.date_i18n("F j, Y, h:i", strtotime($start_date)).' - '.date_i18n("F j, Y, h:i", strtotime($end_date)).'</span>';
                    $output .= '<span>'.$event_address.'</span>';
                    $output .= '<p>'. kidzy_excerpt_max_char(200). '</p>';

				$output .= '</div>'; // single-event
				$output .= '</div>'; // col-xs-12

				if( $x == (12/$column) ){
					$output .= '</div>'; //row	
					$x = 1;	
				}else{
					$x++;	
				}
			}

			wp_reset_query();

			if($x !=  1 ){
				$output .= '</div>'; //row	
			}	
		}
	$output .= '</div>'; //container	
	$output .= '</div>'; //themeum-speaker-listing 	

	// pagination
	ob_start();
	themeum_pagination($eventsnews->max_num_pages);
	
	$output .= ob_get_contents();
	ob_clean();	

	return $output;
     
}); 

//Visual Composer addons register
if (class_exists('WPBakeryVisualComposerAbstract')) {
  vc_map(array(
    "name" => esc_html__("Display Events", "themeum-core"),
    "base" => "themeum_events_news",
    'icon' => 'icon-thm-speaker-listing',
    "class" => "",
    "description" => esc_html__("Display Events", "themeum-core"),
    "category" => esc_html__('Kidzy', "themeum-core"),
    "params" => array(

    array(
        "type" 			=> "textfield",
        "heading" 		=> esc_html__("Number Of Post Show", "themeum-core"),
        "param_name" 	=> "count_post",
        "value" 		=> "6",
    ),        

	array(
		"type" 			=> "dropdown",
		"heading" 		=> esc_html__("Number Of Column", "themeum-core"),
		"param_name" 	=> "column",
		"value" 		=> array('column 2'=>'6','column 3'=>'4','column 4'=>'3'),
		),	                 

      array(
        "type" 			=> "textfield",
        "heading" 		=> esc_html__("Custom Class", "themeum-core"),
		"param_name" 	=> "custom_style",        
        "value" 		=> "",
        ),
      )

    ));
}





