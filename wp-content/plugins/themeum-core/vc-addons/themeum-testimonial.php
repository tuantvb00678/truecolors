<?php
if(!class_exists('kidzytestiminials'))
{
	class kidzytestiminials
	{
		function __construct()
		{
			add_action('init', array($this, 'kidzy_testmonial_items'));			
			add_shortcode( 'client_feedback', array($this, 'client_feedback' ) );
			add_shortcode( 'client_feedback_items', array($this, 'client_feedback_items' ) );
		}

		function client_feedback($atts, $content = null)
		{
			$class 		= '';
			extract(shortcode_atts(array(
				'class'  => '',
			), $atts));

			$output  = '<div class="addon-client-feedbacks ' . esc_attr($class) . '">';
				$output  .= '<div id="client-feedback-message" class="owl-carousel">';
				$output .= do_shortcode($content);
				$output .= '</div>';//owl-carousel			
			$output .= '</div>';//addon-client-feedbacks


			return $output;
		}

		function client_feedback_items($atts,$content = null)
		{
			$name 		= '';
			$relation 	= '';
			$image 		= '';
			$message 	= '';
			$font_size 	= '';
			extract(shortcode_atts(array(
				'name'     		=> '',
				'relation'     	=> '',
				'message' 		=> '',
				'image' 		=> '',
				'font_size'  	=> '',
			), $atts));

			$output = '';
			$size = '';
			if($font_size) $size = 'font-size:' . (int) esc_attr( $font_size ) . 'px;line-height: normal;';
			
			$src_image   = wp_get_attachment_image_src($image, 'full');
				$output  .= '<div class="client-feedback-inner testimonials text-center">';
					$output  .= '<div class="testimonial-img">';
					$output  .= '<img src="'. esc_url($src_image[0]) .'" alt="'. $name .'">';
					$output  .= '</div>';

					$output  .= '<div class="testimonial-details">';
						$output  .= '<i class="fa fa-quote-right"></i>';
					    $output  .= '<h4>'. esc_attr($name) .'</h4>';
					    $output  .= '<span>'. esc_attr($relation) .'</span>';
					    $output  .= '<p style="'.$size.'">'. balanceTags($message) .'</p>';
					$output  .= '</div>';

				$output  .= '</div>';//special-dish-content-wrapper
			return $output;
		}

		
		// Shortcode Functions for frontend editor
		function front_client_feedback($atts, $content = null)
		{
			// Do nothing
			$class = '';
			extract(shortcode_atts(array(
				'class'  => '',
			), $atts));

			$output  = '<div class="addon-client-feedbacks ' . esc_attr($class) . '">';
				$output .= '<div id="carousel-testimonial" class="carousel slide text-center" data-ride="carousel">';
					$output .= '<div class="carousel-inner">';
					$output .= do_shortcode($content);
					$output .= '</div>';//carousel-inner

					/*Controls */
					$output .= '<a class="left testimonial-carousel-control" href="#carousel-testimonial" role="button" data-slide="prev">';
					$output .= '<i class="fa fa-caret-square-o-left"></i>';
					$output .= '</a>';
					$output .= '<a class="right testimonial-carousel-control" href="#carousel-testimonial" role="button" data-slide="next">';
					$output .= '<i class="fa fa-caret-square-o-right"></i>';
					$output .= '</a>';
				$output .= '</div>';//carousel-testimonial
			$output .= '</div>';//addon-client-feedbacks

			return $output;
		}
		function front_client_feedback_items($atts,$content = null)
		{
			// Do nothing
			$name = '';
			$relation = '';
			$image = '';
			$message = '';
			extract(shortcode_atts(array(
				'name'     => '',
				'message' 	=> '',
				'image' 	=> '',
			), $atts));
			$output='';
			$src_image   = wp_get_attachment_image_src($image, 'full');
			$output  .= '<div class="client-feedback-inner">';
				$output  .= '<div class="testimonial-img">';
				$output  .= '<img class="client-feedback-img img-responsive" src="'. esc_url($src_image[0]) .'" alt="'. $name .'">';
				$output  .= '<i class="fa fa-quote-right"></i>';
				$output  .= '</div>';
				
				$output  .= '<h3>'. esc_attr($name) .'</h3>';
				$output  .= '<h4>'. esc_attr($relation) .'</h4>';
				$output  .= '<p>'. balanceTags($message) .'</p>';
				$output .= wpb_js_remove_wpautop($content, true);
			$output  .= '</div>';//special-dish-content-wrapper

			return $output;
		}



		function kidzy_testmonial_items()
		{
			if(function_exists('vc_map'))
			{
				vc_map(
				array(
				   "name" 				=> __("Kidzy Testiminials","themeum-core"),
				   "base" 				=> "client_feedback",
				   "class" 				=> "",
				   "icon" 				=> "icon-special-dish-wrap",
				   "category" 			=> "Kidzy",
				   "as_parent" 			=> array('only' => 'client_feedback_items'),
				   "description" 		=> __("Text blocks connected together in one list.","themeum-core"),
				   "content_element" 	=> true,
				   "show_settings_on_create" => true,
				   "params" => array(
						array(
							"type" 			=> "textfield",
							"class" 		=> "",
							"heading" 		=> __("Add Custom Class","themeum-core"),
							"param_name" 	=> "class",
							"description" 	=> __("Add Custom Class","themeum-core")
						),	
																
					),
					"js_view" => 'VcColumnView'
				));
				// Add Testmonial Item
				vc_map(
					array(
					   "name" => __("Kidzy Testiminials Item","themeum-core"),
					   "base" => "client_feedback_items",
					   "class" => "",
					   "icon" => "icon-special-dish-list",
					   "category" => "Kidzy",
					   "content_element" => true,
					   "as_child" => array('only' => 'client_feedback'),
					   "is_container"    => false,
					   "params" => array(
							array(
								"type" 			=> "textfield",
								"class" 		=> "",
								"heading" 		=> __("Name","themeum-core"),
								"param_name" 	=> "name",
								"description" 	=> __("Title","themeum-core")
							),

							array(
								"type" 			=> "textfield",
								"class" 		=> "",
								"heading" 		=> __("Relation","themeum-core"),
								"param_name" 	=> "relation",
								"description" 	=> __("Relation","themeum-core")
							),

							array(
								"type" 			=> "textarea",
								"class" 		=> "",
								"heading" 		=> __("Client Message","themeum-core"),
								"param_name" 	=> "message",
								"description" 	=> __("Client Message","themeum-core")
							),
							array(
								"type" => "textfield",
								"heading" => esc_html__("Font Size", 'themeum-core'),
								"param_name" => "font_size",
								"value" => ""
								),									
							array(
								"type" 			=> "attach_image",
								"heading" 		=> esc_html__("Upload Food Image", 'themeum-core'),
								"param_name" 	=> "image",
								"value" 		=> "",
							),
							
					   )
					) 
				);
			}//endif
		}
	}
}
global $kidzytestiminials;
if(class_exists('WPBakeryShortCodesContainer'))
{
	class WPBakeryShortCode_client_feedback extends WPBakeryShortCodesContainer {
        function content( $atts, $content = null ) {
            global $kidzytestiminials;
            return $kidzytestiminials->front_client_feedback($atts, $content);
        }
	}
	class WPBakeryShortCode_client_feedback_items extends WPBakeryShortCode {
        function content($atts, $content = null ) {
            global $kidzytestiminials;
            return $kidzytestiminials->front_client_feedback_items($atts, $content);
        }
	}
}
if(class_exists('kidzytestiminials'))
{
	$kidzytestiminials = new kidzytestiminials;
}