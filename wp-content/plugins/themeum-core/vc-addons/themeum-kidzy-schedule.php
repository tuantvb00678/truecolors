<?php
add_shortcode( 'themeum_kidzy_schedule', 'themeum_kidzy_schedule_function');

function themeum_kidzy_schedule_function($atts, $content = null) {
	
	$heading 		= '';
	$class_category = '';
	$class  		= '';

	extract(shortcode_atts(array(
		'heading' 		=> '',
		'class_category'=> '',		
		'class' 		=> '',
	), $atts));

    global $wpdb;
    global $post;

    $args = array(
        'post_type' => 'schedule',
        'tax_query' => array(
                        array(
                            'taxonomy' => 'schedule_tag',
                            'field'    => 'slug',
                            'terms'    => $class_category,
                    ),
                ),      
        'order'           => 'ASC',
        'posts_per_page'  => 1
    );

    $show_schedule = new WP_Query($args);

    $output = '';
    $days = array("sun", "mon", "tue", "wed", "thu", "fri", "sat"); 

    if ( $show_schedule->have_posts() ){
        while($show_schedule->have_posts()) {
            $show_schedule->the_post();
            
            $class_schedule = get_post_meta(get_the_ID(),'themeum_class_schedule',true);

    $output .= '<section class="class-schedule">';
        $output .= '<div class="container">';
            $output .= '<div class="row">';
                $output .= '<div class="col-sm-12 col-xs-12 text-center">';
                    $output .= '<div class="routine-details">';
                        $output .= '<table class="schedule-table">';


            $d=0;
            //Start of if Sunday Schedule

            if ( is_array($class_schedule) && !empty($class_schedule) ) {
                foreach ($class_schedule as $value) {
                  for($i=0;$i<7;$i++){
                    $schedule_list[$d][$days[$i]] = !empty($value['themeum_'.$days[$i].'day_subject']);
                  }
                  $d++;                                       
                }
            }
            //End of if class Schedule

            //Count close and active day
            $cn=0;
            $an=0;
            for($j=0;$j<7;$j++){
                $c=0;
                
                for($i=0;$i<$d;$i++){
                    if($schedule_list[$i][$days[$j]]==''){
                        $c++;
                    }//End of if
                }//End of i
                
                if($c==$d){
                    $closed_day[$cn++] =  $days[$j];
                }//End of if c equal d
                else{
                    $active_day[$an++] =  $days[$j];
                }

            }//End of j


            $output .= '<thead>';
                $output .= '<tr>';
                    $output .= '<th>Schedule</th>';
                    for($i=0;$i<$an;$i++){
                        $output .= '<th class="day-name">'.$active_day[$i].'</th>';
                    }
                $output .= '</tr>';
            $output .= '</thead>';
            $output .= '<tbody>';


            //Start of if Sunday Schedule
            if ( is_array($class_schedule) && !empty($class_schedule) ) {
                foreach ($class_schedule as $value) {
                  $output .= '<tr>';
                    $output .= '<td>'.esc_html($value['themeum_class_time_start']).' - '.esc_html($value['themeum_class_time_end']).'</td>'; 
                    for($i=0;$i<$an;$i++){
                        if ( !empty($value['themeum_'.$active_day[$i].'day_subject']) ) {
                            $output .= '<td>'.$value['themeum_'.$active_day[$i].'day_subject'].'</td>';
                        }  
                    }
                  $output .= '</tr>';                                      
                }
            }
            //End of if class Schedule




                            $output .= '</tbody>';
                        $output .= '</table>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    $output .= '</section>';//schedule-section

        }
    }        


    wp_reset_query();
	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Display Schedule", 'themeum-core'),
		"base" => "themeum_kidzy_schedule",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("show schedule for an class", 'themeum-core'),
		"category" => esc_html__('Kidzy', 'themeum-core'),
		"params" => array(

        array(
            "type"          => "textfield",
            "heading"       => esc_html__("Heading: ", 'themeum-core'),
            "param_name"    => "heading",
            "value"         => '',
        ),  

		array(
			"type"           => "dropdown",
			"heading"        => esc_html__("Category Filter", 'themeum-core'),
			"param_name"     => "class_category",
			"value"          => themeum_cat_list('schedule_tag'),
		),	                 

	      array(
	        "type"         => "textfield",
	        "heading"      => esc_html__("Custom Class", "themeum-core"),
	        "param_name"   => "class",
	        "value"        => "",
	        ),

			)

		));
}?>

