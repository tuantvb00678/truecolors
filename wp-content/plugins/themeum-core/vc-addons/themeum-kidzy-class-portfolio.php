<?php
/*--------------------------------------------------------------
 *			Portfolio Slider Shortcode
 *-------------------------------------------------------------*/

add_shortcode('themeum_class_portfolio','themeum_class_portfolio_shortcode');

function themeum_class_portfolio_shortcode($atts, $content=null)
{
	extract(shortcode_atts(array(
	'name' 		=> '',
	'column' 	=> '3',
	'class' 	=> '',
	'number' 	=> '6'
	), $atts));

	$filters = get_terms('class_category');
	$output = '';

    $output .= '<section class="classes-section-2">';
    	$output .= '<div class="container">';

		$output .= '<div class="row">';
            $output .= '<div class="col-sm-12 col-xs-12">';
                $output .= '<div class="class-sort-btn-section">';
                    $output .= '<ul class="sort-btn pull-left">';

        $output .= '<li class="active filter" data-filter="mix"><a href="#">All</a></li>';

		foreach ($filters as $filter)
		{
		   $output .= '<li class="filter" data-filter="'.$filter->slug.'" id="'.$filter->slug.'"><a href="#">'.$filter->name.'</a></li>';
		}

                    $output .= '</ul>';

                    $output .= '<form role="search" method="get" action="'.esc_url(site_url('/')).'" class="class-search pull-right custom-search">';
                        $output .= '<input type="search" name="s" placeholder="Search Classes">';
                        $output .= '<input type="hidden" name="post_type" value="class" />';
                        $output .= '<button type="submit"><i class="fa fa-search"></i></button>';
                    $output .= '</form>';
                    $output .= '<div class="clearfix"></div>';
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';

        $output .= '</div>';
    $output .= '</section>';

	$output .='<div class="row">';
	$output .= '<div id="mixer">';

	global $wpdb;
  	global $post;
  	$temp_post = $post;
	
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	$args = array(
			'post_type'			=> 'class',
			'posts_per_page' 	=> $number,
			'orderby' 			=> 'menu_order',
			'order' 			=> 'ASC',
			'paged' => $paged
		);

  	$show_class = new WP_Query($args);


  	if ( $show_class->have_posts() ){
		while($show_class->have_posts()) {
			$show_class->the_post();

			$class_status = get_post_meta(get_the_ID(),'themeum_class_status',true);
      $class_link = get_post_meta(get_the_ID(),'themeum_class_link',true);
			$years_old = get_post_meta(get_the_ID(),'themeum_years_old',true);
			$class_size = get_post_meta(get_the_ID(),'themeum_class_size',true);
			$tution_fee = get_post_meta(get_the_ID(),'themeum_tution_fee',true);
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-blog-single' );
			
			//Filter List Item
			$terms = get_the_terms( $post->ID, 'class_category' );
			$term_name = '';

			if ($terms) {
				foreach ( $terms as $term ) {
					$term_name .= ' '.$term->slug;
				}
			}


        	$output .= '<div class="col-sm-4 col-xs-12 mix'.$term_name.'">';
                $output .= '<div class="single-class theme-class">';
                    
                    if($image){
                    $output .= '<div class="class-img">';
                        $output .= '<img class="img-responsive" src="'.esc_url( $image[0] ).'" alt="'.get_the_title().'">';
                        $output .= '<div class="class-hover">';
                            $output .= '<a href="'.get_the_permalink().'" class="popup"><i class="icon-link"></i></a>';
                        $output .= '</div>';
                    $output .= '</div>';
                	}

                    $output .= '<div class="class-details">';
                        $output .= '<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
                        $output .= '<a href="'.$class_link.'" class="black">'.$class_status.'</a>';

                        
                        $output .= '<div class="clearfix">';
                        	if($years_old){
                            $output .= '<div class="class-meta pull-left">';
                                $output .= '<span>Lứa tuổi</span>';
                                $output .= '<p>'.$years_old.'</p>';
                            $output .= '</div>';
                        	}

                        	if($class_size){
                            $output .= '<div class="class-meta pull-left">';
                                $output .= '<span>Sĩ số</span>';
                                $output .= '<p>'.$class_size.'</p>';
                            $output .= '</div>';
                        	}

                        	if($tution_fee){
                            $output .= '<div class="class-meta pull-left">';
                                $output .= '<span>Học phí</span>';
                                $output .= '<a href="/contact" class="block">'.$tution_fee.'</a>';
                            $output .= '</div>';
                        	}
                        $output .= '</div>';
                    	

                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';

		}//End of while
	}//End of IF

	// pagination
	ob_start();
	themeum_pagination($show_class->max_num_pages);
	$output .= ob_get_contents();
	ob_clean();

	$output .='<div>';
	$output .= '<div>';


	$post = $temp_post;
	wp_reset_query();

	#wp_reset_postdata();
	return $output;

}

//Visual Composer addons register
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => __("Themeum Class Portfolio", "themeum"),
		"base" => "themeum_class_portfolio",
		'icon' => 'icon-thm-portfolio',
		"class" => "",
		"description" => __("Widget Class Portfolio", "themeum"),
		"category" => __('Kidzy', "themeum"),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => __("Themeum Portfolio", "themeum"),
				"param_name" => "portfolio",
				"value" => "",
				),

			array(
				"type" => "textfield",
				"heading" => __("Themeum Number Of Class: ", "themeum"),
				"param_name" => "number",
				"value" => "",
				),

			array(
				"type" => "dropdown",
				"heading" => __("Select Column", "themeum"),
				"param_name" => "column",
				"value" => array('Select'=>'','2'=>'2','3'=>'3','4'=>'4','5'=>'5'),
				),

			)

		));
}
