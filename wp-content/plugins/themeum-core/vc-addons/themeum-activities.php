<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//Upcoming Events
add_shortcode( 'themeum_activities', function($atts, $content = null) {
	$button_url 		= '';
	$target 			= '';
	extract(shortcode_atts(array(
		'image'						=> '',
		'target'		 			=> '_blank',
		'menu_icon'					=> '',
		'icon_size'					=> '',
		'title'						=> '',
		'introtext'					=> '',
		'button_url'				=> '',
		'background'				=> '',
		'background_transparency'	=> '',
		'class'						=> ''
		), $atts));

	global $post;

	$output = $style1 = '';

	$style = '';
	if($background_transparency=='') $background_transparency = 1;
	if($background) $style .= 'background-color:' . kidzy_themeumrgba($background, $background_transparency). ';';
	if($background) $style1 .= 'color:' . kidzy_themeumrgba($background, $background_transparency). ';';

	$output  .= '<a target="'.$target.'" href="'.$button_url.'"> <div class="welcome-img ' . $class .'">';			
		$image   	= wp_get_attachment_image_src($image, 'full');
		$output    .= '<img alt="" src="' . esc_url($image[0]). '">';

	        $output  	.= '<div class="welcome-content" style="'.$style.'">';
	        	$output  	.= '<div class="svg-overlay" style="'.$style1.'"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none"><path d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path></svg></div>';
				if($menu_icon) {
					$font = '';
					if ($icon_size) {
						$font 	= 'font-size: '.(int)esc_attr($icon_size).'px';
					} else {
						$font 	= 'font-size: 40px';
					}
					$icon_style  = 'style="'.$font.';"';
					$output		.= '<i '.$icon_style.' class="'.esc_attr( $menu_icon ).'"></i>';
				}

	            if ($title)
					$output  	.= '<h4>' . esc_attr($title) . '</h4>';
				if ($introtext) 
	            	$output  	.= '<p>' . esc_attr($introtext) . '</p>';
	        $output  	.= '</div>';

	$output .= '</div></a>';
	return $output;

});

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => __("Themeum Activities", "themeum-core"),
		"base" => "themeum_activities",
		'icon' => 'icon-thm-gallery',
		"class" => "",
		"description" => "Themeum Activities",
		"category" 	=> __('Kidzy', "themeum-core"),
		"params" => array(

			array(
				"type" => "attach_image",
				"heading" => esc_html__("Volunteer Image", 'themeum-core'),
				"param_name" => "image",
				"value" => "",
			),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Icon Name ", 'themeum-core'),
				"param_name" => "menu_icon",
				"value" => getIconsLists(),
			),

			array(
				"type" => "textfield", 
				"heading" => esc_html__("Icon Font Size", 'themeum-core'),
				"param_name" => "icon_size",
				"value" => "128",
			),

			array(
				"type" => "textfield",
				"heading" => __("Title", 'themeum-core'),
				"param_name" => "title",
				"value" => "",
				"admin_label"=>true,
				),

			array(
				"type" => "textarea", 
				"heading" => esc_html__("Content", 'themeum-core'),
				"param_name" => "introtext",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"heading" => __("Custom URL", "themeum-core"),
				"param_name" => "button_url",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__("Target Link", 'themeum-core'),
				"param_name" => "target",
				"value" => array('Select'=>'','Self'=>'_self','Blank'=>'_blank','Parent'=>'_parent'),
				),	

			array(
				"type" => "colorpicker",
				"heading" => esc_html__("Body Background", 'themeum-core'),
				"param_name" => "background",
				"value" => "",
			),	

			array(
				"type" => "textfield",
				"heading" => esc_html__("Background Transparency", 'themeum-core'),
				"param_name" => "background_transparency",
				"value" => ""
			),

			array(
				"type" => "textfield",
				"heading" => __("Extra CSS Class", "themeum-core"),
				"param_name" => "class",
				"value" => "",
				"description" => "Add your class"
				),

			)
		));
}

function kidzy_themeumrgba($hex, $opacity) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }

   $rgb = array($r, $g, $b);

   return 'rgba(' . implode(",", $rgb) . ', ' . $opacity . ')';
}