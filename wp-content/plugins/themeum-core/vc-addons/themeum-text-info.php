<?php
add_shortcode( 'themeum_text_info', 'themeum_text_info_function');


function themeum_text_info_function($atts, $content = null) {

	$icon_img 			= '';
	$img_class			= '';

	$icon_name 			= '';
	$icon_bg_color 		= '';
	$icon_font_size 	= '';
	$icon_color 		= '';

	$title 				= '';
	$title_text_heading = '';
	$title_text_align 	= '';
	$title_font_size	= '';
	$title_font_weight	= '';
	$title_color 		= '';
	$title_padding		= '';

	$subtitle 			= '';
	$subtitle_text_heading = '';
	$subtitle_text_align = '';
	$subtitle_font_size = '';
	$subtitle_font_weight = '';
	$subtitle_color 	= '';
	$subtitle_padding 	= '';

	$text1				= '';
	$text_text_align 	= '';
	$text_font_size  	= '';
	$text_font_weight 	= '';
	$text_color 		= '';
	$text_padding 		= '';	

	$class 				= '';



	extract(shortcode_atts(array(

		'icon_img'			=> '',
		'img_class'			=> '',

		'icon_name' 		=> '',
		'icon_bg_color' 	=> '',
		'icon_font_size'	=> '',
		'icon_color'		=> '',

		'title'				=> '',
		'title_text_heading'=> '',
		'title_text_align' 	=> '',
		'title_font_size'	=> '44',
		'title_font_weight'	=> '400',
		'title_color' 		=> '#92278f',
		'title_padding'		=> '0px 0px 0px 0px',

		'subtitle' 			=> '',
		'subtitle_text_heading' => '',
		'subtitle_text_align' => '',
		'subtitle_font_size' => '24',
		'subtitle_font_weight' => '',
		'subtitle_color'	=> '#c7c7c7',
		'subtitle_padding' 	=> '0px 0px 0px 0px',

		'text1'				=> '',
		'text_text_align' 	=> '',
		'text_font_size'  	=> '',
		'text_font_weight' 	=> '',
		'text_color' 		=> '',
		'text_padding' 		=> '',

		'class'				=> '',		

	), $atts));


	$title_style = $icon_color_bg = $title_class = $subtitle_class = $output = '';
	if($icon_bg_color){ $icon_color_bg .= 'background:'.esc_attr($icon_bg_color).';'; }

	if($title_text_align) $title_class .= 'text-align:' . $title_text_align  . ';';
	if($title_font_size) $title_class .= 'font-size:' . (int) esc_attr( $title_font_size ) . 'px;';
	if($title_font_weight) $title_class .= 'font-weight:' . (int) esc_attr( $title_font_weight ) . ';';
	if($title_color) $title_class .= 'color:' . esc_attr( $title_color )  . ';';	
	if($title_padding) $title_class .= 'padding:' . esc_attr( $title_padding )  . ';';	

	$subtitle_class = '';
	if($subtitle_text_align) $subtitle_class .= 'text-align:' . $subtitle_text_align  . ';';
	if($subtitle_font_size) $subtitle_class .= 'font-size:' . (int) esc_attr( $subtitle_font_size ) . 'px;';
	if($subtitle_font_weight) $subtitle_class .= 'font-weight:' . (int) esc_attr( $subtitle_font_weight ) . ';';
	if($subtitle_color) $subtitle_class .= 'color:' . esc_attr( $subtitle_color )  . ';';	
	if($subtitle_padding) $subtitle_class .= 'padding:' . esc_attr( $subtitle_padding )  . ';';

	$text_class = '';
	if($text_text_align) $text_class .= 'text-align:' . $text_text_align  . ';';
	if($text_font_size) $text_class .= 'font-size:' . (int) esc_attr( $text_font_size ) . 'px;';
	if($text_font_weight) $text_class .= 'font-weight:' . (int) esc_attr( $text_font_weight ) . ';';
	if($text_color) $text_class .= 'color:' . esc_attr( $text_color )  . ';';	
	if($text_padding) $text_class .= 'padding:' . esc_attr( $text_padding )  . ';';	

	$icon_style = $icon_styles1 = '';
	if($icon_font_size) $icon_style  = 'style="font-size:'.$icon_font_size.'px; color:'.$icon_color.';"';

	if($icon_font_size) $icon_styles1  = 'style="border-radius: 4px; padding: 10px 10px 15px 10px; box-shadow: inset 0 -4px 0px 0px rgba(0, 0, 0, 0.25) ;font-size:'.$icon_font_size.'px; color:'.$icon_color.'; '.$icon_color_bg.' "';

	$icon_image   = wp_get_attachment_image_src($icon_img, 'full');	



	$output .= '<div class="col-sm-12 col-xs-12">';
		$output .= '<div class="themeum-text-info '.esc_attr($class).'">';
		    
		    if ($icon_image) {
			    $output .= '<div class="'.esc_attr($img_class).'">';
		            $output .= '<img src="'.$icon_image[0].'" alt="icon image">';
		        $output .= '</div>';
	    	}

	    	if($icon_bg_color == true){
	    		$output .= '<div class="'.esc_attr($img_class).'">';
	        		$output .= '<i '.$icon_styles1.' class="'.esc_attr($icon_name).'"></i>';
	        	$output .= '</div>';
			}else{
				$output .= '<div class="'.esc_attr($img_class).'">';
	        		$output .= '<i '.$icon_style.' class="'.esc_attr($icon_name).'"></i>';
	        	$output .= '</div>';
			}

			if($title){
	        	$output .= '<'.$title_text_heading.' style="'.$title_class.'">'.$title.'</'.$title_text_heading.'>';
			}

	        if($subtitle){
	        	$output .= '<'.$subtitle_text_heading.' style="'.$subtitle_class.'">'.$subtitle.'</'.$subtitle_text_heading.'>';
	    	}

	        if($text1){
	        	$output .= '<p style="'.$text_class.'">'.$text1.'</p>';
			}

		$output .= '</div>';
	$output .= '</div>';


	return $output;
}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("Themeum Text Info", 'themeum-core'),
	"base" => "themeum_text_info",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Widget Text Info show", 'themeum-core'),
	"category" => __('Kidzy', 'themeum-core'),
	"params" => array(

		#Icon Image		
		array(
			"type" 			=> "attach_image",
			"heading" 		=> esc_html__("Upload Icon Image", 'themeum-core'),
			"param_name" 	=> "icon_img",
			"value" 		=> "",
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Custom Image Class ", 'themeum-core'),
			"param_name" 	=> "img_class",
			"value" 		=> "",
		),	

		#Element for Icon
		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Icon Name ", 'themeum-core'),
			"param_name" 	=> "icon_name",
			"value" 		=> getIconsLists(),
		),

		array(
			"type" 			=> "colorpicker",
			"heading" 		=> esc_html__("Icon Background Color", 'themeum-core'),
			"param_name" 	=> "icon_bg_color",
			"value" 		=> "#92278f",
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Icon Font Size: ", 'themeum-core'),
			"param_name" 	=> "icon_font_size",
			"value" 		=> "40",
		),

		array(
			"type" 			=> "colorpicker",
			"heading" 		=> esc_html__("Icon Color", 'themeum-core'),
			"param_name" 	=> "icon_color",
			"value" 		=> "#0072bc",
		),				

		#Elements for title
		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Top Title: ", 'themeum-core'),
			"param_name" 	=> "title",
			"value" 		=> "",
		),

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Title Heading", 'themeum-core'),
			"param_name" 	=> "title_text_heading",
			"value" 		=> array('Select'=>'','H1'=>'h1','H2'=>'h2','H3'=>'h3','H4'=>'h4','H5'=>'h5','SPAN'=>'span'),
			"default"		=> "h2"
			),		

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Title Alignment", 'themeum-core'),
			"param_name" 	=> "title_text_align",
			"value" 		=> array('Select'=>'','justify'=>'justify','left'=>'left','right'=>'right','center'=>'center'),
			),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Title Font Size", 'themeum-core'),
			"param_name" 	=> "title_font_size",
			"value" 		=> "44",
		),		

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Title Font Wight", 'themeum-core'),
			"param_name" 	=> "title_font_weight",
			"value" 		=> array('Select'=>'','400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" 			=> "colorpicker",
			"heading" 		=> esc_html__("Title Color", 'themeum-core'),
			"param_name" 	=> "title_color",
			"value" 		=> "#92278f",
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Title Padding", 'themeum-core'),
			"param_name" 	=> "title_padding",
			"value" 		=> "0px 0px 0px 0px",
		),		



		# Elements for Subtitle
		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Sub-Title: ", 'themeum-core'),
			"param_name" 	=> "subtitle",
			"value" 		=> "",
		),

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Sub-title Heading", 'themeum-core'),
			"param_name" 	=> "subtitle_text_heading",
			"value" 		=> array('Select'=>'','H1'=>'h1','H2'=>'h2','H3'=>'h3','H4'=>'h4','H5'=>'h5','SPAN'=>'span'),
			"default"		=> "h4"
			),			

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Sub-Title Alignment", 'themeum-core'),
			"param_name" 	=> "subtitle_text_align",
			"value" 		=> array('Select'=>'','justify'=>'justify','left'=>'left','right'=>'right','center'=>'center'),
			),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Sub-Title Font Size", 'themeum-core'),
			"param_name" 	=> "subtitle_font_size",
			"value" 		=> "24",
		),		

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Sub-Title Font Wight", 'themeum-core'),
			"param_name" 	=> "subtitle_font_weight",
			"value" 		=> array('Select'=>'','400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" 			=> "colorpicker",
			"heading" 		=> esc_html__("Sub-Title Color", 'themeum-core'),
			"param_name" 	=> "subtitle_color",
			"value" 		=> "#92278f",
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Sub-Title Padding", 'themeum-core'),
			"param_name" 	=> "subtitle_padding",
			"value" 		=> "0px 0px 0px 0px",
		),

		# Elements for Text
		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Text-1: ", 'themeum-core'),
			"param_name" 	=> "text1",
			"value" 		=> "",
		),

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Text Alignment", 'themeum-core'),
			"param_name" 	=> "text_text_align",
			"value" 		=> array('Select'=>'','justify'=>'justify','left'=>'left','right'=>'right','center'=>'center'),
			),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Text Font Size", 'themeum-core'),
			"param_name" 	=> "text_font_size",
			"value" 		=> "24",
		),		

		array(
			"type" 			=> "dropdown",
			"heading" 		=> esc_html__("Text Font Wight", 'themeum-core'),
			"param_name" 	=> "text_font_weight",
			"value" 		=> array('Select'=>'','400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" 			=> "colorpicker",
			"heading" 		=> esc_html__("Text Color", 'themeum-core'),
			"param_name" 	=> "text_color",
			"value" 		=> "#92278f",
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> esc_html__("Text Padding", 'themeum-core'),
			"param_name" 	=> "text_padding",
			"value" 		=> "0px 0px 0px 0px",
		),


		#Custom CSS
		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Custom Class ", 'themeum-core'),
			"param_name" 	=> "class",
			"value" 		=> "",
		),
		
		)
	));
}
?>