<?php
add_shortcode( 'themeum_kidzy_class', 'themeum_kidzy_class_function');

function themeum_kidzy_class_function($atts, $content = null) {
	
	$heading 		= '';
	$class_category = '';
	$count_post 	= '';
	$column 		= '';
	$class  		= '';

	extract(shortcode_atts(array(
		'heading' 		=> '',
		'class_category'=> '',
    	'count_post' 	=>	7,
    	'column' 		=> '',
		'class' 		=> '',
		), $atts));


	global $wpdb;
  	global $post;

  	$args = array(
       'post_type' 	=> 'class',
	   'tax_query'  => array(
							array(
								'taxonomy' => 'class_category',
								'field'    => 'slug',
								'terms'    => $class_category,
							),
						),
      'order' 			=> 'ASC',
      'posts_per_page'	=> esc_attr($count_post)
    );

  	$show_class = new WP_Query($args);

	$output = '';

  	if ( $show_class->have_posts() ){
		while($show_class->have_posts()) {
			$show_class->the_post();

			$class_status = get_post_meta(get_the_ID(),'themeum_class_status',true);
      $class_link = get_post_meta(get_the_ID(),'themeum_class_link',true);
			$years_old = get_post_meta(get_the_ID(),'themeum_years_old',true);
			$class_size = get_post_meta(get_the_ID(),'themeum_class_size',true);
			$tution_fee = get_post_meta(get_the_ID(),'themeum_tution_fee',true);
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-blog-single' );


        	$output .= '<div class="col-sm-4 col-xs-12">';
                $output .= '<div class="single-class">';

                if($image){
                    $output .= '<div class="class-img">';
                        $output .= '<img class="img-responsive" src="'.esc_url( $image[0] ).'" alt="'.get_the_title().'">';
                        $output .= '<a class="class-hover" href="'.get_the_permalink().'" >';
                            $output .= '<span class="popup"><i class="icon-link"></i></span>';
                            $output .= '<div class="class__short-description">'.wp_trim_words( get_the_content(), 30, '...' ).'</div>';
                        $output .= '</a>';
                    $output .= '</div>';
                }
                
                    $output .= '<div class="class-details">';
                        $output .= '<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
                        $output .= '<a href="'.$class_link.'" class="black">'.$class_status.'</a>';
                        $output .= '<div class="clearfix">';
                            $output .= '<div class="class-meta pull-left">';
                                $output .= '<span>Lứa tuổi</span>';
                                $output .= '<p>'.$years_old.'</p>';
                            $output .= '</div>';
                            $output .= '<div class="class-meta pull-left">';
                                $output .= '<span>Sĩ số</span>';
                                $output .= '<p>'.$class_size.'</p>';
                            $output .= '</div>';
                            $output .= '<div class="class-meta pull-left">';
                                $output .= '<span>Học phí</span>';
                                $output .= '<a href="/contact" class="block">'.$tution_fee.'</a>';
                            $output .= '</div>';
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';

		}//End of while
	}//End of IF

	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Display Classes", 'themeum-core'),
		"base" => "themeum_kidzy_class",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("All Class display by Category", 'themeum-core'),
		"category" => esc_html__('Kidzy', 'themeum-core'),
		"params" => array(

		array(
			"type" 		=> "dropdown",
			"heading" 	=> esc_html__("Category Filter", 'themeum-core'),
			"param_name" => "class_category",
			"value" 	=> themeum_cat_list('class_category'),
		),

	    array(
	        "type" 		=> "textfield",
	        "heading" 	=> esc_html__("Number Of Post Show", "themeum-core"),
	        "param_name" => "count_post",
	        "value" 	=> "6",
	    ),

		array(
			"type" 		=> "dropdown",
			"heading" 	=> esc_html__("Number Of Column", "themeum-core"),
			"param_name" => "column",
			"value" 	=> array('No Column'=>'','column 2'=>'6','column 3'=>'4','column 4'=>'3'),
			),

	      array(
	        "type" 		=> "textfield",
	        "heading" 	=> esc_html__("Custom Class", "themeum-core"),
	        "param_name" => "class",
	        "value" 	=> "",
	        ),

			)

		));
}?>
