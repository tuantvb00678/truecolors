<?php
  add_shortcode( 'themeum_kidzy_teacher', 'themeum_kidzy_teacher_function');
  
  function themeum_kidzy_teacher_function($atts, $content = null) {
    
    $count_post 	= '';
    $column 		= '';
    $class  		= '';
    
    extract(shortcode_atts(array(
      'count_post' 	=>	4,
      'column' 		=> '',
      'class' 		=> '',
    ), $atts));
    
    
    global $wpdb;
    global $post;
    
    
    $args = array(
      'post_type' 		=> 'teacher',
      'order' 			=> 'ASC',
      'posts_per_page'	=> esc_attr($count_post)
    );
    
    $show_teacher = new WP_Query($args);
    
    $output = '<div class="box_our_teachers">';
    
    if ( $show_teacher->have_posts() ){
      while($show_teacher->have_posts()) {
        $show_teacher->the_post();
        
        $teacher_designation = get_post_meta(get_the_ID(),'themeum_designation',true);
        $teacher_facebook = get_post_meta(get_the_ID(),'themeum_facebook',true);
        $teacher_twitter = get_post_meta(get_the_ID(),'themeum_twitter',true);
        $teacher_gplus = get_post_meta(get_the_ID(),'themeum_gplus',true);
        $teacher_linkedin = get_post_meta(get_the_ID(),'themeum_linkedin',true);
        $teacher_instagram = get_post_meta(get_the_ID(),'themeum_instagram',true);
        $teacher_flickr = get_post_meta(get_the_ID(),'themeum_flickr',true);
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'kidzy-medium' );
        
        
        $output .= '<div class="col-sm-3 col-xs-12 text-center">';
        $output .= '<div class="single-team">';
        $output .= '<div class="team-img">';
        if($image){
          $output .= '<a href="'.get_the_permalink().'"><img class="img-responsive" src="'.esc_url( $image[0] ).'" alt="'.get_the_title().'"></a>';
        }
        $output .= '<ul class="team-social">';
        if($teacher_facebook){
          $output .= '<li><a href="'.$teacher_facebook.'" class="facebook"><i class="fa fa-facebook"></i></a></li>';
        }
        if($teacher_twitter){
          $output .= '<li><a href="'.$teacher_twitter.'" class="twitter"><i class="fa fa-twitter"></i></a></li>';
        }
        if($teacher_gplus){
          $output .= '<li><a href="'.$teacher_gplus.'" class="gplus"><i class="fa fa-google-plus"></i></a></li>';
        }
        if($teacher_linkedin){
          $output .= '<li><a href="'.$teacher_linkedin.'" class="linkedin"><i class="fa fa-linkedin"></i></a></li>';
        }
        if($teacher_instagram){
          $output .= '<li><a href="'.$teacher_instagram.'" class="gplus"><i class="fa fa-instagram"></i></a></li>';
        }
        if($teacher_flickr){
          $output .= '<li><a href="'.$teacher_flickr.'" class="flickr"><i class="fa fa-flickr"></i></a></li>';
        }
        $output .= '</ul>';
        $output .= '</div>';
        $output .= '<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
        $output .= '<p>'.$teacher_designation.'</p>';
        $output .= '</div>';
        $output .= '</div>';
        
      }//End of while
    }//End of IF
    $output .= '</div>';
    wp_reset_query();
    
    return $output;
  }


//Visual Composer
  if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
      "name" => esc_html__("Display Teacher", 'themeum-core'),
      "base" => "themeum_kidzy_teacher",
      'icon' => 'icon-thm-latest-news',
      "class" => "",
      "description" => esc_html__("Display all of the teacher", 'themeum-core'),
      "category" => esc_html__('Kidzy', 'themeum-core'),
      "params" => array(
        array(
          "type" 			=> "dropdown",
          "heading" 		=> esc_html__("Type Display", "themeum-core"),
          "param_name" 	=> "type_display",
          "value" 		=> array('Teacher'=> 'teacher','Classes'=>'classes'),
        ),
        
        array(
          "type" 			=> "textfield",
          "heading" 		=> esc_html__("Number Of Post Show", "themeum-core"),
          "param_name" 	=> "count_post",
          "value" 		=> "4",
        ),
        
        array(
          "type" 			=> "dropdown",
          "heading" 		=> esc_html__("Number Of Column", "themeum-core"),
          "param_name" 	=> "column",
          "value" 		=> array('No Column'=>'','column 2'=>'6','column 3'=>'4','column 4'=>'3'),
        ),
        
        array(
          "type" 			=> "textfield",
          "heading" 		=> esc_html__("Custom Class", "themeum-core"),
          "param_name" 	=> "class",
          "value" 		=> "",
        ),
      
      )
    
    ));
  }
