<?php
  add_shortcode( 'themeum_latest_news', 'themeum_latest_news_function');
  
  function themeum_latest_news_function($atts, $content = null) {
    
    $category 	= 'themeumall';
    $number 	= '5';
    $column 	= '';
    $class  	= '';
    $order_by	= 'date';
    $order		= 'DESC';
    
    extract(shortcode_atts(array(
      'category' 		=> 'themeumall',
      'number' 		=> '5',
      'column' 		=> '5',
      'class' 		=> '',
      'order_by'		=> 'date',
      'order'			=> 'DESC',
    ), $atts));
    
    global $post;
    global $wpdb;
    $output = $style = $style1 = $all_bg = '';
    
    // Basic Query
    $args = array(
      'post_status'		=> 'publish',
      'posts_per_page'	=> esc_attr($number),
      'order'				=> $order,
      'orderby'			=> $order_by
    );
    
    // Category Add
    if( ( $category != '' ) && ( $category != 'themeumall' ) ){
      $args2 = array(
        'tax_query' => array(
          array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => $category,
          ),
        ),
      );
      $args = array_merge( $args,$args2 );
    }
    $data = new WP_Query($args);
  
    ob_start();
    if ( $data->have_posts() ) : ?>
      <div class="single-news__grid">
        <div class="single-news__wrapper">
          <div class="single-news__container">
            <div class="single-news__inner">
              <div class="single-news__list f jcc ais">
                <?php foreach($data->posts as $post) : ?>
                  <?php
                    $post_id = $post->ID;
                    $post_title = $post->post_title;
                    $post_image = wp_get_attachment_url( get_post_thumbnail_id($post_id), 'thumbnail' );
                    $post_description = kidzy_excerpt_max_charlength_post_id(300, $post_id);
                    $post_link = get_the_permalink($post_id);
                    $post_date_value = get_the_time('Y-m-d H:i');
                    $post_date = get_the_time('l, j F, Y');
                    $post_day = get_the_time('j');
                    $post_month = get_the_time('M');
                    $post_tags = get_the_terms($post_id, 'category');
                    $post_new = get_post_meta( $post_id, 'themeum__post_new',true );
                  ?>
                  <div class="single-news__item">
                    <div class="single-news__item-inner">
                      <div class="single-news__item-details relative">
                        <?php if (!empty($post_image)) : ?>
                          <div class="single-news__item-image relative">
                            <a href="<?php echo $post_link; ?>" class="block">
                              <figure class="single-news__item-image-wrapper" style="background-image: url(<?php echo $post_image; ?>);">
                                <span class="image-alt" role="img" aria-label="<?php echo esc_attr( $post_title ); ?>"></span>
                              </figure>
                            </a>
                            <a class="single-news__item-label-date" href="<?php echo $post_link; ?>">
                              <div class="single-news__item-label-day"><?php echo $post_day; ?></div>
                              <div class="single-news__item-label-day"><?php echo $post_month; ?></div>
                            </a>
  
                            <?php if (!empty($post_new)) : ?>
                              <div class="single-news__item-new">
                                <?= _get_svg('icon-new');?>
                              </div>
                            <?php endif; ?>
                            
                          </div>
                        <?php endif; ?>
  
                        <div class="single-news__item--info">
                          <?php if (!empty($post_title)) : ?>
                            <h5 class="single-news__item-title">
                              <a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a>
                            </h5>
                          <?php endif; ?>
                          
                          <?php if (!empty($post_description)) :?>
                            <div class="single-news__item-description">
                              <?php echo $post_description; ?>
                            </div>
                          <?php endif;?>
                          
                          <?php if (!empty($post_date)) : ?>
                            <div class="single-news__item-date">
                              <i class="fa fa-clock-o" aria-hidden="true"></i>
                              <time datetime="<?php echo $post_date_value; ?>"><?php echo $post_date; ?></time>
                            </div>
                          <?php endif; ?>
        
                          <?php if (!empty($post_tags)) : ?>
                          <div class="single-news__item-tags">
                            <ul>
                              <?php foreach ($post_tags as $post_tag): ?>
                                <li>
                                  <i class="fa fa-tag" aria-hidden="true"></i>
                                  <a class="single-news__item-tag-link" href="<?php echo get_tag_link($post_tag->term_id); ?>" rel="category tag"><?php echo $post_tag->name; ?></a>
                                </li>
                              <?php endforeach; ?>
                            </ul>
                          </div>
                        <?php endif; ?>
                        </div>
                      </div>
    
                      <?php
                        $views = get_post_meta( $post_id, 'themeum__post_view_number',true );
                        $comments = get_post_meta( $post_id, 'themeum__post_comment_number',true );
                        $likes = get_post_meta( $post_id, 'themeum__post_like_number',true );
      
                        $comments_count = get_comments_number( $post_id );
                        $total_comment = ($comments_count <= 1) ? 0 : $comments_count;
                        $total_views = ($views <= 1) ? 0 : $views;
                        $total_likes = ($likes <= 1) ? 0 : $likes;
    
                      ?>
                      <div class="single-news__item-action f jcb aic">
                        <div class="single-news__item-action-group f jcc aic">
                          <a href="<?php echo $post_link; ?>" class="single-news__item-view f jcc aic black">
                            <?= _get_svg('icon-view');?> <?php echo $total_views; ?>
                          </a>
                          <a href="<?php echo $post_link; ?>" class="single-news__item-comment f jcc aic black">
                            <?= _get_svg('icon-comment');?> <?php echo $total_comment; ?>
                          </a>
                        </div>
                        <div class="single-news__item-like f jcc aic js-handle-click-like" data-id="<?php echo $post_id?>">
                          <?= _get_svg('icon-heart');?> <span class="js-like-count"><?php echo $total_likes; ?></span>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                <?php endforeach; ?>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
    endif;
  
    $output = ob_get_clean();
    return $output;
  }


//Visual Composer
  if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map(array(
      "name" => esc_html__("Latest News", 'themeum-core'),
      "base" => "themeum_latest_news",
      'icon' => 'icon-thm-latest-news',
      "class" => "",
      "description" => esc_html__("Latest News", 'themeum-core'),
      "category" => esc_html__('Kidzy', 'themeum-core'),
      "params" => array(
        
        
        array(
          "type" 			=> "dropdown",
          "heading" 		=> esc_html__("Category Filter", 'themeum-core'),
          "param_name" 	=> "category",
          "value" 		=> themeum_cat_list('category'),
        ),
        
        array(
          "type" 			=> "textfield",
          "heading" 		=> esc_html__("Number of items", 'themeum-core'),
          "param_name" 	=> "number",
          "value" 		=> "5",
        ),
        
        array(
          "type" 			=> "dropdown",
          "heading" 		=> esc_html__("Number Of Column", "themeum-core"),
          "param_name" 	=> "column",
          "value" 		=> array('column 2'=>'6','column 3'=>'4','column 4'=>'3'),
        ),
        
        array(
          "type" 			=> "dropdown",
          "heading" 		=> esc_html__("OderBy", 'themeum-core'),
          "param_name" 	=> "order_by",
          "value" 		=> array('Select'=>'','Date'=>'date','Title'=>'title','Modified'=>'modified','Author'=>'author','Random'=>'rand'),
        ),
        
        array(
          "type" 			=> "dropdown",
          "heading" 		=> esc_html__("Order", 'themeum-core'),
          "param_name" 	=> "order",
          "value" 		=> array('Select'=>'','DESC'=>'DESC','ASC'=>'ASC'),
        ),
        
        array(
          "type" 			=> "textfield",
          "heading" 		=> esc_html__("Custom Class", 'themeum-core'),
          "param_name" 	=> "class",
          "value" 		=> "",
        ),
      
      )
    
    ));
  }
