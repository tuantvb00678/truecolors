<?php
add_shortcode( 'themeum_buttons', 'themeum_buttons_function');


function themeum_buttons_function($atts, $content = null) {

	$button_text 		= '';
	$button_url 		= '';
	$color 				= '';
	$bg_color 			= '';
	$button_padding 	= '';
	$button_margin	 	= '';
	$btn_layout 		= '';
	$target 			= '';

	extract(shortcode_atts(array(
		'target'		 	=> '_blank',
		'button_text'		=> '',
		'button_url'		=> '',
		'color' 			=> '',
		'bg_color'			=> '',
		'button_padding'	=> '',
		'button_margin'		=> '',
		'btn_layout'		=> '',

	), $atts));


	$output = $inline1 = $inline2 = $inline3 = $inline_css1 = $icon_styles1 = '';

	if($button_margin) $inline2   	= 'margin:'.$button_margin.';';
	if($button_padding) $inline3  	= 'padding:'.$button_padding.';';
	if($btn_layout) $inline1  		= 'float:'.$btn_layout.'; text-align:'.$btn_layout.';';
	if($bg_color){ $inline_css1 	= 'background:'.esc_attr($bg_color).';'; }


	if($bg_color) $icon_styles1  = 'style="'. $inline3 .';'. $inline_css1 .'; box-shadow: 0 4px 0px 0 '. $bg_color .', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1); -webkit-box-shadow: 0 4px 0px 0 '.$bg_color.', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1);"';

	if($btn_layout=="center"){
		$btn_style = 'style="'. $inline3 .';'. $inline_css1 .'; box-shadow: 0 4px 0px 0 '. $bg_color .', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1); -webkit-box-shadow: 0 4px 0px 0 '.$bg_color.', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1); margin: 0 auto;"';
	}
	else{
		$btn_style = 'style="'. $inline3 .';'. $inline_css1 .'; box-shadow: 0 4px 0px 0 '. $bg_color .', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1); -webkit-box-shadow: 0 4px 0px 0 '.$bg_color.', 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1);"';
	}
			
			$output .= '<div class="callto-button-content" style="'. $inline1  .' '. $inline2 .'">';

				if( $button_text != '' ){
					$output .= '<a target="'.$target.'" href="'.$button_url.'" class="primary-btn" ' . $btn_style . '>'.$button_text.'</a>';
				}

			$output .= '</div>';

	return $output;

}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("Themeum Button", "themeum-core"),
	"base" => "themeum_buttons",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Themeum Button", "themeum-core"),
	"category" => __('Kidzy', "themeum-core"),
	"params" => array(		

		array(
			"type" => "textfield",
			"heading" => __("Button Text", "themeum-core"),
			"param_name" => "button_text",
			"value" => "",
		),

		array(
			"type" => "textfield",
			"heading" => __("Button URL", "themeum-core"),
			"param_name" => "button_url",
			"value" => "",
		),

		array(
			"type" => "colorpicker",
			"heading" => __("Button Font Color", "themeum-core"),
			"param_name" => "color",
			"value" => "#ed1c24",
		),

		array(
			"type" => "colorpicker",
			"heading" => __("Button Background Color", "themeum-core"),
			"param_name" => "bg_color",
			"value" => "#92278f",
		),

		array(
			"type" => "textfield",
			"heading" => esc_html__("Button Padding", 'themeum-core'),
			"param_name" => "button_padding",
			"value" => "0px 0px 0px 0px",
		),
		array(
			"type" => "dropdown",
			"heading" => esc_html__("Target Link", 'themeum-core'),
			"param_name" => "target",
			"value" => array('Select'=>'','Self'=>'_self','Blank'=>'_blank','Parent'=>'_parent'),
			),	
		array(
			"type" => "textfield",
			"heading" => esc_html__("Button Margin", 'themeum-core'),
			"param_name" => "button_margin",
			"value" => "0",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Button Layout", 'themeum-core'),
			"param_name" => "btn_layout",
			"value" => array('Select'=>'','Center'=>'center','Left'=>'left','Right'=>'right'),
		),

		array(
			"type" => "textfield",
			"heading" => __("Custom Class ", "themeum-core"),
			"param_name" => "class",
			"value" => "",
			),

		)
	));
}