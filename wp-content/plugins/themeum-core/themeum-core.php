<?php
/*
* Plugin Name: Themeum Core
* Plugin URI: http://www.themeum.com/item/core
* Author: Themeum
* Author URI: http://www.themeum.com
* License - GNU/GPL V2 or Later
* Description: Themeum Core is a required plugin for this theme.
* Version: 1.8
*/
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// language
add_action( 'init', 'themeum_core_language_load' );
function themeum_core_language_load(){
    $plugin_dir = basename(dirname(__FILE__))."/languages/";
    load_plugin_textdomain( 'themeum-core', false, $plugin_dir );
}

if( !function_exists("themeum_cat_list") ){
    // List of Group
    function themeum_cat_list( $category ){
        global $wpdb;
        $sql = "SELECT * FROM `".$wpdb->prefix."term_taxonomy` INNER JOIN `".$wpdb->prefix."terms` ON `".$wpdb->prefix."term_taxonomy`.`term_taxonomy_id`=`".$wpdb->prefix."terms`.`term_id` AND `".$wpdb->prefix."term_taxonomy`.`taxonomy`='".$category."'";
        $results = $wpdb->get_results( $sql );

        $cat_list = array();
        $cat_list['All'] = 'themeumall';  
        if(is_array($results)){
            foreach ($results as $value) {
                $cat_list[$value->name] = $value->slug;
            }
        }
        return $cat_list;
    }
}

if(!function_exists('kidzy_excerpt_max_char')):
    function kidzy_excerpt_max_char($charlength) {
        $excerpt = get_the_excerpt();
        $charlength++;

        if ( mb_strlen( $excerpt ) > $charlength ) {
            $subex = mb_substr( $excerpt, 0, $charlength - 5 );
            $exwords = explode( ' ', $subex );
            $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
            if ( $excut < 0 ) {
                return mb_substr( $subex, 0, $excut );
            } else {
                return $subex;
            }

        } else {
            return $excerpt;
        }
    }
endif;


// Metabox Include
include_once( 'post-type/meta_box.php' );
include_once( 'post-type/meta-box/meta-box.php' );
include_once( 'post-type/meta-box-group/meta-box-group.php' );

include_once( 'post-type/kidzy-teachers.php');
include_once( 'post-type/kidzy-classes.php');
include_once( 'post-type/kidzy-event.php');
include_once( 'post-type/kidzy-schedule.php');

// Redux integration
global $themeum_options; 
include_once( 'lib/redux/framework.php' );
include_once( 'lib/admin-config.php' );
include_once( 'import-functions.php' );

//login up
include_once( 'lib/registration.php' );
include_once( 'lib/ajax-login.php' );

// New Shortcode
include_once( 'vc-addons/fontawesome-helper.php' );
include_once( 'vc-addons/fontbistro-helper.php' );
include_once( 'vc-addons/themeum-latest-news.php' );

# kidzy shortcode
include_once( 'vc-addons/themeum-activities.php' );
include_once( 'vc-addons/themeum-testimonial.php' );
include_once( 'vc-addons/themeum-listing.php' );
include_once( 'vc-addons/themeum-buttons.php' );

#----------------------------------------------
#           Start: Kidyz shortcode
#----------------------------------------------
include_once( 'vc-addons/themeum-text-info.php' );
include_once( 'vc-addons/themeum-kidzy-class.php' );
include_once( 'vc-addons/themeum-kidzy-teacher.php' );
include_once( 'vc-addons/themeum-kidzy-events.php' );
include_once( 'vc-addons/themeum-kidzy-class-portfolio.php');
include_once( 'vc-addons/themeum-kidzy-schedule.php');
#----------------------------------------------
#           End: Kidyz shortcode
#----------------------------------------------



# Add CSS for Frontend
add_action( 'wp_enqueue_scripts', 'themeum_core_style' );
if(!function_exists('themeum_core_style')):
    function themeum_core_style(){

        # CSS
        wp_enqueue_style('animate',plugins_url('assets/css/animate.css',__FILE__));
        wp_enqueue_style('font-awesome',plugins_url('assets/css/font-awesome.min.css',__FILE__));

        wp_enqueue_style('reduxadmincss',plugins_url('assets/css/reduxadmincss.css',__FILE__));
        wp_enqueue_style('magnific-popup',plugins_url('assets/css/magnific-popup.css',__FILE__));
        wp_enqueue_style('themeum-owl-carousel',plugins_url('assets/css/owl.carousel.css',__FILE__));
        wp_enqueue_style('themeum-core',plugins_url('assets/css/themeum-core.css',__FILE__));
        wp_enqueue_style('meta-box-group-css',plugins_url('post-type/meta-box-group/group.css',__FILE__));        

        #js
        wp_enqueue_script('jquery.inview.min',plugins_url('assets/js/jquery.inview.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('jquery.countdown',plugins_url('assets/js/jquery.countdown.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('jquery.counterup.min',plugins_url('assets/js/jquery.counterup.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('owl-carousel-min',plugins_url('assets/js/owl.carousel.min.js',__FILE__), array('jquery')); 
        wp_enqueue_script('jquery.magnific-popup',plugins_url('assets/js/jquery.magnific-popup.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('mixIt',plugins_url('assets/js/mixIt.js',__FILE__), array('jquery')); 
                       
        wp_enqueue_script('themeumcore-js',plugins_url('assets/js/main.js',__FILE__), array('jquery'));
        wp_enqueue_script('meta-box-group-js',plugins_url('post-type/meta-box-group/group.js',__FILE__), array('jquery'));          
      
    }
endif;

function beackend_theme_reduxadmincss()
{
    wp_enqueue_style('reduxadmincss',plugins_url('assets/css/reduxadmincss.css',__FILE__));
}
add_action( 'admin_print_styles', 'beackend_theme_reduxadmincss' );
