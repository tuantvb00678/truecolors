<?php
/*--------------------------------------------------------------
 *			Register Schedule Post Type
 *-------------------------------------------------------------*/

function themeum_post_type_schedule()
{
	$labels = array( 
			'name'                	=> _x( 'Schedule', 'Portfolio', 'themeum-core' ),
			'singular_name'       	=> _x( 'Schedule', 'Portfolio', 'themeum-core' ),
			'menu_name'           	=> __( 'Schedule', 'themeum-core' ),
			'parent_item_colon'   	=> __( 'Parent Schedule:', 'themeum-core' ),
			'all_items'           	=> __( 'All Schedule', 'themeum-core' ),
			'view_item'           	=> __( 'View Schedule', 'themeum-core' ),
			'add_new_item'        	=> __( 'Add New Schedule', 'themeum-core' ),
			'add_new'             	=> __( 'New Schedule', 'themeum-core' ),
			'edit_item'           	=> __( 'Edit Schedule', 'themeum-core' ),
			'update_item'         	=> __( 'Update Schedule', 'themeum-core' ),
			'search_items'        	=> __( 'Search Schedule', 'themeum-core' ),
			'not_found'           	=> __( 'No article found', 'themeum-core' ),
			'not_found_in_trash'  	=> __( 'No article found in Trash', 'themeum-core' )
		);

	$args = array(  
			'labels'             	=> $labels,
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_in_menu'       	=> true,
			'show_in_admin_bar'   	=> true,
			'can_export'          	=> true,
			'has_archive'        	=> true,
			'hierarchical'       	=> false,
			'menu_position'      	=> null,
			'supports'           	=> array( 'title','editor','thumbnail','comments')
		);

	register_post_type('schedule',$args);

}

add_action('init','themeum_post_type_schedule');


/*--------------------------------------------------------------
 *			View Message When Updated Schedule
 *-------------------------------------------------------------*/

function themeum_update_message_schedule()
{
	global $post, $post_ID;

	$message['schedule'] = array(
					0 	=> '',
					1 	=> sprintf( __('Schedule updated. <a href="%s">View Schedule</a>', 'themeum-core' ), esc_url( get_permalink($post_ID) ) ),
					2 	=> __('Custom field updated.', 'themeum-core' ),
					3 	=> __('Custom field deleted.', 'themeum-core' ),
					4 	=> __('Schedule updated.', 'themeum-core' ),
					5 	=> isset($_GET['revision']) ? sprintf( __('Schedule restored to revision from %s', 'themeum-core' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
					6 	=> sprintf( __('Schedule published. <a href="%s">View Schedule</a>', 'themeum-core' ), esc_url( get_permalink($post_ID) ) ),
					7 	=> __('Schedule saved.', 'themeum-core' ),
					8 	=> sprintf( __('Schedule submitted. <a target="_blank" href="%s">Preview Schedule</a>', 'themeum-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
					9 	=> sprintf( __('Schedule scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Schedule</a>', 'themeum-core' ), date_i18n( __( 'M j, Y @ G:i','themeum-core'), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
					10 	=> sprintf( __('Schedule draft updated. <a target="_blank" href="%s">Preview Schedule</a>', 'themeum-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		);

return $message;
}

add_filter( 'post_updated_messages', 'themeum_update_message_schedule' );


/*--------------------------------------------------------------
 *			Register Custom Taxonomies
 *-------------------------------------------------------------*/

function themeum_create_schedule_taxonomy()
{
	$labels = array(	'name'              => _x( 'Categories', 'taxonomy general name','themeum-core'),
						'singular_name'     => _x( 'Category', 'taxonomy singular name','themeum-core' ),
						'search_items'      => __( 'Search Category','themeum-core'),
						'all_items'         => __( 'All Category','themeum-core'),
						'parent_item'       => __( 'Parent Category','themeum-core'),
						'parent_item_colon' => __( 'Parent Category:','themeum-core'),
						'edit_item'         => __( 'Edit Category','themeum-core'),
						'update_item'       => __( 'Update Category','themeum-core'),
						'add_new_item'      => __( 'Add New Category','themeum-core'),
						'new_item_name'     => __( 'New Category Name','themeum-core'),
						'menu_name'         => __( 'Category','themeum-core')
		);

	$args = array(	'hierarchical'      => true,
					'labels'            => $labels,
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
		);

	register_taxonomy('schedule_tag',array( 'schedule' ),$args);

}

add_action('init','themeum_create_schedule_taxonomy');
?>