<?php
/**
 * Admin feature for Custom Meta Box
 *
 * @author 		Themeum
 * @category 	Admin Core
 * @package 	Varsity
 *-------------------------------------------------------------*/


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Registering meta boxes
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

add_filter( 'rwmb_meta_boxes', 'themeum_kidzy_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */

function themeum_kidzy_register_meta_boxes( $meta_boxes )
{

	$teachers = get_all_posts('teacher');

	/**
	 * Prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */

	// Better has an underscore as last sign
	$prefix = 'themeum_';

	/**
	 * Register Post Meta for Movie Post Type
	 *
	 * @return array
	 */


// --------------------------------------------------------------------------------------------------------------
// ------------------------------------------- Schedule Post type -----------------------------------------------
// --------------------------------------------------------------------------------------------------------------

// Add Sunday Schedule Timeline
$meta_boxes[] = array(
	'id' 		=> 'schedule-meta',
	'title' 	=> __( 'Class Schedule Timeline', 'themeum-core' ),
	'pages' 	=> array( 'schedule'),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'autosave' 	=> true,
	'fields' 	=> array(

		//Sunday Schedule Timeline
		array(
		    'name' => '', // Optional
		    'id' => 'themeum_class_schedule',
		    'type'   => 'group',
		    'clone'  => true,
		    'fields' => array(
				// Time Start
				array(
					'name'       => __( 'Time Start: ', 'themeum-core' ),
					'id'         => "{$prefix}class_time_start",
					'type'       => 'time',
					'js_options' => array(
						'appendText'      => __( '(hh-mm)', 'themeum-core' ),
						'dateFormat'      => __( 'hh-mm', 'themeum-core' ),
						'showButtonPanel' => true,
					),
				),
				// Time
				array(
					'name'       => __( 'Time End: ', 'themeum-core' ),
					'id'         => "{$prefix}class_time_end",
					'type'       => 'time',
					'js_options' => array(
						'appendText'      => __( '(hh-mm)', 'themeum-core' ),
						'dateFormat'      => __( 'hh-mm', 'themeum-core' ),
						'showButtonPanel' => true,
					),
				),
				array(
					'name'          => __( 'Sunday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}sunday_subject",
					'type'          => 'text',
					'std'           => ''
				),
				array(
					'name'          => __( 'Monday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}monday_subject",
					'type'          => 'text',
					'std'           => ''
				),
				array(
					'name'          => __( 'Tuesday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}tueday_subject",
					'type'          => 'text',
					'std'           => ''
				),
				array(
					'name'          => __( 'Wednesday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}wedday_subject",
					'type'          => 'text',
					'std'           => ''
				),
				array(
					'name'          => __( 'Thursday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}thuday_subject",
					'type'          => 'text',
					'std'           => ''
				),
				array(
					'name'          => __( 'Friday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}friday_subject",
					'type'          => 'text',
					'std'           => ''
				),
				array(
					'name'          => __( 'Saturday Subject: ', 'themeum-core' ),
					'id'            => "{$prefix}satday_subject",
					'type'          => 'text',
					'std'           => ''
				),
		    ),
		),
	)
);

// --------------------------------------------------------------------------------------------------------------
// ----------------------------------------- Kidzy Event  -------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

	/**
	 * Register Post Meta for Event Post Type
	 *
	 * @return array
	 */

	$meta_boxes[] = array(
		'id' 		=> 'event-post-meta',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' 	=> __( 'Event Item Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' 	=> array( 'event'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' 	=> 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' 	=> 'high',

		// Auto save: true, false (default). Optional.
		'autosave' 	=> true,

		// List of meta fields
		'fields' 	=> array(

			array(
				'name'  		=> __( 'Start Date and Time', 'themeum-core' ),
				'id'    		=> "{$prefix}event_start_datetime",
				'desc'  		=> __( 'Event Start Date and Time', 'themeum-core' ),
				'type'  		=> 'datetime',
				'std'   		=> ''
			),
			array(
				'name'  		=> __( 'Event End Date and Time', 'themeum-core' ),
				'id'    		=> "{$prefix}event_end_datetime",
				'desc'  		=> __( 'Event Date and Time', 'themeum-core' ),
				'type'  		=> 'datetime',
				'std'   		=> ''
			),
			array(
				'name'          => __( 'Description About Speaker', 'themeum-core' ),
				'id'            => "{$prefix}event_about_speaker",
				'desc'			=> __( 'Write some word for Speaker & Guest honor', 'themeum-core' ),
				'type'          => 'textarea',
				'std'           => '',
			),
			array(
				'name'  		=> __( 'Performers', 'themeum-core' ),
				'id'    		=> "{$prefix}event_performers",
				'desc'  		=> '',
				'type'     		=> 'select_advanced',
				'options'  		=> $teachers,
				'multiple'    	=> true,
				'placeholder' 	=> __( 'Select Performers', 'themeum-core' ),
			),

			//

			array(
				'name'          => esc_html__( 'Location', 'themeum-core' ),
				'id'            => "{$prefix}address_location",
				'type'          => 'textarea',
				'std'           => '',
			),

			array(
				'name'  		=> __( 'Map', 'themeum-core' ),
				'id'    		=> "{$prefix}event_location_map",
				'desc'  		=> '',
				'type'  		=> 'map',
				'std'           => '23.709921,90.40714300000002,16',
				'style'         => 'width: 500px; height: 260px;',
				'address_field' => "{$prefix}address_location",
			),




			array(
				'name'  		=> __( 'Disable Map From Single Page:', 'themeum-core' ),
				'id'    		=> "{$prefix}event_map_trigger",
				'type'  		=> 'checkbox',
				'std'   		=> '1'
			),
		)
	);


// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------- Class Post type ------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

	$meta_boxes[] = array(
		'id' 		=> 'class-meta',
		'title' 	=> __( 'Class', 'themeum-core' ),
		'pages' 	=> array( 'class'),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,

		'fields' => array(
			array(
				'name'  => esc_html__( 'Class Status', 'themeum-core' ),
				'id'    => "{$prefix}class_status",
				'desc'  => esc_html__( 'What is the status of this class now?', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
      array(
        'name'  => esc_html__( 'Link', 'themeum-core' ),
        'id'    => "{$prefix}class_link",
        'desc'  => esc_html__( 'Link Thông Tin Tuyển Sinh', 'themeum-core' ),
        'type'  => 'text',
        'std'   => ''
      ),
			array(
				'name'  => esc_html__( 'Lứa tuổi:', 'themeum-core' ),
				'id'    => "{$prefix}years_old",
				'desc'  => esc_html__( 'How years old for this class?', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Sĩ số:', 'themeum-core' ),
				'id'    => "{$prefix}class_size",
				'desc'  => esc_html__( 'Class size for this class.', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Học phí:', 'themeum-core' ),
				'id'    => "{$prefix}tution_fee",
				'desc'  => esc_html__( 'Tution fee for this class', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  		=> __( 'Giáo viên', 'themeum-core' ),
				'id'    		=> "{$prefix}class_teacher",
				'desc'  		=> '',
				'type'     		=> 'select_advanced',
				'options'  		=> $teachers,
				'multiple'    	=> true,
				'placeholder' 	=> __( 'Select Teachers for this class', 'themeum-core' ),
			),
		)
	);

	// Add Class Subject list
	$meta_boxes[] = array(
		'id' 		=> 'class-subject-meta',
		'title' 	=> __( 'Subject List', 'themeum-core' ),
		'pages' 	=> array( 'class'),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(

			// Course List
			array(
			    'name' => '', // Optional
			    'id' => 'themeum_course_list',
			    'type'   => 'group',
			    'clone'  => true,
			    'fields' => array(
					array(
						'name'          => __( 'Subject Name', 'themeum-core' ),
						'id'            => "{$prefix}class_subject_name",
						'type'          => 'text',
						'std'           => ''
					),
					array(
						'name'          => __( 'Class Schedule', 'themeum-core' ),
						'id'            => "{$prefix}class_schedule",
						'type'          => 'text',
						'std'           => ''
					),
					
			    ),
			),

			)
		);



// --------------------------------------------------------------------------------------------------------------
// ------------------------------------------- Teacher Post type ------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

	$meta_boxes[] = array(
		'id' 		=> 'teacher-meta',
		'title' 	=> __( 'Teacher', 'themeum-core' ),
		'pages' 	=> array( 'teacher'),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,

		'fields' => array(
			array(
				'name'  => esc_html__( 'Designation', 'themeum-core' ),
				'id'    => "{$prefix}designation",
				'desc'  => esc_html__( 'Your designation', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),

			array(
				'name'  => esc_html__( 'Facebook URL:', 'themeum-core' ),
				'id'    => "{$prefix}facebook",
				'desc'  => esc_html__( 'Share your Facebook profile', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Twitter URL:', 'themeum-core' ),
				'id'    => "{$prefix}twitter",
				'desc'  => esc_html__( 'Share your Twitter profile', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Google Plus URL:', 'themeum-core' ),
				'id'    => "{$prefix}gplus",
				'desc'  => esc_html__( 'Share your Google Plus profile', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Linkedin URL:', 'themeum-core' ),
				'id'    => "{$prefix}linkedin",
				'desc'  => esc_html__( 'Share your Linkedin profile', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Instagram URL:', 'themeum-core' ),
				'id'    => "{$prefix}instagram",
				'desc'  => esc_html__( 'Share your Instagram profile', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
			array(
				'name'  => esc_html__( 'Flickr URL:', 'themeum-core' ),
				'id'    => "{$prefix}flickr",
				'desc'  => esc_html__( 'Share your Flickr profile', 'themeum-core' ),
				'type'  => 'text',
				'std'   => ''
			),
		)
	);

	// Add Teacher Timeline
	$meta_boxes[] = array(
		'id' 		=> 'teacher-timeline-meta',
		'title' 	=> __( 'Teacher Timeline', 'themeum-core' ),
		'pages' 	=> array( 'teacher'),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(

			// Teacher Timeline
			array(
			    'name' => '', // Optional
			    'id' => 'themeum_teacher_timeline',
			    'type'   => 'group',
			    'clone'  => true,
			    'fields' => array(
					// Date
					array(
						'name'       => __( 'Date', 'themeum-core' ),
						'id'         => "{$prefix}timeline_date",
						'type'       => 'date',
						'js_options' => array(
							'appendText'      => __( '(yyyy-mm-dd)', 'themeum-core' ),
							'dateFormat'      => __( 'yy-mm-dd', 'themeum-core' ),
							'changeMonth'     => true,
							'changeYear'      => true,
							'showButtonPanel' => true,
						),
					),
					array(
						'name'          => __( 'Position: ', 'themeum-core' ),
						'id'            => "{$prefix}timeline_designation",
						'type'          => 'text',
						'std'           => ''
					),
					array(
						'name'          => __( 'Description: ', 'themeum-core' ),
						'id'            => "{$prefix}timeline_school",
						'type'          => 'text',
						'std'           => ''
					),
					
			    ),
			),

			)
		);
// --------------------------------------------------------------------------------------------------------------
// ----------------------------------------- Time Line Post type ------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

		$meta_boxes[] = array(
			'id' 		=> 'timeline-meta',
			'title' 	=> __( 'Timeline', 'themeum-core' ),
			'pages' 	=> array( 'time_line'),
			'context' 	=> 'normal',
			'priority' 	=> 'high',
			'autosave' 	=> true,

			'fields' => array(
				array(
					// Field name - Will be used as label
					'name'  => esc_html__( 'Sub Title', 'themeum-core' ),
					// Field ID, i.e. the meta key
					'id'    => "{$prefix}timeline_subtitle",
					'desc'  => esc_html__( 'Write Your Sub Title', 'themeum-core' ),
					'type'  => 'textarea',
					// Default value (optional)
					'std'   => ''
				),
				array(
					// Field name - Will be used as label
					'name'  => esc_html__( 'Date', 'themeum-core' ),
					// Field ID, i.e. the meta key
					'id'    => "{$prefix}timeline_date",
					'desc'  => esc_html__( 'Write Date', 'themeum-core' ),
					'type'  => 'text',
					// Default value (optional)
					'std'   => ''
				)
				
			)
		);

// --------------------------------------------------------------------------------------------------------------
// ----------------------------------------- Photo Gallery Open -------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
  
  $meta_boxes[] = array(
    'id' 		=> 'gallery-meta',
    'title' 	=> __( 'Gallery Settings', 'themeum-core' ),
    'pages' 	=> array( 'gallery'),
    'context' 	=> 'normal',
    'priority' 	=> 'high',
    'autosave' 	=> true,
    
    'fields' 	=> array(
      array(
        'name'  		=> __( 'Gallery Facebook Link', 'themeum-core' ),
        'id'    		=> "{$prefix}_gallery_facebook_link",
        'type'  		=> 'text',
        'std'   		=> ''
      ),
//      array(
//        'name'  		=> __( 'Gallery Items', 'themeum-core' ),
//        'id'    		=> "{$prefix}politist_event_gallery",
//        'type'  		=> 'image_advanced',
//        'std'   		=> ''
//      ),
    )
  );

// --------------------------------------------------------------------------------------------------------------
// ----------------------------------------- Testimonials -------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
  
  $meta_boxes[] = array(
    'id' 		=> 'testimonial-meta',
    'title' 	=> __( 'Testimonial Settings', 'themeum-core' ),
    'pages' 	=> array( 'testimonial'),
    'context' 	=> 'normal',
    'priority' 	=> 'high',
    'autosave' 	=> true,
    
    'fields' 	=> array(
      array(
        'name'  		=> __( 'Sub Title', 'themeum-core' ),
        'id'    		=> "{$prefix}_testimonial_sub_title",
        'type'  		=> 'text',
        'std'   		=> ''
      ),
    )
  );

	// --------------------------------------------------------------------------------------------------------------
	// ----------------------------------------- Post Open ----------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------
	$meta_boxes[] = array(
		'id' => 'post-meta-quote',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Quote Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Qoute Text', 'themeum-core' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}qoute",
				'desc'  => esc_html__( 'Write Your Qoute Here', 'themeum-core' ),
				'type'  => 'textarea',
				// Default value (optional)
				'std'   => ''
			),
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Qoute Author', 'themeum-core' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}qoute_author",
				'desc'  => esc_html__( 'Write Qoute Author or Source', 'themeum-core' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => ''
			)
			
		)
	);



	$meta_boxes[] = array(
		'id' => 'post-meta-link',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Link Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Link URL', 'themeum-core' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}link",
				'desc'  => esc_html__( 'Write Your Link', 'themeum-core' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => ''
			)
			
		)
	);


	$meta_boxes[] = array(
		'id' => 'post-meta-audio',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Audio Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Audio Embed Code', 'themeum-core' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}audio_code",
				'desc'  => esc_html__( 'Write Your Audio Embed Code Here', 'themeum-core' ),
				'type'  => 'textarea',
				// Default value (optional)
				'std'   => ''
			)
			
		)
	);

	$meta_boxes[] = array(
		'id' => 'post-meta-video',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Video Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				// Field name - Will be used as label
				'name'  => esc_html__( 'Video Embed Code/ID', 'themeum-core' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}video",
				'desc'  => esc_html__( 'Write Your Vedio Embed Code/ID Here', 'themeum-core' ),
				'type'  => 'textarea',
				// Default value (optional)
				'std'   => ''
			),
			array(
				'name'     => esc_html__( 'Select Vedio Type/Source', 'themeum-core' ),
				'id'       => "{$prefix}video_source",
				'type'     => 'select',
				// Array of 'value' => 'Label' pairs for select box
				'options'  => array(
					'1' => esc_html__( 'Embed Code', 'themeum-core' ),
					'2' => esc_html__( 'YouTube', 'themeum-core' ),
					'3' => esc_html__( 'Vimeo', 'themeum-core' ),
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => '1'
			),
			
		)
	);
  
  $meta_boxes[] = array(
    'id' => 'post-meta-video',
    'title' => esc_html__( 'Post Views, Comments, Likes', 'themeum-core' ),
    'pages' => array( 'post'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    
    // List of meta fields
    'fields' => array(
      array(
        // Field name - Will be used as label
        'name'  => esc_html__( 'Bài Viết Mới', 'themeum-core' ),
        // Field ID, i.e. the meta key
        'id'    => "{$prefix}_post_new",
        'desc'  => esc_html__( 'Hiển Thị Label New Là Bài Viết Mới', 'themeum-core' ),
        'type'  => 'checkbox',
        // Default value (optional)
        'std'   => ''
      ),
      array(
        // Field name - Will be used as label
        'name'  => esc_html__( 'Số Lượt xem', 'themeum-core' ),
        // Field ID, i.e. the meta key
        'id'    => "{$prefix}_post_view_number",
        'desc'  => esc_html__( 'Write Your number views Here', 'themeum-core' ),
        'type'  => 'text',
        // Default value (optional)
        'std'   => '10'
      ),
      array(
        // Field name - Will be used as label
        'name'  => esc_html__( 'Số Lượt Comment', 'themeum-core' ),
        // Field ID, i.e. the meta key
        'id'    => "{$prefix}_post_comment_number",
        'desc'  => esc_html__( 'Write Your number comment Here', 'themeum-core' ),
        'type'  => 'text',
        // Default value (optional)
        'std'   => '10'
      ),
      array(
        // Field name - Will be used as label
        'name'  => esc_html__( 'Số Lượt Like', 'themeum-core' ),
        // Field ID, i.e. the meta key
        'id'    => "{$prefix}_post_like_number",
        'desc'  => esc_html__( 'Write Your number likes Here', 'themeum-core' ),
        'type'  => 'text',
        // Default value (optional)
        'std'   => '10'
      ),
    )
  );


	$meta_boxes[] = array(
		'id' => 'post-meta-gallery',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Gallery Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(
			array(
				'name'             => esc_html__( 'Gallery Image Upload', 'themeum-core' ),
				'id'               => "{$prefix}gallery_images",
				'type'             => 'image_advanced',
				'max_file_uploads' => 6,
			)
		)
	);
  
  $meta_boxes[] = array(
    'id' => 'post-meta-location',
    
    // Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => esc_html__( 'Post Location Settings', 'themeum-core' ),
    
    // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array( 'location'),
    
    // Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',
    
    // Order of meta box: high (default), low. Optional.
    'priority' => 'high',
    
    // Auto save: true, false (default). Optional.
    'autosave' => true,
    
    // List of meta fields
    'fields' => array(
      array(
        'name'             => esc_html__( 'Cở sở mới', 'themeum-core' ),
        'id'               => "{$prefix}location_new",
        'desc'  => esc_html__( 'Tick để Hiển thị icon là cơ sở mới', 'themeum-core' ),
        'type'             => 'checkbox',
      )
    )
  );
	// --------------------------------------------------------------------------------------------------------------
	// ----------------------------------------- Post Close ---------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------


	// --------------------------------------------------------------------------------------------------------------
	// ----------------------------------------- Page Open ----------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------
	$meta_boxes[] = array(
		'id' => 'page-meta-settings',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Page Settings', 'themeum-core' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'page'),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// List of meta fields
		'fields' => array(
			array(
				'name'             => esc_html__( 'Upload Sub Title Banner Image', 'themeum-core' ),
				'id'               => "{$prefix}subtitle_images",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
			),

			array(
				'name'             => esc_html__( 'Upload Sub Title BG Color', 'themeum-core' ),
				'id'               => "{$prefix}subtitle_color",
				'type'             => 'color',
				'std' 			   => "#191919"
			),
		)
	);
	// --------------------------------------------------------------------------------------------------------------
	// ----------------------------------------- Page Close ---------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------


	return $meta_boxes;
}


function get_all_posts($post_type)
{
	$args = array(
			'post_type' => $post_type,  // post type name
			'posts_per_page' => -1,   //-1 for all post
		);

	$posts = get_posts($args);

	$post_list = array();

	if (!empty( $posts ))
	{
		foreach ($posts as $post)
		{
			setup_postdata($post);
			$post_list[$post->post_name] = $post->post_title;
		}
		wp_reset_postdata();
		return $post_list;
	}
	else
	{
		return $post_list;
	}
}

