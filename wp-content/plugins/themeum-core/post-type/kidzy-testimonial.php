<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Admin functions for the Testmonial post type
 *
 * @author 		Themeum
 * @category 	Admin
 * @package 	Kidzy
 * @version     1.0
 *-------------------------------------------------------------*/

/**
 * Register post type Testmonial
 *
 * @return void
 */

function themeum_post_type_testmonial()
{
	$labels = array( 
		'name'                	=> _x( 'Testmonial', 'Testmonial', 'themeum-core' ),
		'singular_name'       	=> _x( 'Testmonial', 'Testmonial', 'themeum-core' ),
		'menu_name'           	=> __( 'Testmonial', 'themeum-core' ),
		'parent_item_colon'   	=> __( 'Parent Testmonial:', 'themeum-core' ),
		'all_items'           	=> __( 'All Testmonial', 'themeum-core' ),
		'view_item'           	=> __( 'View Testmonial', 'themeum-core' ),
		'add_new_item'        	=> __( 'Add New Testmonial', 'themeum-core' ),
		'add_new'             	=> __( 'New Testmonial', 'themeum-core' ),
		'edit_item'           	=> __( 'Edit Testmonial', 'themeum-core' ),
		'update_item'         	=> __( 'Update Testmonial', 'themeum-core' ),
		'search_items'        	=> __( 'Search Testmonial', 'themeum-core' ),
		'not_found'           	=> __( 'No article found', 'themeum-core' ),
		'not_found_in_trash'  	=> __( 'No article found in Trash', 'themeum-core' )
		);

	$args = array(  
		'labels'             	=> $labels,
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_in_menu'       	=> true,
		'show_in_admin_bar'   	=> true,
		'can_export'          	=> true,
		'has_archive'        	=> true,
		'hierarchical'       	=> false,
		'menu_position'      	=> null,
		'menu_icon'				=> 'dashicons-media-interactive',
		'supports'           	=> array( 'title','editor','thumbnail','comments')
		);

	register_post_type('testmonial',$args);

}

add_action('init','themeum_post_type_testmonial');


/**
 * View Message When Updated Project
 *
 * @param array $messages Existing post update messages.
 * @return array
 */

function themeum_update_message_Testmonial( $messages )
{
	global $post, $post_ID;

	$message['Testmonial'] = array(
		0 => '',
		1 => sprintf( __('Testmonial updated. <a href="%s">View Testmonial</a>', 'themeum-core' ), esc_url( get_permalink($post_ID) ) ),
		2 => __('Custom field updated.', 'themeum-core' ),
		3 => __('Custom field deleted.', 'themeum-core' ),
		4 => __('Testmonial updated.', 'themeum-core' ),
		5 => isset($_GET['revision']) ? sprintf( __('Testmonial restored to revision from %s', 'themeum-core' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Testmonial published. <a href="%s">View Testmonial</a>', 'themeum-core' ), esc_url( get_permalink($post_ID) ) ),
		7 => __('Testmonial saved.', 'themeum-core' ),
		8 => sprintf( __('Testmonial submitted. <a target="_blank" href="%s">Preview Testmonial</a>', 'themeum-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Testmonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Testmonial</a>', 'themeum-core' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Testmonial draft updated. <a target="_blank" href="%s">Preview Testmonial</a>', 'themeum-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		);

return $message;
}

add_filter( 'post_updated_messages', 'themeum_update_message_Testmonial' );