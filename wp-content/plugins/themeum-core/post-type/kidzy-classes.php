<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Admin functions for the class post type
 *
 * @author 		Themeum
 * @category 	Admin
 * @package 	Kidzy
 *-------------------------------------------------------------*/

/*--------------------------------------------------------------
*			Register Class Post Type
*-------------------------------------------------------------*/

add_action( 'init', 'kidzy_class_init' );

function kidzy_class_init() {
	$labels = array(
		'name'               => _x( 'Classes', 'class', 'themeum-core' ),
		'singular_name'      => _x( 'Classes', 'class', 'themeum-core' ),
		'menu_name'          => _x( 'Classes', 'class', 'themeum-core' ),
		'name_admin_bar'     => _x( 'Classes', 'class', 'themeum-core' ),
		'add_new'            => _x( 'Add New', 'class', 'themeum-core' ),
		'add_new_item'       => __( 'Add New Class', 'themeum-core' ),
		'new_item'           => __( 'New Class', 'themeum-core' ),
		'edit_item'          => __( 'Edit Class', 'themeum-core' ),
		'view_item'          => __( 'View Class', 'themeum-core' ),
		'all_items'          => __( 'All Classes', 'themeum-core' ),
		'search_items'       => __( 'Search Class', 'themeum-core' ),
		'parent_item_colon'  => __( 'Parent Class:', 'themeum-core' ),
		'not_found'          => __( 'No Class found.', 'themeum-core' ),
		'not_found_in_trash' => __( 'No Class found in Trash.', 'themeum-core' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'themeum-core' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'class' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-welcome-write-blog',		
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'class', $args );
}


function init_class_category() {
  $labels = array(
    'name'              => _x( 'Classes Categories', 'themeum-core' ),
    'singular_name'     => _x( 'Classes Categories', 'themeum-core' ),
    'search_items'      => __( 'Search Class Categories' ),
    'all_items'         => __( 'All Classes Categories' ),
    'parent_item'       => __( 'Parent Class Category' ),
    'parent_item_colon' => __( 'Parent Class Category:' ),
    'edit_item'         => __( 'Edit Class Category' ), 
    'update_item'       => __( 'Update Class Category' ),
    'add_new_item'      => __( 'Add New Class Category' ),
    'new_item_name'     => __( 'New Class Category' ),
    'menu_name'         => __( 'Class Category' ),
  );
  $args = array(
    'labels' 			=> $labels,
    'hierarchical' 		=> true,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,    
  );
  register_taxonomy( 'class_category', 'class', $args );
}
add_action( 'init', 'init_class_category', 0 );