<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Admin functions for the Teacher post type
 *
 * @author 		Themeum
 * @category 	Admin
 * @package 	Kidzy
 *-------------------------------------------------------------*/

/*--------------------------------------------------------------
*			Register Teachers Post Type
*-------------------------------------------------------------*/
  
  add_action( 'init', 'kidzy_doitac_init' );
  
  function kidzy_doitac_init() {
    $labels = array(
      'name'               => _x( 'Đối Tác', 'doitac', 'themeum-core' ),
      'singular_name'      => _x( 'Đối Tác', 'doitac', 'themeum-core' ),
      'menu_name'          => _x( 'Đối Tác', 'doitac', 'themeum-core' ),
      'name_admin_bar'     => _x( 'Đối Tác', 'doitac', 'themeum-core' ),
      'add_new'            => _x( 'Thêm Mới', 'doitac', 'themeum-core' ),
      'add_new_item'       => __( 'Add New Đối Tác', 'themeum-core' ),
      'new_item'           => __( 'New Đối Tác', 'themeum-core' ),
      'edit_item'          => __( 'Edit Đối Tác', 'themeum-core' ),
      'view_item'          => __( 'View Đối Tác', 'themeum-core' ),
      'all_items'          => __( 'All Đối Tác', 'themeum-core' ),
      'search_items'       => __( 'Search Đối Tác', 'themeum-core' ),
      'parent_item_colon'  => __( 'Parent Đối Tác:', 'themeum-core' ),
      'not_found'          => __( 'No Đối Tác found.', 'themeum-core' ),
      'not_found_in_trash' => __( 'No Đối Tác found in Trash.', 'themeum-core' )
    );
    
    $args = array(
      'labels'             => $labels,
      'description'        => __( 'Description.', 'themeum-core' ),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'teacher' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'menu_icon'			 => 'dashicons-admin-users',
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );
    
    register_post_type( 'doitac', $args );
  }
  
add_action( 'init', 'kidzy_teacher_init' );

function kidzy_teacher_init() {
	$labels = array(
		'name'               => _x( 'Teachers', 'teacher', 'themeum-core' ),
		'singular_name'      => _x( 'Teacher', 'teacher', 'themeum-core' ),
		'menu_name'          => _x( 'Teachers', 'teacher', 'themeum-core' ),
		'name_admin_bar'     => _x( 'Teacher', 'teacher', 'themeum-core' ),
		'add_new'            => _x( 'Add New', 'teacher', 'themeum-core' ),
		'add_new_item'       => __( 'Add New Teacher', 'themeum-core' ),
		'new_item'           => __( 'New Teacher', 'themeum-core' ),
		'edit_item'          => __( 'Edit Teacher', 'themeum-core' ),
		'view_item'          => __( 'View Teacher', 'themeum-core' ),
		'all_items'          => __( 'All Teachers', 'themeum-core' ),
		'search_items'       => __( 'Search Teachers', 'themeum-core' ),
		'parent_item_colon'  => __( 'Parent Teachers:', 'themeum-core' ),
		'not_found'          => __( 'No Teachers found.', 'themeum-core' ),
		'not_found_in_trash' => __( 'No Teachers found in Trash.', 'themeum-core' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'themeum-core' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'teacher' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-admin-users',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'teacher', $args );
}

add_action( 'init', 'kidzy_testimonial_init' );

function kidzy_testimonial_init() {
  $labels = array(
    'name'               => _x( 'Testimonials', 'testimonial', 'themeum-core' ),
    'singular_name'      => _x( 'Testimonial', 'testimonial', 'themeum-core' ),
    'menu_name'          => _x( 'Testimonials', 'testimonial', 'themeum-core' ),
    'name_admin_bar'     => _x( 'Testimonial', 'testimonial', 'themeum-core' ),
    'add_new'            => _x( 'Add New', 'testimonial', 'themeum-core' ),
    'add_new_item'       => __( 'Add New Testimonial', 'themeum-core' ),
    'new_item'           => __( 'New Testimonial', 'themeum-core' ),
    'edit_item'          => __( 'Edit Testimonial', 'themeum-core' ),
    'view_item'          => __( 'View Testimonial', 'themeum-core' ),
    'all_items'          => __( 'All Testimonials', 'themeum-core' ),
    'search_items'       => __( 'Search Testimonials', 'themeum-core' ),
    'parent_item_colon'  => __( 'Parent Testimonials:', 'themeum-core' ),
    'not_found'          => __( 'No Testimonials found.', 'themeum-core' ),
    'not_found_in_trash' => __( 'No Testimonials found in Trash.', 'themeum-core' )
  );
  
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'themeum-core' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'testimonial' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'			 => 'dashicons-admin-comments',
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  );
  
  register_post_type( 'testimonial', $args );
}

add_action( 'init', 'kidzy_location_init' );

function kidzy_location_init() {
  $labels = array(
    'name'               => _x( 'Locations', 'location', 'themeum-core' ),
    'singular_name'      => _x( 'Location', 'location', 'themeum-core' ),
    'menu_name'          => _x( 'Locations', 'location', 'themeum-core' ),
    'name_admin_bar'     => _x( 'Location', 'location', 'themeum-core' ),
    'add_new'            => _x( 'Add New', 'location', 'themeum-core' ),
    'add_new_item'       => __( 'Add New Location', 'themeum-core' ),
    'new_item'           => __( 'New Location', 'themeum-core' ),
    'edit_item'          => __( 'Edit Location', 'themeum-core' ),
    'view_item'          => __( 'View Location', 'themeum-core' ),
    'all_items'          => __( 'All Locations', 'themeum-core' ),
    'search_items'       => __( 'Search Locations', 'themeum-core' ),
    'parent_item_colon'  => __( 'Parent Locations:', 'themeum-core' ),
    'not_found'          => __( 'No Locations found.', 'themeum-core' ),
    'not_found_in_trash' => __( 'No Locations found in Trash.', 'themeum-core' )
  );
  
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'themeum-core' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'location' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'			 => 'dashicons-welcome-write-blog',
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  );
  
  register_post_type( 'location', $args );
}

add_action( 'init', 'kidzy_grapeseed_init' );

function kidzy_grapeseed_init() {
  $labels = array(
    'name'               => _x( 'GrapeSEED', 'grapeseed', 'themeum-core' ),
    'singular_name'      => _x( 'GrapeSEED', 'grapeseed', 'themeum-core' ),
    'menu_name'          => _x( 'GrapeSEED', 'grapeseed', 'themeum-core' ),
    'name_admin_bar'     => _x( 'GrapeSEED', 'grapeseed', 'themeum-core' ),
    'add_new'            => _x( 'Add New', 'grapeseed', 'themeum-core' ),
    'add_new_item'       => __( 'Add New GrapeSEED', 'themeum-core' ),
    'new_item'           => __( 'New GrapeSEED', 'themeum-core' ),
    'edit_item'          => __( 'Edit GrapeSEED', 'themeum-core' ),
    'view_item'          => __( 'View GrapeSEED', 'themeum-core' ),
    'all_items'          => __( 'All GrapeSEED', 'themeum-core' ),
    'search_items'       => __( 'Search GrapeSEED', 'themeum-core' ),
    'parent_item_colon'  => __( 'Parent GrapeSEED:', 'themeum-core' ),
    'not_found'          => __( 'No GrapeSEED found.', 'themeum-core' ),
    'not_found_in_trash' => __( 'No GrapeSEED found in Trash.', 'themeum-core' )
  );
  
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'themeum-core' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'grapeseed' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'			 => 'dashicons-welcome-write-blog',
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  );
  
  register_post_type( 'grapeseed', $args );
}

add_action( 'init', 'kidzy_gallery_init' );

function kidzy_gallery_init() {
  $labels = array(
    'name'               => _x( 'Gallery', 'gallery', 'themeum-core' ),
    'singular_name'      => _x( 'Gallery', 'gallery', 'themeum-core' ),
    'menu_name'          => _x( 'Gallery', 'gallery', 'themeum-core' ),
    'name_admin_bar'     => _x( 'Gallery', 'gallery', 'themeum-core' ),
    'add_new'            => _x( 'Add New', 'gallery', 'themeum-core' ),
    'add_new_item'       => __( 'Add New Gallery', 'themeum-core' ),
    'new_item'           => __( 'New Gallery', 'themeum-core' ),
    'edit_item'          => __( 'Edit Gallery', 'themeum-core' ),
    'view_item'          => __( 'View Gallery', 'themeum-core' ),
    'all_items'          => __( 'All Gallery', 'themeum-core' ),
    'search_items'       => __( 'Search Gallery', 'themeum-core' ),
    'parent_item_colon'  => __( 'Parent Gallery:', 'themeum-core' ),
    'not_found'          => __( 'No Gallery found.', 'themeum-core' ),
    'not_found_in_trash' => __( 'No Gallery found in Trash.', 'themeum-core' )
  );
  
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'themeum-core' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'gallery' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'			 => 'dashicons-welcome-write-blog',
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  );
  
  register_post_type( 'gallery', $args );
}
